<?php

namespace App\Http\Controllers\Products;
use Exception;
use App\Http\Controllers\Controller;
use App\Models\ECommerce\Options;
use App\Models\ECommerce\Product;
use App\Models\ECommerce\ProductCategory;
use App\Models\ECommerce\ProductImage;
use App\Models\ECommerce\ProductVariation;
use App\Models\ECommerce\RestoOrder;
use App\Models\Sellers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Query\Builder;
use App\Http\Controllers\Orders\CartController;
use DB;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Str;


class ProductController extends Controller
{

    /**List of products in the admin */
    public function products(){
        if(Auth::user()->role == 1 || Auth::user()->role == 3){
            $products =  DB::table('sellers')
            ->join('products','products.vendor_id','sellers.id')
            ->where('deleted_at',null)
            ->get();
        }elseif(Auth::user()->role == 4){
            $products =  DB::table('sellers')
            ->join('products','products.vendor_id','sellers.id')
            ->where('sellers.seller_id',Auth::user()->store_id)
            ->get();
        }
        return view('product.products',compact('products'));
    }
    public function displayAllProducts(Request $request){
        $categories = ProductCategory::groupBy('category')->get();
        $products = DB::table('products')
                    
                    ->join('product_images','product_images.product_id','products.id')
                    ->join('sellers','sellers.id','products.vendor_id')
                    ->join('product_variations','products.id','product_variations.product_id')
                    ->where('product_images.is_primary',1)
                    ->where('product_variations.is_primary',1)
                    ->where('product_variations.is_active',1)
                    ->where('products.deleted_at',null)
                    ->paginate(75);
                    // dd($products);
        $itemsPerPage = $products->count();

        $order_options = "";
        $orders = "";
        if ($request->session()->has('order_id_session')) {
            $cart_orders = new CartController;
            
            $orders = $cart_orders->displayCartItems($request);
            $order_options = $cart_orders->getOrderOptions($request,$request->session()->get('order_id_session'));
        
            
        }
            
       
        
        return view('product.all-products',compact('categories','products','itemsPerPage','orders','order_options'));
    }
    
    public function searchedProducts($keyword){
        $categories = ProductCategory::groupBy('category')->get();
        $products = DB::table('products')
                    ->join('product_variations','products.id','product_variations.product_id')
                    ->join('product_images','product_images.product_id','products.id')
                    ->join('sellers','sellers.id','products.vendor_id')
                    ->join('product_categories','product_categories.product_id','products.id')
                    ->where('products.deleted_at',null)
                    ->where('product_images.is_primary',1)
                    ->where('product_variations.is_primary',1)
                    
                        ->where(function(Builder $query) use ($keyword) {
                            $query->where('product_variations.variation_title','LIKE','%'.$keyword.'%')
                            ->orWhere('products.title','LIKE','%'.$keyword.'%')
                            ->orWhere('products.description','LIKE','%'.$keyword.'%')
                            ->orWhere('product_categories.category','LIKE','%'.$keyword.'%')
                            ->orWhere('sellers.store_name','LIKE','%'.$keyword.'%')
                            ->orWhere('sellers.seller_name','LIKE','%'.$keyword.'%');
                        })
                    ->groupBy('products.title')
                    ->paginate(75);
                   
        
        
        return view('product.searched-products',compact('categories','products','keyword'));
    }

    public function searchedAdminproducts($keyword){
        $search = 1;
        $products = DB::table('products')
                    ->join('product_variations','product_variations.product_id','products.id')
                    ->join('product_images','product_images.product_id','products.id')
                    ->join('sellers','sellers.id','products.vendor_id')
                    ->join('product_categories','product_categories.product_id','products.id')
                    ->where('products.deleted_at',null)
                    ->where('product_images.is_primary',1)
                    ->where('product_variations.is_primary',1)
                        ->where(function(Builder $query) use ($keyword) {
                            $query->where('product_variations.variation_title','LIKE','%'.$keyword.'%')
                            ->orWhere('products.title','LIKE','%'.$keyword.'%')
                            ->orWhere('products.description','LIKE','%'.$keyword.'%')
                            ->orWhere('product_categories.category','LIKE','%'.$keyword.'%')
                            ->orWhere('sellers.store_name','LIKE','%'.$keyword.'%')
                            ->orWhere('sellers.seller_name','LIKE','%'.$keyword.'%');
                        })
                    ->groupBy('products.title')
                    ->paginate(75);
                    // ->toSQL();
                    // dd($products);
            return view('product.products',compact('products','keyword','search'));
    }
    /**Add Product */
    public function addProduct(){

        //get vendors
        $vendors = Sellers::all();
        $categories = DB::table('product_categories')->groupBy('category')->get();
        return view('product.add-product',compact('vendors','categories'));
    }

    /**Display product in admin */
    public function product($product_id){
        try{
            $vendor = Product::find($product_id)->seller->store_name;
        }catch(Exception $e){
            return view('errors.404');
        }
        
        $product = Product::where('id',$product_id)->first();
        $product_images = ProductImage::where('product_id',$product_id)->get();
        $image = ProductImage::where('product_id',$product_id)->first();
        $categories = ProductCategory::where('product_id',$product_id)->get();
        $product_variations = ProductVariation::where('product_id',$product_id)->get();
        $options = Options::where('product_id',$product_id)->get();
        return view('product.product',compact('vendor','product_images','image','categories','product','product_variations','options'));
    }

    /**Display a product in public url */
    public function displayProduct(Request $request, $product_title, $product_id){
        try{
            $vendor = Product::find($product_id)->seller->store_name;
            }catch(Exception $e){
                return view('errors.404');
            }
            
        $product = Product::where('id',$product_id)
                    ->where('title',$product_title)
                    ->first();
        if($product == null){
            return view('errors.404');
        }
        $product_images = ProductImage::where('product_id',$product_id)->get();
        $image = ProductImage::where('product_id',$product_id)->first();
        $categories = ProductCategory::where('product_id',$product_id)->get();
        $product_variations = ProductVariation::where('product_id',$product_id)->where('is_active',1)->get();
        $options = Options::where('product_id',$product_id)->get();
        
        

        $cart_orders = new CartController;
        $order_options = "";
        $orders = "";
        if ($request->session()->has('order_id_session')) {
            $orders = $cart_orders->displayCartItems($request);
            $order_options = $cart_orders->getOrderOptions($request);
        }
        
        return view('product.display-product',compact('product_images','image','categories','product','product_variations','options','order_options','vendor','orders'));
    }

    /**Edit Product */
    public function editProduct($product_id){
        $images = Product::find($product_id)->images()->get();
        $variants = Product::find($product_id)->variants()->get();
        $categories = Product::find($product_id)->categories()->get();
        $options = Product::find($product_id)->options()->get();
       
        // $option1 = Options::find($product_id);
        //return $comment->post->title;
        //$products = Sellers::find(1123)->products()->get();
        $vendor = Product::find($product_id)->seller;
        $vendors = Sellers::all();
        
        return view('product.edit-product',compact('vendors','vendor','variants','images','categories','options'));
    }

    /**Save Product */
    public function saveProduct(Request $request){
        $product = Product::create([
            'title'=>$request->title,
            'description'=>$request->description,
            'discount'=>$request->discount,
            'vendor_id'=>$request->vendor_id
        ]);
        ProductVariation::create([
            'product_id' => $product->id,
            'price' => $request->price,
            'variation_title'=>$request->title,
            'weight'=>$request->weight,
            'stocks'=>$request->quantity,
            'is_primary'=>1
        ]);

        ProductCategory::create([
            'product_id' => $product->id,
            'category'=>$request->category
        ]);
        
        ProductImage::create([
            'product_id' => $product->id,
            'image_link'=>$request->imagelink,
            'is_primary'=>1
        ]);

        return redirect(route('product.product',['id'=>$product->id]));
    }
    public function deleteProduct(Request $request){
        Product::where('id', $request->id)->delete();
        return response()->json(["status"=>"success"]);
    }

     /***Add Image */
     public function addImage(Request $request){
            
        ProductImage::create([
            'product_id' => $request->product_id,
            'image_link'=>$request->image_link,
        ]);

        return response()->json(['status'=>"Image added successfully"]);
    }
    /**End Add Image */
    public function deleteImage(Request $request){
        ProductImage::where('id', $request->id)->delete();
        return response()->json(["status"=>"success"]);
    }
    public function editTitle(Request $request){
        $product = Product::find($request->id);
        $product->title = $request->title;
        $product->save();

        return response()->json(["status"=>"success"]);
    }

    public function saveDescription(Request $request){
        $description = Product::find($request->id);
        $description->description = $request->description;
        $description->save();

        return response()->json($request);
    }
    /*** Variant ****/
    public function addVariant($product_id){
        return view('product.add-variant',compact('product_id'));
    }

    public function saveVariant(Request $request){
        ProductVariation::create([
            'product_id' => $request->product_id,
            'price' => $request->price,
            'variation_title'=>$request->variation_title,
            'weight'=>$request->weight,
            'stocks'=>$request->stocks
        ]);

        $variantCount = Product::find($request->product_id)->variants()->count();
        
        Product::where('id',$request->product_id)
        ->update([
            'variant_count'=> $variantCount
        ]);

        return back()->with('status','Successfully added a new variant');
    }

    public function editVariant(Request $request){
        ProductVariation::where('id',$request->id)
        ->update([
            'variation_title' => $request->variation_title,
            'price' => $request->price,
            'stocks' => $request->quantity
        ]);
        return response()->json(['status'=>"Edited successfully"]);
    }

    //Delete Variant
    public function deleteVariant(Request $request){
        $variant_count = Product::where('id',$request->productId)->first();
        $variant_count = $variant_count->variant_count - 1;
        Product::where('id',$request->productId)
        ->update([
            'variant_count'=>$variant_count,
        ]);
        ProductVariation::where('id', $request->id)->delete();
        RestoOrder::where('variation_id',$request->id)
                ->where('is_checkout',0)
                ->delete();
        return response()->json(["status"=>"success"]);
    }

    public function variantCount($product_id){
        $variantsCount = Product::find($product_id)->variants()->count();
        return response()->json($variantsCount);
    }

    public function disableVariant(Request $request){
        $variant_count = Product::where('id',$request->productId)->first();
        $variant_count = $variant_count->variant_count - 1;
        Product::where('id',$request->productId)
        ->update([
            'variant_count'=>$variant_count,
        ]);
        ProductVariation::where('id',$request->id)
        ->update([
            'is_active' => 0,
        ]);
        return response()->json('success');
    }
    public function enableVariant(Request $request){
        $variant_count = Product::where('id',$request->productId)->first();
        $variant_count = $variant_count->variant_count + 1;
        Product::where('id',$request->productId)
        ->update([
            'variant_count'=>$variant_count,
        ]);
        ProductVariation::where('id',$request->id)
        ->update([
            'is_active' => 1,
        ]);
    }
    /*** End Variant****/

   

    /***Add Options */
    public function saveOption(Request $request){
            
        Options::create([
            'product_id' => $request->product_id,
            'option_name'=>$request->option_name,
            'option_price'=>$request->option_price,
        ]);

        return response()->json(['status'=>"Option added successfully"]);
    }
    public function saveEditedOption(Request $request){
        Options::where('id',$request->id)
        ->update([
            'option_name' => $request->option_name,
            'option_price' => $request->option_price,
        ]);
        return response()->json(['status'=>"Edited successfully"]);
    }

    public function deleteOption(Request $request){
        Options::where('id', $request->id)->delete();
        return response()->json(["status"=>"success"]);
    }

    // End Options

    

    public function addCategory(Request $request){
        ProductCategory::create([
            'product_id' => $request->product_id,
            'category'=>$request->category,
        ]);

        return response()->json(['status'=>"Category added successfully"]);
    }

    public function updateCategory(Request $request){
        ProductCategory::where('id',$request->id)
        ->update([
            'category' => $request->category,
        ]);
        return response()->json(['status'=>"Edited successfully"]);
    }

    public function deleteCategory(Request $request){
        ProductCategory::where('id', $request->id)->delete();
        return response()->json(["status"=>"success"]);
    }
    

    public function saveDiscount(Request $request){
        
        $variants = Product::find($request->id)->variants()->get();
        foreach($variants as $variant){
            if($request->discount > $variant->price){
                return response()->json("Discount cannot be greater than variant's price");
            }
            else{
                Product::where('id',$request->id)
                ->update([
                    'discount' => $request->discount,
                ]);

                return response()->json("Success");
            }
        }
    }  
}
