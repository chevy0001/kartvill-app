<?php

namespace App\Http\Controllers;

use DateTime;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Sellers;
use App\Models\CityCode;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\ShopifyOrders;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     
    public function index()
    {
       
        $fDate = new SalesReportsController;
      
        $month = new DateTime(Carbon::now());
            $month = $month->format('Y-m-d');
            $dt = Carbon::createFromFormat('Y-m-d',$month);
            $month = date('m', strtotime($month));
            $monthName = $dt->format('F');
        $dailySales =DB::table('shopify_orders')
        ->where('financial_status','paid')
        ->whereMonth('created_at', $month)
        ->select('*', DB::raw('
                SUM(vendor_net) as total_sales, 
                SUM(commission) as commission,
                SUM(product_price) as vendor_gross,
                DATE(created_at) as date
                '))
        ->groupBy('date')        
        ->get();
        //dd($dailySales);


                $array_commissions = array();
                    foreach($dailySales as $sales){
                array_push($array_commissions,$sales->commission);

            }   
            $array_date = array();
                    foreach($dailySales as $sales){
                array_push($array_date, $fDate->formatDate($sales->created_at) );

            }    
                 
        return view('admin.index',compact('dailySales','array_commissions','array_date'));
    }

    public function updateToken(){

    }

    public function userSearch(Request $request){
        if($request->input == null){
            return redirect('users');
        }
        return redirect('users-'.$request->input);
    }
    public function riderSearch(Request $request){
        if($request->input == null){
            return redirect('riders');
        }
        return redirect('riders-'.$request->input);
    }
    public function users($value=null){
        
       if($value == null || $value == "clear"){
        
            if(Auth::user()->role == 1){
                $users = User::where('role','<>','-1')->get();
            }
            else{
                $users = User::where('role','<>','1')
                    ->where('role','<>','4')
                    ->where('role','<>','2')
                    ->where('role','<>','5')
                    ->where('role','<>','-1')
                    ->get();
            }
       } 
       else{
        
        $users = User::where('first_name','LIKE',"%{$value}%")
                    ->orWhere('last_name','LIKE',"%{$value}%")
                    ->orWhere('email','LIKE',"%{$value}%")
                    ->orWhere('phone_number','LIKE',"%{$value}%")
                    ->get();
                    
       }
 
        return view('admin.users',compact('users'));
    }

    public function newUsers($value=null){
        
        if($value == null || $value == "clear"){
         
            $users = User::where('role','0')->get();
        } 
        else{
         
         $users = User::where('first_name','LIKE',"%{$value}%")
                     ->orWhere('last_name','LIKE',"%{$value}%")
                     ->orWhere('email','LIKE',"%{$value}%")
                     ->orWhere('phone_number','LIKE',"%{$value}%")
                     ->get();
                     
        }
  
         return view('admin.new-users',compact('users'));
     }

    public function vendors(){
        $users = User::where('role','5')->get();
        return view('admin.vendors',compact('users'));
    }

    public function riders($value=null){
       
        if($value == null || $value == ""){
         
            
                 $users = User::where('role','2')->get();
             
        } 
        else{
  
         $users = User::where('role',2)
                     ->where('first_name','LIKE',"%{$value}%")
                     ->orWhere('last_name','LIKE',"%{$value}%")
                     ->orWhere('email',$value)
                     ->orWhere('phone_number','LIKE',"%{$value}%")
                     ->get();
                     
        }
        
         
         return view('admin.riders',compact('users'));
     }
    public function addUser(){
        $provinces = CityCode::all();
        return view('admin.adduser',compact('provinces'));
    }

    public function editUser($userid){
        
        $user = User::where('id',$userid)->first(); //Create join here with Seller table
        if($user->store_id != null){
            $store = Sellers::where('seller_id',$user->store_id)->first();
        }
        else{
            $store = '';
        }
        // dd($store);
        if($user === null){
            return view('errors.404');
        }
        
        //If not admin, they are not able to view admin and investors profile
        if(Auth::user()->role != 1){
            if($user->role == 1 || $user->role == 4){
                return view('errors.404');
            }
        }
        $provinces = CityCode::all();
        
        return view('admin.edit-user',compact('user','provinces','store'));
    }
    public function updateUser(Request $request, $id){
       // dd($id);

        $province_code = CityCode::where('province',$request->province)->first();
        

        User::where('id',$id)
            ->update([
                'first_name'=>htmlspecialchars_decode($request->first_name),
                'last_name'=>htmlspecialchars_decode($request->last_name),
                'email'=>htmlspecialchars_decode($request->email),
                'phone_number'=>htmlspecialchars_decode($request->phone_number),
                'street_address'=>htmlspecialchars_decode($request->street_address),
                'city'=>htmlspecialchars_decode($request->city),
                'province'=>htmlspecialchars_decode($request->province),
                'province_code'=>htmlspecialchars_decode($province_code->province_code),
                'country'=>htmlspecialchars_decode($request->country),
                'zip'=>htmlspecialchars_decode($request->zip),
                'plate_number'=>$request->plate_number,
                'vehicle_type'=>$request->vehicle_type,
                'longitude'=>$request->longitude,
                'latitude'=>$request->latitude,
                'email'=>htmlspecialchars_decode($request->email),
                'role'=>$request->role,
                'store_id'=>$request->store_id,
                
            ]);

            return back();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $func = new FunctionsController;
        $str= Str::random(10);
        $province_code = CityCode::where('province',$request->province)->first();

        $request->validate([
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);

        User::create([
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'email'=>$request->email,
            'phone_number'=>$request->phone_number,
            'street_address'=>$request->street_address,
            'city'=>$request->city,
            'province'=>$request->province,
            'province_code'=>$province_code->province_code,
            'country'=>$request->country,
            'zip'=>$request->zip,
            'plate_number'=>$request->plate_number,
            'vehicle_type'=>$request->vehicle_type,
            'role'=>$request->role,
            'password'=>  Hash::make($str),               
                                ]); 
                                
        $fullname = $request->first_name ." ".$request->last_name;
        $func->resetEmail($request->email,$fullname,$str);

        return redirect('/users')->with('status','Added a user successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verification(){
        return view('errors.verify');
    }
    
}
