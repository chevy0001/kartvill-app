<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\ShopifyOrders;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class DeliveryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        if(Auth::user()->role != 2 ){
            $deliveries = DB::table('users')
                    ->join('shopify_orders','shopify_orders.rider_id','users.id')
                    ->select('users.*','shopify_orders.*')
                    ->orderBy('shopify_orders.created_at', 'desc')
                    ->simplePaginate(50);
        }
        else{
            $deliveries = DB::table('users')
                    ->where('rider_id', Auth::user()->id)
                    ->join('shopify_orders','shopify_orders.rider_id','users.id')
                    ->select('users.*','shopify_orders.*')
                    ->orderBy('shopify_orders.created_at', 'desc')
                    ->simplePaginate(50);
           

        }            
        
        return view('delivery.admin.index',compact('deliveries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function assignedRider($order_number,$customer_id){
        // $rider_id = ShopifyOrders::where('order_number',$order_number)
        //             ->where('rider_id','>',0)
        //             ->where('customer_id',$customer_id)            
        //             ->first();
        // if(isset($rider_id)){
            // $rider_details = User::where('id',$rider_id->rider_id)->first();
            
        $order = ShopifyOrders::where('order_number',$order_number)->first();    
            if($order->shipping_code == "Pabili"){
                $order_details = DB::table("users")
                ->join('shopify_orders','users.id','=','shopify_orders.rider_id')
                ->join('pabili_orders','users.id','=','pabili_orders.rider_id')
                ->where('shopify_orders.order_number',$order_number)
                ->where('shopify_orders.customer_id',$customer_id)
                ->where('pabili_orders.order_id',$order->order_id)
                ->select('pabili_orders.*','users.first_name','users.last_name','users.phone_number','shopify_orders.delivery_fee')
                ->first();
            }else{
                $order_details = DB::table("users")
                ->join('shopify_orders','users.id','=','shopify_orders.rider_id')
                ->where('shopify_orders.order_number',$order_number)
                ->where('shopify_orders.customer_id',$customer_id)
                ->select('users.first_name','users.last_name','users.phone_number','shopify_orders.delivery_fee')
                ->first();
            }
            
           
                
        if(isset($order_details)){
            return response()->json($order_details);
        }
        else{
            
            return response()->json(['Status'=>'Looking for rider']);
        }
        
        

    } 
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
