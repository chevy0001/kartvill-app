<?php


namespace App\Http\Controllers\Orders;
use App\Http\Controllers\Controller;
use App\Models\ECommerce\RestoOrder;
use App\Models\ECommerce\Product;
use App\Models\Sellers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;

class RestoOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = DB::table('orders')
            ->orderBy('id','desc')
            ->paginate(10);
       
        // $orders_modal = DB::table('')
        return view('digital-menu.orders',compact('orders'));
    }

    //vendor-orders
    public function vendorOrders()
    {
        $orders = DB::table('resto_orders')
            ->join('products','resto_orders.product_id','products.id')
            ->join('sellers','sellers.id','products.vendor_id')
            ->join('orders','resto_orders.order_id','orders.order_id')
            ->where('sellers.seller_id',Auth::user()->store_id)
            ->groupBy('orders.id')
            ->orderBy('orders.id','desc')
            ->paginate(100);

           

   
        // $orders_modal = DB::table('')
        return view('digital-menu.vendor-orders',compact('orders'));
    }

    public function order($order_id)
    {
       
            $orders = DB::table('products')
                ->join('product_variations','product_variations.product_id','products.id')
                ->join('product_images','products.id','product_images.product_id')
                ->join('resto_orders','resto_orders.variation_id','product_variations.id')
                ->join('sellers','sellers.id','products.vendor_id')
                ->join('orders','orders.order_id','resto_orders.order_id')
                ->where('orders.id',$order_id)
                ->where('product_variations.is_active',1)
                ->where('product_images.is_primary',1)
                ->where('is_checkout',1)
                ->get();
                
                $order_options = DB::table('order_options')
                ->join('options','options.id','order_options.option_id')
                ->where('order_options.order_id',$orders[0]->order_id)
                ->get();   
       

        return view('digital-menu.vendor-order',compact('orders','order_options','order_id'));
    }
    
    //d-vendor-order-
    public function vendorOrder($order_id)
    {
        $orders = DB::table('products')
            ->join('product_variations','product_variations.product_id','products.id')
            ->join('product_images','products.id','product_images.product_id')
            ->join('resto_orders','resto_orders.variation_id','product_variations.id')
            ->join('sellers','sellers.id','products.vendor_id')
            ->join('orders','orders.order_id','resto_orders.order_id')
            ->where('sellers.seller_id',Auth::user()->store_id)
            ->where('orders.id',$order_id)
            ->where('resto_orders.order_fulfillment_status','!=',"0")
            ->where('product_images.is_primary',1)
            ->where('resto_orders.is_checkout',1)
            ->get();
            // ->toSql();
             
           
        
           

            $order_options = DB::table('order_options')
            ->join('options','options.id','order_options.option_id')
            ->where('order_options.order_id',$orders[0]->order_id)
            ->get();   
            

        return view('digital-menu.vendor-order',compact('orders','order_options','order_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ECommerce\RestoOrder  $restoOrder
     * @return \Illuminate\Http\Response
     */
    public function show(RestoOrder $restoOrder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ECommerce\RestoOrder  $restoOrder
     * @return \Illuminate\Http\Response
     */
    public function edit(RestoOrder $restoOrder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ECommerce\RestoOrder  $restoOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RestoOrder $restoOrder)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ECommerce\RestoOrder  $restoOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(RestoOrder $restoOrder)
    {
        //
    }
}
