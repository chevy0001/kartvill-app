<?php

namespace App\Http\Controllers\Orders;
use Exception;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FunctionsController;
use App\Models\ECommerce\RestoOrder;
use App\Models\ECommerce\OrderOption;
use App\Models\ECommerce\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;
use DateTime;
use DB;

class CartController extends Controller
{
    public function cart(Request $request){
        $cart_orders = new CartController;
        $order_options = "";
        $orders = "";
        $customer_name="";
        $customer_contact="";
        $table_number="";
        // $request->session()->forget('order_id_session');
        // dd($request->session()->get('order_id_session'));
        if ($request->session()->has('order_id_session')) {
            $orders = $cart_orders->displayCartItems($request);
            $order_options = $cart_orders->getOrderOptions($request);
           
            

            
            $customer_name = $request->session()->get('customer_name');
           $customer_contact = $request->session()->get('customer_contact');
           $table_number = $request->session()->get('table_number');
           return view('cart.cart',compact('order_options','orders','customer_name','customer_contact','table_number'));
        }else{
            return redirect('/order-status');
        }
        // dd($request->session()->has('order_id_session'));
    }
    public function getOrderOptions(Request $request){
        if ($request->session()->has('order_id_session')) {
            $order_id = $request->session()->get('order_id_session');
            
            $options = DB::table('order_options')
            ->join('options','options.id','order_options.option_id')
            ->where('order_options.order_id',$order_id)
            ->get();

            return $options;
        }
        else{
            return "";
        }
    }
    public function addToCart(Request $request){
        $order_id = "";
        $value = "";
        
        
        
           
        if ($request->session()->has('order_id_session')) {
           $order_id = $request->session()->get('order_id_session');
        }
        else{
            
            $order_id = new DateTime(Carbon::now());
            $order_id = $order_id->format('zdmisYHis');
            $order_id = Str::random(3)."".Str::random(3).$order_id;
            
            $request->session()->put('order_id_session', $order_id);
            
        }

        if ($request->session()->has('device_id')) {
            $device_id = $request->session()->get('device_id');
         }
         else{
             $device_id = new DateTime(Carbon::now());
             $device_id = $device_id->format('zdmisYHis');
             $device_id = Str::random(4)."".Str::random(3).$device_id;
             $request->session()->put('device_id', $device_id);
         }
        
        $validated = $request->validate([
            'product_id' => 'required|numeric',
            'variation_id'=>'required|numeric',
            'quantity'=>'required|numeric|min:1',
            'option_ids'=>'',
            'option_counter'=>'',
        ]);
        try{
            $option_key = null;
            if($request->option_ids != ""){
                $option_key = $request->option_ids;
                $option_id = explode("::",$option_key);
                
                for($x = 0; $x <  intval($request->option_counter); $x++){
                    $order_options = OrderOption::updateOrCreate(
                                ['order_id' => $order_id, 
                                'option_id' => $option_id[$x+1],
                                'option_key'=>$option_key."::".$request->variation_id],       
                            );
                 }
            }

            $cart_orders = RestoOrder::updateOrCreate(
                ['order_id' => $order_id, 
                'product_id' => $request->product_id,
                'variation_id'=>$request->variation_id,
                'option_key'=>$option_key."::" .$request->variation_id],
                [
                    'quantity'=>$request->quantity,
                    'device_id'=>$device_id,
                ]
            );

        
        }catch(Exception $e){
            return response($e);
        }

    $order_count = new CartController;
    $order_count = $order_count->cartOrderCount($request);
        
    return response()->json(["status"=>"success",'order_count'=>$order_count]);
    }

    public function cartOrderCount(Request $request){
        $order_count = 0;
        if ($request->session()->has('order_id_session')) {
            $order_id = $request->session()->get('order_id_session');
            $orders_count = DB::table('product_variations')
                ->join('resto_orders','resto_orders.variation_id','product_variations.id')
                ->where('product_variations.is_active',1)
                ->where('resto_orders.order_id',$order_id)
                ->count();
                return response()->json(['order_count'=>$orders_count]);
         }else{
            return response()->json(['order_count'=>$orders_count]);
         }
    }

    public function displayCartItems(Request $request){
        if ($request->session()->has('order_id_session')) {
            $order_id = $request->session()->get('order_id_session');
            $orders = DB::table('products')
                ->join('sellers','sellers.id','products.vendor_id')
                ->join('product_variations','product_variations.product_id','products.id')
                ->join('product_images','products.id','product_images.product_id')
                ->join('resto_orders','resto_orders.variation_id','product_variations.id')
                ->where('product_variations.is_active',1)
                ->where('product_images.is_primary',1)
                ->where('order_id',$order_id)
                ->where('is_checkout',0)
                ->get();
                return $orders;
               
        }
        else{
            return "";
        }
    }
    

    public function removeOrder(Request $request){
        $option_key = RestoOrder::where('id', $request->id)->first();
        OrderOption::where('option_key', $option_key->option_key)->delete();
        RestoOrder::where('id', $request->id)->delete();
        $order_count = new CartController;
        $order_count = $order_count->cartOrderCount($request);
        
        return response()->json(["status"=>"success",'order_count'=>$order_count]);
        // return response()->json(["status"=>"success"]);
    }

    public function updateOrderQuantity(Request $request){
        if ($request->session()->has('order_id_session')) {
            $update_order = RestoOrder::find($request->id);
            $update_order->quantity = $request->quantity;
            $update_order->save();
            return response()->json('Success');
        }
        else{
            return response()->json('Error no order active session tracked.');
        }
    }

    public function checkout(Request $request){

        $func = new \App\Http\Controllers\FunctionsController;
        if ($request->session()->has('order_id_session')) {
            try{
                $order = Order::create([
                    'order_id'=> $request->session()->get('order_id_session'),
                    'customer_name'=>$request->customer_name,
                    'customer_contact'=>$request->customer_contact,
                    'table_number'=>$request->table_number,
                    'total'=>$request->total,
                    'device_id'=>$request->session()->get('device_id'),
                ]);
                
                RestoOrder::where('order_id', $request->session()->get('order_id_session'))
                    ->update(['is_checkout' => 1]);

                $request->session()->forget('order_id_session');
                
            //     $date = new DateTime(Carbon::now());
            //     $date = $date->format('d/m/Y i:s');
                
            //     $order_id = Hash::make($date."-".Str::random(100));
            // //Create a session
            //     $request->session()->put('order_id_session', $order_id);
                $request->session()->put('customer_name', $request->customer_name);
                $request->session()->put('customer_contact', $request->customer_contact);
                $request->session()->put('table_number', $request->table_number);


                return response()->json('success');

            }catch(Exception $e){
                $func->logger($e);
            }
            
        }else{
            return response()->json('Error no order active session tracked.');
        }
    }

    public function orderStatus(){
        
        return view('cart.order-status');
    }

    public function cancelProductOrder(Request $request){
        RestoOrder::where('order_id', $request->order_id)
                    ->where('product_id',$request->product_id)
                    ->where('variation_id',$request->variation_id)
                    ->where('option_key',$request->option_key)
                    ->update(['order_fulfillment_status' => 0]);
        return response()->json('success');
    }
    
    
    public function cancelAllOrders(Request $request){
        for($i = 0; $i<$request->count; $i++){
            RestoOrder::where('order_id', $request->order_id)
                ->where('product_id',$request->product_id)
                ->where('variation_id',$request->variation_id)
                ->where('option_key',$request->option_key)
                ->update(['order_fulfillment_status' => '-1']);
        }
        return response()->json('success');
    }
    

    public function acceptAllOrders(Request $request){
        for($i = 0; $i<$request->count; $i++){
            RestoOrder::where('order_id', $request->order_id)
                ->where('product_id',$request->product_id)
                ->where('variation_id',$request->variation_id)
                ->where('option_key',$request->option_key)
                ->update(['order_fulfillment_status' => '1']);
        }
        return response()->json('success');
    }
    
    public function serveAllOrders(Request $request){
        for($i = 0; $i<$request->count; $i++){
            RestoOrder::where('order_id', $request->order_id)
                ->where('product_id',$request->product_id)
                ->where('variation_id',$request->variation_id)
                ->where('option_key',$request->option_key)
                ->update(['order_fulfillment_status' => '2']);
        }
        return response()->json('success');
    }
}
