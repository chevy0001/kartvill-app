<?php

namespace App\Http\Controllers;

use App\Models\RetrieveAllOrders;
use Illuminate\Http\Request;

class RetrieveAllOrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function retrieveorders(){
        $customer_id = 6702617657644;
        $array = array(   
                'format'=> 'json',
            );
            $access_token = env('ACCESS_TOKEN');
            $funcw = new FunctionsController();
            $customer = $funcw->shopify_call($access_token, 'market-spays.myshopify.com','/admin/api/2022-04/orders.json', $array, 'GET');
            $customer = json_decode($customer['response'], JSON_PRETTY_PRINT);
            dd($customer);
    } 

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RetrieveAllOrders  $retrieveAllOrders
     * @return \Illuminate\Http\Response
     */
    public function show(RetrieveAllOrders $retrieveAllOrders)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RetrieveAllOrders  $retrieveAllOrders
     * @return \Illuminate\Http\Response
     */
    public function edit(RetrieveAllOrders $retrieveAllOrders)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RetrieveAllOrders  $retrieveAllOrders
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RetrieveAllOrders $retrieveAllOrders)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RetrieveAllOrders  $retrieveAllOrders
     * @return \Illuminate\Http\Response
     */
    public function destroy(RetrieveAllOrders $retrieveAllOrders)
    {
        //
    }
}
