<?php

namespace App\Http\Controllers;


use DateTime;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\ShopifyOrders;
use Illuminate\Support\Facades\DB;

class SalesReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sales_reports = ShopifyOrders::where('financial_status','paid')->get();
        //$sales_reports = ShopifyOrders::all();
        $sales = DB::table('shopify_orders')
                ->whereDate('created_at',Carbon::today())
                ->select('*', DB::raw('
                        SUM(vendor_net) as total_sales, 
                        SUM(commission) as commission,
                        SUM(product_price) as vendor_gross
                        '))
                ->groupBy('vendor')
                ->get(); 

                //$newOrderCount = ShopifyOrders::where('is_open',0)->count();
                //session(['newOrderCount' => $newOrderCount]);

        return view('sales.index',compact('sales'));
    }

    public function daily(Request $request){
        $createDate = new DateTime($request->date);
        $date = $createDate->format('Y-m-d');
        return redirect('/admin/sales/reports/daily/'.$date.'/'.$request->vendor);
       
    }

    public function vendorDailySales($vendor,$date){
        $dailySales = ShopifyOrders::where('vendor','LIKE',"%{$vendor}%")
                ->where('financial_status','paid')
                ->whereDate('created_at',$date)
                ->get();

        return view('vendor.daily-sales',compact('dailySales','vendor','date'));
    }
    public function dailySales($date=null,$vendor=null){  
        
                if($date == null){
                    $date = new DateTime(Carbon::now());
                    $date = $date->format('Y-m-d');
                }
                //dd($request->vendor);
                
               
                $sales = DB::table('shopify_orders')
                ->where('financial_status','paid')
                ->whereDate('created_at',date($date))
                ->where('vendor','LIKE',"%{$vendor}%")
                ->select('*', DB::raw('
                        SUM(vendor_net) as total_sales, 
                        SUM(commission) as commission,
                        SUM(product_price) as vendor_gross
                        '))
                ->groupBy('vendor')
                ->get(); 
            
                     
                
        $fDate = new SalesReportsController;
        $date = $fDate->formatDate($date);
        return view('sales.daily-vendor',compact('sales','date','vendor'));
    }

    public function autoCompleteSearch(Request $request){
            $query = $request->vendor;
          $filterResult = ShopifyOrders::where('vendor', 'LIKE', '%'. $query. '%')->get();
          return response()->json($filterResult);
    }

    public function formatDate($date){
        $createDate = new DateTime($date);
        $date = $createDate->format('F d Y');
        return $date;
    }

    public function weekly(Request $request){
        $createDate = new DateTime($request->date);
        $date = $createDate->format('Y-m-d');
        return redirect('/admin/sales/reports/weekly/'.$date.'/'.$request->vendor);
       
    }
    public function weeklySales($date=null,$vendor=null){  
        if($date == null){
            $date = new DateTime(Carbon::now());
            $date = $date->format('Y-m-d');
            $dt = Carbon::createFromFormat('Y-m-d',$date);
            $monthName = $dt->format('F');
        }else{
            $dt = Carbon::createFromFormat('Y-m-d',$date);
            $monthName = $dt->format('F');   
        }
       
        $fDate = new SalesReportsController;


        $startOfWeek = $fDate->formatDate(Carbon::create($date)->startOfWeek());
        $endOfWeek = $fDate->formatDate(Carbon::create($date)->endOfWeek());

        
        
        if($vendor == null){
        $sales = DB::table('shopify_orders')
        ->whereBetween('created_at', [Carbon::create($date)->startOfWeek(), Carbon::create($date)->endOfWeek()])
         ->where('financial_status','paid')
        ->select('*', DB::raw('
                SUM(vendor_net) as total_sales, 
                SUM(commission) as commission,
                SUM(product_price) as vendor_gross
                '))
        ->groupBy('vendor')
        ->get();
        //dd($sales);  
        }
        else{
            
        $sales = DB::table('shopify_orders')
        ->whereBetween('created_at', [Carbon::create($date)->startOfWeek(), Carbon::create($date)->endOfWeek()])
        ->where('financial_status','paid')
        ->where('vendor','LIKE',"%{$vendor}%")
        ->select('*', DB::raw('
                SUM(vendor_net) as total_sales, 
                SUM(commission) as commission,
                SUM(product_price) as vendor_gross
                '))
        ->groupBy('vendor')
        ->get();  
        }
return view('sales.weekly-vendor',compact('sales','startOfWeek','endOfWeek','vendor'));
}

public function vendorWeeklySales($vendor=null,$date=null){  
    if($date == null){
        $date = new DateTime(Carbon::now());
        $date = $date->format('Y-m-d');
        $dt = Carbon::createFromFormat('Y-m-d',$date);
        $monthName = $dt->format('F');
    }else{
        $dt = Carbon::createFromFormat('Y-m-d',$date);
        $monthName = $dt->format('F');   
    }
   
    $fDate = new SalesReportsController;


    $startOfWeek = $fDate->formatDate(Carbon::create($date)->startOfWeek());
    $endOfWeek = $fDate->formatDate(Carbon::create($date)->endOfWeek());

    
    $sales = DB::table('shopify_orders')
    ->whereBetween('created_at', [Carbon::create($date)->startOfWeek(), Carbon::create($date)->endOfWeek()])
    ->where('financial_status','paid')
    ->where('vendor','LIKE',"%{$vendor}%")
    ->get();  
//dd($sales);
return view('vendor.weekly-sales',compact('sales','startOfWeek','endOfWeek','vendor','date'));
}

public function countOrders(){
    
         //return view('layouts.admin',compact('countNewOrders'));   
}
    public function orders()
    {
        
        // $orders = DB::table('shopify_orders') 
        // ->orderBy('order_number', 'desc')
        // ->get();

       
       $orders = DB::table('delivery_notifications')
       ->rightJoin('shopify_orders','shopify_orders.order_id','=','delivery_notifications.order_id')
       ->orderBy('shopify_orders.order_number', 'desc')
       ->groupBy('shopify_orders.order_id')
       ->get();


       ShopifyOrders::where('is_open',0)
            ->update(['is_open'=>1]);

       //dd($orders);
        return view('sales.orders',compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


     public function monthly(Request $request){
        //$createDate = new DateTime($request->month);
        //$month = $createDate->format('Y-m-d');
        //dd($request->vendor);
        return redirect('/admin/sales/reports/monthly/'.$request->month.'/'.$request->year.'/'.$request->vendor);
     }
    public function monthlySales($month=null, $year=null, $vendor=null){
        
        
        if($month == null){
            $month = new DateTime(Carbon::now());
            $month = $month->format('Y-m-d');
            $dt = Carbon::createFromFormat('Y-m-d',$month);
            $month = date('m', strtotime($month));
            $monthName = $dt->format('F');
        }else{
           
            //$monthNumber = date('m', strtotime($date));
            
            $dateObj   = DateTime::createFromFormat('!m', $month);
            $monthName = $dateObj->format('F'); 
            $month = $month;
        }
       
        if($year == null){
            $year = date('Y');
        }


       if($vendor == null){
        $sales = ShopifyOrders::whereMonth('created_at', $month)
        ->where('financial_status','paid')
        ->whereYear('created_at',$year)
        ->select('*', DB::raw('
            SUM(vendor_net) as total_sales, 
            SUM(commission) as commission,
            SUM(product_price) as vendor_gross
            '))
        ->groupBy('vendor')
        ->get(); 
       }else{
        $sales = ShopifyOrders::whereMonth('created_at', $month)
        ->where('financial_status','paid')
        ->where('vendor','LIKE',"%{$vendor}%")
        ->whereYear('created_at', $year)
        ->select('*', DB::raw('
            SUM(vendor_net) as total_sales, 
            SUM(commission) as commission,
            SUM(product_price) as vendor_gross
            '))
    ->groupBy('vendor')
    ->get(); 
       }
      
       
        //$years = DB::table('shopify_orders')->select('created_at')->groupBy('created_at')->distinct()->get();
            //dd($sales);
            //$year = date('Y', strtotime($year->created_at));
        return view('sales.monthly-vendor',compact('sales','month','vendor','monthName','year'));    
    }

    public function vendorMonthlySales($vendor=null, $month=null ){
        if($month == null){
            $month = new DateTime(Carbon::now());
            $month = $month->format('Y-m-d');
            $dt = Carbon::createFromFormat('Y-m-d',$month);
            $month = date('m', strtotime($month));
            $monthName = $dt->format('F');
        }else{
           
            //$monthNumber = date('m', strtotime($date));
            
            $dateObj   = DateTime::createFromFormat('!m', $month);
            $monthName = $dateObj->format('F'); 
            $month = $month;
        }
       
       if($vendor == null){
            $sales = ShopifyOrders::whereMonth('created_at', $month)
            ->where('financial_status','paid')
            ->whereYear('created_at', date('Y'))
            
            
            ->get(); 
       }else{
            $sales = ShopifyOrders::whereMonth('created_at', $month)
            ->where('financial_status','paid')
            ->where('vendor','LIKE',"%{$vendor}%")
            ->whereYear('created_at', date('Y'))
            
            
            ->get(); 
       }
       
       
       
        
            //dd($sales);
        return view('vendor.monthly-sales',compact('sales','month','vendor','monthName'));   
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SalesReports  $salesReports
     * @return \Illuminate\Http\Response
     */
    
}
