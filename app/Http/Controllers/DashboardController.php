<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\User;
use App\Models\Wallet;
use App\Models\Sellers;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Models\ShopifyOrders;

class DashboardController extends Controller
{
    public function index(){
        if(auth()->user()->role == 2)
        {
            return redirect('/delivery');
        }
        elseif(auth()->user()->role == 3){
            return redirect('/staff');
        }
        else{
            // $func = new FunctionsController;
            // dd($func->repair(json_encode('Ñ')));
            return view('dashboard');
        }
    }

    public function test(){
        return view('test');
    }

    public function updateCustomer(Request $request){
        Customer::where('id',$request->id)
        ->update([
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name
        ]);
    
        return response(json_encode('successs'));
    }

    public function getTest($id){
        $response = Customer::where('id',$id)->first();
        
        return response()->json($response);
    }

    public function testPaidWallet(){
        
        $funcw = new FunctionsController;
        //Deduct wallet here
        $vendor_name = "Gensans test's Stores";
        $balance = 0;
        $order_number = 5607;
        $shipping_code = "Free for Pickup (Exclusive for Kartvillage)";
        //Get commission from $order
        try{
            $order = ShopifyOrders::where('order_number',$order_number)->first();
        }catch(Exception $e){
            $funcw->logger($e);
        }
        
       
       
        //Get Seller Id from $seller
        try{
            $seller = Sellers::where('store_name',$vendor_name)->first();
           
        }catch(Exception $e){
            $funcw->logger($e);
        }
       
        if($seller->isKartvillage && $shipping_code == "Free for Pickup (Exclusive for Kartvillage)"){
        //Get user id from $user
            try{
                $user = User::where('store_id',$seller->seller_id)->first();
               
            }catch(Exception $e){
                $funcw->logger($e);
            }
            
            //Get wallet balance here;
            try{
                $user_wallet = Wallet::where('user_id',$user->id)->first();
            }catch(Exception $e){
                $funcw->logger($e);
            }
            
            $balance = $user_wallet->balance - $order->commission;
           
            try{
                
             $update = Wallet::where('user_id',$user->id)->update(['balance'=>$balance]);
             
            }catch(Exception $e){
                $funcw->logger($e);
            }
            dd($update);
            $funcw->logger("Entered the wallet deduction");
        }
    }

    
    
    
}
