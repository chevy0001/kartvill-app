<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Sellers;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\ShopifyOrders;
use App\Models\Wallet;
use PHPMailer\PHPMailer\SMTP;
use Illuminate\Support\Facades\DB;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class ProfileController extends Controller
{

    public function forgotPassword(){
       
        return view('auth.forgot-password');
    }
    public function index($id){
        $wallet_balance = 0;
        $user = User::where('id',$id)->first();
        $store_name = "";
        if(Auth::user()->role != 1){
            if($user->role == 1 || $user->role == 4){
                return back();
            }
        }
        if(Auth::user()->role == 2 || Auth::user()->role == 4){
            if($user->id != Auth::user()->id){
                return back();
            }
        }
        if(Auth::user()->role == 5){
            return redirect('/wallet');
        }

        
        if($user == null){
            return view('errors.404');
        }
        
        else{

            
            if($user->role <> 5){
                $deliveries = DB::table('shopify_orders')->where('rider_id',$id)
                ->orderBy('created_at','desc')
                ->paginate(50);
                $wallet_balance = 0;
            }
            else{
                $store_name = Sellers::where('seller_id',$user->store_id)->first();
                
                $deliveries = ShopifyOrders::where('vendor',$store_name->store_name)
                    ->where('shipping_code','Free for Pickup (Exclusive for Kartvillage)')
                    ->orderBy('created_at','desc')
                    ->paginate(50);
                    $wallet_bal = Wallet::where('user_id',$id)->first();
                    $wallet_balance = $wallet_bal->balance;
            }
           
            //  dd($deliveries);
           
            $completed_deliveries = DB::table('shopify_orders')->where('order_status','delivered')->where('rider_id',$id)->count();
            $pending_deliveries = DB::table('shopify_orders')->where('order_status','<>','delivered')->where('rider_id',$id)->count();
            $revenue = ShopifyOrders::where('rider_id',$id)->where('order_status','delivered')->sum('delivery_fee');
            // dd($user);
            return view('profile.profile',compact('user','deliveries','completed_deliveries','pending_deliveries','revenue','wallet_balance','store_name'));
        }
    }


    public function confirmPassword($reset_token){

        $requested_account = DB::table('password_resets')->where('token',$reset_token)->first();
        if($requested_account == null){
            return redirect('/login');
        }
        else{
            
            return view('auth.confirm-password',compact('reset_token','requested_account'));
        }
       
    }

    public function changePassword(Request $request){
        $password = Hash::make($request->password);

        $requested_account = DB::table('password_resets')->where('token',$request->token)->first();
        if($requested_account == null || $requested_account->token == '-1'){
            return redirect('/login');
        }
        else{
            $isChange = User::where('email',$requested_account->email)
            ->update(['password'=> $password]);
            
            DB::table('password_resets')
                ->where('token', $request->token)
                ->where('email',$requested_account->email)
                ->update(['token' => '-1']);

            DB::table('password_resets')
                ->where('email',$requested_account->email)
                ->update(['token' => '-1']);  


              return redirect('/login')->with('success','Password successfully changed');
        }
    }
    public function resetPasswordMail(Request $request){
        
        $funcw = new FunctionsController;
        $profile = User::where('email',$request->useremail)->first();
        //  dd($profile);
        if($profile == null){
            return back()->with('error','Cannot find your account please signup.');
            $funcw->logger('No profile. From reset Password.'.$request->useremail);
        }else{

            $attempt = DB::table('password_resets')
                ->where('email',$request->useremail)
                ->where('token','<>','-1')
                ->count();
                //  dd($attempt);
            if($attempt > 2){
                return back()->with('error','Too much attempt. Please check your email for the password reset link.');
            }else{

                $created_at = Carbon::now()->format('Y-m-d H:m:s');
                $reset_token = Str::random(60);

                DB::table('password_resets')->insert([
                    'email'=>$request->useremail,
                    'token'=>$reset_token,
                    'created_at'=>$created_at
                ]);
                    // the message
                    $msg = "Reset your password here: https://kartvillapps.com/resetpassword-$reset_token";
                    // use wordwrap() if lines are longer than 70 characters
                    $msg = wordwrap($msg,70);
                    // send email

                    // $headers = 'From: no-reply@kartvill.com\r\n';
                    // mail($request->useremail, 'Reset Password', $msg, $headers);


                    $mail = new PHPMailer(true);


                    try {
                        //Server settings
                        $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
                        $mail->isSMTP();                                            //Send using SMTP
                        $mail->Host       = env('PHP_HOST');                     //Set the SMTP server to send through
                        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
                        $mail->Username   = env('PHP_USERNAME');                     //SMTP username
                        $mail->Password   = env('PHP_PASSWORD');                               //SMTP password
                        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
                        $mail->Port       = env('PHP_PORT');                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`
                    
                        //Recipients
                        $mail->setFrom('no-reply@kartvillapps.com', 'Kartvill');
                        $mail->addAddress($request->useremail, $profile->first_name);     //Add a recipient
                                                //Name is optional
                    
                        
                    
                        
                        //Content
                        $mail->isHTML(true);                                  //Set email format to HTML
                        $mail->Subject = 'Reset Password';
                        $mail->Body    = $msg;
                    
                        $mail->send();
                        echo 'Message has been sent';
                    } catch (Exception $e) {
                        $funcw->logger("Message could not be sent. Mailer Error: {$mail->ErrorInfo}");
                    }




                    $funcw->logger('Successful Email. From reset Password.'.$request->useremail);
                    return back()->with('status','Please check your email for password reset link.');
            }
        
        }
         
        

        

    }

    public function destroy($id)
    {
        // $user = User::find($id);
        User::where('id',$id)
            ->update([
                'role'=> '-1',
        ]);
        // $user->delete();
        return back()->with('success','User successfully archive');
    }

    
}
