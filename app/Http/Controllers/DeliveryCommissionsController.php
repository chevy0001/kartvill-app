<?php

namespace App\Http\Controllers;

use App\Models\DeliveryCommissions;
use Illuminate\Http\Request;

class DeliveryCommissionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DeliveryCommissions  $deliveryCommissions
     * @return \Illuminate\Http\Response
     */
    public function show(DeliveryCommissions $deliveryCommissions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DeliveryCommissions  $deliveryCommissions
     * @return \Illuminate\Http\Response
     */
    public function edit(DeliveryCommissions $deliveryCommissions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DeliveryCommissions  $deliveryCommissions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DeliveryCommissions $deliveryCommissions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DeliveryCommissions  $deliveryCommissions
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeliveryCommissions $deliveryCommissions)
    {
        //
    }
}
