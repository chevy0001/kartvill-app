<?php

namespace App\Http\Controllers\Investor;

use DateTime;
use Carbon\Carbon;
use App\Models\ShopifyOrders;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SalesReportsController;
use Illuminate\Http\Request;

class InvestorController extends Controller
{
   public function index(){


    $date = new DateTime(Carbon::now());
    $date = $date->format('Y-m-d');
    
    // Daily Sales
    $dailyTotalSales = ShopifyOrders::whereDate('created_at',$date)
                ->where('financial_status','paid')
                ->sum('commission');

    $defaultDailySales = ShopifyOrders::where('financial_status','paid')
    ->whereDate('created_at',$date)
    ->get();

    //End of Daily Sales

    $dt = Carbon::createFromFormat('Y-m-d',$date);
    $monthName = $dt->format('F');
    $fDate = new SalesReportsController;
    $startOfWeek = $fDate->formatDate(Carbon::create($date)->startOfWeek());
    $endOfWeek = $fDate->formatDate(Carbon::create($date)->endOfWeek());
    
    
    $month = date('m', strtotime($date));
    $year = date('Y',strtotime($date));

    //Start of weekly sales
        $weeklyTotalSales = DB::table('shopify_orders')
            ->whereMonth('created_at', $month)
            ->whereBetween('created_at', [Carbon::create($date)->startOfWeek(), Carbon::create($date)->endOfWeek()])
            ->where('financial_status','paid')
            ->sum('commission');
        
        $defaultWeeklyTotalSales = DB::table('shopify_orders')
        ->whereMonth('created_at', $month)
        ->whereBetween('created_at', [Carbon::create($date)->startOfWeek(), Carbon::create($date)->endOfWeek()])
        ->where('financial_status','paid')
        ->select('*', DB::raw('
                SUM(vendor_net) as total_sales, 
                SUM(commission) as commission,
                SUM(product_price) as vendor_gross
                '))
        ->groupBy('vendor')
        ->get();  
    //End of weekly sales



    $monthlyTotalSales = ShopifyOrders::whereMonth('created_at', $month)
        ->where('financial_status','paid')
        ->whereYear('created_at',$year)
        ->sum('commission');
       
        $defaultMonthlyTotalSales = ShopifyOrders::whereMonth('created_at', $month)
        ->where('financial_status','paid')
        ->whereYear('created_at',$year)
        ->select('*', DB::raw('
            SUM(vendor_net) as total_sales, 
            SUM(commission) as commission,
            SUM(product_price) as vendor_gross
            '))
        ->groupBy('vendor')
        ->get(); 
    $totalCommission = ShopifyOrders::where('financial_status','paid')->sum('commission');   
    
    
    
    
    return view('investor.index',compact('dailyTotalSales','weeklyTotalSales','monthlyTotalSales','totalCommission','date','defaultDailySales','defaultWeeklyTotalSales','defaultMonthlyTotalSales','monthName'));
   }


   
   public function dailyOrders(Request $request){
    $dailySales = DB::table('shopify_orders')
                ->where('financial_status','paid')
                ->whereDate('created_at',date($request->date))
                ->select('*', DB::raw('
                        SUM(vendor_net) as total_sales, 
                        SUM(commission) as commission,
                        SUM(product_price) as vendor_gross
                        '))
                ->groupBy('vendor')
                ->get(); 

    $dailyTotalSales = ShopifyOrders::whereDate('created_at',$request->date)
    ->where('financial_status','paid')
    ->sum('commission');

         if($dailySales->count() > 0){
            return response()->json(['data'=>$dailySales, 'status'=>'1','commission'=>$dailyTotalSales]);
         }
         else{
            return response()->json(['status'=>'0']);
         }
    
    }

    public function weeklyOrders(Request $request){
        $month = date('m', strtotime($request->date));
        $weeklyTotalSales = DB::table('shopify_orders')
            ->whereMonth('created_at', $month)
            ->whereBetween('created_at', [Carbon::create($request->date)->startOfWeek(), Carbon::create($request->date)->endOfWeek()])
            ->where('financial_status','paid')
            ->sum('commission');

        $defaultWeeklyTotalSales = DB::table('shopify_orders')
        ->whereMonth('created_at', $month)
        ->whereBetween('created_at', [Carbon::create($request->date)->startOfWeek(), Carbon::create($request->date)->endOfWeek()])
        ->where('financial_status','paid')
        ->select('*', DB::raw('
                SUM(vendor_net) as total_sales, 
                SUM(commission) as commission,
                SUM(product_price) as vendor_gross
                '))
        ->groupBy('vendor')
        ->get();  

        if($defaultWeeklyTotalSales->count() > 0){
            return response()->json(['data'=>$defaultWeeklyTotalSales, 'status'=>'1','commission'=>$weeklyTotalSales]);
         }
         else{
            return response()->json(['status'=>'0']);
         }
    
    }

    public function monthlyorders(Request $request){
        $dt = Carbon::createFromFormat('Y-m-d',$request->date);
        $monthName = $dt->format('F');
        $month = date('m', strtotime($request->date));
        $year = date('Y',strtotime($request->date));

        $monthlyTotalSales = ShopifyOrders::whereMonth('created_at', $month)
        ->where('financial_status','paid')
        ->whereYear('created_at',$year)
        ->sum('commission');
       
        $defaultMonthlyTotalSales = ShopifyOrders::whereMonth('created_at', $month)
        ->where('financial_status','paid')
        ->whereYear('created_at',$year)
        ->select('*', DB::raw('
            SUM(vendor_net) as total_sales, 
            SUM(commission) as commission,
            SUM(product_price) as vendor_gross
            '))
        ->groupBy('vendor')
        ->get(); 

        if($defaultMonthlyTotalSales->count() > 0){
            return response()->json(['data'=>$defaultMonthlyTotalSales, 'status'=>'1','commission'=>$monthlyTotalSales,'monthName'=>$monthName]);
         }
         else{
            return response()->json(['status'=>'0']);
         }
    }
}
