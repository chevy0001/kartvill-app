<?php

namespace App\Http\Controllers;

use DateTime;
use Exception;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Sellers;
use App\Models\CityCode;
use App\Models\Customer;
use App\Models\PabiliOrder;
use Illuminate\Http\Request;
use App\Models\ShopifyOrders;
use App\Models\KartvillSettings;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\FunctionsController;

class SellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $city_code = CityCode::all();
        
        return view('vendor.add-seller',compact('city_code'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $func = new FunctionsController;
        $store_name = $func->repair($request->store_name); 
        $seller_name = $func->repair($request->seller_name);
        $seller = Sellers::create([
                'seller_id'=>$request->seller_id,
                'store_name'=>htmlspecialchars_decode($store_name),
                'seller_name'=>htmlspecialchars_decode($seller_name),
                'seller_email'=>htmlspecialchars_decode($request->seller_email),
                'seller_contact'=>htmlspecialchars_decode($request->seller_contact),
                'country'=>htmlspecialchars_decode($request->country),
                'city'=>htmlspecialchars_decode($request->city),
                'store_address'=>htmlspecialchars_decode($request->store_address),
                'warehouse_address'=>htmlspecialchars_decode($request->warehouse_address),
                'latitude'=>$request->latitude,
                'longitude'=>$request->longitude,
                'province_code'=>$request->province_code,
                'zip'=>$request->zip,
                'commission'=>$request->commission,
                'store_type'=>$request->store_type,
        ]);

        $log = new FunctionsController;
        $logs = Auth::user()->first_name . " ". Auth::user()->last_name ." added a new seller. $store_name";
        $log->logger($logs);
        
        return redirect('/sellers-'.$store_name)->with('success','Successfully added '.$store_name);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function newSellers(){
        $sellers = Sellers::where('seller_id',null)
        ->orderBy('created_at','desc')
        ->paginate(50);
        return view('vendor.new-sellers',compact('sellers'));
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($sellerid)
    {
        $seller = Sellers::where('seller_id',$sellerid)
        ->orWhere('id',$sellerid)
        ->first();

        $province_codes  = CityCode::all();
        // dd($seller);
        return view('vendor.seller-edit',compact('seller','province_codes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $sellerid = null)
    {
        //dd($sellerid);
        if($sellerid == null){
            return view('errors.404');

        }
        else{

       
        $func = new FunctionsController();
        // $store_name = $func->repair($request->store_name); 
        // $seller_name = $func->repair($request->seller_name); 
        //$store_name = $func->repairName2($store_name);
        $store_phone = $func->repairPhoneNumber($request->seller_contact);
        if($sellerid == null){
            $sellerid = $request->seller_id;
        }
        //dd($sellerid);
        
        try{

        $update = Sellers::where('id',$sellerid)
            ->update([
                'seller_id'=>$request->seller_id,
                'store_name'=>htmlspecialchars_decode($request->store_name),
                'seller_name'=>htmlspecialchars_decode($request->seller_name),
                'seller_email'=>$request->seller_email,
                'cashier_number'=>$request->cashier_number,
                'seller_contact'=>htmlspecialchars_decode($store_phone),
                'city'=>htmlspecialchars_decode($request->city),
                'store_address'=>htmlspecialchars_decode($request->store_address),
                'warehouse_address'=>htmlspecialchars_decode($request->warehouse_address),
                'country'=>htmlspecialchars_decode($request->country),
                'latitude'=>$request->latitude,
                'longitude'=>$request->longitude,
                'province_code'=>$request->province_code,
                'commission'=>$request->commission,
                'store_type'=>$request->store_type,
                'zip'=>$request->zip,
                'isKartvillage'=>$request->isKartvillage
            ]);
        }catch(Exception $e){
            return back()->with('error','Error: '. $e);
        }
            // $func->logger("is seller updated: ".$update);
            // dd($update);
            $response = Auth::user()->first_name. " Updated merchant: $request->store_name";
            $func->logger($response);
            return back()->with('success','Successfully edited '. $request->store_name);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function searchOrders(Request $request){
        
        return redirect("/kartvillorders-".$request->order_number);
     }
    public function vendorOrders($ordernumber = null){
      if($ordernumber != null){
        if(Auth::user()->role == 5 || Auth::user()->role == 1 || Auth::user()->role == 4){
            $seller_name = DB::table('users')
                ->rightJoin('sellers','sellers.seller_id','users.store_id')
                ->where('users.id',Auth::user()->id)
                ->first();
                
            $orders = DB::table('shopify_orders')->where('vendor','LIKE','%'.$seller_name->store_name.'%')   
                ->where('order_number',$ordernumber) 
                ->orderBy('shopify_orders.created_at', 'desc')
                ->paginate(1);
                // dd($orders);
                return view('vendor.kartvillage',compact('orders'));
        }
        else{
            if(Auth::user()->role < 1){
                return ('Wait for admin verification.');
            }
            else{
                $orders = null;
            }   
        }
      }else{
        if(Auth::user()->role == 5 || Auth::user()->role == 1 || Auth::user()->role == 4){
            $seller_name = DB::table('users')
                ->rightJoin('sellers','sellers.seller_id','users.store_id')
                ->where('users.id',Auth::user()->id)
                ->first();
                
            $orders = DB::table('shopify_orders')->where('vendor','LIKE','%'.$seller_name->store_name.'%')    
                ->orderBy('shopify_orders.created_at', 'desc')
                ->paginate(50);
                // dd($orders);
        }
        else{
            if(Auth::user()->role < 1){
                return ('Wait for admin verification.');
            }
            else{
                $orders = null;
            }   
        }
        
        // $orders = ShopifyOrders::where('rider_id',0)
        //          ->orWhere('rider_id',null)->get();
        //dd($orders);
        return view('vendor.kartvillage',compact('orders'));
      }
        
    }
    public function todayOrders(){
        $date = new DateTime(Carbon::now());
        $date = $date->format('Y-m-d');
        if(Auth::user()->role == 5 || Auth::user()->role == 1 || Auth::user()->role == 4){
            $seller_name = DB::table('users')
                ->rightJoin('sellers','sellers.seller_id','users.store_id')
                ->where('users.id',Auth::user()->id)
                ->first();
                
            $orders = DB::table('shopify_orders')->where('vendor','LIKE','%'.$seller_name->store_name.'%')    
                ->orderBy('shopify_orders.created_at', 'desc')
                ->whereDate('created_at',date($date))
                ->paginate(50);
                // dd($orders);
        }
        else{
            if(Auth::user()->role < 1){
                return ('Wait for admin verification.');
            }
            else{
                $orders = null;
            }   
        }
    
        return view('vendor.today-orders',compact('orders'));
    }

    public function vendorOrder($orderId){
        $kartvill_setting = KartvillSettings::find(1)->first();
        $array = array(         
                'format'=> 'json',
        );

            $deliveryCrews = User::where('role',2)
            ->where('availability',1)
            ->get();
        $access_token = env('ACCESS_TOKEN');
        
        $URL = env('URL');
        $func = new FunctionsController;
        //Get from the API
        
         $orders = $func->shopify_call($access_token, 'market-spays.myshopify.com','/admin/api/2022-04/orders/'.$orderId.'.json', $array, 'GET');
        //$orders = $func->shopify_call($access_token, 'market-spays.myshopify.com','/admin/api/2022-04/orders/4695964156061.json', $array, 'GET');
        
        $orders = json_decode($orders['response'], JSON_PRETTY_PRINT);
       
        if($orders['order']['line_items'][0]['vendor'] <> ""){
            $vendor_name = $orders['order']['line_items'][0]['vendor'];
        }
        else{
            foreach($orders['order']['line_items']['0']['properties'] as $order){
                if($order['name'] === '_wk_vendor')
                $vendor_name = $order['value'];
               
            }
        }

        
        if(isset($orders['errors'])){
            return view('errors.404');
        }
        //Get Shipping Address here
                
                if(isset($orders['order']['shipping_address']['address1'])){
                    $customer_id = $orders['order']['customer']['id'];
                    $customer_id = str_replace(' ', '', $customer_id);
                    $customer = Customer::where('customer_id',$customer_id)->first();
                    $customer_address = $orders['order']['shipping_address']['address1'].','.$orders['order']['shipping_address']['city'].','.$orders['order']['shipping_address']['country'];
                }else if(isset($orders['order']['customer']['default_address']['address1'])){
                    $customer_address = $orders['order']['customer']['default_address']['address1'].','.$orders['order']['customer']['default_address']['city'].','.$orders['order']['customer']['default_address']['country'];
                    $customer_id = $orders['order']['customer']['id'];
                    $customer_id = str_replace(' ', '', $customer_id);
                    $customer = Customer::where('customer_id',$customer_id)->first();
                }else if(isset($orders['order']['billing_address']['address1'])){
                    $customer_address = $orders['order']['billing_address']['address1'].','.$orders['order']['billing_address']['city'].','.$orders['order']['billing_address']['country'];
                    $customer_id = $orders['order']['customer']['id'];
                    $customer_id = str_replace(' ', '', $customer_id);
                    $customer = Customer::where('customer_id',$customer_id)->first();
                }
                else{
                    $customer_address = "Robinsons, General Santos City, South Cotabato";
                    $customer = "";
                }
               
                $vendor_name = $func->repair($vendor_name);
                

                $sellers = Sellers::where('store_name',$vendor_name)->get();
            
                //Get the nearest Seller from the customer
                $shortest_distance = 10000000000;
                $vendor_address = "Robinsons, General Santos City";
            
                foreach($sellers as $vendor){
                    if($vendor === null){
                        $vendor_address = "Robinsons, General Santos City";
                    }else{
                        if($vendor->warehouse_address === null){
                            $vendor_address = $vendor->store_address.",".$vendor->city.",".$vendor->zip.",".$vendor->city;
                        }
                        else{
                            $vendor_address = $vendor->warehouse_address;
                        }
                    }    
            
            
            
                    //Destination is the address of the customer
                    //Vendor is the address of the vendor
                  if($vendor_address != "No Address"){ 
                        $data = $func->getDistance($customer_address,$vendor_address);
                        $distance_data = explode("-",$data);
                        $km = $distance_data[0];// KM
                        
                        if($km < $shortest_distance){
                            $vendor_id = $vendor->id;
                            $duration = $distance_data['1'];// Duration
                        }
                    }
                }
                if($vendor_address != "No Address"){
                    $duration_explode = explode(" ",$duration);
                    $duration = $duration_explode[0];
            
            
                    $commission = $kartvill_setting->default_kartvill_commission/100;
                    $seller = Sellers::where('id',$vendor_id)->first();
                    if($seller === null){
                        $store_address = "SM City, General Santos City"; 
                        $commission = $kartvill_setting->default_kartvill_commission/100;
                    }
                    elseif($seller->commission == 0 || $seller->commission === null ){
                        $commission = $kartvill_setting->default_kartvill_commission/100;
                    }
                    else{
                        $commission = $seller->commission/100;
                    }

                    if($orders['order']['current_total_discounts'] > 1){
                        $commission = $commission - ($commission * 0.2);
                    }
                
                    $sub_total = str_replace(",","",$orders['order']['current_subtotal_price']);
                   
                    
                    $merchant_payable = number_format($sub_total / (1 +($commission)),2);
                    
                    $merchant_payable = str_replace(",","",$merchant_payable);
                    
                    $kartvill_commission = number_format($sub_total - $merchant_payable,2);
      
                }
                else{
                    $merchant_payable = 0;
                    $seller = "";
                    $kartvill_commission=0;
                    $duration=0;
                    $km = 0;

                }
                $order_id = "".$orders['order']['id'];
                // $pabili_address = DB::table('pabili_orders')->where('order_id',$order_id)->first();
                
                // $pabili_address = PabiliOrder::where('order_id',$order_id)->first();
                $pabili_address = PabiliOrder::where('order_id', $order_id)->first();
                 
                if(isset($pabili_address)){
                    $seller = $pabili_address;
                    $warehouse = "";
                }
                else{
                    if(isset($seller->warehouse_address)){
                        $warehouse = str_replace("+",'%2B',$seller->warehouse_address);
                    }
                    elseif(isset($seller->store_address)){
                        $warehouse = $seller->store_address;
                    }
                    else{
                     $warehouse = "";
                    }
                }
                
        //End of getting vendor address            


        foreach($orders['order']['line_items'] as $product){
            $images = $func->shopify_call($access_token, 'market-spays.myshopify.com','/admin/api/2022-01/products/'.$product['product_id'].'.json', $array, 'GET');

        }
        $images = json_decode($images['response'], JSON_PRETTY_PRINT);
        $order = ShopifyOrders::where('order_id','like','%'.$orderId.'%')->first();

        $vendor = Sellers::where('store_name',$order->vendor)->first();

        // dd($vendor);
      
        return view('vendor.order',compact('vendor','customer','orders','merchant_payable','seller','kartvill_commission','access_token','array','order','warehouse','duration','km'));
    
    }

    public function printReceipt($orderId){
        $kartvill_setting = KartvillSettings::find(1)->first();
        $array = array(         
                'format'=> 'json',
        );

            $deliveryCrews = User::where('role',2)
            ->where('availability',1)
            ->get();
        $access_token = env('ACCESS_TOKEN');
        
        $URL = env('URL');
        $func = new FunctionsController;
        //Get from the API
        
         $orders = $func->shopify_call($access_token, 'market-spays.myshopify.com','/admin/api/2022-04/orders/'.$orderId.'.json', $array, 'GET');
        //$orders = $func->shopify_call($access_token, 'market-spays.myshopify.com','/admin/api/2022-04/orders/4695964156061.json', $array, 'GET');
        
        $orders = json_decode($orders['response'], JSON_PRETTY_PRINT);
       
        if($orders['order']['line_items'][0]['vendor'] <> ""){
            $vendor_name = $orders['order']['line_items'][0]['vendor'];
        }
        else{
            foreach($orders['order']['line_items']['0']['properties'] as $order){
                if($order['name'] === '_wk_vendor')
                $vendor_name = $order['value'];
               
            }
        }

        
        if(isset($orders['errors'])){
            return view('errors.404');
        }
        //Get Shipping Address here
                
                if(isset($orders['order']['shipping_address']['address1'])){
                    $customer_id = $orders['order']['customer']['id'];
                    $customer_id = str_replace(' ', '', $customer_id);
                    $customer = Customer::where('customer_id',$customer_id)->first();
                    $customer_address = $orders['order']['shipping_address']['address1'].','.$orders['order']['shipping_address']['city'].','.$orders['order']['shipping_address']['country'];
                }else if(isset($orders['order']['customer']['default_address']['address1'])){
                    $customer_address = $orders['order']['customer']['default_address']['address1'].','.$orders['order']['customer']['default_address']['city'].','.$orders['order']['customer']['default_address']['country'];
                    $customer_id = $orders['order']['customer']['id'];
                    $customer_id = str_replace(' ', '', $customer_id);
                    $customer = Customer::where('customer_id',$customer_id)->first();
                }else if(isset($orders['order']['billing_address']['address1'])){
                    $customer_address = $orders['order']['billing_address']['address1'].','.$orders['order']['billing_address']['city'].','.$orders['order']['billing_address']['country'];
                    $customer_id = $orders['order']['customer']['id'];
                    $customer_id = str_replace(' ', '', $customer_id);
                    $customer = Customer::where('customer_id',$customer_id)->first();
                }
                else{
                    $customer_address = "Robinsons, General Santos City, South Cotabato";
                    $customer = "";
                }
               
                $vendor_name = $func->repair($vendor_name);
                

                $sellers = Sellers::where('store_name',$vendor_name)->get();
            
                //Get the nearest Seller from the customer
                $shortest_distance = 10000000000;
                $vendor_address = "Robinsons, General Santos City";
            
                foreach($sellers as $vendor){
                    if($vendor === null){
                        $vendor_address = "Robinsons, General Santos City";
                    }else{
                        if($vendor->warehouse_address === null){
                            $vendor_address = $vendor->store_address.",".$vendor->city.",".$vendor->zip.",".$vendor->city;
                        }
                        else{
                            $vendor_address = $vendor->warehouse_address;
                        }
                    }    
            
            
            
                    //Destination is the address of the customer
                    //Vendor is the address of the vendor
                  if($vendor_address != "No Address"){ 
                        $data = $func->getDistance($customer_address,$vendor_address);
                        $distance_data = explode("-",$data);
                        $km = $distance_data[0];// KM
                        
                        if($km < $shortest_distance){
                            $vendor_id = $vendor->id;
                            $duration = $distance_data['1'];// Duration
                        }
                    }
                }
                if($vendor_address != "No Address"){
                    $duration_explode = explode(" ",$duration);
                    $duration = $duration_explode[0];
            
            
                    $commission = $kartvill_setting->default_kartvill_commission/100;
                    $seller = Sellers::where('id',$vendor_id)->first();
                    if($seller === null){
                        $store_address = "SM City, General Santos City"; 
                        $commission = $kartvill_setting->default_kartvill_commission/100;
                    }
                    elseif($seller->commission == 0 || $seller->commission === null ){
                        $commission = $kartvill_setting->default_kartvill_commission/100;
                    }
                    else{
                        $commission = $seller->commission/100;
                    }

                    if($orders['order']['current_total_discounts'] > 1){
                        $commission = $commission - ($commission * 0.2);
                    }
                
                    $sub_total = str_replace(",","",$orders['order']['current_subtotal_price']);
                   
                    
                    $merchant_payable = number_format($sub_total / (1 +($commission)),2);
                    
                    $merchant_payable = str_replace(",","",$merchant_payable);
                    
                    $kartvill_commission = number_format($sub_total - $merchant_payable,2);
      
                }
                else{
                    $merchant_payable = 0;
                    $seller = "";
                    $kartvill_commission=0;
                    $duration=0;
                    $km = 0;

                }
                $order_id = "".$orders['order']['id'];
                // $pabili_address = DB::table('pabili_orders')->where('order_id',$order_id)->first();
                
                // $pabili_address = PabiliOrder::where('order_id',$order_id)->first();
                $pabili_address = PabiliOrder::where('order_id', $order_id)->first();
                 
                if(isset($pabili_address)){
                    $seller = $pabili_address;
                    $warehouse = "";
                }
                else{
                    if(isset($seller->warehouse_address)){
                        $warehouse = str_replace("+",'%2B',$seller->warehouse_address);
                    }
                    elseif(isset($seller->store_address)){
                        $warehouse = $seller->store_address;
                    }
                    else{
                     $warehouse = "";
                    }
                }
                
        //End of getting vendor address            


        foreach($orders['order']['line_items'] as $product){
            $images = $func->shopify_call($access_token, 'market-spays.myshopify.com','/admin/api/2022-01/products/'.$product['product_id'].'.json', $array, 'GET');

        }
        $images = json_decode($images['response'], JSON_PRETTY_PRINT);
        $order = ShopifyOrders::where('order_id','like','%'.$orderId.'%')->first();

        $vendor = Sellers::where('store_name',$order->vendor)->first();

        // dd($vendor);
        $date = new DateTime(Carbon::now());
        $date = $date->format('m/d/Y h:i:s A');
        return view('vendor.receipt',compact('date','vendor','customer','orders','merchant_payable','seller','kartvill_commission','access_token','array','order','warehouse','duration','km'));
    
    }
    public function bluetooth(){
        return response()->json(["order_number"=>"1234"]);
    }

    //vendor accepts order from kartvillapps
    public function acceptOrder(Request $request){
        $func = new FunctionsController; 
        //Get phone of customer
            $array = array(   
                'format'=> 'json',
            );
            $access_token = env('ACCESS_TOKEN');
            $order_id = "".$request->order_id;
            $customer = $func->shopify_call($access_token, 'market-spays.myshopify.com','/admin/api/2022-04/orders/'.$order_id.'.json', $array, 'GET');
            
           
            $customer = json_decode($customer['response'], JSON_PRETTY_PRINT);
            
            if(isset($customer['order']['shipping_address']['phone'])){
                $customer_phone = $customer['order']['shipping_address']['phone'];
            }else{
                $customer_phone = $customer['order']['default_address']['phone'];
            }
            
            
            
           
            $customer_phone=$func->repairPhoneNumber($customer_phone);
            $customer_phone = str_replace("+1","+",$customer_phone);
            $customer_phone = str_replace("+","",$customer_phone);
            
            $txtbox_api = env('TXTBOX_API_KEY');

            $message = "Your order is being prepared. Thank you for waiting. Happy day.";
        
          if($request->status == 'accepted'){
            try{
                ShopifyOrders::where('order_id',$request->order_id)
                    ->update([
                    'fulfillment_status'=>"accepted",
                ]);
                
                $func->smsTxtBox($message,$customer_phone,$txtbox_api,1);
                return back()->with("status","The order is accepted successfully. We send a message to the customer.");       
                
           }catch(Exception $e){
                $func->logger($e);
                return response()->json(['success' => 500]);
           }   
          }elseif($request->status == "ready"){
            $message = "Your order is ready. Our crew will serve it for you.";
            try{
                ShopifyOrders::where('order_id',$request->order_id)
                    ->update([
                    'fulfillment_status'=>"ready",
                ]);
                
                $func->smsTxtBox($message,$customer_phone,$txtbox_api,1);
                return back()->with("status","We inform the customer about the order status. Thank you.");       
                
           }catch(Exception $e){
                $func->logger($e);
                return response()->json(['success' => 500]);
           }   
          }elseif($request->status == "served"){
            
            try{
                ShopifyOrders::where('order_id',$request->order_id)
                    ->update([
                    'fulfillment_status'=>"served",
                ]);
                
                
                return back()->with("status","The status updated succesfully.");       
                
           }catch(Exception $e){
                $func->logger($e);
                return response()->json(['success' => 500]);
           }   
          }
             
    }

    public function vendorRejectOrder(Request $request){
        
       $func = new FunctionsController; 
       try{
            ShopifyOrders::where('order_id',$request->order_id)
                ->update([
                'fulfillment_status'=>"Cancelled",
                'financial_status'=>'Refunded'
            ]);
            $msg = "The order id $request->order_id has been cancelled by the merchant. https://market-spays.myshopify.com/admin/orders/$request->order_id";
            $subject = "Cancelled $request->order_id";
            $func->sendEmail("marketspays@gmail.com","Order ID".$request->order_id,$msg,$subject);

            return response()->json(['success' => 200]);
       }catch(Exception $e){
            $func->logger($e);
            return response()->json(['success' => 500]);
       }
            
           
             
        
       
        
    }

    public function destroy($id)
    {
        $seller = Sellers::find($id);
        $seller->delete();
        return back();
    }
}
