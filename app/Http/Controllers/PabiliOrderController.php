<?php

namespace App\Http\Controllers;

use App\Models\PabiliOrder;
use Illuminate\Http\Request;

class PabiliOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PabiliOrder  $pabiliOrder
     * @return \Illuminate\Http\Response
     */
    public function show(PabiliOrder $pabiliOrder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PabiliOrder  $pabiliOrder
     * @return \Illuminate\Http\Response
     */
    public function edit(PabiliOrder $pabiliOrder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PabiliOrder  $pabiliOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PabiliOrder $pabiliOrder)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PabiliOrder  $pabiliOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(PabiliOrder $pabiliOrder)
    {
        //
    }
}
