<?php

namespace App\Http\Controllers\Delivery;

use Exception;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Wallet;
use App\Models\Sellers;
use App\Models\Customer;
use App\Models\PabiliOrder;
use Illuminate\Http\Request;
use App\Models\ShopifyOrders;
use App\Models\KartvillSettings;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\DeliveryNotifications;
use App\Http\Controllers\FunctionsController;

class DeliveryCrewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $availability = User::where('id',Auth::user()->id)->first();  
        return view('delivery.index',compact('availability'));
    }

    public function notifications(){
        //dd(Auth::user()->id);
        if(Auth::user()->role == 3 || Auth::user()->role == 1 || Auth::user()->role == 4){
            $orders = DB::table('users')
                ->rightJoin('shopify_orders','shopify_orders.rider_id','users.id')
                ->where('shipping_code','<>',"Free for Pickup (Exclusive for Kartvillage)")
                ->orderBy('shopify_orders.created_at', 'desc')
                ->paginate(50);
                //dd($orders);
        }
        elseif(Auth::user()->role == 2 && Auth::user()->availability == 1){
            $orders = DB::table('shopify_orders')
                ->where('rider_id',0)
                // ->where('is_assigned',Auth::user()->id)
                ->where('order_status',null)
                ->where('rider_id','<>',-1)
                ->where('shipping_code','<>',"Free for Pickup (Exclusive for Kartvillage)")
                ->orWhere('rider_id',null)
                ->orderBy('created_at', 'desc')
                ->paginate(50);

                $count_orders = DB::table('shopify_orders')
                ->where('rider_id',0)
                // ->where('is_assigned',Auth::user()->id)
                ->where('order_status',null)
                ->where('rider_id','<>',-1)
                ->orWhere('rider_id',null)
                ->count();
        }
        else{
            if(Auth::user()->role < 1){
                return ('Wait for admin verification.');
            }
            else{
                $orders = null;
            }   
        }
        // dd($orders);
        // $orders = ShopifyOrders::where('rider_id',0)
        //          ->orWhere('rider_id',null)->get();
        //dd($orders);
        return view('delivery.notifications',compact('orders'));
    }

    public function searchKartvillageOrder(Request $request){
        return redirect("/order-kartvillage-".$request->order_number);
    }
    public function kartvillage($ordernumber = null){
        
       if($ordernumber != null){
        if(Auth::user()->role == 3 || Auth::user()->role == 1 || Auth::user()->role == 4){
            $orders = DB::table('users')
                ->rightJoin('shopify_orders','shopify_orders.rider_id','users.id')
                ->where('shopify_orders.order_number',$ordernumber)
                ->where('shipping_code',"Free for Pickup (Exclusive for Kartvillage)")
                ->orderBy('shopify_orders.created_at', 'desc')
                ->paginate(1);
                //dd($orders);
        }
        else{
            if(Auth::user()->role < 1){
                return ('Wait for admin verification.');
            }
            else{
                $orders = null;
            }   
        }
       } else{
        if(Auth::user()->role == 3 || Auth::user()->role == 1 || Auth::user()->role == 4){
            $orders = DB::table('users')
                ->rightJoin('shopify_orders','shopify_orders.rider_id','users.id')
                ->where('shipping_code',"Free for Pickup (Exclusive for Kartvillage)")
                ->orderBy('shopify_orders.created_at', 'desc')
                ->paginate(50);
                //dd($orders);
        }
        else{
            if(Auth::user()->role < 1){
                return ('Wait for admin verification.');
            }
            else{
                $orders = null;
            }   
        }
       }
        

        return view('delivery.kartvillage',compact('orders'));
    }
    
    public function countOrders(){
           
        if(Auth::user()->role !=2){
            $count_orders = DB::table('shopify_orders')
                ->where('rider_id',0)
                // ->where('is_assigned',Auth::user()->id)
                ->where('order_status',null)
                ->where('shipping_code','<>',"Free for Pickup (Exclusive for Kartvillage)")
                ->where('rider_id','<>',-1)
                ->orWhere('rider_id',null)
                ->count();
        
        }
        else{
            $count_orders = DB::table('shopify_orders')
                ->where('rider_id',0)
                // ->where('is_assigned',Auth::user()->id)
                ->where('order_status',null)
                ->where('rider_id','<>',-1)
                ->where('shipping_code','<>',"Free for Pickup (Exclusive for Kartvillage)")
                ->orWhere('rider_id',null)
                ->count();
        
        }
            

        return response()->json($count_orders);
    }
    public function acceptedOrders(){

       
        $orders = ShopifyOrders::where('rider_id',Auth::user()->id)->get();
        return view('delivery.accepted',compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function orders($orderId){

        $kartvill_setting = KartvillSettings::find(1)->first();
        $array = array(         
                'format'=> 'json',
        );

            $deliveryCrews = User::where('role',2)
            ->where('availability',1)
            ->get();
        $access_token = env('ACCESS_TOKEN');
        
        $URL = env('URL');
        $func = new FunctionsController;
        //Get from the API
        
         $orders = $func->shopify_call($access_token, 'market-spays.myshopify.com','/admin/api/2022-04/orders/'.$orderId.'.json', $array, 'GET');
        //$orders = $func->shopify_call($access_token, 'market-spays.myshopify.com','/admin/api/2022-04/orders/4695964156061.json', $array, 'GET');
        
        $orders = json_decode($orders['response'], JSON_PRETTY_PRINT);
       
        if($orders['order']['line_items'][0]['vendor'] <> ""){
            $vendor_name = $orders['order']['line_items'][0]['vendor'];
        }
        else{
            foreach($orders['order']['line_items']['0']['properties'] as $order){
                if($order['name'] === '_wk_vendor')
                $vendor_name = $order['value'];
               
            }
        }

        
        if(isset($orders['errors'])){
            return view('errors.404');
        }
        //Get Shipping Address here
                
                if(isset($orders['order']['shipping_address']['address1'])){
                    $customer_id = $orders['order']['customer']['id'];
                    $customer_id = str_replace(' ', '', $customer_id);
                    $customer = Customer::where('customer_id',$customer_id)->first();
                    $customer_address = $orders['order']['shipping_address']['address1'].','.$orders['order']['shipping_address']['city'].','.$orders['order']['shipping_address']['country'];
                }else if(isset($orders['order']['customer']['default_address']['address1'])){
                    $customer_address = $orders['order']['customer']['default_address']['address1'].','.$orders['order']['customer']['default_address']['city'].','.$orders['order']['customer']['default_address']['country'];
                    $customer_id = $orders['order']['customer']['id'];
                    $customer_id = str_replace(' ', '', $customer_id);
                    $customer = Customer::where('customer_id',$customer_id)->first();
                }else if(isset($orders['order']['billing_address']['address1'])){
                    $customer_address = $orders['order']['billing_address']['address1'].','.$orders['order']['billing_address']['city'].','.$orders['order']['billing_address']['country'];
                    $customer_id = $orders['order']['customer']['id'];
                    $customer_id = str_replace(' ', '', $customer_id);
                    $customer = Customer::where('customer_id',$customer_id)->first();
                }
                else{
                    $customer_address = "Robinsons, General Santos City, South Cotabato";
                    $customer = "";
                }
               
                $vendor_name = $func->repair($vendor_name);
                

                $sellers = Sellers::where('store_name',$vendor_name)->get();
            
                //Get the nearest Seller from the customer
                $shortest_distance = 10000000000;
                $vendor_address = "Robinsons, General Santos City";
            
                foreach($sellers as $vendor){
                    if($vendor === null){
                        $vendor_address = "Robinsons, General Santos City";
                    }else{
                        if($vendor->warehouse_address === null){
                            $vendor_address = $vendor->store_address.",".$vendor->city.",".$vendor->zip.",".$vendor->city;
                        }
                        else{
                            $vendor_address = $vendor->warehouse_address;
                        }
                    }    
            
            
            
                    //Destination is the address of the customer
                    //Vendor is the address of the vendor
                  if($vendor_address != "No Address"){ 
                    
                        $data = $func->getDistance($customer_address,$vendor_address);
                        $distance_data = explode("-",$data);
                        $km = $distance_data[0];// KM
                        
                        if($km < $shortest_distance){
                            $vendor_id = $vendor->id;
                            $duration = $distance_data['1'];// Duration
                        }
                    }
                }
                if($vendor_address != "No Address"){
                    $duration_explode = explode(" ",$duration);
                    $duration = $duration_explode[0];
            
            
                    $commission = $kartvill_setting->default_kartvill_commission/100;
                    $seller = Sellers::where('id',$vendor_id)->first();
                    if($seller === null){
                        $store_address = "SM City, General Santos City"; 
                        $commission = $kartvill_setting->default_kartvill_commission/100;
                    }
                    elseif($seller->commission == 0 || $seller->commission === null ){
                        $commission = $kartvill_setting->default_kartvill_commission/100;
                    }
                    else{
                        $commission = floatval($seller->commission)/100;
                    }

                
                    $sub_total = str_replace(",","",$orders['order']['current_subtotal_price']);
                   
                    
                    $merchant_payable = number_format($sub_total / (1 +($commission)),2);
                    
                    $merchant_payable = str_replace(",","",$merchant_payable);
                    
                    
                    $kartvill_commission = number_format($sub_total - $merchant_payable,2);
                    $kartvill_commission = str_replace(",","",$kartvill_commission);
      
                }
                else{
                    $merchant_payable = 0;
                    $seller = "";
                    $kartvill_commission=0;
                    $duration=0;
                    $km = 0;

                }
                $order_id = "".$orders['order']['id'];
                // $pabili_address = DB::table('pabili_orders')->where('order_id',$order_id)->first();
                
                // $pabili_address = PabiliOrder::where('order_id',$order_id)->first();
                $pabili_address = PabiliOrder::where('order_id', $order_id)->first();
                 
                if(isset($pabili_address)){
                    $seller = $pabili_address;
                    $warehouse = "";
                }
                else{
                    if(isset($seller->warehouse_address)){
                        $warehouse = str_replace("+",'%2B',$seller->warehouse_address);
                    }
                    elseif(isset($seller->store_address)){
                        $warehouse = $seller->store_address;
                    }
                    else{
                     $warehouse = "";
                    }
                }
                
        //End of getting vendor address            


        foreach($orders['order']['line_items'] as $product){
            $images = $func->shopify_call($access_token, 'market-spays.myshopify.com','/admin/api/2022-01/products/'.$product['product_id'].'.json', $array, 'GET');

        }
        $images = json_decode($images['response'], JSON_PRETTY_PRINT);
        $order = ShopifyOrders::where('order_id','like','%'.$orderId.'%')->first();
        // dd($orders);
       
        return view('delivery.orders',compact('customer','orders','merchant_payable','seller','kartvill_commission','access_token','array','order','warehouse','duration','km'));
    }


    public function availability(Request $request){
        User::where('id',Auth::user()->id)
            ->update([
                'availability'=>$request->availability
            ]);
            return response(json_encode('successs'));
    }

    public function editDeliveryFee(Request $request){

        $kartvill_setting = KartvillSettings::find(1)->first();
        $delivery_fee = $request->delivery_fee;
        $delivery_commission = $delivery_fee * $kartvill_setting->default_rider_commission;
        $order_id = $request->pabili_order_id;
        $rider_id = Auth::user()->id;
        
        $func = new FunctionsController;
        try{
            
            ShopifyOrders::where('order_id',$order_id)
                    ->update([
                        'delivery_fee'=>$delivery_fee,
                        'delivery_commission'=>$delivery_commission,
                        'rider_id'=>$rider_id,
                    ]);

            
            $customer_order = ShopifyOrders::where('order_id',$order_id)->first();
            $token = $customer_order->token;
            $message="Your updated delivery fee for Pabili is ". number_format($delivery_fee,2) ." Pesos. Please check your order here kartvill.com/account/orders/$token. Happy Day!";
            $customer_phone = Customer::where('customer_id',$customer_order->customer_id)->first();
            $txtbox_api = env('TXTBOX_API_KEY');
            $active=1;
            
            $func->smsTxtBox($message,$customer_phone->phone_number,$txtbox_api,$active);            

        }catch(Exception $e){
            $func->logger($e);
        }

        try{
    
            PabiliOrder::updateOrCreate(
                    ['order_id'=>$order_id],    
                    [
                        // 'shop_address'=>$request->store_address,
                        'latitude'=>$request->seller_pabili_lat,
                        'longitude'=>$request->seller_pabili_long,
                        'rider_id'=>$rider_id,
                    ]);
        }catch(Exception $e){
            $func->logger($e);
        }
               
        return back()->with("status","The Delivery Fee has been updated successfully.");       
    }

    public function acceptReject(Request $request){
        
        $kartvill_setting = KartvillSettings::find(1)->first();
        $array = array(   
            'format'=> 'json',
        );
           $funcw = new FunctionsController;
            $rider_id = Auth::user()->id;
            $access_token = env('ACCESS_TOKEN');
            $activeSMS = 1;
            
            // dd(Auth::user()->availablity);
            // if(Auth::user()->availablity == false){
            //     return redirect('/user-profile-'.Auth::user()->id)->with('error','You are offline you cannot accept any order');
            // }

            //Check if there is already assigned rider
            $check_assigned = ShopifyOrders::where('order_id','like', '%'.$request->order_id.'%')->first();
            
            $customer = $funcw->shopify_call($access_token, 'market-spays.myshopify.com','/admin/api/2022-04/customers/'.$check_assigned->customer_id.'json', $array, 'GET');
            $customer = json_decode($customer['response'], JSON_PRETTY_PRINT);
            $customer_phone = $customer['customer']['default_address']['phone'];
             

            if($customer['customer']['default_address']['phone'] != null){
                $customer_phone = $customer['customer']['default_address']['phone'];
            }
            elseif($customer['customer']['default_address']['phone'] == null){
                if($customer['customer']['addresses'][0]['phone'] != null ){
                    $customer_phone = $customer['customer']['addresses'][0]['phone'];
                }
                else{
                    $customer_phone = $customer['customer']['addresses'][1]['phone'];
                }
            }
            else{
                $response = "Customer did not provide phone number ".$customer['customer']['first_name'];
                $activeSMS = 0;
                $funcw->logger($response);
            }

          
            $duration = $request->duration;

            $rider_wallet = Wallet::where('user_id',$rider_id)->first();
            $message_balance = "";
            $customer_phone=$funcw->repairPhoneNumber($customer_phone);
            $customer_phone = str_replace("+1","+",$customer_phone);
            $txtbox_api = env('TXTBOX_API_KEY');
            $user = User::where('id',$rider_id)->first();
            $updated_at = Carbon::now()->format('Y-m-d H:m:s');
            
            if($check_assigned->rider_id == 0 || ($check_assigned->rider_id == Auth::user()->id && $check_assigned->order_status === null)){
                
               
                if($check_assigned->financial_status == 'paid' && $rider_wallet->balance < $check_assigned->commission){
                    ShopifyOrders::where('order_id','like','%'.$request->order_id.'%')
                    ->update([
                        'order_status'=>'accepted',
                        'rider_id'=>$rider_id,
                        'updated_at'=>$updated_at,
                    ]);
                    
                    
                    return back()->with('status','The order has been already paid. The delivery fee will be transferred to your wallet after a successful delivery.');
                }
                if($rider_wallet->balance < $check_assigned->commission){
                    return back()->with('status','You don\'t have enough wallet balance.');
                }
                elseif($check_assigned->financial_status == 'paid'){
                    ShopifyOrders::where('order_id','like','%'.$request->order_id.'%')
                    ->update([
                        'order_status'=>'accepted',
                        'rider_id'=>$rider_id,
                        'updated_at'=>$updated_at,
                    ]);
                    return back()->with('status','The order has been already paid. The delivery fee will be transferred to your wallet after a successful delivery.');
                }
                
                else{
                ShopifyOrders::where('order_id','like','%'.$request->order_id.'%')
                    ->update([
                        'order_status'=>'accepted',
                        'rider_id'=>$rider_id,
                        'updated_at'=>$updated_at,
                    ]);

                    $estimated_duration = $duration + 25 ;
                    $max_duration = $estimated_duration + 10;
                    $message_balance = "Successfully delivered.";
                    $response = $user->first_name . " accepted the order: " .$check_assigned->order_number;
                    $message = "Our rider $user->first_name has been assigned and will pick up your order. Happy Day!";
                    
                    $funcw->smsTxtBox($message,$customer_phone,$txtbox_api,$activeSMS,$check_assigned->order_number);
                    $funcw->logger($response);
                    $k_commission = $check_assigned->commission + $check_assigned->delivery_commission;
                    $new_balance = $rider_wallet->balance - $k_commission;


                    Wallet::where('user_id', $rider_id)
                    ->update([
                        'balance'=>$new_balance,
                    ]);
                    return back()->with('status','Order Succesfully Accepted. Deduction: ₱'. $k_commission. '. Your new wallet balance is: ₱'.$new_balance);
                    
                    
                }
                
            }
            else{
                
                
                //dd($check_assigned->fulfillment_status);
                    if($check_assigned->fulfillment_status == 'Cancelled'){
                        return back()->with('error','Sorry the customer cancelled the order. If you already accepted the order please check your wallet if you are refunded. Thank you.');
                    }
                
                if($request->status === "picked-up"){
                        ShopifyOrders::where('order_id','like','%'.$request->order_id.'%')
                    ->update([
                        'order_status'=>'picked-up',
                        'updated_at'=>$updated_at,
                    ]);
                    $estimated_duration = $duration + 10;
                    $response = $user->first_name . " picked up the order. Order number: ".$check_assigned->order_number;
                    $message = "Our rider $user->first_name has picked up your order. Estimated delivery is $estimated_duration minutes. Happy day!";
                    //dd($message);
                    $funcw->smsTxtBox($message,$customer_phone,$txtbox_api,$activeSMS,$check_assigned->order_number);
                    $funcw->logger($response);
                    return back()->with('status','Order Picked up.');
                }
                elseif($request->status === "delivered"){
                       // dd($new_balance);
                        ShopifyOrders::where('order_id','like','%'.$request->order_id.'%')
                        ->update([
                        'order_status'=>'delivered',
                        'updated_at'=>$updated_at,
                        ]);
                        $response = $user->first_name . " delivered the order. Order number: ".$check_assigned->order_number;
                        $message = "Rider ". $user->first_name. " successfully delivered Order Number:  ".$check_assigned->order_number;
                        
                        $funcw->logger($response);    
                        $customer = Customer::where('customer_id',$check_assigned->customer_id)->first();
                    
                        if($customer->latitude == null && $customer->longitude == null){
                            Customer::where('customer_id',$check_assigned->customer_id)
                            ->update([
                                'longitude'=>$request->customerLongitude,
                                'latitude'=>$request->customerLatitude
                            ]);
                            
                            
                            $response = "Customer ".$check_assigned->customer_id." coordinates was updated by Rider: ".$user->first_name." via delivered button";
                            $funcw->logger($response);

                            
                        }
                        
                        if($check_assigned->financial_status == 'paid'){
                            $net_delivery_fee = $check_assigned->delivery_fee - $check_assigned->delivery_commission;
                            $new_balance = $rider_wallet->balance + $net_delivery_fee + $kartvill_setting->bonus_delivery_fee;
                            $message_balance = "Successfully delivered. The delivery fee ₱$net_delivery_fee  has been transferred to your wallet";
                            
                            Wallet::where('user_id', $rider_id)
                                ->update([
                                    'balance'=>$new_balance,
                                ]);
                            $funcw->logger("$rider_id: added $kartvill_setting->bonus_delivery_fee order number: $request->order_id");
                            return back()->with('status',$message_balance);
                        }
                        else{
                            $new_balance = $rider_wallet->balance +$kartvill_setting->bonus_delivery_fee;
                            Wallet::where('user_id', $rider_id)
                                ->update([
                                    'balance'=>$new_balance,
                                ]);
                                $funcw->logger("$rider_id: added $kartvill_setting->bonus_delivery_fee order number: $request->order_id");
                            $message_balance = "Successfully delivered";
                            return back()->with('status',$message_balance);
                        }
                    
                    }
                    else{
                        return back();
                    }
                return back();
            } 
            
     

        
        return back();
    }
    

    public function delivered(Request $request, $orderId){
        DeliveryNotifications::where('order_id', $orderId)
            ->update([
                'is_delivered'=>$request->is_delivered,
            ]);

            $query = array(
                "Content-type" => "application/json" // Tell Shopify that we're expecting a response in JSON format
            );
            $func = new FunctionsController;
            $fulfilled = $func->shopify_call(env('ACCESS_TOKEN'), 'market-spays.myshopify.com','/admin/api/2022-04/orders/'.$orderId.'/fulfillments.json',array(),'POST');    
            $fulfilled = json_decode($fulfilled['response'], JSON_PRETTY_PRINT);
            //dd($fulfilled);
        return back();    
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
