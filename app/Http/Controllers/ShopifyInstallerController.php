<?php

namespace App\Http\Controllers;

use App\Models\DeliveryRates;
use App\Models\ShopifyInstaller;
use Illuminate\Http\Request;

class ShopifyInstallerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $shopify = $_GET;
        
        if($shopify == null){
            return redirect('/');
        }
        $check = ShopifyInstaller::where('shop_url', $shopify['shop'] )->first();
        // dd($check);
        
        if($check == null){
            return redirect("/shopify/install?shop=".$shopify['shop']);
            exit();
        }else{
            session(['access_token' => $check->access_token]);
            session(['shop_url' => $check->shop_url]);
            $webhook = new ShopifyWebhooksController;
            $webhook->webhooks('orders/create','/webhook/order-product-webhook','json');
            $webhook->webhooks('orders/paid','/webhook/order-paid-webhook','json');
            $webhook->webhooks('orders/fulfilled','/webhook/fulfill-item','json');
            $webhook->webhooks('orders/partially_fulfilled','/webhook/partial-fulfill-item','json');
            $webhook->webhooks('orders/cancelled','/webhook/cancel-order','json');
            $webhook->webhooks('products/create','/webhook/create-product-webhook','json');
            
            
            
       
           print_r($webhook);
            //dd($rate);
            return view('shopify.index');
        }   
        
        
                
    }
    public function checkShopifyAuth(){
        
       
    }
    public function token()
    {
        // Set variables for our request
        $api_key = env('API_KEY');
        $shared_secret = env('SHARED_SECRET');
        $params = $_GET; // Retrieve all request parameters
        $hmac = $_GET['hmac']; // Retrieve HMAC request parameter

        
        //$webhook = new ShopifyWebhooksController;
        

        $params = array_diff_key($params, array('hmac' => '')); // Remove hmac from params
        ksort($params); // Sort params lexographically

        $computed_hmac = hash_hmac('sha256', http_build_query($params), $shared_secret);

        // Use hmac data to check that the response is from Shopify or not
        if (hash_equals($hmac, $computed_hmac)) {

            // Set variables for our request
            $query = array(
                "client_id" => $api_key, // Your API key
                "client_secret" => $shared_secret, // Your app credentials (secret key)
                "code" => $params['code'] // Grab the access key from the URL
            );

            // Generate access token URL
            $access_token_url = "https://" . $params['shop'] . "/admin/oauth/access_token";

            // Configure curl client and execute request
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $access_token_url);
            curl_setopt($ch, CURLOPT_POST, count($query));
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
            $result = curl_exec($ch);
            curl_close($ch);

             // Store the access token
             $result = json_decode($result, true);
             $access_token = $result['access_token'];

            //Start ... Then the cURL call to create the Carrier Service within the store:
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://market-spays.myshopify.com/admin/api/2022-04/carrier_services.json');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"carrier_service\":
                {
                    \"name\":\"Kartvill Delivery\",
                    \"callback_url\":\"https://kartvillapps.com/rates\",
                    \"service_discovery\":true,
                    \"format\": \"json\"
                }}");

            $headers = array();
            $headers[] = 'X-Shopify-Access-Token:'.$access_token;
            $headers[] = 'Content-Type: application/json';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $err1 = "no error 1";
            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                $err1 = 'Error:' . curl_error($ch);
            }
            curl_close($ch);
            //

            //Start.... Execute the cURL code and you can then execute this next cURL call to see the service listed:
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://market-spays.myshopify.com/admin/carrier_services');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');


            $headers = array();
            $headers[] = 'Accept: application/json';
            $headers[] = 'X-Shopify-Access-Token:'.$access_token;
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $err2='No error 2';
            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                $err2 = 'Error2:' . curl_error($ch);
            }
            curl_close($ch);
            ///

           $error = $err1." - ".$err2;

            // Show the access token (don't do this in production!)
            //echo $access_token;


            ShopifyInstaller::create([
                'shop_url'=>$params['shop'],
                'access_token'=>$access_token,
            ]);

        
        $log = fopen("Error-fulfillment-installation-log" . rand(100,999).".json", "w") or die('Cannot open or create this file');
        
        fwrite($log,$error );
        fclose($log);
           
            //$webhook->webhooks('app/uninstalled','/app-uninstall-webhook/'.$params['shop'],'json');
            return redirect("https://".$params['shop'] . "/admin/apps/kartvill-app");

    }
    }
    public function install()
    {
        $shop = $_GET['shop'];
        $api_key = env('API_KEY');
        $scopes = env('SCOPES');
        $redirect_uri = env('URL').'/shopify/token';

        // Build install/approval URL to redirect to
        $install_url = "https://" . $shop . "/admin/oauth/authorize?client_id=" . $api_key . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);

        // Redirect
        return redirect($install_url);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ShopifyInstaller  $shopifyInstaller
     * @return \Illuminate\Http\Response
     */
    public function show(ShopifyInstaller $shopifyInstaller)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ShopifyInstaller  $shopifyInstaller
     * @return \Illuminate\Http\Response
     */
    public function edit(ShopifyInstaller $shopifyInstaller)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ShopifyInstaller  $shopifyInstaller
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ShopifyInstaller $shopifyInstaller)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ShopifyInstaller  $shopifyInstaller
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShopifyInstaller $shopifyInstaller)
    {
        //
    }
}
