<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Customer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{

    public function customers($keyword = null){
        if($keyword == null){
            $customers = Customer::all();
        }
        else{
           
            $customers = Customer::where('id','LIKE','%'.$keyword.'%')
                        ->orWhere('full_name','LIKE','%'.$keyword.'%')
                        ->orWhere('email_address','LIKE','%'.$keyword.'%')
                        ->get();
            // if($customers == null){
            //     return view('errors.404');
            // }
        }
       
        return view('customer.customers',compact('customers'));
    }

    public function customerUpdate($customerid){
        $customer = Customer::where('id',$customerid)->first();
        return view('customer.edit-customer',compact('customer'));
    }
    
    public function update(Request $request, $customerid){
        $fullname = ucfirst($request->first_name)." ".ucfirst($request->last_name);
        // dd($fullname);
        Customer::where('id',$customerid)
        ->update([
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'full_name'=>$fullname,
            'phone_number'=>$request->phone_number,
            'email_address'=>$request->email,
            'address'=>$request->address
            
        ]);
        return back();
    }


    public function updateCreateCustomerCoordinates(Request $request){
        
          Customer::updateOrCreate(
            ['customer_id' => $request->customer_id],
            [
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'email_address'=>$request->email_address,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude
            ]);
        
         return response()->json('success');
        
                        
    }

    public function getCurrentCoords($customer_id=null){
        
       $customer = Customer::where('customer_id',$customer_id)->first(); 
      if($customer == null || $customer_id == null  || $customer_id == ""){
        return view('errors.404');
      }
        return response()->json($customer);
      
                      
  }

    public function customerSearch(Request $request){
        if($request->input == null){
            return redirect('customers');
        }
        return redirect('customers-'.$request->input);
    }
            

    public function customer_map($customer_id){
        $customer = Customer::where('customer_id',$customer_id)->first();
        if($customer == null){
            return view('errors.404');
        }
        return view('customer.customer-pin',compact('customer'));
    }

    public function customer_map_update(Request $request){
        Customer::where('customer_id',$request->customer_id)
        ->update([
            'latitude'=>$request->latitude,
            'longitude'=>$request->longitude
            
        ]);
       
         return back();
    }

    public function updateMap($customerID){


        $funcw = new FunctionsController;
        $txtboxAPI = env('TXTBOX_API_KEY');

        try{
            $customer = Customer::where('customer_id',$customerID)->first();
            //dd($customer);
            $customer_phone = $customer->phone_number;
            $customer_phone = $funcw->repairPhoneNumber($customer_phone);
            $customer_phone = str_replace("+1","+",$customer_phone);
            $message = "Happy day! Please update your Map here: www.kartvillapps.com/user-pin-$customerID";
            $funcw->smsTxtBox($message,$customer_phone,$txtboxAPI,1);
            $user = User::where('id',Auth::user()->id)->first();
            $log = $user->first_name." ".$user->last_name." triggered the send update map button to $customerID";
            $funcw->logger($log);
            // dd($message);
            return back();
        }catch(Exception $e){
            $funcw->logger($e);
        }
       
    }

}
