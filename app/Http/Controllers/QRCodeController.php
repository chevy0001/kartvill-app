<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class QRCodeController extends Controller
{
    public function index(){   
        if(Session::get('qr_url') != null){
            $url =  Session::get('qr_url'); 
            $qr =  Session::get('qr_qr'); 
        }else{
            $url = "kartvill.com";
            $qr = QrCode::size(300)->generate($url);
        }
        
        return view('qr.qrcode',compact('url','qr'));
    }

    public function qrcode(Request $request){   
            $url = $request->url;
            $qr = QrCode::size(300)->generate($url);

            session([
                    'qr_url' => $url,
                    'qr_qr'=>$qr
                ]);

            return redirect('/qrcode');

    }

}
