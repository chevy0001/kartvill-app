<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Sellers;
use Illuminate\Http\Request;
use App\Models\DeliveryRates;
use App\Models\FareSetting;
use App\Models\KartvillSettings;
use Illuminate\Support\Facades\DB;

class KartvillSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $commission = KartvillSettings::all()->first();

        $rate = DeliveryRates::select("*")->first();

        //dd($commission);
        return view('admin.settings.index',compact('commission','rate'));
    }

    public function updateCommission(Request $request){
        $flight = KartvillSettings::updateOrCreate(
            ['id' => $request->id],
            ['commission' => $request->commission]
        );


        DB::table('kartvill_settings')->update(['commission'=>$request->commission]);
        return back();
    }
    
    //Ouput Searched Sellers
    public function sellers($value=null){
        $func  = new FunctionsController;
        
        $value = $func->repair($value);
        //$value = $func->repairName2($value);
        if($value == null){
            $sellers = Sellers::paginate(100);
        }
        else{
            $sellers = Sellers::where('seller_id','LIKE',"%{$value}%")
                ->orWhere('store_name','LIKE',"%{$value}%")
                ->orWhere('seller_name','LIKE',"%{$value}%")
                ->orWhere('seller_email','LIKE',"%{$value}%")
                ->orWhere('city','LIKE',"%{$value}%")
                ->orWhere('province_code','LIKE',"%{$value}%")
                ->paginate(100);
        }
        
       
        return view('vendor.sellers',compact('sellers'));
    }

    public function sellerSearch(Request $request){
        if($request->input == null){
            return redirect('sellers');
        }
       return redirect('sellers-'.$request->input);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function saveSettings(Request $request){
        KartvillSettings::where('id',1)
                    ->update([
                        'default_kartvill_commission'=>$request->default_kartvill_commission,
                        'default_rider_commission'=>$request->default_rider_commission,
                        'delivery_rate'=>$request->delivery_rate,
                        'additional_delivery_rate'=>$request->additional_delivery_rate,
                        'minimum_kilometer'=>$request->minimum_kilometer,
                        'minimum_kilometer_rate'=>$request->minimum_kilometer_rate,
                        'bonus_delivery_fee'=>$request->bonus_delivery_fee,
                    ]);

        return response()->json("success");
    }

    public function fareSettings(){
        //query the cities

        $cities = City::all();
        return view('admin.settings.fare',compact('cities'));
    }

    public function saveFareSettings(Request $request){

         FareSetting::updateOrCreate(
            [
                'city_id'=>$request->city_id,
                'trans_mode_id'=>$request->trans_mode_id,
                'vehicle_type_id'=>$request->vehicle_type_id,   
            ],
            [
                'min_fare'=>$request->min_fare,
                'min_distance'=>$request->min_distance,
                'fare_per_km'=>$request->fare_per_km,
            ]);

            return back();
    
    }

    //return all cities via api
    public function getCities(){

        $cities = DB::table('cities')
       ->join('fare_settings','cities.id','=','fare_settings.city_id')
       ->groupBy('city')
       ->get();

        // $cities = City::all();
        return response()->json(['cities'=>$cities]);
    }


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\KartvillSettings  $kartvillSettings
     * @return \Illuminate\Http\Response
     */
    public function show(KartvillSettings $kartvillSettings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\KartvillSettings  $kartvillSettings
     * @return \Illuminate\Http\Response
     */
    public function edit(KartvillSettings $kartvillSettings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\KartvillSettings  $kartvillSettings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KartvillSettings $kartvillSettings)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\KartvillSettings  $kartvillSettings
     * @return \Illuminate\Http\Response
     */
    public function destroy(KartvillSettings $kartvillSettings)
    {
        //
    }
}
