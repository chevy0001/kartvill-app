<?php

namespace App\Http\Controllers;

use DateTime;
use Carbon\Carbon;
use App\Models\Logs;
use App\Models\User;
use App\Models\Sellers;
use App\Models\CityCode;
use App\Models\FareSetting;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use PHPMailer\PHPMailer\SMTP;
use Illuminate\Support\Facades\DB;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

class FunctionsController extends Controller
{
    
    public function shopify_call($token, $shop, $api_endpoint, $query = array(), $method = 'GET', $request_headers = array()) 
    {
    
            // Build URL
            $url = "https://" . $shop . $api_endpoint;
            if (!is_null($query) && in_array($method, array('GET', 	'DELETE'))) $url = $url . "?" . http_build_query($query);
        
            // Configure cURL
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_HEADER, TRUE);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($curl, CURLOPT_MAXREDIRS, 3);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            // curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 3);
            // curl_setopt($curl, CURLOPT_SSLVERSION, 3);
            curl_setopt($curl, CURLOPT_USERAGENT, 'My New Shopify App v.1');
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($curl, CURLOPT_TIMEOUT, 30);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        
            // Setup headers
            $request_headers[] = "";
            if (!is_null($token)) $request_headers[] = "X-Shopify-Access-Token: " . $token;
            curl_setopt($curl, CURLOPT_HTTPHEADER, $request_headers);
        
            if ($method != 'GET' && in_array($method, array('POST', 'PUT'))) {
                if (is_array($query)) $query = http_build_query($query);
                curl_setopt ($curl, CURLOPT_POSTFIELDS, $query);
            }
            
            // Send request to Shopify and capture any errors
            $response = curl_exec($curl);
            $error_number = curl_errno($curl);
            $error_message = curl_error($curl);
        
            // Close cURL to be nice
            curl_close($curl);
        
            // Return an error is cURL has a problem
            if ($error_number) 
            {

                return $error_message;
            } else 
            {
        
                // No error, return Shopify's response by parsing out the body and the headers
                $response = preg_split("/\r\n\r\n|\n\n|\r\r/", $response, 2);
        
                // Convert headers into an array
                $headers = array();
                $header_data = explode("\n",$response[0]);
                $headers['status'] = $header_data[0]; // Does not contain a key, have to explicitly set
                array_shift($header_data); // Remove status, we've already set it above
                foreach($header_data as $part) {
                    $h = explode(":", $part);
                    $headers[trim($h[0])] = trim($h[1]);
                }
        
                // Return headers and Shopify's response
                return array('headers' => $headers, 'response' => $response[1]);
        
            
            
            }
    }

    public static function shopify_call2($token, $shop, $api_endpoint, $query = array(), $method = 'GET', $request_headers = array()) 
    {
    
            // Build URL
            $url = "https://" . $shop . $api_endpoint;
            if (!is_null($query) && in_array($method, array('GET', 	'DELETE'))) $url = $url . "?" . http_build_query($query);
        
            // Configure cURL
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_HEADER, TRUE);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($curl, CURLOPT_MAXREDIRS, 3);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            // curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 3);
            // curl_setopt($curl, CURLOPT_SSLVERSION, 3);
            curl_setopt($curl, CURLOPT_USERAGENT, 'My New Shopify App v.1');
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($curl, CURLOPT_TIMEOUT, 30);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        
            // Setup headers
            $request_headers[] = "";
            if (!is_null($token)) $request_headers[] = "X-Shopify-Access-Token: " . $token;
            curl_setopt($curl, CURLOPT_HTTPHEADER, $request_headers);
        
            if ($method != 'GET' && in_array($method, array('POST', 'PUT'))) {
                if (is_array($query)) $query = http_build_query($query);
                curl_setopt ($curl, CURLOPT_POSTFIELDS, $query);
            }
            
            // Send request to Shopify and capture any errors
            $response = curl_exec($curl);
            $error_number = curl_errno($curl);
            $error_message = curl_error($curl);
        
            // Close cURL to be nice
            curl_close($curl);
        
            // Return an error is cURL has a problem
            if ($error_number) 
            {

                return $error_message;
            } else 
            {
        
                // No error, return Shopify's response by parsing out the body and the headers
                $response = preg_split("/\r\n\r\n|\n\n|\r\r/", $response, 2);
        
                // Convert headers into an array
                $headers = array();
                $header_data = explode("\n",$response[0]);
                $headers['status'] = $header_data[0]; // Does not contain a key, have to explicitly set
                array_shift($header_data); // Remove status, we've already set it above
                foreach($header_data as $part) {
                    $h = explode(":", $part);
                    $headers[trim($h[0])] = trim($h[1]);
                }
        
                // Return headers and Shopify's response
                return array('headers' => $headers, 'response' => $response[1]);
        
            
            
            }
    }

    

    public function repairName($name){
        
        $err_char='\u00e2\u0080\u0099';
        
        $name = str_replace($err_char,"'",$name);
        
        return $name;
     }
     public function repairName2($name){
        $err_char='’';
        $name = str_replace($err_char,"'",$name);
        return $name;
     }
   
    public function repairName4($name){
        $err_char4='\u00c3\u00a9';
        $name = str_replace($err_char4,"é",$name);
        return $name;
     }
     public function repairName5($name){
        $err_char4='\u00c9';
        $name = str_replace($err_char4,"É",$name);
        return $name;
     }
     public function repairName6($name){
        $err_char2='\u00d1';
        $name = str_replace($err_char2,"Ñ",$name);
        return $name;
    }
     public function repairName3($name){
        $err_char2='\u00';
        $name = str_replace($err_char2,"ñ",$name);
        return $name;
    }
    public function repairName7($name){
        $err_char2='&#039';
        $name = str_replace($err_char2,"ñ",$name);
        return $name;
    }

    

    public function repair($name){
        $vendor_name = new FunctionsController;
        $name = $vendor_name->repairName($name);
        $name = $vendor_name->repairName7($name);   
        $name = $vendor_name->repairName2($name);
        $name = $vendor_name->repairName4($name);
        $name = $vendor_name->repairName5($name);
        $name = $vendor_name->repairName6($name);
        $name = $vendor_name->repairName3($name);
        return $name;
    }
    
    
    function validateDate($date, $format = 'Y-m-d'){
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    } 
    
     public function repairPhoneNumber($phone){
        
        $func = new FunctionsController;
        $phone = str_replace(' ','',$phone);
        
        if($phone[0] == '9'){
            $phone = '0'.$phone;
        }

        $func->logger($phone);
        return $phone;

     }

     public function smsTxtBox($message,$phone,$txtbox_api,$active,$ordernumber=null){
        $log = new FunctionsController;
        if($active == 0){
                $res = "The admin turn the sending off or phone error: ".$phone." Order Number: ".$ordernumber;
                $log->logger($res);
        }
        else{
                $curl = curl_init();

                curl_setopt_array($curl, array(
                CURLOPT_URL => "https://ws-v2.txtbox.com/messaging/v1/sms/push",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => false,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => array('message' => $message,'number' => $phone),
                CURLOPT_HTTPHEADER => array(
                "X-TXTBOX-Auth: $txtbox_api"
                ),
            ));
    
                $response_sms = curl_exec($curl);
                $err = curl_error($curl);
        
                curl_close($curl);
        
                if ($err) {
                    $res = "cURL Error #:" . $err." Order Number: ".$ordernumber;
                } else {
                    $res = $response_sms." Order Number: ".$ordernumber;
                }

                $log->logger($res);
            }
            return $res;
        }


public function getDistance($origin,$destination){
    // Google API key
    //$origin = customer address, $destination = vendor address
    $apiKey = env('GOOGLE_MAP');
    $func = new FunctionsController;
            //https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=United Evangelical Church of General Santos, 459F+7HH, Corner Donato Quinto Street, Sampaguita St, 9500 General Santos City, SCO, Philippines&destinations=Sampaguita St, General Santos City, South Cotabato&key=AIzaSyBw6yJJRuziDaIBcadBZHIEHS0vAbNzlqU
    
            $origin =urlencode($origin);
            $destination = urlencode($destination);
            // $url_coords  = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=6.1274719,125.147528&destinations=6.1077604,125.2047538&key=AIzaSyBw6yJJRuziDaIBcadBZHIEHS0vAbNzlqU";
            $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=$origin&destinations=$destination&mode=driving&key=$apiKey";
            $data = @file_get_contents($url);
            $data = json_decode($data,true);
    
            if(isset($data['rows'][0]['elements'][0]['distance']['text'])){
                $miles = $data['rows'][0]['elements'][0]['distance']['text'];// It returns KM now
                $kmvalue = $data['rows'][0]['elements'][0]['distance']['value'];
                $duration = $data['rows'][0]['elements'][0]['duration']['text'];
            // $func->logger($miles);
                $km =  (float)$miles * 1;
                //$func->logger(json_encode($data));
                // dd($km.'-'.$duration.'-'.$kmvalue);
                return $km.'-'.$duration.'-'.$kmvalue;
            }else{
                $km = "12";
                $duration = "30 mins";
                $kmvalue = 11200;
                // dd($km.'-'.$duration.'-'.$kmvalue);
                return $km.'-'.$duration.'-'.$kmvalue; 
            }
            
                // Change address format
            //     $formattedAddrFrom    = str_replace(' ', '+', $addressFrom);
            //     $formattedAddrTo     = str_replace(' ', '+', $addressTo);
                
            //     // Geocoding API request with start address
            //     $geocodeFrom = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddrFrom.'&sensor=false&key='.$apiKey);
            //     $outputFrom = json_decode($geocodeFrom);
            //     if(!empty($outputFrom->error_message)){
            //         return $outputFrom->error_message;
            //     }
                
            //     // Geocoding API request with end address
            //     $geocodeTo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddrTo.'&sensor=false&key='.$apiKey);
            //     $outputTo = json_decode($geocodeTo);
            //     if(!empty($outputTo->error_message)){
            //         return $outputTo->error_message;
            //     }
                
            //     // Get latitude and longitude from the geodata
            //     $latitudeFrom    = $outputFrom->results[0]->geometry->location->lat;
            //     $longitudeFrom    = $outputFrom->results[0]->geometry->location->lng;
            //     $latitudeTo        = $outputTo->results[0]->geometry->location->lat;
            //     $longitudeTo    = $outputTo->results[0]->geometry->location->lng;
                
            //     // Calculate distance between latitude and longitude
            //     $theta    = $longitudeFrom - $longitudeTo;
            //     $dist    = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) +  cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
            //     $dist    = acos($dist);
            //     $dist    = rad2deg($dist);
            //     $miles    = $dist * 60 * 1.1515;
                
            //     // Convert unit and return distance
            //     $unit = strtoupper($unit);
            //     if($unit == "K"){
            //         return round($miles * 1.609344, 2).' km';
            //     }elseif($unit == "M"){
            //         return round($miles * 1609.344, 2).' meters';
            //     }else{
            //         return round($miles, 2).' miles';
            //     }
}

public function getDeliveryRate(Request $request){
    // Google API key

            $apiKey = env('GOOGLE_MAP');
            $func = new FunctionsController;
            //https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=United Evangelical Church of General Santos, 459F+7HH, Corner Donato Quinto Street, Sampaguita St, 9500 General Santos City, SCO, Philippines&destinations=Sampaguita St, General Santos City, South Cotabato&key=AIzaSyBw6yJJRuziDaIBcadBZHIEHS0vAbNzlqU
    
            $origin =$request->store_coords;
            $destination = $request->customer_coords;
            // $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=$origin&destinations=$destination&mode=driving&key=$apiKey";
            $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=$origin&destinations=$destination&key=$apiKey";
            
            $data = @file_get_contents($url);
            $data = json_decode($data,true);

            // $dd = $origin." ---- ".$destination;
            // dd($dd);
            //return $data['rows'][0]['elements'][0]['duration']['value'];
            $miles = $data['rows'][0]['elements'][0]['distance']['text'];// It returns KM now
            $kmvalue = $data['rows'][0]['elements'][0]['distance']['value'];
            $duration = $data['rows'][0]['elements'][0]['duration']['text'];
           // $func->logger($miles);
            $km =  (float)$miles * 1;
            //$func->logger(json_encode($data));
            return response()->json(['km'=>$km, 'duration'=>$duration,'kmvalue'=>$kmvalue]);
            
}
public function getFare(Request $request){
    // Google API key
          try{  
            $apiKey = env('GOOGLE_MAP');
            $func = new FunctionsController;
            //https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=United Evangelical Church of General Santos, 459F+7HH, Corner Donato Quinto Street, Sampaguita St, 9500 General Santos City, SCO, Philippines&destinations=Sampaguita St, General Santos City, South Cotabato&key=AIzaSyBw6yJJRuziDaIBcadBZHIEHS0vAbNzlqU
    
            $origin =$request->origin;
            $destination = $request->destination;
            $city_id=$request->city_id;
            $transport_mode=$request->transport_mode;
            $vehicle_type=$request->vehicle_type;
            $success = true;
            try{
                $fare_setting = FareSetting::where('city_id',$city_id)
                            ->where('trans_mode_id',$transport_mode)
                            ->where('vehicle_type_id',$vehicle_type)
                            ->first();
            }catch(Exception $e){
                $success=false;
            }
            

            // $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=$origin&destinations=$destination&mode=driving&key=$apiKey";
            $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=$origin&destinations=$destination&key=$apiKey";
            
            $data = @file_get_contents($url);
            $data = json_decode($data,true);

            // $dd = $origin." ---- ".$destination;
            // dd($dd);
            //return $data['rows'][0]['elements'][0]['duration']['value'];
            $miles = $data['rows'][0]['elements'][0]['distance']['text'];// It returns KM now
            $kmvalue = $data['rows'][0]['elements'][0]['distance']['value'];
            $duration = $data['rows'][0]['elements'][0]['duration']['text'];
           // $func->logger($miles);
            $km =  (float)$miles * 1;
            //$func->logger(json_encode($data));
            if($success){
                return response()->json([
                    'km'=>$km, 
                    'duration'=>$duration,
                    'kmvalue'=>$kmvalue,
                    'min_fare'=>$fare_setting->min_fare,
                    'min_distance'=>$fare_setting->min_distance,
                    'fare_per_km'=>$fare_setting->fare_per_km
                ]);
            }else{
                return response()->json(['error'=>'No data available for the current city'] );
            }
           
            
            
        }catch(Exception $e){
            return response()->json($e);
          }
}

public function detectNearestRider($vendor_name, $customer_address){

    $func = new FunctionsController;
    $shortest_distance = 10000000000;
    
    $vendors= Sellers::where('store_name',$vendor_name)->get();

    foreach($vendors as $vendor){
        if($vendor === null){
            $vendor_address = "Robinsons, General Santos City";
        }else{
            if($vendor->warehouse_address === null){
                $vendor_address = $vendor->store_address.",".$vendor->city.",".$vendor->zip.",".$vendor->city;
            }
            else{
                $vendor_address = $vendor->warehouse_address;
            }
        }    



        //Destination is the customer
        //Vendor is the address of the vendor
        $km = $func->getDistance($customer_address,$vendor_address);

        if($km < $shortest_distance){
            $shortest_distance = $km;
            $vendor_province_code = $vendor->province_code;
            $vendor_lat = $vendor->latitude;
            $vendor_long = $vendor->longitude;

        }

    }


    $riders = User::where('availability',1)
            ->where('latitude','<>',null)
            ->where('province_code',$vendor_province_code)    
            ->get();

    $vendor_coords = 0;
    if($vendor_lat == null){
        return "vendor no coordinates";
    }
    else{
        $vendor_coords = $vendor_lat.','.$vendor_long;
    }

    $origin = $vendor_lat.','.$vendor_long;
    $rider_id = 0;
    $temp_distance = 1000000000000;
    $ch = curl_init();
    foreach($riders as $rider){
        $destination = $rider->latitude.','.$rider->longitude;

        //   $origin = '6.1149698,125.1699776';
        //   $destination = '6.116070100000001,125.1792207';

        curl_setopt($ch, CURLOPT_URL, 'https://maps.googleapis.com/maps/api/distancematrix/json?origins='.$origin.'&destinations='.$destination.'&mode=driving&key='.env('GOOGLE_MAP').'');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);
        $result = json_decode($result);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
       

        //$rider_id = $rider->id;
        $km =  $result->rows[0]->elements[0]->distance->value/1000;
        
        
        if($km < $temp_distance){
            $temp_distance = $km;
            $rider_id = $rider->id;
            
            //Detect riders within x Km
            // if($km > 5){
            //     $rider_id = 0;
            // }
            // else{
            //     $rider_id = $rider->id;    
            // }
            
        
        }

    }
    curl_close($ch);
    



        // $data['rows'][0]['elements'][0]['distance']['text'];
    //return $result['rows'][0]['elements'][0]['distance']['text'];
    return  $rider_id;


}

public function vat($seller_commission,$price){
   
    if($seller_commission == null){
        $commission_setting = 0.20;
    }
    else{
        $commission_setting = $seller_commission/100;
    }
    $commission = ($price * $commission_setting);
    $vat = ($commission / 1000) * 20;
    $vat2  = $vat;
    if($vat < 1){
        $vat = 1;
    }
    else{
        $vat = $vat;
    }
    return $vat;
}


    public function getRegion($vendor_region, $customer_region ){

        if($vendor_region === $customer_region ){
            return 'mindanao';
        }
        elseif($vendor_region == 'mindanao' && $customer_region == 'vis'){
            return 'visayas';
        }
        elseif($vendor_region == 'mindanao' && $customer_region == 'luzon'){
            return 'luzon';
        }
        elseif($vendor_region == 'mindanao' && $customer_region == 'manila'){
            return 'manila';
        }
        elseif($vendor_region == 'mindanao' && $customer_region == 'island'){
            return 'island';
        }
        elseif($vendor_region == 'visayas' && $customer_region == 'mindanao'){
            return 'visayas';
        }
        elseif($vendor_region == 'visayas' && $customer_region == 'luzon'){
            return 'visayas';
        }
        elseif($vendor_region == 'visayas' && $customer_region == 'manila'){
            return 'visayas';
        }
        elseif($vendor_region == 'visayas' && $customer_region == 'island'){
            return 'island';
        }
        elseif($vendor_region == 'manila' && $customer_region == 'mindanao'){
            return 'manila';
        }
        elseif($vendor_region == 'manila' && $customer_region == 'visayas'){
            return 'visayas';
        }
        elseif($vendor_region == 'manila' && $customer_region == 'manila'){
            return 'manila';
        }
        elseif($vendor_region == 'manila' && $customer_region == 'luzon'){
            return 'manila';
        }
        elseif($vendor_region == 'manila' && $customer_region == 'island'){
            return 'island';
        }
        elseif($vendor_region == 'luzon' && $customer_region == 'mindanao'){
            return 'luzon';
        }
        elseif($vendor_region == 'luzon' && $customer_region == 'visayas'){
            return 'visayas';
        }
        elseif($vendor_region == 'luzon' && $customer_region == 'manila'){
            return 'luzon';
        }
        elseif($vendor_region == 'luzon' && $customer_region == 'island'){
            return 'island';
        }
        elseif($vendor_region == 'island' && $customer_region == 'mindanao'){
            return 'island';
        }
        elseif($vendor_region == 'island' && $customer_region == 'visayas'){
            return 'island';
        }
        elseif($vendor_region == 'island' && $customer_region == 'luzon'){
            return 'island';
        }
        elseif($vendor_region == 'island' && $customer_region == 'manila'){
            return 'island';
        }
        else{
            return 'island';
        }


    }


    public function logger($logs){
        Logs::create([
            'logs'=>$logs,
        ]);

        return response(json_encode('payts',200));
    }   
    
    public static function customerDetails($customer_id){
            $array = array(   
                'format'=> 'json',
            );
            $access_token = env('ACCESS_TOKEN');
            $funcw = new FunctionsController();
            $customer = $funcw->shopify_call($access_token, 'market-spays.myshopify.com','/admin/api/2022-04/customers/'.$customer_id.'json', $array, 'GET');
            $customer = json_decode($customer['response'], JSON_PRETTY_PRINT);
            return $customer;
    }

    public static function orderDetails($order_id){
        $array = array(   
            'format'=> 'json',
        );
        $access_token = env('ACCESS_TOKEN');
        $func = new FunctionsController();
        $order = $func->shopify_call($access_token, 'market-spays.myshopify.com','/admin/api/2022-04/orders/'.$order_id.'/fulfillments.json',array(),'POST');    
        $order = json_decode($order['response'], JSON_PRETTY_PRINT);
        return $order;
    }

    public function sendEmail($useremail,$full_name,$msg,$subject){
        $funcw = new FunctionsController;
        $mail = new PHPMailer(true);
        try {
            //Server settings
            $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
            $mail->isSMTP();                                            //Send using SMTP
            $mail->Host       = env('PHP_HOST');                     //Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
            $mail->Username   = env('PHP_USERNAME');                     //SMTP username
            $mail->Password   = env('PHP_PASSWORD');                               //SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
            $mail->Port       = env('PHP_PORT');                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`
        
            //Recipients
            
            $mail->setFrom('no-reply@kartvillapps.com', 'Kartvill');
            $mail->addAddress($useremail, $full_name);     //Add a recipient
                                
            //Content
            $mail->isHTML(true);                                  //Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body    = $msg;
        
            $mail->send();
            
            $funcw->logger('Message has been sent to '.$useremail);
            return true;
        } catch (Exception $e) {
            $funcw->logger("Message could not be sent. Mailer Error: {$mail->ErrorInfo}");
            return false;
         }
    }

    public function resetEmail($useremail,$full_name,$tmppassword){
                    

        $funcw = new FunctionsController;
        $mail = new PHPMailer(true);
        $profile = User::where('email',$useremail)->first();
                    //  dd($profile);
        if($profile == null){
            return back()->with('error','Cannot find your account please signup.');
            $funcw->logger('No profile. From reset Password.'.$useremail);
        }else{

                $created_at = Carbon::now()->format('Y-m-d H:m:s');
                $reset_token = Str::random(60);

                DB::table('password_resets')->insert([
                    'email'=>$useremail,
                    'token'=>$reset_token,
                    'created_at'=>$created_at
                ]);
                    // the message
                    $msg = "Your temporary password is $tmppassword. Reset your password here: https://kartvillapps.com/resetpassword-$reset_token";
                    $msg = wordwrap($msg,70);
                    try {
                        //Server settings
                        $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
                        $mail->isSMTP();                                            //Send using SMTP
                        $mail->Host       = env('PHP_HOST');                     //Set the SMTP server to send through
                        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
                        $mail->Username   = env('PHP_USERNAME');                     //SMTP username
                        $mail->Password   = env('PHP_PASSWORD');                               //SMTP password
                        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
                        $mail->Port       = env('PHP_PORT');                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`
                    
                        //Recipients
                        
                        $mail->setFrom('no-reply@kartvillapps.com', 'Kartvill');
                        $mail->addAddress($useremail, $full_name);     //Add a recipient
                                            
                        //Content
                        $mail->isHTML(true);                                  //Set email format to HTML
                        $mail->Subject = 'Reset Password';
                        $mail->Body    = $msg;
                    
                        $mail->send();
                        
                        $funcw->logger('Message has been sent to '.$useremail);
                    } catch (Exception $e) {
                        $funcw->logger("Message could not be sent. Mailer Error: {$mail->ErrorInfo}");
                    
                     }
        }       
    }

}
