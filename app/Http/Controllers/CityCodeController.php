<?php

namespace App\Http\Controllers;

use App\Models\CityCode;
use Illuminate\Http\Request;

class CityCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CityCode  $cityCode
     * @return \Illuminate\Http\Response
     */
    public function show(CityCode $cityCode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CityCode  $cityCode
     * @return \Illuminate\Http\Response
     */
    public function edit(CityCode $cityCode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CityCode  $cityCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CityCode $cityCode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CityCode  $cityCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(CityCode $cityCode)
    {
        //
    }
}
