<?php

namespace App\Http\Controllers;

use App\Models\Wallet;
use Illuminate\Http\Request;
use App\Models\ShopifyOrders;

class ShopifyOrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function countNewOrder(){
        $newOrderCount = ShopifyOrders::where('is_open',0)->count();
        
        
        if($newOrderCount == 0)
        {
            $newOrderCount = '';
        }
        return view('admin.count-orders',compact('newOrderCount'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function cancelOrder(Request $request){
       // dd($request->order_number);
        $order_number = $request->order_number;
        $funcw = new FunctionsController;
        $order = ShopifyOrders::where('order_number',$request->order_number)->first();

            if($order == null){
                $funcw->logger('Cancel Order: '. $order_number.' This order has not saved in the database, please check the order create webhook');
            }
            else{
                if($order->financial_status <> "paid" && $order->rider_id <> 0){
                    $wallet = Wallet::where('user_id',$order->rider_id)->first();
                    $refund = $order->commission + $wallet->balance;
                    Wallet::where('user_id',$order->rider_id)
                    ->update([
                        'balance'=>$refund
                    ]);

                    $cancelled_orders = ShopifyOrders::where('order_number',$order_number)
                    ->update([
                        'fulfillment_status'=>'Cancelled',
                        'financial_status'=>'Refunded'
                    ]);
                    $funcw->logger($cancelled_orders);  

                    $funcw->logger('Cancel Order: '.$order_number."Rider ID: $order->rider_id Refunded ");
                }
                else{
                     $cancelled_orders = ShopifyOrders::where('order_number',$order_number)
                    ->update([
                        'fulfillment_status'=>'Cancelled',
                        'financial_status'=>'Refunded'
                    ]);
                    $funcw->logger($cancelled_orders);  
                    $funcw->logger('Cancel Order: '.$order_number." No Refund since the rider has not deducted for commission");
                }
            }
            return back();
    } 
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ShopifyOrders  $shopifyOrders
     * @return \Illuminate\Http\Response
     */
    public function show(ShopifyOrders $shopifyOrders)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ShopifyOrders  $shopifyOrders
     * @return \Illuminate\Http\Response
     */
    public function edit(ShopifyOrders $shopifyOrders)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ShopifyOrders  $shopifyOrders
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ShopifyOrders $shopifyOrders)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ShopifyOrders  $shopifyOrders
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShopifyOrders $shopifyOrders)
    {
        //
    }
}
