<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Wallet;
use App\Models\Sellers;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Models\ShopifyOrders;
use PHPMailer\PHPMailer\SMTP;
use App\Models\CustomerWallet;
use PHPMailer\PHPMailer\PHPMailer;
use App\Models\CustomerPointHistory;
use Illuminate\Foundation\Auth\User;

class APITestController extends Controller
{
    public function readOrderApi(){
        $path = storage_path() . "/app/json/orders.json"; // ie: /var/www/laravel/app/storage/json/filename.json
        $data_json = json_decode(file_get_contents($path), true); 
        
        $funcw = new FunctionsController;
        $func = new ShopifyWebhooksController;
        $log = new FunctionsController;
        $txtbox_api = env('TXTBOX_API_KEY');

        
        
        
        $log->logger(json_encode($data_json));
        
        /* Start of Store Name */      
            $store_name = "";
            if($data_json['line_items'][0]['vendor'] != ""){
                $store_name = $data_json['line_items'][0]['vendor'];
            }
            else{
                foreach($data_json['line_items']['0']['properties'] as $order){
                    if($order['name'] === '_wk_vendor')
                    $store_name = $order['value'];
                
                }
            }

            $store_name = $funcw->repair(json_encode($store_name));
            $store_name = json_decode($store_name);
        /* End of Store Name */

        /* Start Get commission for every store here */
            $seller = Sellers::where('store_name',$store_name)->first();
            if($seller->seller_id == null){
                $logger = "Merchant is new: ". $seller->store_name." :: Order/Create Webhook";
                $log->logger($logger);
            }
            $commission_rate = 0;
            if($seller->commission == null){
                $commission_rate = 0.15;
            }
            else{
                $commission_rate = $seller->commission/100;
            }
        /* End Get commission for every store here */

        
                $checkout_id = "".$data_json['checkout_id'];
                $order_id = "".$data_json['id'];
                $order_number = "".$data_json['order_number'];
                $customer_id = "".$data_json['customer']['id'];
                $presentment_currency = $data_json['presentment_currency'];
                $token = $data_json['token'];
            if(isset($store_name) || $store_name != ""){
                /* Start of Shipping Price */
                $shipping_price = 0;   
                    if(isset($data_json['shipping_lines'][0]['price']) ){
                        $shipping_price = $data_json['shipping_lines'][0]['price'];
                    }
                    else{
                        $shipping_price = 0;
                    }
                /* End of Shipping Price */

                /* Start of Shipping Code */
                    if(isset($data_json['shipping_lines'][0]['code'])){
                        $shipping_code = $data_json['shipping_lines'][0]['code'];   
                    }
                    else if(!isset($data_json['shipping_lines'][0]['code']) && $data_json['line_items'][0]['name'] == "Pabili"){
                        $shipping_code = "Pabili";
                    }
                    else{
                        $shipping_code = "Free for Pickup (Exclusive for Kartvillage)";  
                    }
                /* End of Shipping Code */

                $current_item_total_price = $data_json['current_subtotal_price'];
                $vendor_net = $current_item_total_price /(1+$commission_rate);
                $kartvill_commission = $current_item_total_price -  $vendor_net;

                //Delivery Commission to be deducted
                $delivery_commission = $shipping_price * 0.12;
               
                try{
                    $saved_orders = ShopifyOrders::create([
                    'checkout_id' => $checkout_id,
                    'order_id' => $order_id,
                    'order_number'=>$order_number,
                    'customer_id'=>$customer_id,
                    'vendor'=>$store_name,
                    'total_price'=>$data_json['current_total_price'],//with delivery fee
                    'presentment_currency'=>$presentment_currency,
                    'total_discount'=>$data_json['current_total_discounts'],
                    'financial_status'=>$data_json['financial_status'],
                    'commission'=>$kartvill_commission,
                    'vendor_net'=> $vendor_net,
                    'delivery_fee'=>$shipping_price,
                    'delivery_commission'=>$delivery_commission,
                    'fulfillment_status'=>$data_json['fulfillment_status'],
                    'urlext'=> $order_number,
                    'shipping_code'=>$shipping_code,
                    'note'=>$data_json['customer']['note'],
                    'token'=>$token,
                    //'is_assigned'=>$rider_id,
                    'gateway'=>$data_json['gateway'],
                ]);

               /* Start Email Riders for the order*/
                    if($shipping_price > 0 && $shipping_code == "KFE"){ 
                        $riders = User::where('role',2)
                        ->where('province_code',$data_json['billing_address']['province_code'])
                        ->where('province_code',$data_json['shipping_address']['province_code'])
                        ->where('province_code',$data_json['customer']['default_address']['province_code'])
                        ->get();
                        // dd($riders);
                        foreach($riders as $rider){
                            $msg = "New Order with order number: ".$order_number.". https://kartvillapps.com/delivery-orders-".$order_id;
                            $msg = wordwrap($msg,70);
                            $mail = new PHPMailer(true);

                            try {
                                //Server settings
                                $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
                                $mail->isSMTP();                                            //Send using SMTP
                                $mail->Host       = env('PHP_HOST');                     //Set the SMTP server to send through
                                $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
                                $mail->Username   = env('PHP_USERNAME');                     //SMTP username
                                $mail->Password   = env('PHP_PASSWORD');                               //SMTP password
                                $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
                                $mail->Port       = env('PHP_PORT');                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`
                            
                                //Recipients
                                $mail->setFrom('no-reply@kartvillapps.com', 'Kartvill');
                                $mail->addAddress($rider->email, $rider->first_name);     //Add a recipient
                                                        //Name is optional 
                                //Content
                                $mail->isHTML(true);                                  //Set email format to HTML
                                $mail->Subject = 'Kartvill New Order';
                                $mail->Body    = $msg;
                            
                                // $mail->send();
                                    // echo 'Message has been sent';
                            } catch (Exception $e) {
                                $funcw->logger("Message could not be sent. Mailer Error: {$mail->ErrorInfo}");
                            }
                        }
                    }
                /* End Email Riders for the order* */
                /* Start of Saving new customer here */

                    if(isset($data_json['customer']['id'])){
                        $customer_id = "".$data_json['customer']['id'];
                        $customer_address = $data_json['customer']['default_address']['address1'].", ".$data_json['customer']['default_address']['city'].", ".$data_json['customer']['default_address']['province'].', '.$data_json['customer']['default_address']['country'];
                        $full_name =  $data_json['customer']['first_name'] ." ". $data_json['customer']['last_name'];
                        $customer_phone = $data_json['customer']['default_address']['phone'];
                        $customer_phone =str_replace("+1","+",$customer_phone);
                        $first_name = $data_json['customer']['first_name'];
                        $last_name = $data_json['customer']['last_name'];
                        $email_address = $data_json['customer']['email'];
                    }else{
                        $customer_id="0";
                        $customer_address="0";
                        $full_name="0";
                        $customer_phone="0";
                        $first_name="0";
                        $last_name="0";
                        $email_address="0";
                    }

                    if(isset($data_json['customer']['id'])){
                        $customer = Customer::where('customer_id',$customer_id)->first();
                        if($customer == null){
                            Customer::create([
                                'customer_id' => $customer_id,
                                'full_name' => $full_name,
                                'first_name'=> $first_name,
                                'last_name' => $last_name,                    
                                'email_address' => $email_address,                
                                'phone_number'=>$customer_phone,
                                
                            ]);
                            $customer_log = "New Customer: ".$customer_id." - ".$full_name;
                            $log->logger($customer_log);
                            $address_update_message = "Please confirm if your map pin is accurate. kartvillapps.com/user-pin-".$customer_id;
                            
                        }
                        else{   
                            
                            Customer::where('customer_id',$customer_id)
                            ->update(['address'=>$customer_address]);
                            $address_update_message = "You can update your address pin here. kartvillapps.com/user-pin-".$customer_id;
                            $log->logger('Customer is already registered... Message sent...');
                        }
                        $payment_message = ". Thank you..";
                        
                        $customer_message = "Happy day! Your order has been placed. Order#: $order_number. ".$address_update_message."".$payment_message;
                        $funcw->smsTxtBox($customer_message,$customer_phone,$txtbox_api,0,$order_number);
                    }
                     /* End of Saving new customer */

                    /* Send SMS to the seller*/
                        if($seller->seller_contact == null){
                            $messageToAdmin = "Seller ".$seller->store_name. " phone number is not available, please update.";
                            $log->logger($messageToAdmin);
                        }
                        else{
                            $message = "You have a new order. Order Number: $order_number. Happy Day!";//
                            $funcw->smsTxtBox($message,$seller->seller_contact,$txtbox_api,1,$order_number);//
                            $logg = "Successfully sent the sms to the vendor: $store_name :: ".$seller->seller_contact." :: Create Order Webhook";
                            $log->logger($logg);
                        }
            
            $logger = "Order details has been saved. Order number: ".$order_number;
            $log->logger($logger);
            /* End of sending SMS to the seller*/
            echo $saved_orders;
                }catch(Exception $e){
                    $funcw->logger($order_number."-- ".$e);
                    echo $e;
                }
                
            }else{
                $funcw->logger("No store name. Cannot process order number: ".$order_number);
            }
        
    
    }


    public function cancelOrderAPI(){
        $func = new ShopifyWebhooksController;
        $funcw = new FunctionsController;
        
        $path = storage_path() . "/app/json/orders.json"; // ie: /var/www/laravel/app/storage/json/filename.json
        $data_json = json_decode(file_get_contents($path), true); 
        
        $order_number  =   $data_json['order_number'];

        $funcw->logger('Cancel Order: '.$order_number);
        
            $order = ShopifyOrders::where('order_number',$order_number)->first();
            
            $funcw->logger($order); 
            // $cancelled_orders = ShopifyOrders::where('order_number',$order_number)
            //         ->update([
            //             'fulfillment_status'=>'Cancelled',
            //             'financial_status'=>'Refunded'
            //         ]);
            //   $funcw->logger($cancelled_orders);    
        
            if($order == null){
                $funcw->logger('Cancel Order: '. $order_number.' This order has not saved in the database, please check the order create webhook');
            }
            else{

                if($order->financial_status <> "paid"){
                    $funcw->logger('nothing to update for vendor wallet');
                }else{
                    //COD - Paid
                    if($order->gateway == "Cash on Delivery (COD)"){
                    //Return the commission deducted to the Vendor
                        //Get Seller store id
                        $seller = Sellers::where('seller_name',$order->vendor)->first();
                        $user = User::where('store_id',$seller->seller_id)->first();
                        $user_wallet = Wallet::where('user_id',$user->id)->first();

                        //add commission and balance
                        $balance = 0;
                        $balance = $user_wallet->balance + $order->commission;
                       
                        //Update wallet of seller

                        $refund_wallet = Wallet::where('user_id',$user->id)
                        ->update([
                        'balance'=>$balance,
                        ]);
                        


                    }else{
                        //E-Payment
                        $seller = Sellers::where('seller_name',$order->vendor)->first();
                        $user = User::where('store_id',$seller->seller_id)->first();
                        $user_wallet = Wallet::where('user_id',$user->id)->first();

                        $balance = $order->vendor_net + $user_wallet->balance;
                        $refund_wallet = Wallet::where('user_id',$user->id)
                        ->update([
                        'balance'=>$balance,
                        ]);
                        
                    }
                    
                }


                //For Rider Logic
                if($order->financial_status <> "paid" && $order->rider_id <> 0){
                    $wallet = Wallet::where('user_id',$order->rider_id)->first();
                    $refund = $order->commission + $wallet->balance + $order->delivery_commission;
                    Wallet::where('user_id',$order->rider_id)
                    ->update([
                        'balance'=>$refund
                    ]);

                    $cancelled_orders = ShopifyOrders::where('order_number',$order_number)
                    ->update([
                        'fulfillment_status'=>'Cancelled',
                        'financial_status'=>'Refunded'
                    ]);
                    $funcw->logger($cancelled_orders);  

                    $funcw->logger('Cancel Order: '.$order_number."Rider ID: $order->rider_id Refunded ");
                }
                else{
                    $cancelled_orders = ShopifyOrders::where('order_number',$order_number)
                    ->update([
                        'fulfillment_status'=>'Cancelled',
                        'financial_status'=>'Refunded',
                        'rider_id'=>'-1'
                    ]);
                    $funcw->logger($cancelled_orders);  
                    $funcw->logger('Cancel Order: '.$order_number." No Refund since the rider has not deducted for commission");
                }
            }
       
        
    }

    public function testRateCoords(){
        $apiKey = env('GOOGLE_MAP');
        $origin ='7.036093919928996,125.49441123597198';
        $destination = '6.1209538,125.1975628';
        // $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=$origin&destinations=$destination&mode=driving&key=$apiKey";
        
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=$origin&destinations=$destination&key=$apiKey";
        
        $data = @file_get_contents($url);
        $data = json_decode($data,true);

        // $dd = $origin." ---- ".$destination;
        // dd($dd);
        //return $data['rows'][0]['elements'][0]['duration']['value'];
        $miles = $data['rows'][0]['elements'][0]['distance']['text'];// It returns KM now
        $kmvalue = $data['rows'][0]['elements'][0]['distance']['value'];
        $duration = $data['rows'][0]['elements'][0]['duration']['text'];
       // $func->logger($miles);
        $km =  (float)$miles * 1;
        //$func->logger(json_encode($data));
        dd('km'.$km. 'duration'.$duration.'kmvalue'.$kmvalue);
        return response()->json(['km'=>$km, 'duration'=>$duration,'kmvalue'=>$kmvalue]);
    }





}
