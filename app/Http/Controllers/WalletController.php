<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Http\Request;
use App\Models\WalletHistory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class WalletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function walletBalance(){
        $wallet_balance = DB::table('users')
        ->join('wallets','users.id','wallets.user_id')
        ->where('users.role','2')
        ->paginate(100);
        //dd($wallet_balance);
        return view('wallet.rider-balance',compact('wallet_balance'));
    }

    public function vendorWalletBalance(){
        $wallet_balance = DB::table('users')
        ->join('wallets','users.id','wallets.user_id')
        ->join('sellers','users.store_id','sellers.seller_id')
        ->where('users.store_id','<>',null)
        ->where('users.role','<>','-1')
        ->paginate(100);
        
        return view('wallet.vendor-balance',compact('wallet_balance'));
    }

    public function vendorWalletBalanceDavao(){
        $wallet_balance = DB::table('users')
        ->join('wallets','users.id','wallets.user_id')
        ->join('sellers','users.store_id','sellers.seller_id')
        ->where('users.store_id','<>',null)
        ->where('users.role','<>','-1')
        ->where('users.city','LIKE','%davao%')
        ->paginate(200);
        
        return view('wallet.vendor-balance-davao',compact('wallet_balance'));
    }

    public function vendorWalletBalance1($vendor){
        $func = new FunctionsController;
        $not_filtered = $vendor;
        $vendor = $func->repair($vendor);
        
        $wallet_balance = DB::table('users')
        ->join('wallets','users.id','wallets.user_id')
        ->join('sellers','users.store_id','sellers.seller_id')
        ->where('users.store_id','<>',null)
        ->where('users.role','<>','-1')
        ->where('sellers.isKartvillage',1)
        ->where('sellers.store_name',$vendor)
        ->first();
        
       if(isset($wallet_balance)){
        return response()->json(['balance'=>$wallet_balance->balance,'vendor'=>$vendor,'isJoin'=>$wallet_balance->isKartvillage]);
       }
       else{
        return response()->json(['balance'=>0,'vendor'=>$vendor,'isJoin'=>0]);
       }
        
    
    }

    public function editWallet($user_id){
        $balance = Wallet::where('user_id',$user_id)->first();
        $user = User::find($user_id);
        $wallet_balance = $balance->balance;
        return view('wallet.edit-wallet',compact('wallet_balance','user'));
    }

    public function updateWallet(Request $request, $user_id)
    {

        $amount = $request->amount;
        /**With gcash 2% deduction */
        //$new_balance = $request->current_balance + ($amount - ($amount * 0.02));
        
        $new_balance = $request->current_balance + $amount;
        $log = new FunctionsController;

        Wallet::where('user_id',$user_id)
            ->update(['balance'=>$new_balance]);

        WalletHistory::create([
            'rider_id'=>$user_id,
            'amount'=>$amount,
            'status'=>'Paid',
            'hash'=>'User Edit',
            'request_code'=>'User Edit',
            'request_token'=>'User Edit',
            'created_at'=>Carbon::today()    
        ]);    
    
       $user = User::where('id',Auth::user()->id)->first(); 
       $rider = User::where('id',$user_id)->first(); 
      
       $log->logger($user->first_name." ".$user->last_name." Added wallet to ". $rider->first_name ." ". $rider->last_name." ($user_id). Amount: P$amount");

            return back()->with('success','Wallet Updated');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Wallet  $wallet
     * @return \Illuminate\Http\Response
     */
    public function show(Wallet $wallet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Wallet  $wallet
     * @return \Illuminate\Http\Response
     */
    public function edit(Wallet $wallet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Wallet  $wallet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wallet $wallet)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Wallet  $wallet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Wallet $wallet)
    {
        //
    }
}
