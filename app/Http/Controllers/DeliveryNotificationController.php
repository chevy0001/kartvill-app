<?php

namespace App\Http\Controllers;

use App\Models\DeliveryNotifications;
use Illuminate\Http\Request;
use App\Models\ShopifyOrders;

class DeliveryNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function countOrders(){
        $newOrderCount = ShopifyOrders::where('is_open',0)->count();
        
        if($newOrderCount == 0)
        {
            $newOrderCount = '';
        }
  
        return view('delivery.countorders',compact('newOrderCount'));
    }

    public function assignRider(Request $request){

        DeliveryNotifications::create([
            'user_id'=>$request->rider,
            'order_id'=>$request->order_id
        ]);
        
        ShopifyOrders::where('order_id',$request->order_id )
        ->update(['is_assigned'=>1]);

        return back();
    }

    

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
