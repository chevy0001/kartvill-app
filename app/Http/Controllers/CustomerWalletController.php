<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Customer;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\CustomerWallet;
use App\Models\RedeemableItem;
use Illuminate\Support\Facades\DB;
use App\Models\CustomerPointRedeemHistory;


class CustomerWalletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function returnWalletPoints($customer_id){
       
        $wallet = CustomerWallet::where('customer_id',$customer_id)->first();
        if($wallet == null){
            return response()->json(['remaining_points'=>'0']);
        }
        else{
            return response()->json($wallet);
        }
        
        

    } 
    public function showRedeem($customer_id=null){

        $point = CustomerWallet::where('customer_id',$customer_id)->first();

        $redeemableItems = RedeemableItem::all();
        


        return view('customer.customer-redeem-product', compact('point','redeemableItems','customer_id'));
    }
    public function create()
    {
        return view('customer.items-redeem-post-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->points);
       $func = new FunctionsController;
        try{
            RedeemableItem::create([
                'item_name' => $request->item_name,
                'image_url' => $request->image_url ,
                'vendor_name' => $request->vendor_name,
                'vendor_link'=> $request->vendor_link,
                'description' => $request->description,
                'price'=> $request->price,
                'points' => $request->points,
            ]);
            return back()->with('success','Item has been successfully saved');
        }catch(Exception $e){
            $func->logger('Error adding redeemable item'. $e);
            return back()->with('error','Something is wrong: Error: '.$e);
        }
        

        
    }

    public function customerRedeemItem(){

    }

    public function sendEmailCode(Request $request){
        $func = new FunctionsController;
        
        $code = Str::random(10);
        $request->session()->put('email_code', $code);
        $id = $request->id;
        try{
            $customer = Customer::where('customer_id',$id)->first();
            $success = $func->sendEmail($customer->email_address,$customer->full_name,$code,"Code - ".$code);
            if($success){
                return response()->json('success');
            }
            else{
                return response()->json('error');
            }
        }catch(Exception $e){
            return response()->json('error');
        }
    }

    public function getEmailCode(Request $request){
        $emailcode = $request->session()->get('email_code');

        if($emailcode == $request->emailcode && $emailcode != ""){
            return response()->json('success');
        }
        else{
            return response()->json('error');
        }
        
    }

    public function countRedeem(){
        $count_redeem = DB::table('customer_point_redeem_histories')->where('verified_by_id',null)->count();
        return response()->json($count_redeem);

    }

    public function processRedeem(Request $request){
        $product_id = $request->productid;
        $user_id = $request->userid;

        $item = RedeemableItem::where('id',$product_id)->first();

        CustomerPointRedeemHistory::create([
            'customer_id' => $user_id,
            'item_id'=> $product_id,
            'amount_redeemed'=>$item->price,
            'point_redeemed'=>$item->points,
        ]);

        //get wallet amount
        $wallet = CustomerWallet::where('customer_id',$user_id)->first();
        $remaining_points = $wallet->remaining_points - $item->points;
        
        CustomerWallet::where('customer_id',$user_id)->update([
            'remaining_points'=>$remaining_points,
        ]);

        $request->session()->forget('email_code');
        return response()->json([
            'success'=>'success',
            'remaining_points' => $remaining_points,
        ] );

        
    }

    public function redeemRequest(){
        //Join customers (customer_id), customer_point_redeem_histories (customer_id, item_id), redeemable_items(item_id)
        //$redeem_request = CustomerPointRedeemHistory::where('verified_by_id','<>',null)->get();
        
        $redeem_requests  = DB::table('customer_point_redeem_histories')
                ->join('customers','customers.customer_id','=','customer_point_redeem_histories.customer_id')
                ->join('redeemable_items','redeemable_items.id','=','customer_point_redeem_histories.item_id')
                ->where('customer_point_redeem_histories.verified_by_id','=',null)
                ->orderBy('customer_point_redeem_histories.created_at', 'desc')
                ->get();
        // dd( $redeem_requests );
        
        return view('customer.redeem-request',compact('redeem_requests'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CustomerWallet  $customerWallet
     * @return \Illuminate\Http\Response
     */
    public function show(CustomerWallet $customerWallet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CustomerWallet  $customerWallet
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerWallet $customerWallet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CustomerWallet  $customerWallet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomerWallet $customerWallet)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CustomerWallet  $customerWallet
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomerWallet $customerWallet)
    {
        //
    }
}
