<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Logs;
use App\Models\User;
use App\Models\Wallet;
use App\Models\Sellers;
use App\Models\Customer;
use App\Models\GCashPaid;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\ShopifyOrders;
use PHPMailer\PHPMailer\SMTP;
use App\Models\CustomerWallet;
use App\Models\ShopifyWebhooks;
use App\Models\KartvillSettings;
use Illuminate\Support\Facades\DB;
use PHPMailer\PHPMailer\PHPMailer;
use App\Models\DeliveryCommissions;
use App\Models\CustomerPointHistory;
use App\Models\DeliveryNotifications;

class ShopifyWebhooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    
    public function webhooks($topic, $address, $format ){
       
        $array = array(
            'webhook' => array( 
                'topic' => $topic,
                'address'=> env('URL').$address,
                'format'=> $format
              )
        );

        
        $access_token = session('access_token');
        $shop_url = session('shop_url');
        //dd( $topic,$address,$format,$access_token,$shop_url);
        
        $func = new FunctionsController;
        $registerWebhooks = $func->shopify_call($access_token, $shop_url,'/admin/api/2022-04/webhooks.json',$array,'POST');
        $registerWebhooks = json_decode($registerWebhooks['response'], true);
    
        // $jsonString = file_get_contents(base_path('/public/carrier_service.json'));

        // $data = json_decode($jsonString, true);
        // dd($data);

    }
    public function verify_request($data, $hmac){
        $verify_hmac = base64_encode( hash_hmac('sha256', $data, env('SHARED_SECRET'), true));
        return hash_equals($hmac, $verify_hmac);
    }
     
    public function createProductWebHook(){
        $func = new ShopifyWebhooksController;
        $funcw = new FunctionsController;
        
        $my_hmac = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        $response = '';
        $data = file_get_contents('php://input');
        $utf8 = utf8_encode( $data );
        $data_json = json_decode($utf8, true);
        $vendor_name = json_encode($data_json['vendor']);
        $vendor_name = $funcw->repair($vendor_name);
      
        $vendor_name = json_decode($vendor_name);   
        $verify_merchant = $func->verify_request($data, $my_hmac);
        
        if($verify_merchant){
            $response = $data_json;
            $message = "New merchant has been created or updated: ".$vendor_name;
            $phone = '09462883266';
            $vendor = Sellers::where('store_name',$vendor_name)->first();
            if($vendor === null){
                Sellers::create([
                    'store_name'=>$vendor_name,
                ]);

                $message="New merchant detected: ".$vendor_name;
                $funcw->smsTxtBox($message,$phone,env('TXTBOX_API_KEY'),0);
                $funcw->logger($message);
            }
            
            
        }
        else{
            $response = "This is not from Shopify";
            $funcw->logger($response);
        }
        
        
        $shop_domain = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];
        // $log = fopen("vendor-name" . rand(100,999).".json", "w") or die('Cannot open or create this file');
        
        // fwrite($log, $vendor_name);
        // fclose($log);
    
    }

//Start of Order Paid
     public function orderPaidWebHook(){
        $funcw = new FunctionsController;
        $func = new ShopifyWebhooksController;
        $my_hmac = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        $response = '';
        $data = file_get_contents('php://input');
        $utf8 = utf8_encode( $data );
        $data_json = json_decode($utf8, true);
        
        $verify_merchant = $func->verify_request($data, $my_hmac);
        
        if($verify_merchant){
           
       
            if($data_json['line_items'][0]['vendor'] != ""){
                $vendor_name = $data_json['line_items'][0]['vendor'];
            }
            else{
                foreach($data_json['line_items']['0']['properties'] as $order){
                    if($order['name'] === '_wk_vendor')
                $vendor_name = $order['value'];
                   
                }
            }
        $vendor_name = $funcw->repair(json_encode($vendor_name));    
        $vendor_name = json_decode($vendor_name);
        $vendor = Sellers::where('store_name','LIKE',"%{$vendor_name}%")->first(); //No error here

        //If vendor is not available, It might be new or it updated its store name
            if($vendor === null){
                //Add vendor into seller's
                Sellers::create([
                    'store_name'=>$vendor_name,
                ]); 
            }
            
            $order_number  =   $data_json['order_number'];
            ShopifyOrders::where('order_number',$order_number)
            ->update(['financial_status'=>'paid']);

        //Add point system here 
        $customer_id = "".$data_json['customer']['id'];
        $total_line_item_price = $data_json['total_line_items_price'];
        $customer_point = $total_line_item_price * 0.01;// 1% 100 pesos = 1 point
            try{
                        
                CustomerPointHistory::create([
                    'customer_id' => $customer_id,
                    'points' => $customer_point,
                    'total_order_amount' => $total_line_item_price,
                ]);


                $customer_wallet = CustomerWallet::where('customer_id',$customer_id)->first();
                if($customer_wallet == null){
                    CustomerWallet::create([
                        'customer_id'=> $customer_id,
                        'remaining_points'=> $customer_point,
                    ]);

                }
                else{
                    //Get wallet amount and add the new point obtained
                    $new_customer_points = $customer_wallet->remaining_points + $customer_point;
                    
                    //update customer point amount
                    CustomerWallet::where('customer_id',$customer_id)
                    ->update(['remaining_points'=>$new_customer_points]);
                }


            }catch(Exception $e){
                $funcw->logger($e);
            }
        //End point system

        
            $gateway = $data_json['gateway'];
            $balance = 0;
            // $shipping_code = $data_json['shipping_lines'][0]['code'];
            $shipping_code = "Free for Pickup (Exclusive for Kartvillage)";
            $funcw->logger("Shipping code: ".$shipping_code);
            //Get commission from $order
        //Deduct wallet here
         if(($gateway == "Cash on Delivery (COD)" && $shipping_code == "Free for Pickup (Exclusive for Kartvillage)") || ($gateway == "Cash on Delivery (COD)" && $vendor->isKartvillage == 1) )
         {
       
            try{
                $order = ShopifyOrders::where('order_number',$order_number)->first();
                $funcw->logger("enetered Orders query for $order_number");
            }catch(Exception $e){
                $funcw->logger($e);
            }
            
            //Get Seller Id from $seller
            try{
                $seller = Sellers::where('store_name',$vendor_name)->first();
                $funcw->logger("Vendor Name $vendor_name -- Seller ID: ".$seller->seller_id);
            }catch(Exception $e){
                $funcw->logger($e);
            }
            
            if($seller->isKartvillage && $shipping_code == "Free for Pickup (Exclusive for Kartvillage)"){
            //Get user id from $user
                try{
                    $user = User::where('store_id',$seller->seller_id)->first();
                    $funcw->logger("Entered user query: ".$seller->seller_id);
                }catch(Exception $e){
                    $funcw->logger($e);
                }
            
                //Get wallet balance here;
                try{
                    $user_wallet = Wallet::where('user_id',$user->id)->first();
                    $funcw->logger("Wallet balance of $user->first_name $user->last_name: ".$user_wallet->balance);
                }catch(Exception $e){
                    $funcw->logger($e);
                }
                
                $balance = $user_wallet->balance - $order->commission;

                try{
                    $update = Wallet::where('user_id',$user->id)->update(['balance'=>$balance]);
                    $funcw->logger("New Wallet balance of $user->id is $balance".$update ." less commission of $order->commission"); 
                }catch(Exception $e){
                    $funcw->logger($e);
                }
                $funcw->logger("Entered the wallet deduction");
            }
            else{
                $funcw->logger("The $vendor_name is not part of Kartvillage");
            }
         }
           //End deduct wallet here 
        $response = "Successfully Mark as Paid the Order Number: ".$order_number;
        $funcw->logger($response);
        }
        else{
            $response = "This is not from Shopify :: Order Paid";
            $funcw->logger($response);
        }
        
       // $financial_status  =   $data_json['financial_status'];
            
     }
//End of OrderPaid

    
//End Order Product
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
        // Fullfill items
        public function fulfillItems(){
            $func = new ShopifyWebhooksController;
            $funcw = new FunctionsController;
            $my_hmac = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
            $response = '';
            $data = file_get_contents('php://input');
            $utf8 = utf8_encode( $data );
            $data_json = json_decode($utf8, true);
            
            $verify_merchant = $func->verify_request($data, $my_hmac);
            $txtbox_api = env('TXTBOX_API_KEY');    

            if($verify_merchant){
                $order_number  =   $data_json['order_number'];
            
                try{
                    ShopifyOrders::where('order_number',$order_number)
                    ->update(['fulfillment_status'=>'Fullfilled']);
                    $response = "Successfully fulfilled items.";  
                    $funcw->logger("Fulfill items: ".$response);
                    // $customer_phone = $data_json['shipping_address']['phone'];
                    // $customer_phone =str_replace("+1","+",$customer_phone);
                    //$funcw->smsTxtBox("Happy day! Your order is ready. Order number: $order_number.",$customer_phone,$txtbox_api,1,$order_number);

                }catch(Exception $e){
                    $funcw->logger($e);
                }
                
           
           
            }
            else{
                $response = "This is not from Shopify";
                $funcw->logger("Fulfill items: ".$response);
            }
            


        // $financial_status  =   $data_json['financial_status'];
            
            

    
        } 
        //End fulfillment

        public function cancelOrder(){
            $func = new ShopifyWebhooksController;
            $funcw = new FunctionsController;
            $my_hmac = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
            $response = '';
            $data = file_get_contents('php://input');
            $utf8 = utf8_encode( $data );
            $data_json = json_decode($utf8, true);
            
            $verify_merchant = $func->verify_request($data, $my_hmac);
            
            $order_number  =   $data_json['order_number'];

            $funcw->logger('Cancel Order: '.$order_number);
            if($verify_merchant){
                $order = ShopifyOrders::where('order_number',$order_number)->first();
                
                $funcw->logger($order); 
                // $cancelled_orders = ShopifyOrders::where('order_number',$order_number)
                //         ->update([
                //             'fulfillment_status'=>'Cancelled',
                //             'financial_status'=>'Refunded'
                //         ]);
                //   $funcw->logger($cancelled_orders);    
            
                if($order == null){
                    $funcw->logger('Cancel Order: '. $order_number.' This order has not saved in the database, please check the order create webhook');
                }
                else{


                    if($order->financial_status <> "paid"){
                        $funcw->logger('Nothing to update for vendor wallet. Order number: '.$order_number);
                    }else{
                        //COD - Paid
                        if($order->gateway == "Cash on Delivery (COD)"){
                        //Return the commission deducted to the Vendor
                            //Get Seller store id
                            $seller = Sellers::where('seller_name',$order->vendor)->first();
                            $user = User::where('store_id',$seller->seller_id)->first();
                            $user_wallet = Wallet::where('user_id',$user->id)->first();
    
                            //add commission and balance
                            $balance = 0;
                            $balance = $user_wallet->balance + $order->commission;
                           
                            //Update wallet of seller
    
                            $refund_wallet = Wallet::where('user_id',$user->id)
                            ->update([
                            'balance'=>$balance,
                            ]);
                            
    
    
                        }else{
                            //E-Payment
                            $seller = Sellers::where('seller_name',$order->vendor)->first();
                            $user = User::where('store_id',$seller->seller_id)->first();
                            $user_wallet = Wallet::where('user_id',$user->id)->first();
    
                            $balance = $order->vendor_net + $user_wallet->balance;
                            $refund_wallet = Wallet::where('user_id',$user->id)
                            ->update([
                            'balance'=>$balance,
                            ]);
                            
                        }
                        
                    }


                    if($order->financial_status <> "paid" && $order->rider_id <> 0){
                        $wallet = Wallet::where('user_id',$order->rider_id)->first();
                        $refund = $order->commission + $wallet->balance + $order->delivery_commission;
                        Wallet::where('user_id',$order->rider_id)
                        ->update([
                            'balance'=>$refund
                        ]);

                        $cancelled_orders = ShopifyOrders::where('order_number',$order_number)
                        ->update([
                            'fulfillment_status'=>'Cancelled',
                            'financial_status'=>'Refunded'
                        ]);
                        $funcw->logger($cancelled_orders);  

                        $funcw->logger('Cancel Order: '.$order_number."Rider ID: $order->rider_id Refunded ");
                    }
                    else{
                        $cancelled_orders = ShopifyOrders::where('order_number',$order_number)
                        ->update([
                            'fulfillment_status'=>'Cancelled',
                            'financial_status'=>'Refunded',
                            'rider_id'=>'-1'
                        ]);
                        $funcw->logger($cancelled_orders);  
                        $funcw->logger('Cancel Order: '.$order_number." No Refund since the rider has not deducted for commission");
                    }
                }
            }
            else{
                $funcw->logger("Cancel Order: Not from Shopify");
                
            }
            
        }


        public function partialFulfillItems(){
            $func = new ShopifyWebhooksController;
            $funcw = new FunctionsController; 
            $my_hmac = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
            $response = '';
            $data = file_get_contents('php://input');
            $utf8 = utf8_encode( $data );
            $data_json = json_decode($utf8, true);
            
            $verify_merchant = $func->verify_request($data, $my_hmac);
            
            if($verify_merchant){
                $order_number  =   $data_json['order_number'];
                ShopifyOrders::where('order_number',$order_number)
                ->update(['fulfillment_status'=>'Partially Fulfilled']);
                $response = "Partially Fulfilled Order Number:  $order_number";
                $funcw->logger($response);
            }
            else{
                $response = "This is not from Shopify";
                $funcw->logger($response);
            }
            
        } 

    
//Create Order
    public function orderProductWebHook(){
        $funcw = new FunctionsController;
        $func = new ShopifyWebhooksController;
        $log = new FunctionsController;
        $txtbox_api = env('TXTBOX_API_KEY');

        $my_hmac = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        $response = '';
        $data = file_get_contents('php://input');
        $utf8 = utf8_encode( $data );
        $data_json = json_decode($utf8, true);
        
        $verify_merchant = $func->verify_request($data, $my_hmac);
        $log->logger(json_encode($data_json));
        
        /* Start of Store Name */      
            $store_name = "";
            if($data_json['line_items'][0]['vendor'] != ""){
                $store_name = $data_json['line_items'][0]['vendor'];
            }
            else{
                foreach($data_json['line_items']['0']['properties'] as $order){
                    if($order['name'] === '_wk_vendor')
                    $store_name = $order['value'];
                
                }
            }

            $store_name = $funcw->repair(json_encode($store_name));
            $store_name = json_decode($store_name);
        /* End of Store Name */

        $kartvill_setting = KartvillSettings::find(1)->first();

        /* Start Get commission for every store here */
            $seller = Sellers::where('store_name',$store_name)->first();
            if($seller->seller_id == null){
                $logger = "Merchant is new: ". $seller->store_name." :: Order/Create Webhook";
                $log->logger($logger);
            }
            $commission_rate = 0;
            if($seller->commission == null){
                $commission_rate = $kartvill_setting->default_kartvill_commission/100;
            }
            else{
                $commission_rate = $seller->commission/100;
            }
        /* End Get commission for every store here */

        if($verify_merchant){
            $kartvill_setting = KartvillSettings::find(1)->first();
                $checkout_id = "".$data_json['checkout_id'];
                $order_id = "".$data_json['id'];
                $order_number = "".$data_json['order_number'];
                $customer_id = "".$data_json['customer']['id'];
                $presentment_currency = $data_json['presentment_currency'];
                $token = $data_json['token'];
            if(isset($store_name) || $store_name != ""){
                /* Start of Shipping Price */
                $shipping_price = 0;   
                    if(isset($data_json['shipping_lines'][0]['price']) ){
                        $shipping_price = $data_json['shipping_lines'][0]['price'];
                    }
                    else{
                        $shipping_price = 0;
                    }
                /* End of Shipping Price */

                /* Start of Shipping Code */
                    if(isset($data_json['shipping_lines'][0]['code'])){
                        $shipping_code = $data_json['shipping_lines'][0]['code'];   
                    }
                    else if(!isset($data_json['shipping_lines'][0]['code']) && $data_json['line_items'][0]['name'] == "Pabili"){
                        $shipping_code = "Pabili";
                    }
                    else{
                        $shipping_code = "Free for Pickup (Exclusive for Kartvillage)";  
                    }
                /* End of Shipping Code */

                $current_item_total_price = $data_json['current_subtotal_price'];
                
                $vendor_net = $current_item_total_price /(1+$commission_rate);
                $kartvill_commission = $current_item_total_price -  $vendor_net;
                if($data_json['current_total_discounts'] > 1){
                    $commission_rate = $commission_rate - ($commission_rate * 0.2);
                    $vendor_net = $current_item_total_price /(1+$commission_rate); 
                    $kartvill_commission = $current_item_total_price -  $vendor_net;
                }
                
                
                

                

                
                //Delivery Commission to be deducted
                $delivery_commission = $shipping_price * ($kartvill_setting->default_rider_commission/100);
               

                try{
                    ShopifyOrders::create([
                    'checkout_id' => $checkout_id,
                    'order_id' => $order_id,
                    'order_number'=>$order_number,
                    'customer_id'=>$customer_id,
                    'vendor'=>$store_name,
                    'total_price'=>$data_json['current_total_price'],//with delivery fee
                    'presentment_currency'=>$presentment_currency,
                    'total_discount'=>$data_json['current_total_discounts'],
                    'financial_status'=>$data_json['financial_status'],
                    'commission'=>$kartvill_commission,
                    'vendor_net'=> $vendor_net,
                    'delivery_fee'=>$shipping_price,
                    'delivery_commission'=>$delivery_commission,
                    'fulfillment_status'=>$data_json['fulfillment_status'],
                    'urlext'=> $order_number,
                    'shipping_code'=>$shipping_code,
                    'note'=>$data_json['customer']['note'],
                    'token'=>$token,
                    //'is_assigned'=>$rider_id,
                    'gateway'=>$data_json['gateway'],
                ]);

                /* Start Email Riders for the order*/
                    if($shipping_price > 0 && $shipping_code == "KFE"){ 
                        $riders = User::where('role',2)
                        ->where('province_code',$data_json['billing_address']['province_code'])
                        ->where('province_code',$data_json['shipping_address']['province_code'])
                        ->where('province_code',$data_json['customer']['default_address']['province_code'])
                        ->get();
                        
                        foreach($riders as $rider){
                            $msg = "New Order with order number: ".$order_number.". https://kartvillapps.com/delivery-orders-".$order_id;
                            $msg = wordwrap($msg,70);
                            $mail = new PHPMailer(true);

                            try {
                                //Server settings
                                $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
                                $mail->isSMTP();                                            //Send using SMTP
                                $mail->Host       = env('PHP_HOST');                     //Set the SMTP server to send through
                                $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
                                $mail->Username   = env('PHP_USERNAME');                     //SMTP username
                                $mail->Password   = env('PHP_PASSWORD');                               //SMTP password
                                $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
                                $mail->Port       = env('PHP_PORT');                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`
                            
                                //Recipients
                                $mail->setFrom('no-reply@kartvillapps.com', 'Kartvill');
                                $mail->addAddress($rider->email, $rider->first_name);     //Add a recipient
                                                        //Name is optional 
                                //Content
                                $mail->isHTML(true);                                  //Set email format to HTML
                                $mail->Subject = 'Kartvill New Order';
                                $mail->Body    = $msg;
                            
                                $mail->send();
                                    // echo 'Message has been sent';
                                } catch (Exception $e) {
                                    $funcw->logger("Message could not be sent. Mailer Error: {$mail->ErrorInfo}");
                                }
                        }
                    }
                /* End Email Riders for the order* */
                
                /* Start of Saving new customer here */

                if(isset($data_json['customer']['id'])){
                    $customer_id = "".$data_json['customer']['id'];
                        //$customer_address = $data_json['customer']['default_address']['address1'].", ".$data_json['customer']['default_address']['city'].", ".$data_json['customer']['default_address']['province'].', '.$data_json['customer']['default_address']['country'];
                        $customer_address = $data_json['shipping_address']['address1'].','.$data_json['shipping_address']['city'].','.$data_json['shipping_address']['province'].','.$data_json['shipping_address']['country'];
                        $full_name =  $data_json['customer']['first_name'] ." ". $data_json['customer']['last_name'];
                        $customer_phone = $data_json['customer']['default_address']['phone'];
                        $customer_phone =str_replace("+1","+",$customer_phone);
                        $first_name = $data_json['customer']['first_name'];
                        $last_name = $data_json['customer']['last_name'];
                        $email_address = $data_json['customer']['email'];
                }else{
                    $customer_id="0";
                    $customer_address="0";
                    $full_name="0";
                    $customer_phone="0";
                    $first_name="0";
                    $last_name="0";
                    $email_address="0";
                }

                if(isset($data_json['customer']['id'])){
                    $customer = Customer::where('customer_id',$customer_id)->first();
                    if($customer == null){
                        Customer::create([
                            'customer_id' => $customer_id,
                            'full_name' => $full_name,
                            'first_name'=> $first_name,
                            'last_name' => $last_name,                    
                            'email_address' => $email_address,                
                            'phone_number'=>$customer_phone,
                            
                        ]);
                        $customer_log = "New Customer: ".$customer_id." - ".$full_name;
                        $log->logger($customer_log);
                        $address_update_message = "Please confirm if your map pin is accurate. kartvillapps.com/user-pin-".$customer_id;
                        
                    }
                    else{   
                        
                         Customer::where('customer_id',$customer_id)
                         ->update(['address'=>$customer_address]);
                        $address_update_message = "You can update your address pin here. kartvillapps.com/user-pin-".$customer_id;
                        $log->logger('Customer is already registered... Message sent...');
                    }
                    $payment_message = ". Thank you..";
                       
                    $customer_message = "Happy day! Your order has been placed. Order#: $order_number. ".$address_update_message."".$payment_message;
                    $funcw->smsTxtBox($customer_message,$customer_phone,$txtbox_api,1,$order_number);
                }
                /* End of Saving new customer */

                /* Send SMS to the seller*/
                    if($seller->seller_contact == null){
                        $messageToAdmin = "Seller ".$seller->store_name. " phone number is not available, please update.";
                        $log->logger($messageToAdmin);
                    }
                    else{
                        $table_number = $data_json['shipping_address']['address1'];
                        
                        if(strlen($table_number) > 4){
                            $table_number = "";
                        }else{
                            $table_number = " at Table# $table_number";
                        }
                        $message = "You have a new order. Order Number: $order_number $table_number. https://kartvillapps.com/vendor-order-$order_id. Happy Day!";//
                        $funcw->smsTxtBox($message,$seller->seller_contact,$txtbox_api,1,$order_number);//
                        $logg = "Successfully sent the sms to the vendor: $store_name :: ".$seller->seller_contact." :: Create Order Webhook";
                        $log->logger($logg);
                    }
                    
                    $logger = "Order details has been saved. Order number: ".$order_number;
                    $log->logger($logger);
                    /* End of sending SMS to the seller*/

                    }catch(Exception $e){
                        $funcw->logger($order_number."-- ".$e);
                    }
            }else{
                $funcw->logger("No store name. Cannot process order number: ".$order_number);
            }
        }else{
            $funcw->logger("Someone trying to access from outside");
        }
    }
}
