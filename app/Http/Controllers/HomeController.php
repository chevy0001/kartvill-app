<?php

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use App\Models\ShopifyOrders;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Session\Session;

class HomeController extends Controller
{

    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        
        // if (view()->exists($request->path())) {
        //     return view($request->path());
        // }
        // return view('errors.404');
    }

    public function root()
    {
        $func = new FunctionsController;
        // $origin = '6.1149698,125.1699776';
        // $destination = '6.116070100000001,125.1792207';
        //$rider_id = $func->detectNearestRider('Gensans test\'s Stores');
        //dd($km);
        if(Auth::user()->role == null || Auth::user()->role == 0){
            return view('errors.verify');
        }
            $month = new DateTime(Carbon::now());
            $month = $month->format('Y-m-d');
            $dt = Carbon::createFromFormat('Y-m-d',$month);
            $month = date('m', strtotime($month));
            $monthName = $dt->format('F');
            $year = date('Y');

           

        if(Auth::user()->role != 2 && Auth::user()->role != 5 && Auth::user()->role != 4){
            $deliveries = DB::table('shopify_orders')->paginate(50);
            $completed_deliveries = DB::table('shopify_orders')->where('financial_status','paid')->count();
            $pending_deliveries = DB::table('shopify_orders')->where('order_status','<>','delivered')
                                ->where('financial_status','<>','paid')->count();
            $revenue = ShopifyOrders::where('financial_status','paid')->sum('commission');

            //$delivery_fee for non rider is the total revenue for the month
            $delivery_fee = ShopifyOrders::whereMonth('created_at', $month)
            ->where('order_status','delivered')
            ->where('financial_status','paid')
            ->whereYear('created_at', $year)
            ->sum('commission');
            
            
        }
        elseif(Auth::user()->role == 4){
            return redirect('/salesdashboard');
        }
        else{
            return redirect('/user-profile-'.Auth::user()->id);
            $delivery_fee = ShopifyOrders::where('rider_id',Auth::user()->id)
            ->whereMonth('created_at', $month)
            ->where('order_status','delivered')
            ->where('financial_status','paid')
            ->whereYear('created_at', $year)
            ->sum('delivery_fee');
            
            

            $deliveries = DB::table('shopify_orders')->where('rider_id',Auth::user()->id)->paginate(50);
            $completed_deliveries = DB::table('shopify_orders')->where('order_status','delivered')->where('rider_id',Auth::user()->id)->count();
            $pending_deliveries = DB::table('shopify_orders')->where('order_status','<>','delivered')->where('rider_id',Auth::user()->id)->count();
            $revenue = ShopifyOrders::where('rider_id',Auth::user()->id)->where('order_status','delivered')->sum('delivery_fee');
        }
           

        return view('index',compact('deliveries','completed_deliveries','pending_deliveries','revenue','delivery_fee'));
    }
    public function dummy($id,$id2){
        //dd($id2);
        return view('admin.dummy');
    }
   

    /*Language Translation*/
    public function lang($locale)
    {
        if ($locale) {
            App::setLocale($locale);
            Session::put('lang', $locale);
            Session::save();
            return redirect()->back()->with('locale', $locale);
        } else {
            return redirect()->back();
        }
    }

    public function updateProfile(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email'],
            'avatar' => ['nullable', 'image', 'mimes:jpg,jpeg,png', 'max:1024'],
        ]);

        $user = User::find($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');

        if ($request->file('avatar')) {
            $avatar = $request->file('avatar');
            $avatarName = time() . '.' . $avatar->getClientOriginalExtension();
            $avatarPath = public_path('/images/');
            $avatar->move($avatarPath, $avatarName);
            $user->avatar = '/images/' . $avatarName;
        }

        $user->update();
        if ($user) {
            Session::flash('message', 'User Details Updated successfully!');
            Session::flash('alert-class', 'alert-success');
            return response()->json([
                'isSuccess' => true,
                'Message' => "User Details Updated successfully!"
            ], 200); // Status code here
        } else {
            Session::flash('message', 'Something went wrong!');
            Session::flash('alert-class', 'alert-danger');
            return response()->json([
                'isSuccess' => true,
                'Message' => "Something went wrong!"
            ], 200); // Status code here
        }
    }

    public function updatePassword(Request $request, $id)
    {
        $request->validate([
            'current_password' => ['required', 'string'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            return response()->json([
                'isSuccess' => false,
                'Message' => "Your Current password does not matches with the password you provided. Please try again."
            ], 200); // Status code
        } else {
            $user = User::find($id);
            $user->password = Hash::make($request->get('password'));
            $user->update();
            if ($user) {
                Session::flash('message', 'Password updated successfully!');
                Session::flash('alert-class', 'alert-success');
                return response()->json([
                    'isSuccess' => true,
                    'Message' => "Password updated successfully!"
                ], 200); // Status code here
            } else {
                Session::flash('message', 'Something went wrong!');
                Session::flash('alert-class', 'alert-danger');
                return response()->json([
                    'isSuccess' => true,
                    'Message' => "Something went wrong!"
                ], 200); // Status code here
            }
        }
    }

    public function error(){
        return view('errors.404');
    }
}
