<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Sellers;
use App\Models\CityCode;
use App\Models\JntRates;
use Illuminate\Http\Request;
use App\Models\DeliveryRates;
use App\Models\KartvillSettings;
use App\Models\ShopifyInstaller;

class RatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // log the raw request -- this makes debugging much easier
        
                $kartvill_setting = KartvillSettings::find(1)->first();
                $input = file_get_contents('php://input');
                //file_put_contents($filename.'-input', $input);
                $func = new FunctionsController;
           
                
                // parse the request
                $rates = json_decode($input, true);
                $rat = json_encode($rates);
                
                 

                $func = new FunctionsController;
                 $func->logger($rat);


                //Get the address of the customer
                $customer_address = $rates['rate']['destination']['address1'].','.$rates['rate']['destination']['city'] ;

                //Get vendor address

                //1. Get vendor name first
                if($rates['rate']['items']['0']['vendor'] != null || $rates['rate']['items']['0']['vendor'] != ""){
                    $vendor_name = $rates['rate']['items']['0']['vendor'];
                }
                else{
                    $vendor_name = $rates['rate']['items']['0']['properties']['_wk_vendor'];
                }    

                //2. Repair vendor name by removing special character replacement such as /002
                $vendor_name = $func->repair($vendor_name);

                //3. Query the vendor address using vendor name
                $sellers = Sellers::where('store_name','LIKE',"%{$vendor_name}%")->get();

                //4. Get the shortest distance from the customer to possible multiple vendor branches
                $shortest_distance = 10000000000000000000000000000000000;
                $shortest_vendor_address = "";
            
                foreach($sellers as $seller){

                    if($seller === null){
                        $vendor_address = "Robinsons, General Santos City";
                    }else{
                        if($seller->warehouse_address === null){
                            $vendor_address = $seller->store_address.",".$seller->city.",".$seller->zip.",".$seller->city;
                        }
                        else{
                            $vendor_address = $seller->warehouse_address;
                        }
                    }    
    
                    $data = $func->getDistance($customer_address,$vendor_address);
                    
                    $distance_data = explode("-",$data);
                    $km = round($distance_data[2]/1000,2);
                    

                    if($km < $shortest_distance){
                        $shortest_distance = $km;
                        $shortest_vendor_address = $seller->store_address;
                        $vendor_province_code = $seller->province_code;
                        $duration = $distance_data[1];
                    }
    
                }
                // $func->logger("KM val".$distance_data[2]);
                $duration_explode = explode(" ",$duration);
                $duration = $duration_explode[0];
                $duration += 10;
                $max_duration = $duration + 10;
                $km = $shortest_distance;

                $local_description = "$km km | $duration to $max_duration minutes | ".$shortest_vendor_address;
                
                // $func->logger('Shortest distance: '.$km);
                //$func->logger('Shortest address: '.$shortest_vendor_address.". $km Kms");
                // $func->logger('vendor_province_code : '.$vendor_province_code);

        //Compute the rate using the shortest distance
        
        //Check if the distance is more than 20 kilometers it must be delivered by JNT
        //Check if the place is free delivery or not
        //Check if it's minimum distance set minimum fee
        // Check the region of customer and vendor for rate

        /* Get the customer region*/
        $customer_province_code = $rates['rate']['destination']['province'];
        try{
            $customer_region = CityCode::where('province_code',$customer_province_code)->first();
        }catch(Exception $e){
            $func->logger($e);
        }
        

        try{
            $vendor_region = CityCode::where('province_code',$vendor_province_code)->first();
        }catch(Exception $e){  
            $func->logger($e);
        }  
        //  $func->logger('Vendor Region:'. $vendor_region->region);      
        //  $func->logger('Customer Region:'. $customer_region->region);       

         $getRegion = $func->getRegion($vendor_region->region,$customer_region->region);


       //Compute Weight 
       $weight = 0;
       $total_price = 0;
       $func->logger($getRegion);
           foreach($rates['rate']['items'] as $item) {
            $quantity = $item['quantity'];
            // $weight += $item['grams'] * $quantity;

             $price = $quantity * $item['price'];

             $total_price = $total_price + $price;
              
           }
           $total_price = $total_price/100;
           $func->logger("Total Price :".$total_price);
        try{
            $getJNTRate = JntRates::where('region',$getRegion)
            ->where('weight_min', '<=', $weight )
            ->where('weight_max', '>=', $weight)
            ->first();
        }catch(Exception $e){
            $func->logger($e);
        }
      
       $jntrate = $getJNTRate->rate;
       $jntrate = ($jntrate + 10) * 100;

       
        //Free delivery for the listed
        if($customer_province_code !== "PH-SCO" && $km < 20){
            if($km < $kartvill_setting->minimum_kilometer){
                $fee = $kartvill_setting->minimum_kilometer_rate;
            }
            elseif($km > $kartvill_setting->minimum_kilometer){
                $fee = $km * $kartvill_setting->delivery_rate;//10 is per KM
            }
            $fee = ($fee + 5) * 100;

      

            $output = array('rates' => array(
                array(
                    'service_name' => 'Kartvill Express',
                    'service_code' => 'KFE',
                    'total_price' => $fee,
                    'description' => $local_description,
                    'currency' => 'PHP',
                    // 'min_delivery_date' => $on_min_date,
                    // 'max_delivery_date' => $on_max_date
                ),
            ));
        }    

        elseif($km > 20){
            $output = array('rates' => array(
                array(
                    'service_name' => 'Kartvill Express National Delivery',
                    'service_code' => 'JNT',
                    'total_price' => $jntrate,
                    'description' => "7-14 Days Delivery anywhere in the Philippines",
                    'currency' => 'PHP',
                    // 'min_delivery_date' => $on_min_date,
                    // 'max_delivery_date' => $on_max_date
                ),
            ));
        }
        elseif($total_price < 100){
            $fee1 = $km * $kartvill_setting->minimum_kilometer;
            $fee = 120 * 100;
                $output = array('rates' => array(
                    array(
                        'service_name' => 'Kartvill Express',
                        'service_code' => 'KFE',
                        'total_price' => $fee,
                        'description' => "Minimum order of 100 Pesos to get delivery fee for as low as $fee1 Pesos",
                        'currency' => 'PHP',
                        // 'min_delivery_date' => $on_min_date,
                        // 'max_delivery_date' => $on_max_date
                    ),
                ));
        }   
        else{
            //Fee is 0 for now
            // $fee = 0;
            //If less than the minimum
            if($km <= $kartvill_setting->minimum_kilometer){
                $fee = $kartvill_setting->minimum_kilometer_rate * 100;
                $output = array('rates' => array(
                    array(
                        'service_name' => 'Kartvill Express',
                        'service_code' => 'KFE',
                        'total_price' => $fee,
                        'description' => $local_description,
                        'currency' => 'PHP',
                        // 'min_delivery_date' => $on_min_date,
                        // 'max_delivery_date' => $on_max_date
                    ),
                ));
            }
            //If more than 3 Km and less than 20 Km
            else{
               $fee = (($km * $kartvill_setting->delivery_rate) + $kartvill_setting->additional_delivery_rate) * 100;
                $output = array('rates' => array(
                    array(
                        'service_name' => 'Kartvill Express',
                        'service_code' => 'KFE',
                        'total_price' => $fee,
                        'description' => $local_description,
                        'currency' => 'PHP',
                        // 'min_delivery_date' => $on_min_date,
                        // 'max_delivery_date' => $on_max_date
                    ),
                ));
            }
            
        }

        $json_output = json_encode($output);
        return response($json_output);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function shopifyRate(){
       
        return view('shopify.set-rate');
        
            
        
    }

    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        DeliveryRates::updateOrCreate(
            [
            'id'=>$request->id,
            ],
            [
            'base_rate'=>$request->baseRate,
            'perKm'=>$request->perKM
            ]);
        return back();
    }

    public function rates(){
        $rate = DeliveryRates::where('rate_id','kartvillexpress')->first();
        return view('shopify.set-rate',compact('rate'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
