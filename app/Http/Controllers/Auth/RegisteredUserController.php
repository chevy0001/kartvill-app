<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\Wallet;
use App\Models\Sellers;
use App\Models\CityCode;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use App\Providers\RouteServiceProvider;
use App\Http\Controllers\FunctionsController;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $sellers = Sellers::all();
        $provinces = CityCode::all();
        return view('auth.register',compact('sellers','provinces'));
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'phone_number' => ['required', 'string', 'max:13'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
            'street_address' => ['required', 'string', 'max:255'],
            'city' => ['required', 'string', 'max:255'],
            'province'=>['required'],
            'country'=>['required'],
            'zip'=>['required', 'max:8'],
            
        ]);

        $province_code = CityCode::where('province',$request->province)->first();

        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone_number' => $request->phone_number,
            'email' => $request->email,
            'store_id'=>$request->store_id,
            'password' => Hash::make($request->password),
            'street_address'=>$request->street_address,
            'city'=>$request->city,
            'province'=>$request->province,
            'province_code'=>$province_code->province_code,
            'country'=>$request->country,
            'zip'=>$request->zip,
            'vehicle_type'=>$request->vehicle_type,
            'plate_number'=>$request->plate_number,
            'role'=>0,
        ]);

        Wallet::create([
            'user_id' => $user->id,
        ]);
        // $func = new FunctionsController;
        // $func->logger($request->first_name." ".$request->last_name." - IP: ".$request->ip());

        event(new Registered($user));

        Auth::login($user);

        return redirect(RouteServiceProvider::HOME);
    }
}
