<?php

namespace App\Http\Controllers;

use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\FunctionsController;
use App\Models\KartvillSettings;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $availability = Store::where('id',1)->first();
        $kartvill_setting = KartvillSettings::find(1)->first();
        
       return view('store.settings',compact('availability','kartvill_setting'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function availability(Request $request)
    {
        $func = new FunctionsController;
        // ShopifyOrders::where('order_number',$order_number)
        // ->update(['financial_status'=>'paid']);
        Store::where('id',1)
        ->update([
            'availability'=>$request->availability
        ]);

        $message="Someone open or closed the store";
        if($request->availability == true){
            $message = Auth::user()->first_name. " Opened the store";
        }
        else{
            $message = Auth::user()->first_name. " Closed the store";
        }
        

        $func->logger($message);
        return response()->json('success');
    }

    public function getAvailability(){
        $availability = Store::where('id',1)->first();
        return response()->json($availability);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store)
    {
        //
    }
}
