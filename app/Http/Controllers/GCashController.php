<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Wallet;
use App\Models\Sellers;
use App\Models\Customer;
use App\Models\GCashPaid;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\ShopifyOrders;
use App\Models\WalletHistory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class GCashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function gcashpaid($orderNumber){

        $order = ShopifyOrders::where('order_number',$orderNumber)->first();
        $orderNumber = $order->order_number;
        $totalAmount = $order->total_amount;
            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://g.payx.ph/payment_request',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array(
                'x-public-key' => 'pk_43b6cd3c23dd4e942e1135508a3364cf',
                'amount' => '190',
                'description' => 'Kartvill Order Number: '
            ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            // dd($response);
    }

    public function gcashpaid2($orderNumber){
        
       
        $orders = ShopifyOrders::where('order_number',$orderNumber)->first();

       
        
        if($orders == null){
            return view('gcash.expired');
        }
        else{
            if($orders->financial_status == "paid"){
                return view('gcash.expired');
            }
        }
        

        $cust = Customer::where('urlext',$orderNumber)->first();
        $name = $cust->full_name;
        $email = $cust->email_address;
        $phone = $cust->phone_number;
        $orderNumber = $orders->order_number;
        $totalAmount = $orders->product_price;

        
        $url = 'https://getpaid.gcash.com/paynow?public_key=pk_43b6cd3c23dd4e942e1135508a3364cf&amount='.$totalAmount.'&customeremail='.$email.'&customername='.$name.'&customermobile='.$phone.'&expiry=6&description=Kartvill Order Number '.$orderNumber;
        
        return redirect($url);
        //return view('gcash.index',compact('totalAmount','orderNumber','name','email','phone'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function wallet(){

        
        $wallet = Wallet::where('user_id',Auth::user()->id)->first();
        if($wallet == null){
            Wallet::create([
                'user_id'=>Auth::user()->id,
            ]); 
        }

        if(Auth::user()->role != 2 && Auth::user()->role != 5){
            $orders = ShopifyOrders::all();
            $topups = DB::table('wallet_histories') 
                        ->orderBy('id', 'DESC')
                        ->get();
        }
        elseif(Auth::user()->role == 5){
            $store_name = Sellers::where('seller_id',Auth::user()->store_id)->first();
            $orders = ShopifyOrders::where('vendor',$store_name->store_name)
                    ->where('shipping_code','Free for Pickup (Exclusive for Kartvillage)')
                    ->where('financial_status','paid')
                    ->get();
            
            $topups = DB::table('wallet_histories') 
                        ->where('rider_id',Auth::user()->id)
                        ->orderBy('id', 'DESC')
                        ->get();
        }
        else{
            $orders = ShopifyOrders::where('rider_id',Auth::user()->id)->get();
            
            $topups = DB::table('wallet_histories') 
                        ->where('rider_id',Auth::user()->id)
                        ->orderBy('id', 'DESC')
                        ->get();
        }
     

        $store_name = Sellers::where('seller_id',Auth::user()->store_id)->first();
        if(isset($store_name)){
            return view('delivery.wallet',compact('wallet','orders','topups','store_name'));
        }
        else{
            $store_name = null;
            return view('delivery.wallet',compact('wallet','orders','topups','store_name'));
        }

        
    }

    public function topUpCash(Request $request){
        $user = Wallet::where('user_id',Auth::user()->id)->first();
        $wallet = Wallet::where('user_id',Auth::user()->id)->first();
        $request_token = Str::random(20);
        $fullname = Auth::user()->first_name ." ".Auth::user()->last_name;
        $func = new FunctionsController;
        $updated_at = Carbon::now()->format('Y-m-d H:m:s');
        $hash = Str::random(20);
        $code = Str::random(10);
        if($user == null){
            Wallet::create([
                'user_id'=>Auth::user()->id,
                'requested_amount'=>$request->amount,
                'hash'=>$hash,
                'request_code'=>$code,
                'request_token'=>$request_token,
            ]);
            
        }else{
            Wallet::where('user_id',Auth::user()->id)
            ->update([
                        'hash'=>$hash,
                        'requested_amount'=>$request->amount,
                        'request_code'=>$code,
                        'request_token'=>$request_token,
                        'created_at'=>$updated_at,
                        'updated_at'=>$updated_at, 
                    ]);
           
        }

        

        //Create Tansaction History
        WalletHistory::create([
            'rider_id'=>Auth::user()->id,
            'amount'=>$request->amount,
            'hash'=>$hash,
            'request_code'=>$code,
            'status'=>'pending',
            'request_token'=>$request_token,
        ]);

        return response()->json(['success'=>'success']);

    }

    public function topup(Request $request){
        
    $wallet = Wallet::where('user_id',Auth::user()->id)->first();
    $request_token = Str::random(20);

    $fullname = Auth::user()->first_name ." ".Auth::user()->last_name;
    $redirect_url = 'https://kartvillapps.com/successgcash-'.$request_token;
    //pk_
    $key = env('GCASH_API');
    $curl = curl_init();
   

    curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://g.payx.ph/payment_request',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => array(
        'x-public-key' =>  $key,
        'amount' => $request->topup,
        'description' => $fullname.' Top-up. Please note that the Gcash Getpaid will deduct 2% service fee.',
        'customername'=>$fullname,
        'customermobile'=>Auth::user()->phone_number,
        'customeremail'=>Auth::user()->email,
        'webhooksuccessurl'=>'https://kartvillapps.com/successpaid',
        'redirectsuccessurl'=>$redirect_url
    ),
    ));

    $func = new FunctionsController;

    $response = curl_exec($curl);
    
    curl_close($curl);

    
    $response = json_decode($response,true);
    // dd($response);
    $updated_at = Carbon::now()->format('Y-m-d H:m:s');
    if(isset($response['success'])){
        $user = Wallet::where('user_id',Auth::user()->id)->first();
        if($user == null){
            Wallet::create([
                'user_id'=>Auth::user()->id,
                'requested_amount'=>$response['data']['amount'],
                'hash'=>$response['data']['hash'],
                'request_code'=>$response['data']['code'],
                'request_token'=>$request_token,
            ]);
            
        }else{
            Wallet::where('user_id',Auth::user()->id)
            ->update([
                        'hash'=>$response['data']['hash'],
                        'requested_amount'=>$response['data']['amount'],
                        'request_code'=>$response['data']['code'],
                        'request_token'=>$request_token,
                        'created_at'=>$updated_at,
                        'updated_at'=>$updated_at, 
                    ]);
           
        }

        

        //Create Tansaction History
        WalletHistory::create([
            'rider_id'=>Auth::user()->id,
            'amount'=>$response['data']['amount'],
            'hash'=>$response['data']['hash'],
            'request_code'=>$response['data']['code'],
            'status'=>$response['data']['status'],
            'request_token'=>$request_token,
        ]);

       
    }
    else{
        return ($response['error']);
    }
    // $func->logger($response);
    // $response = json_decode($response);
    //echo $response;
    $checkouturl = $response['data']['checkouturl'];
    // $checkouturl = "https://getpaid.gcash.com/checkout/".$response['data']['hash'];
    return redirect($checkouturl);

    
    }
    public function successPaid(Request $request){
        $data = $_REQUEST;
        //$utf8 = utf8_encode( $data );
        // $data_json = json_decode($utf8, true);


        $funcw = new FunctionsController;
        $funcw->logger($data); 

    }
    public function success($requestToken, Request $request ){
        $func = new FunctionsController();
        $url = $request->fullUrl();
        $hash = explode("=",$url);

        $wallet = Wallet::where('hash', $hash[1])
        ->where('request_token',$requestToken)
        ->first();

        if($wallet == null){
            return redirect('/wallet')->with('status','Top up failed!');
            
        }
        else{
            $balance = $wallet->balance;
            //get Requested amount
            $requested_amount = $wallet->requested_amount;
            $new_balance = $balance + ($requested_amount - ($requested_amount * 0.02));//minus 2 percent
    
            $updated_at = Carbon::now()->format('Y-m-d H:m:s');
            WalletHistory::where('hash', $hash[1])
            ->where('request_token',$requestToken)
            ->update([
                'prev_balance'=>$balance,
                'new_balance'=>$new_balance,
                'status'=>'Paid',
                'updated_at'=>$updated_at,
            ]);
            //Transfer requested amount to the balance
            Wallet::where('hash', $hash[1])
            ->where('request_token',$requestToken)
            ->update([
                'balance'=>$new_balance,
                'requested_amount'=>'0',
                'hash'=>null,
                'request_code'=>null,
                'request_token'=>null,
            ]);
            // return view('errors.success');
            $func->logger('Top up sucess');
            return redirect('/wallet')->with('status','Top up added');
        }
        


       
    }
   
    public function topUpRequest()
    {
        $topups = DB::table('users')
                ->where('wallets.hash','<>',null)
                ->where('wallets.requested_amount','<>','0')
                ->join('wallets','users.id','=','wallets.user_id')
                ->orderBy('wallets.created_at','desc')
                ->get();
         //dd($topups);

        
        return view('admin.topup-request',compact('topups'));
    }

    public function countRequest(){
        $count = DB::table('wallets')
        ->where('hash','<>',null)
        ->where('requested_amount','<>','0')
        ->count();
        return response()->json($count);
    }

    public function approveTopUp(Request $request){

        $log = new FunctionsController;
        //dd($request->rider_id);
        //Get wallet balance
        $wallet = Wallet::where('user_id',$request->rider_id)
                ->where('hash',$request->hash)
                ->first();
       
         $balance = $wallet->balance;
         //get Requested amount
         $requested_amount = $wallet->requested_amount;
         /** With gcash 2% deduction */
        //  $new_balance = $balance + ($requested_amount-($requested_amount * 0.02));
         $new_balance = $balance + $requested_amount;

         $updated_at = Carbon::now()->format('Y-m-d H:m:s');
         WalletHistory::where('rider_id',$request->rider_id)
         ->where('hash',$request->hash)
         ->update([
            'prev_balance'=>$balance,
            'new_balance'=>$new_balance,
            'status'=>'Paid',
            'updated_at'=>$updated_at,
         ]);
        
         

       
        //Transfer requested amount to the balance
        Wallet::where('user_id',$request->rider_id)
        ->where('hash',$request->hash)
        ->update([
            'balance'=>$new_balance,
            'requested_amount'=>'0',
            'hash'=>null,
            'request_code'=>null,
            'request_token'=>null,
        ]);

        $funcw = new FunctionsController;
        
        $log2 = "Rider ID:$request->rider_id Old Balance: $balance + $requested_amount = $new_balance";
        $funcw->logger($log2);

        $log = Auth::user()->first_name." Approved top-up amount: ".$requested_amount. "by Rider ID: ".$request->rider_id;
        $funcw->logger($log);

        return back();

    }

    public function rejectTopup(Request $request){
        Wallet::where('user_id',$request->rider_id)
        ->where('hash',$request->hash)
        ->update([
            'requested_amount'=>'0',
            'hash'=>null,
            'request_code'=>null,
            'request_token'=>null,
        ]);

        
        $funcw = new FunctionsController;
        $log = Auth::user()->first_name." Rejected top up of Rider ID: ".$request->rider_id;
        $funcw->logger($log);
    return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
