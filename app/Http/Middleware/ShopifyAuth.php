<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\ShopifyInstaller;

class ShopifyAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        $shopify = $_GET;
        if($shopify == null){
            return redirect('/');
        }
        $check = ShopifyInstaller::where('shop_url', $shopify['shop'] )->first();
        if($check == null){
            return redirect("/shopify/install?shop=".$shopify['shop']);
            exit();
        }else{
            return $next($request);
        }

        
    }
}
