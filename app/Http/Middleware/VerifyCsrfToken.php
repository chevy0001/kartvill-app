<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array<int, string>
     */
    protected $except = [
        '/webhook/create-product-webhook',
        '/webhook/order-product-webhook',
        '/webhook/order-paid-webhook',
        '/webhook/fulfill-item',
        '/webhook/partial-fulfill-item',
        '/webhook/cancel-order',
        '/rates',
        '/gcash/get-paid2/*',
        '/set-delivery-rate',
        '/customer-coordinate-update',
        '/success-paid',
        '/seller-search',
        '/user-search',
        '/get-fare'
    ];
}
