<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RetrieveAllOrders extends Model
{
    use HasFactory;
    protected $fillable = [
        'checkout_id',
        'order_id',
        'order_number',
        'customer_id',
        'vendor',
        'total_price',
        'presentment_currency',
        'total_discount',
        'financial_status',
        'commission',
        'vendor_net',
        'delivery_fee',
        'delivery_commission',
        'is_open',
        'is_assigned',
        'fulfillment_status',
        'rider_id',
        'product_price',
        'order_status',
        'urlext',
        'shipping_code',
        'gateway'
        

        
    ];
}
