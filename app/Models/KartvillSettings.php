<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KartvillSettings extends Model
{
    use HasFactory;
    protected $fillable = [
        'default_kartvill_commission',
        'default_rider_commission',
        'delivery_rate',
        'additional_delivery_rate',
        'minimum_kilometer',
        'minimum_kilometer_rate',
        'bonus_delivery_fee',
    ]; 
}
