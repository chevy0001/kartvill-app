<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliveryCommissions extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_number',
        'delivery_fee',
        'delivery_method',
        'status'

    ];
}
