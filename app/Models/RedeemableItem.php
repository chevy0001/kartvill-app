<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RedeemableItem extends Model
{
    use HasFactory;
    protected $fillable = [
        'item_name',
        'image_url',
        'description',
        'vendor_name',
        'vendor_link',
        'points',
        'price',
        'isActive',
    ];
}
