<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FareSetting extends Model
{
    use HasFactory;
    protected $fillable = [
        'city_id',
        'trans_mode_id',
        'vehicle_type_id',
        'min_fare',
        'min_distance',
        'fare_per_km'
    ];
}
