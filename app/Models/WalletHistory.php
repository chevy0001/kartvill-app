<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WalletHistory extends Model
{
    use HasFactory;
    protected $fillable = [
        'rider_id',
        'prev_balance',
        'new_balance',
        'amount',
        'hash',
        'request_code',
        'status',
        'request_token',
    ];
   
}
