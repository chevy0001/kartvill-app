<?php

namespace App\Models\ECommerce;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_id',
        'customer_name',
        'customer_contact',
        'fulfillment_status',
        'payment_status',
        'table_number',
        'request',
        'device_id',
        'total',
        'privilege_discount',
    ];
}
