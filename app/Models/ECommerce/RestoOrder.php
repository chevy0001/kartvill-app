<?php

namespace App\Models\ECommerce;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RestoOrder extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_id',
        'product_id',
        'variation_id',
        'quantity',
        'is_checkout',
        'option_key',
        'order_fulfillment_status',
        'device_id'
    ];
    
    //Option Key is composed of option id and at the last index is the variation id
    // ::4::2:15
    //4 and 2 is the option_id while the 15 is the variation_id

    public function options(): HasMany
    {
        return $this->hasMany(OrderOption::class, 'product_id', 'id');
    }
}
