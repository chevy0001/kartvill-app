<?php

namespace App\Models\ECommerce;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        'vendor_id',
        'title',
        'description',
        'discount',
        'variant_count',
    ];

   /**
     * Get all of the comments for the Product
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images(): HasMany
    {
        return $this->hasMany(ProductImage::class, 'product_id', 'id');
    }

    public function variants(): HasMany
    {
        return $this->hasMany(ProductVariation::class, 'product_id', 'id');
    }

    public function categories(): HasMany
    {
        return $this->hasMany(ProductCategory::class, 'product_id', 'id');
    }

    public function options(): HasMany
    {
        return $this->hasMany(Options::class, 'product_id', 'id');
    }

    // /**
    //  * Get the user that owns the Product
    //  *
    //  * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    //  */
    // public function user(): BelongsTo
    // {
    //     return $this->belongsTo(User::class, 'foreign_key', 'other_key');
    // }
    public function seller(): BelongsTo
    {
        return $this->belongsTo(\App\Models\Sellers::class,'vendor_id','id');
    }


}
