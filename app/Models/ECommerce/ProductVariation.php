<?php

namespace App\Models\ECommerce;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductVariation extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        'product_id',
        'variation_title',
        'price',
        'stocks',
        'weight',
        'is_primary',
        'is_active'
    ];

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
}
