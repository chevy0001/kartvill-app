<?php

namespace App\Models\ECommerce;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderOption extends Model
{
    use HasFactory;
    protected $fillable = [
        'order_id',
        'option_id',
        'option_key',
    ];

    public function restoOrders(): BelongsTo
    {
        return $this->belongsTo(RestoOrders::class);
    }
}
