<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerPointRedeemHistory extends Model
{
    use HasFactory;
    protected $fillable = [
        'customer_id',
        'item_id',
        'amount_redeemed',
        'point_redeemed',
        'verified_by_id',
    ];
}
