<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PabiliOrder extends Model
{
    use HasFactory;
    protected $fillable = [
        'order_id',
        'store_address',
        'latitude',
        'longitude',
        'rider_id'
    ];
}
