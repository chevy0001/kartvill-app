<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Model;

class Sellers extends Model
{
    use HasFactory;
    protected $fillable = [
        'seller_id',
        'store_name',
        'seller_name',
        'seller_email',
        'seller_contact',
        'country',
        'city',
        'store_address',
        'warehouse_address',
        'cashier_number',
        'state',
        'zip',
        'commission',
        'latitude',
        'longitude',
        'store_type',
        'province_code',
        'isKartvillage',
    ];

    public function products(): HasMany
    {
        return $this->hasMany(\App\Models\Ecommerce\Product::class, 'vendor_id', 'id');
    }
}
