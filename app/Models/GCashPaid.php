<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GCashPaid extends Model
{
    use HasFactory;
    protected $fillable = [
        'order_number',
        'rider_id',
        'total_amount',
        'is_paid',
        'delivery_fee',
    ];
}
