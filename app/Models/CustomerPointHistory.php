<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerPointHistory extends Model
{
    use HasFactory;

    protected $fillable = [
        'customer_id',
        'points',
        'total_order_amount'
    ];
}
