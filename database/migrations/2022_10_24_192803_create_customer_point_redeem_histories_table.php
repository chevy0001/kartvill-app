<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('customer_point_redeem_histories', function (Blueprint $table) {
            $table->id();
            $table->string('customer_id');
            $table->string('item_id');
            $table->string('amount_redeemed');
            $table->string('point_redeemed');
            $table->string('verified_by_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_point_redeem_histories');
    }
};
