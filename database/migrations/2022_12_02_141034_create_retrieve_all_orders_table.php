<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retrieve_all_orders', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('checkout_id');
            $table->bigInteger('order_id')->unique();
            $table->bigInteger('order_number');
            $table->bigInteger('customer_id');
            $table->bigInteger('rider_id');
            $table->string('vendor')->nullable();
            $table->string('total_price')->nullable();
            $table->string('product_price')->nullable();
            $table->string('presentment_currency')->nullable();
            $table->string('total_discount')->nullable();
            $table->string('financial_status')->nullable();
            $table->string('commission')->nullable();
            $table->string('vendor_net')->nullable();
            $table->string('delivery_fee')->nullable();
            $table->string('delivery_commission')->nullable();
            $table->boolean('is_open')->default(0);
            $table->boolean('is_assigned')->default(0);
            $table->string('fulfillment_status')->nullable();
            $table->string('order_status')->nullable();
            $table->string('gateway')->nullable();
            $table->string('urlext')->nullable();
            $table->string('shipping_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retrieve_all_orders');
    }
};
