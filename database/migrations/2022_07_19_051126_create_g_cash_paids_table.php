<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('g_cash_paids', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('rider_id');
            $table->string('order_number')->unique();
            $table->string('total_amount');
            $table->boolean('is_paid');
            $table->string('delivery_fee');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('g_cash_paids');
    }
};
