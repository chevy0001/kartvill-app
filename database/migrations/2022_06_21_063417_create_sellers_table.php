<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sellers', function (Blueprint $table) {
            $table->id();
            $table->string('seller_id')->nullable();
            $table->string('store_name')->nullable();
            $table->string('seller_name')->nullable();
            $table->string('seller_email')->nullable();
            $table->string('seller_contact')->nullable();
            $table->string('country')->nullable();
            $table->string('province_code')->nullable();
            $table->string('city')->nullable();
            $table->string('store_address')->nullable();
            $table->string('warehouse_address')->nullable();
            $table->string('cashier_number')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('commission')->nullable();
            $table->string('store_type')->nullable();
            $table->boolean('isKartvillage')->default(0);
            $table->timestamps();
            
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sellers');
    }
};
