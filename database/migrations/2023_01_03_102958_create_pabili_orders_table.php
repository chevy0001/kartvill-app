<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pabili_orders', function (Blueprint $table) {
            $table->id();
            $table->string("order_id");
            $table->string("shop_address")->nullable();;
            $table->string("latitude")->nullable();;
            $table->string("longitude")->nullable();
            $table->string("rider_id")->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pabili_orders');
    }
};
