<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('redeemable_items', function (Blueprint $table) {
            $table->id();
            $table->string('item_name');
            $table->string('image_url');
            $table->string('description')->nullable();
            $table->string('vendor_name');
            $table->string('vendor_link');
            $table->string('points');
            $table->string('price');
            $table->boolean('isActive')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('redeemable_items');
    }
};
