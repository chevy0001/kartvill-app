<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kartvill_settings', function (Blueprint $table) {
            $table->id();
            $table->string('default_kartvill_commission')->nullable();
            $table->string('default_rider_commission')->nullable();
            $table->string('delivery_rate')->nullable();
            $table->string('additional_delivery_rate')->nullable();
            $table->string('minimum_kilometer')->nullable();
            $table->string('minimum_kilometer_rate')->nullable();
            $table->string('bonus_delivery_fee')->nullable();
            $table->timestamps();
        });
    }

       
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kartvill_settings');
    }
};
