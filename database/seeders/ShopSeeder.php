<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ShopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            DB::table('shopify_installers')->insert([
                'shop_url' => 'kartvill-sms.myshopify.com',
                'access_token' => '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',
            ] 
        );

        DB::table('shopify_installers')->insert([
            'shop_url' => 'market-spays.myshopify.com',
            'access_token' => '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',
        ] 
    );

            
    }
}
