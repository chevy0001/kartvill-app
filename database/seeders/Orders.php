<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class Orders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shopify_orders')->insert([
            'checkout_id'=>22772812251293,
            'order_number'=>1232,
            'customer_id'=>6061797933213,
            'product_id'=>7322159022237,
            'vendor'=>'test\'s Store',
            'product_name'=>'Slipper',
            'product_price'=>'230.00',
            'product_quantity'=>'1',
            'presentment_currency'=>'PHP',
            'total_discount'=>'0.00',
            'financial_status'=>'pending',
            'commission'=>'34.5',
            'vendor_net'=>'195.5',
            'created_at'=>'2022-06-13 06:32:30',
            'updated_at'=>'2022-06-13 06:32:30'
            ] 
        );

        DB::table('shopify_orders')->insert([
            'checkout_id'=>22772843085981,
            'order_number'=>1233,
            'customer_id'=>6061797933213,
            'product_id'=>7356732801181,
            'vendor'=>'Art\'sy Cakes Gensan',
            'product_name'=>'Baby Back Ribs',
            'product_price'=>'212.75',
            'product_quantity'=>'1',
            'presentment_currency'=>'PHP',
            'total_discount'=>'0.00',
            'financial_status'=>'pending',
            'commission'=>'31.9125',
            'vendor_net'=>'180.8375',
            'created_at'=>'2022-06-13 06:32:30',
            'updated_at'=>'2022-06-13 06:32:30'
            ] 
        );

    }
}
