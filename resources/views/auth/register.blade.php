@extends('layouts.master-without-nav')

@section('title')
    @lang('translation.Register')
@endsection
@section('css')
    <link href="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('body')

    <body>
    @endsection

    @section('content')

        <div class="account-pages my-5 pt-sm-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card overflow-hidden">
                            <div class="bg-primary bg-soft">
                                <div class="row">
                                    <div class="col-7">
                                        <div class="text-primary p-4">
                                            <h5 class="text-primary">Free Register</h5>
                                            <p>Kartvill Registration</p>
                                        </div>
                                    </div>
                                    <div class="col-5 align-self-end">
                                        <img src="{{ URL::asset('/assets/images/profile-img.png') }}" alt=""
                                            class="img-fluid">
                                    </div>
                                </div>
                            </div>
                            <div class="card-body pt-0">
                                <div>
                                    <a href="index">
                                        <div class="avatar-md profile-user-wid mb-4">
                                            <span class="avatar-title rounded-circle bg-light">
                                                <img src="{{ URL::asset('/assets/images/logo.svg') }}" alt=""
                                                    class="rounded-circle" height="34">
                                            </span>
                                        </div>
                                    </a>
                                </div>
                                <div class="p-2">
                                    <form method="POST" class="form-horizontal" action="{{ route('register') }}" enctype="multipart/form-data">
                                        @csrf
                                        @method("POST")
                                        <div class="mb-3">
                                            <label for="first_name" class="form-label">First Name</label>
                                            <input type="text" class="form-control @error('first_name') is-invalid @enderror" id="first_name"
                                            value="{{ old('first_name') }}" name="first_name" placeholder="First name" autofocus required>
                                            @error('first_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="last_name" class="form-label">Last Name</label>
                                            <input type="text" class="form-control @error('last_name') is-invalid @enderror" id="last_name"
                                            value="{{ old('last_name') }}" name="last_name" placeholder="Last Name" autofocus required>
                                            @error('last_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="useremail" class="form-label">Email</label>
                                            <input type="email" class="form-control @error('email') is-invalid @enderror" id="useremail"
                                            value="{{ old('email') }}" name="email" placeholder="Enter email" autofocus required>
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="mb-3">
                                            <label for="phone_number" class="form-label">Phone Number</label>
                                            <input type="text" class="form-control @error('phone_number') is-invalid @enderror"
                                            value="{{ old('phone_number') }}" id="phone_number" name="phone_number" autofocus required
                                                placeholder="Phone Number">
                                            @error('phone_number')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        
                                        <div class="mb-3">
                                            <label for="store_name" class="form-label">Store Name <small>*If you are a vendor please enter your Store Name Here</small></label>
                                            <select name="store_id" id="store_id" class="form-control">
                                                <option value="null">None</option>
                                               @foreach ($sellers as $seller )
                                               <option value="{{ $seller->seller_id }}">{{ $seller->store_name }}</option>
                                               @endforeach 
                                            </select>
                                        </div>

                                        <div class="mb-3">
                                            <label for="userpassword" class="form-label">Password</label>
                                            <input type="password" class="form-control @error('password') is-invalid @enderror" id="userpassword" name="password"
                                                placeholder="Enter password" autofocus required>
                                                @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="mb-3">
                                            <label for="confirmpassword" class="form-label">Confirm Password</label>
                                            <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" id="confirmpassword" name="password_confirmation"
                                            name="password_confirmation" placeholder="Enter Confirm password" autofocus required>
                                            @error('password_confirmation')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="street_address" class="form-label">Street Address</label>
                                            <input  class="form-control" type="text"  id="street_address" name="street_address" placeholder="Enter Street Address" :value="old('street_address')"  required />
                                            
                                        </div>
                                        <div class="mb-3">
                                            <label for="city" class="form-label">City</label>
                                            <input  class="form-control" type="text" id="city"  name="city"  placeholder="Enter City" value="General Santos City"  required />
                                        </div>
                                        <div class="mb-3">
                                            <label for="formrow-inputState" class="form-label">Province</label>
                                            
                                                <select name="province" id="province" class="form-control select2">
                                                    @foreach ( $provinces as $province )
                                                    <option value="{{ $province->province }}">{{ $province->province }}</option>
                                                    @endforeach
                                                </select>
                                            
                                        </div>
                                        <div class="mb-3">
                                            <label for="country" class="form-label">Country</label>
                                            <input class="form-control"  type="text" id="country" name="country" :value="old('country')" placeholder="Enter Country" value="Philippines" required />
                                        </div>
                                        <div class="mb-3">
                                            <label for="zip" class="form-label">Zip</label>
                                            <input class="form-control" type="text"  id="zip" name="zip" :value="old('zip')"  placeholder="Enter Zip" required />
                                           
                                        </div>
                                        <div class="mb-3">
                                            <label for="formrow-inputState" class="form-label">Vehicle Type</label>
                                            
                                                <select name="vehicle_type" id="vehicle_type" class="form-select">
                                                    <option value="n/a" >N/A</option>
                                                    <option value="bicycle" >Bicycle</option>
                                                    <option value="motorcycle">Motorcycle</option>
                                                    <option value="car">Car</option>
                                                    
                                                </select>
                                            
                                        </div>
                                        <div class="mb-3">
                                            <label for="plate_number" class="form-label">Plate Number</label>
                                            <input  class="form-control" type="text" id="plate_number" name="plate_number" placeholder="Enter Plate Number" :value="old('plate_number')"   />
                                        </div>


                                        {{-- <div class="mb-3">
                                            <label for="userdob">Date of Birth</label>
                                            <div class="input-group" id="datepicker1">
                                                <input type="text" class="form-control @error('dob') is-invalid @enderror" placeholder="dd-mm-yyyy"
                                                    data-date-format="dd-mm-yyyy" data-date-container='#datepicker1' data-date-end-date="0d" value="{{ old('dob') }}"
                                                    data-provide="datepicker" name="dob" autofocus required>
                                                <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                @error('dob')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div> --}}

                                       
                                        <div class="mt-4 d-grid">
                                            <button class="btn btn-primary waves-effect waves-light"
                                                type="submit">Register</button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                        <div class="mt-5 text-center">

                            <div>
                                <p>Already have an account ? <a href="{{ url('login') }}" class="fw-medium text-primary">
                                        Login</a> </p>
                                <p>© <script>
                                        document.write(new Date().getFullYear())

                                    </script> Kartvill
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    @endsection
@section('script')
    <script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/select2/select2.min.js') }}"></script>
@endsection
