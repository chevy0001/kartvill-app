<x-admin-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            
            <form action="{{ route('post.sales.weekly') }}" method="POST">
                @csrf
                Select Date  <input type="datetime-local" id="date" name="date"
                class="transition duration-150 ease-in-out appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                <input type="text" placeholder="Enter Store" value="{{ $vendor }}" id="vendor" name="vendor" class=" transition duration-150 ease-in-out appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                <button type="submit" value="Submit" class="px-4 m-1 py-2 bg-indigo-500 hover:bg-indigo-700 rounded-lg text-white">  Submit</button>   
                
            </form>
<div><h1> Weekly Sales Report {{ $startOfWeek }} to {{ $endOfWeek }}</h1></div>            
<div class="relative overflow-x-auto shadow-md sm:rounded-lg">
    <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
        <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
                <th scope="col" class="px-6 py-3">
                    Vendor Name
                </th>
                
                
                
                <th scope="col" class="px-6 py-3">
                    Vendor Gross
                </th>
                <th scope="col" class="px-6 py-3">
                    Discount
                </th>
                <th scope="col" class="px-6 py-3">
                    Total Commission
                </th>
                <th scope="col" class="px-6 py-3">
                    Vendor Net
                </th>
                <th scope="col" class="px-6 py-3">
                    Date   
                </th>
                
            </tr>
        </thead>
    <tbody>
        @foreach ($sales as $report )
            <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                <th scope="row" class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">
                    <a href="/vendor/weekly-sales/{{ $report->vendor }}/{{ date('Y-m-d', strtotime($report->created_at)); }}">{{ $report->vendor }}</a>
                </th>
                
                
                
                <td class="px-6 py-4">
                    {{ $report->vendor_gross }}
                </td>
                <td class="px-6 py-4">
                    {{ $report->total_discount }}
                </td>
                <td class="px-6 py-4">
                    {{ $report->commission }}
                </td>
                <td class="px-6 py-4">
                    {{ $report->total_sales }}
                </td>

                <td class="px-6 py-4">
                    {{ date('d-m-Y', strtotime($report->created_at)) }}
                </td>

                
            </tr>
        @endforeach
    </tbody>
    </table>
    </div>
    
        </div>
    </div>
</x-admin-layout>
  
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js">
    </script>
    <script type="text/javascript">
    
        var route = "{{ url('autocomplete-search') }}";
        $('#vendor').typeahead({
            source: function (query, process) {
                return $.get(route, {
                    query: query
                }, function (data) {
                    return process(data);
                });
            }
        });
    </script>