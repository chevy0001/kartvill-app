<x-admin-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            
            <form action="{{ route('post.sales.monthly') }}" method="POST">
                @csrf

                Select Month  <select id="date" name="month" 
                class="transition duration-150 ease-in-out appearance-none bg-white border border-gray-400 rounded-md py-2 pl-2   
                text-base leading-normal transition duration-150 ease-in-out lg:text-lg sm:leading-5">
                
                <option value="{{ $month }}">{{ $monthName }}</option>
                <option value="01">January</option>
                <option value="02">February</option>
                <option value="03">March</option>
                <option value="04">April</option>   
                <option value="05">May</option>
                <option value="06">June</option>
                <option value="07">July</option>
                <option value="08">August</option>
                <option value="09">September</option>
                <option value="10">October</option>
                <option value="11">November</option>
                <option value="12">December</option>
            </select>
            Select Year  
            <select id="year" name="year" 
                class="transition duration-150 ease-in-out appearance-none bg-white border border-gray-400 rounded-md py-2 pl-2   
                text-base leading-normal transition duration-150 ease-in-out lg:text-lg sm:leading-5">
                <option value="{{ $year }}">{{ $year }}</option>
              <option value="2021">2021</option>
              <option value="2022">2022</option>
              <option value="2023">2023</option>
              <option value="2024">2024</option>
              <option value="2025">2025</option>
            </select>

                
                <input type="text" placeholder="Enter Store" id="vendor" name="vendor" value="{{ $vendor ?? '' }}" class=" transition duration-150 ease-in-out appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                <button type="submit" value="Submit" class="px-4 m-1 py-2 bg-indigo-500 hover:bg-indigo-700 rounded-lg text-white">  Submit</button>   
                
            </form>
            <div><h1>Monthly Sales Report of the Month of {{ $monthName }}  </h1></div>       
<div class="relative overflow-x-auto shadow-md sm:rounded-lg">
    <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
        <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
                <th scope="col" class="px-6 py-3">
                    Vendor Name
                </th>
                
                
                
                <th scope="col" class="px-6 py-3">
                    Vendor Gross
                </th>
                <th scope="col" class="px-6 py-3">
                    Discount
                </th>
                <th scope="col" class="px-6 py-3">
                    Total Commission
                </th>
                <th scope="col" class="px-6 py-3">
                    Vendor Net
                </th>
                <th scope="col" class="px-6 py-3">
                    Date   
                </th>
                
            </tr>
        </thead>
    <tbody>
        @foreach ($sales as $report )
            <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                <th scope="row" class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">
                <a href="/vendor/monthly-sales/{{ $report->vendor }}/{{ date('m', strtotime($report->created_at)); }}"> {{ $report->vendor }} </a>
                </th>
                
                
                
                <td class="px-6 py-4">
                    {{ $report->vendor_gross }}
                </td>
                <td class="px-6 py-4">
                    {{ $report->total_discount }}
                </td>
                <td class="px-6 py-4">
                    {{ $report->commission }}
                </td>
                <td class="px-6 py-4">
                    {{ $report->total_sales }}
                </td>

                <td class="px-6 py-4">
                    {{ date('d-m-Y', strtotime($report->created_at)) }}
                </td>

                
            </tr>
        @endforeach
    </tbody>
    </table>
    </div>
    
        </div>
    </div>
</x-admin-layout>
  
