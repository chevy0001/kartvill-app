@extends('layouts.master')

@section('title') @lang('Rider Wallet Balance') @endsection

@section('css')
    <!-- Responsive Table css -->
    <link href="{{ URL::asset('/assets/libs/rwd-table/rwd-table.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Orders @endslot
        @slot('title') Order Details @endslot
    @endcomponent
    <form class="app-search d-none d-lg-block">
        <div class="position-relative">
            <input type="text" class="form-control" placeholder="@lang('translation.Search')">
            <span class="bx bx-search-alt"></span>
        </div>
    </form>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-rep-plugin">
                        <div class="table-responsive mb-0" data-pattern="priority-columns">
                            <table id="tech-companies-1" class="table table-striped">
                                <thead>
                                    <tr>
                                        {{-- <th>Order ID</th> --}}
                                        
                                        <th data-priority="1">#</th>
                                        <th data-priority="1">Full Name</th>
                                        <th data-priority="1">Balance Amount</th>
                                        <th data-priority="1">Date</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($wallet_balance as $index => $balance )
                                    <tr >
                                        {{-- <th>{{ $order->order_id }}</th> --}}
                                        <td>{{ $index + 1 }}</td>
                                        <td>{{ $balance->first_name }} {{ $balance->last_name }}</td>
                                        <th>{{ number_format($balance->balance,2) }}</th>
                                        <td>{{ $balance->updated_at }}</td>
                                        
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            
                        </div>

                    </div>

                    
                </div>
            </div>
        </div> <!-- end col -->
        
    </div> <!-- end row -->
    <div class="row">
        <div class="col-lg-12 pagination pagination-rounded justify-content-center mt-4">
            {{ $wallet_balance->links()  }}
        </div>
    </div>
    
@endsection
@section('script')
    <!-- Responsive Table js -->
    <script src="{{ URL::asset('/assets/libs/rwd-table/rwd-table.min.js') }}"></script>

    <!-- Init js -->
    <script src="{{ URL::asset('/assets/js/pages/table-responsive.init.js') }}"></script>

    <script>
     
      



       
    </script>
@endsection
