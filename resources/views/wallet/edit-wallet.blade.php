@extends('layouts.master')

@section('title') @lang('Edit Wallet') @endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Store @endslot
@slot('title') Settings @endslot
@endcomponent

@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif

<div class="row">
    <div class="col-xl-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Wallet of {{ $user->first_name }} {{ $user->last_name }}</h4>
                <h4 class="card-title mb-4">Current Balance {{ $wallet_balance }}</h4>

              
                    <div>
                        <form action="/updatewallet-{{ $user->id }}" method="post">
                            @csrf
                            @method('POST')
                            Add load here
                            <input type="hidden"  class="form-control" name="current_balance" value="{{ $wallet_balance }}" >
                            <input type="number"  class="form-control" name="amount" value="1000" required>
                            <input type="submit" class="btn btn-success mt-3" value="Add Load">
                        </form>
                    </div>
               
            </div>
            <!-- end card body -->
        </div>
        <!-- end card -->
    </div>
   
</div>
<!-- end row -->
@endsection