<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title" key="t-user">Sales</li>
                    <li>
                        <a href="javascript: void(0);" class="has-arrow waves-effect">
                            <i class="bx bx-home-circle"></i>
                            <span key="t-home">@lang('Home')</span>
                        </a>
                        <ul class="sub-menu" aria-expanded="false">
                            <li><a href="/" key="t-home">@lang('Home')</a></li>
                        </ul>
                        
                    </li>
                    
                </li>
            </ul>   
                
        </div>
        
       
    </div>
</div>

<!-- Left Sidebar End -->
