<!-- JAVASCRIPT -->
<script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBw6yJJRuziDaIBcadBZHIEHS0vAbNzlqU&callback=initMap"></script>
<script src="{{ URL::asset('assets/libs/jquery/jquery.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/bootstrap/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/metismenu/metismenu.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/simplebar/simplebar.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/node-waves/node-waves.min.js')}}"></script>
<script>
    $('#change-password').on('submit',function(event){
        event.preventDefault();
        var Id = $('#data_id').val();
        var current_password = $('#current-password').val();
        var password = $('#password').val();
        var password_confirm = $('#password-confirm').val();
        $('#current_passwordError').text('');
        $('#passwordError').text('');
        $('#password_confirmError').text('');
        $.ajax({
            url: "{{ url('update-password') }}" + "/" + Id,
            type:"POST",
            data:{
                "current_password": current_password,
                "password": password,
                "password_confirmation": password_confirm,
                "_token": "{{ csrf_token() }}",
            },
            success:function(response){
                $('#current_passwordError').text('');
                $('#passwordError').text('');
                $('#password_confirmError').text('');
                if(response.isSuccess == false){ 
                    $('#current_passwordError').text(response.Message);
                }else if(response.isSuccess == true){
                    setTimeout(function () {   
                        window.location.href = "{{ route('root') }}"; 
                    }, 1000);
                }
            },
            error: function(response) {
                $('#current_passwordError').text(response.responseJSON.errors.current_password);
                $('#passwordError').text(response.responseJSON.errors.password);
                $('#password_confirmError').text(response.responseJSON.errors.password_confirmation);
            }
        });
    });
</script>

@yield('script')

<!-- App js -->
<script src="{{ URL::asset('assets/js/app.min.js')}}"></script>

<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
    
     
            axios.get('/topup-count')
            .then(function (response) {
                
                document.getElementById('topup-count').innerHTML = response['data'];
                // console.log(response['data']);
            })
            .catch(function (error) {
                // handle error
                // console.log(error);
            })
            .then(function () {
                // always executed
            });
        
</script>

<script>
     var countorder;
     var x = document.getElementById("myAudio"); 
     var cookie_val;

            //Count new orders
            setInterval(() => {
                axios.get('/countOrders')
                .then(function (response) {
                    // console.log(response['data']);

                    countorder = response['data'];

                    cookie_val = getCookie("cookie_val");
                    
                    if(cookie_val < countorder){
                    
                        x.play();
                        setCookie("cookie_val",countorder,1);
                        // console.log("play and vibrate");
                        navigator.vibrate(2000);
                    
                    }
                    else if(cookie_val > countorder){
                        x.pause();
                        setCookie("cookie_val",countorder,1);
                        // console.log("pause because the order is decreasing");
                    }
                    
                    else if(cookie_val == countorder){
                        x.pause();
                        // console.log("pause");
                    }
                    else{
                        x.pause();
                    }
                    document.getElementById('order-count').innerHTML = response['data'];
                    document.getElementById('order-count1').innerHTML = response['data'];  
                })
                .catch(function (error) {
                    
                })
                .then(function () {
                
                });

            }, 2000);    
    
    
            //Count redeem rewards
            setInterval(() => {
                axios.get('/count-redeem')
                .then(function (response) {
                    // console.log(response['data']);
                    countorder = response['data'];
                    document.getElementById('redeem-request').innerHTML = response['data'];
                    
                })
                .catch(function (error) {
                    
                })
                .then(function () {
                
                });

            }, 2000);    


function setCookie(cname,cvalue,exdays) {
  const d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  let expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  let name = cname + "=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  for(let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}


function vibrate(){
    navigator.vibrate(2000);
}

</script>


<script>
// get the latest coordinates of the rider
var riderLong = 0, riderLat = 0;
var delay = 1000;
if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
        riderLat = position.coords.latitude;
        riderLong = position.coords.longitude;

        setInterval(() => {
            axios.post('/save-current-coords', {    
            latitude: riderLat,
            longitude: riderLong
           
            })
            .then(function (response) {
                // console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });
        }, 1000);
           

      
       
    }, function() {
         handleLocationError(true, infoWindow, map.getCenter());
    });
   
}
 else {
    // Browser doesn't support Geolocation
    alert('Location is not activated');
}

</script>

@yield('script-bottom')