<!-- JAVASCRIPT -->
<script src="{{ URL::asset('assets/libs/jquery/jquery.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/bootstrap/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/metismenu/metismenu.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/simplebar/simplebar.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/node-waves/node-waves.min.js')}}"></script>


@yield('script')

<!-- App js -->
<script src="{{ URL::asset('assets/js/app.min.js')}}"></script>

<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

<script>
    
        axios.get('/order-count')
        .then(function (response) {
            document.getElementById('cartCount').innerHTML = response['data']['order_count'];
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
        .then(function () {
            // always executed
        });

        function removeOrder(order_id){
                axios.post('/remove-order', {
                id:order_id,    
            })
            .then(function (response) {
                $( "#cart-notification" ).load(window.location.href + " #cart-notification" );
                document.getElementById('cartCount').innerHTML = response['data']['order_count']['original']['order_count'];
            })
            .catch(function (error) {
                console.log(error);
            });
        
        }

       
</script>

@yield('script-bottom')