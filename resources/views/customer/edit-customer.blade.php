@extends('layouts.master')

@section('title') @lang('translation.Form_Layouts') @endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') <a href="/customers">Customer</a> @endslot
@slot('title') Edit Customer @endslot
@endcomponent

<div class="row">
    <div class="col-xl-8">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Edit Profile of {{ ucfirst($customer->first_name) }} {{ ucfirst($customer->last_name) }}</h4>

                <form method="post" action="/customer-update-{{ $customer->id }}">
                        @csrf
                    @method('POST')
                    <div class="row mb-4">
                        <x-label for="first_name"  class="col-sm-3 col-form-label" :value="__('First Name')" />
                        <div class="col-sm-9">
                            <x-input id="first_name" class="form-control" type="text" name="first_name" id="first_name" :value="old('first_name')" value="{{ ucfirst($customer->first_name) }}" placeholder="Enter First Name" required autofocus />   
                        </div>
                    </div>
                    <div class="row mb-4">
                        <x-label for="last_name"  class="col-sm-3 col-form-label" :value="__('Last Name')" />
                        <div class="col-sm-9">
                            <x-input id="last_name" class="form-control" type="text" id="last_name" name="last_name" :value="old('last_name')" value="{{ ucfirst($customer->last_name) }}" placeholder="Enter Last Name" required /> 
                        </div>
                    </div>
                    
                    <div class="row mb-4">
                        <x-label for="email" class="col-sm-3 col-form-label" :value="__('Email')" />
                            
                        <div class="col-sm-9">
                            <x-input id="email" class="form-control" type="email" id="email" name="email" :value="old('email')" value="{{ $customer->email_address }}"  placeholder="Enter Email Address" required />
                        </div>
                    </div>

                    <div class="row mb-4">
                        <x-label for="phone_number" class="col-sm-3 col-form-label" :value="__('Phone Number')" />     
                        <div class="col-sm-9">
                            <x-input id="phone_number" class="form-control" type="text"  id="phone_number" name="phone_number" placeholder="Enter Phone Number" :value="old('phone_number')" value="{{ $customer->phone_number }}" required /> 
                        </div>
                    </div>

                    <div class="row mb-4">
                        <x-label for="address" class="col-sm-3 col-form-label" :value="__('Full Address')" />
                        <div class="col-sm-9">
                            <x-input id="address" class="form-control" type="text"  id="address" name="address" placeholder="Enter Full Address" :value="old('address')" value="{{ $customer->address }}" required />
                        </div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-sm-9">
                            <div>
                                <button type="submit" class="btn btn-primary w-md">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- end card body -->
        </div>
        <!-- end card -->
    </div>
    <!-- end col -->
</div>
<!-- end row -->

@endsection