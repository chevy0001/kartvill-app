@extends('layouts.master')

@section('title') @lang('translation.User_List') @endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Contacts @endslot
        @slot('title') Customer List @endslot
    @endcomponent
    <form class="app-search d-none d-lg-block" method="POST" action="/customer-search">
        @csrf
        @method('POST')
        <div class="position-relative">
            <input type="text" class="form-control" name="input" placeholder="@lang('translation.Search')" >
            <span class="bx bx-search-alt"></span>
        </div>
    </form>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table align-middle table-nowrap table-hover">
                            <thead class="table-light">
                                <tr>
                                    <th scope="col" style="width: 70px;">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Contact</th>
                                    <th scope="col">Address</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ( $customers as $customer )
                                <tr>
                                    <td>
                                        <div class="avatar-xs">
                                            <span class="avatar-title rounded-circle">
                                                C
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <h5 class="font-size-14 mb-1"><a href="" class="text-dark">{{ $customer->full_name }}</a></h5>
                                        <p class="text-muted mb-0">Customer</p>
                                    </td>
                                    <td>{{ $customer->email_address }}</td>
                                    <td>
                                        <div>
                                            <a href="#" class=" font-size-11 m-1">{{ $customer->phone_number }}</a>
                                            
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <p class="text-muted mb-0">{{ $customer->address }}</p>
                                        </div>
                                    </td>
                                    
                                    
                                    <td>
                                        <ul class="list-inline font-size-20 contact-links mb-0">
                                            <li class="list-inline-item px-2">
                                                <a href="" title="Message"><i class="bx bx-message-square-dots"></i></a>
                                            </li>
                                            <li class="list-inline-item px-2">
                                                <a href="/user-profile-{{ $customer->id }}" title="Profile"><i class="bx bx-user-circle"></i></a>
                                            </li>
                                            <li class="list-inline-item px-2">
                                                <a href="/customer-update-{{ $customer->id }}" title="Edit"><i class="bx bx-edit"></i></a>
                                            </li>
                                            <li class="list-inline-item px-2">
                                                <a href="/delete-user-{{ $customer->id }}" title="trash"><i class="bx bx-trash"></i></a>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 pagination pagination-rounded justify-content-center mt-4">
                            {{-- {{ $users->links()  }} --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection


