@extends('layouts.master')

@section('title') @lang('Post Item Prize') @endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Forms @endslot
@slot('title') Post Item Prize @endslot
@endcomponent


@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif
<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                

                <form method="post" action="/item-redeem-post">
                    @csrf
                    @method('POST')
                    <div class="row mb-4">
                        <label for="" class="col-sm-3 col-form-label">Item Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="item_name" id="" placeholder="Item Name " required>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label for="" class="col-sm-3 col-form-label">Image Url</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="image_url" id="" placeholder="Image Url " required>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label for="" class="col-sm-3 col-form-label">Vendor Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="vendor_name" id="" placeholder="Vendor Name " required>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label for="" class="col-sm-3 col-form-label">Vendor Link</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="vendor_link" id="" placeholder="Vendor Link " required>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label for="" class="col-sm-3 col-form-label">Description</label>
                        <div class="col-sm-9">
                            <textarea  class="form-control" name="description" height="3" width="20" id="" ></textarea>    
                            </div>
                    </div>
                    <div class="row mb-4">
                        <label for="" name="price" class="col-sm-3 col-form-label">Price</label>
                        <div class="col-sm-9">
                            <input type="number" min="1" step="0.1" class="form-control" name="price" id="price" placeholder="Price" required>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label for="" name="points" class="col-sm-3 col-form-label">Points</label>
                        <div class="col-sm-9">
                            <input type="number" min="1" step="0.1" class="form-control" name="points" id="points" placeholder="Points" required>
                        </div>
                    </div>
                    
                    
                    <div class="row justify-content-end">
                        <div class="col-sm-9">
                            <div>
                                <button type="submit" class="btn btn-primary w-md">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- end card body -->
        </div>
        <!-- end card -->
    </div>
    <!-- end col -->
    <!-- end col -->

    
    <!-- end col -->
</div>
<!-- end row -->

@endsection