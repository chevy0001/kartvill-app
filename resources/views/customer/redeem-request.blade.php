@extends('layouts.master')

@section('title') @lang('translation.Responsive_Table') @endsection

@section('css')
    <!-- Responsive Table css -->
    <link href="{{ URL::asset('/assets/libs/rwd-table/rwd-table.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Topup @endslot
        @slot('title') Topup Request @endslot
    @endcomponent
    <form class="app-search d-none d-lg-block">
        <div class="position-relative">
            <input type="text" class="form-control" placeholder="@lang('translation.Search')">
            <span class="bx bx-search-alt"></span>
        </div>
    </form>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-rep-plugin">
                        <div class="table-responsive mb-0" data-pattern="priority-columns">
                            <table id="tech-companies-1" class="table table-striped">
                                <thead>
                                    <tr>
                                        {{-- <th>Order ID</th> --}}
                                        <th >#</th>
                                        <th data-priority="1">Full Name</th>
                                        <th data-priority="1">Phone Number</th>
                                        <th data-priority="1">Address</th>
                                        <th data-priority="1">Item Name</th>
                                        <th data-priority="1">Image</th>
                                        <th data-priority="1">Vendor</th>
                                        <th data-priority="1">Points</th>
                                        <th data-priority="1">Price</th>
                                        <th data-priority="1">Date</th>
                                        <th data-priority="1">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($redeem_requests as $index => $request )
                                    <tr >
                                        {{-- <th>{{ $order->order_id }}</th> --}}
                                        <td>{{ $index + 1 }}</td>
                                        <td>{{ $request->full_name }}</td>
                                        <th>{{ $request->phone_number }}</th>
                                        <th>{{ $request->address }}</th>
                                        <td>{{ $request->item_name }}</td>
                                        <td>Image</td>
                                        <td>{{ $request->vendor_name }}</td>
                                        <td>{{ $request->point_redeemed }}</td>
                                        <td>{{ $request->price }}</td>
                                        <td>{{ $request->created_at }}</td>
                                        <td>
                                            <li class="list-inline-item px-2">
                                               <form action="/approve-" method="post">
                                                @csrf
                                                @method('POST')
                                                    <input type="hidden" name="hash" value="">
                                                    <input type="hidden" name="request_id" value="{{ $request->id }}">
                                                    <button  id="but{{ $request->id }}" class="btn btn-success waves-effect waves-light" 
                                                        title="Approve" data-hash="" type="submit"><i class="bx bx-check-double font-size-16 align-middle me-2"></i>Approve</button>
                                                </form>
                                            </li>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            
                        </div>

                    </div>

                    
                </div>
            </div>
        </div> <!-- end col -->
        
    </div> <!-- end row -->
    <div class="row">
        <div class="col-lg-12 pagination pagination-rounded justify-content-center mt-4">
            {{-- {{ $orders->links()  }} --}}
        </div>
    </div>
    
@endsection
@section('script')
    <!-- Responsive Table js -->
    <script src="{{ URL::asset('/assets/libs/rwd-table/rwd-table.min.js') }}"></script>

    <!-- Init js -->
    <script src="{{ URL::asset('/assets/js/pages/table-responsive.init.js') }}"></script>

    <script>
     
      



       
    </script>
@endsection
