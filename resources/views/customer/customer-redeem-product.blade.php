@extends('layouts.master-without-nav')

@section('title')
    @lang('Kartvill Redeem')
@endsection

@section('body')

    <body>
    @endsection

    @section('content')

        <div class="home-btn d-none d-sm-block">
            <a href="index" class="text-dark"><i class="fas fa-home h2"></i></a>
        </div>

        <section class="my-5 pt-sm-5">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <div class="home-wrapper">
                            <h2 class="mt-5" >You have <span id="remaining-points">{{ $point->remaining_points ?? '0' }}</span> available points</h2>
                            <h3 class="mt-5">Redeem Items Here</h3>
                            <p>Exchange your points with these delicious and cool items.</p>

                           
                            <div class="row">
                                @foreach ( $redeemableItems as $item)
                                    <div class="col-md-4">
                                        <div class="card mt-4 maintenance-box">
                                            <div class="card-body">
                                                <h5 class="font-size-15 text-uppercase">{{ $item->item_name }}</h5>
                                                <img class="rounded me-2" alt="200x200" width="200" src="{{ $item->image_url }}" data-holder-rendered="true"></p>
                                                <p><a class="text-muted mb-0" href="{{ $item->vendor_link ?? 'a' }}">By {{ $item->vendor_name }}</a>
                                                <p>{{ $item->description }}</p>
                                                <p class="text-muted mb-0">{{ $item->points }} Points</p>
                                            
                                                @if(!isset($point->remaining_points) || $point->remaining_points < $item->points)
                                                    <p class="text-muted mb-0">Not enough points</p>
                                                @else    
                                                    <button class="btn btn-primary" data-productid="{{ $item->id }} " data-toggle="modal" data-target="#exampleModal" 
                                                        data-productname="{{ $item->item_name }}" data-id="{{ $item->id }}" data-points="{{ $item->points }}" 
                                                        data-availpoints="{{ $point->remaining_points ?? '0' }}">Redeem</button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach 
                            </div>
                        
                            <!-- end row -->
                        </div>
                    </div>
                </div>
            </div>


            {{-- modal --}}
            {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Open modal for @mdo</button>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@fat">Open modal for @fat</button>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">Open modal for @getbootstrap</button> --}}
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                        <p> Are you sure you want to redeem the following item?</p>
                        <p id="product-name"></p>
                        <div class="input-group mb-3">
                            <input type="text" onkeyup="getEmailCode()" class="form-control" type="text" name="emailcode" 
                            id="emailcode" placeholder="Enter SMS Code" aria-label="sms" aria-describedby="sms">
                            <button class="btn btn-success input-group-text" id="btnGetEmail" onclick="sendEmail({{ $customer_id }})">
                                Get Email Code</button>
                          </div>
                      <form method="post" action="/">
                        <input type="hidden" name="productid" id="productid">
                        <input type="hidden" name="userid" id="userid" value="{{ $customer_id }}">
                        
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary" onclick="redeem()" id="btnproceed" disabled>Proceed</button>
                    </div>
                  </div>
                </div>
              </div>

              <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
              <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
              <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
              <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
              <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
              <script>
                
                $('#exampleModal').on('show.bs.modal', function (event) {
                    var button = $(event.relatedTarget) // Button that triggered the modal
                    var productname = button.data('productname') // Extract info from data-* attributes
                    var id = button.data('id');
                    var points = button.data('points')
                    var availpoints = button.data('availpoints')
                    
                    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                    var modal = $(this)
                    modal.find('.modal-title').text('Available points: ' + availpoints)
                    modal.find('#productid').val(id)
                    // modal.find('.modal-body textarea').val(points)
                    document.getElementById('product-name').innerHTML = productname + " for " + points + " Kartvill Points"
                    })
                   
                
                function redeem(){
                    var productid = document.getElementById('productid').value;
                    var userid = document.getElementById('userid').value;
                    
                    axios.post('/process-redeem', {
                        productid: productid,
                        userid:userid
                        
                    })
                    .then(function (response) {
                         if(response['status'] == 200){
                            alert('We will confirm and process your claim all you need to pay is the delivery fee. Thank you.');
                            document.getElementById('btnproceed').disabled = true;
                            document.getElementById('remaining-points').innerHTML = response['data']['remaining_points'];
                            document.getElementById('emailcode').value = "";
                        }
                        console.log(response);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                }    
                function sendEmail(id){

                    axios.post('/sendemailcode', {
                        id: id,
                        
                    })
                    .then(function (response) {
                        if(response['status'] == 200){
                            alert('Please check your email for the verification code.');
                            document.getElementById('btnGetEmail').disabled = true;
                            
                        }
                        console.log(response);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                    
                }    
                function getEmailCode(){

                    var email_code_value = document.getElementById('emailcode').value;
                    console.log(email_code_value);
                    axios.post('/getemailcode', {
                        emailcode: email_code_value,
                        
                    })
                    .then(function (response) {
                        if(response['data'] == "success"){
                           
                            document.getElementById('btnproceed').disabled = false;
                            document.getElementById('btnGetEmail').disabled = false;
                            
                        }
                        else{
                            
                            document.getElementById('btnproceed').disabled = true;
                        }
                        console.log(response['data']);
                        console.log(response);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });


                    
                }

              </script>
        </section>

    @endsection
