@extends('layouts.master-without-nav')


@section('title') @lang('Map Pin Update') @endsection

@section('content')


<div class="account-pages my-5 pt-5">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4">Please turn on your location so we can automatically locate your address.
                            <small>(This process is one time only or if needed to update.)</small>
                        </h4>
                        
                        <form method="post" action="/customer-map-update">
                            @csrf
                            @method('POST')
                            <div class="row mb-4">
                                <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Your Address Details</label>
                                <div class="col-sm-9">
                                    {{ $customer->full_name }}, {{ $customer->address }}
                                    <input type="hidden" class="form-control" name="customer_id" id="customer_id" value="{{ $customer->customer_id }}"  required>
                                    <input type="hidden" class="form-control" name="latitude" id="latitude"  required>
                                    <input type="hidden" class="form-control"name="longitude"  id="longitude"  required >
                                </div>
                            </div>  
                            <div>
                                <button type="submit" class="btn btn-success w-md">Confirm Address</button>
                                
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        
                        <div class="table-responsive" id="map" style="width:100;height:300px;" ></div>
                    </div>

                    <script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBw6yJJRuziDaIBcadBZHIEHS0vAbNzlqU&callback=initMap"></script>
                    <script>
                        // Note: This example requires that you consent to location sharing when
                        // prompted by your browser. If you see the error "The Geolocation service
                        // failed.", it means you probably did not give permission for the browser to
                        // locate you.
                        //Get initial location of every user
                        //Update/Get current location of every user
                        var map, infoWindow;
                        var cus_lat = parseFloat(@json($customer->latitude ?? '0'));
                        var cus_long = parseFloat(@json($customer->longitude ?? '0'));
                        var lati,long;
                        function initMap() {
                          
                          infoWindow = new google.maps.InfoWindow;
                  
                          // Try HTML5 geolocation.
                          if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(function(position) {
                             
                                
                                
                                if(cus_lat == 0 || cus_long == 0 || cus_lat == null || cus_long == null){
                                  lati = position.coords.latitude;
                                  long = position.coords.longitude;
                                }
                                else{
                                  lati = cus_lat;
                                  long = cus_long;
                                }
                             
                              var pos = {
                                lat: lati,
                                lng: long
                                
                              };
                              map = new google.maps.Map(document.getElementById('map'), {
                              center: {lat: lati, lng: long},
                              zoom: 15
                          });
                            document.getElementById('latitude').value = lati;
                            document.getElementById('longitude').value = long;
                            //   infoWindow.setPosition(pos);
                            // infoWindow.setContent('Location found.');

                              marker = new google.maps.Marker({
                                    position: new google.maps.LatLng(lati,long),
                                    name:name,
                                    map: map,
                                    draggable:false,
                                    title:'Customer',
                                    
                                }); 

                                window.google.maps.event.addListener(map, 'drag', function (event) {
                                marker.setPosition( map .getCenter() );
                                // var latLng = marker.latLng;
                                document.getElementById('latitude').value = marker.getPosition().lat();
                                document.getElementById('longitude').value = marker.getPosition().lng();
                            });

                        //         google.maps.event.addListener(marker, 'dragend', function(marker) {
                        //         var latLng = marker.latLng;
                        //         document.getElementById('latitude').value = latLng.lat();
                        //         document.getElementById('longitude').value = latLng.lng();
                        //  });

                            //   infoWindow.open(map);
                              map.setCenter(pos);
                            }, function() {
                              handleLocationError(true, infoWindow, map.getCenter());
                            });
                          
                            
                              
                          } else {
                            // Browser doesn't support Geolocation
                            handleLocationError(false, infoWindow, map.getCenter());
                          }
                        }
                  
                        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                          infoWindow.setPosition(pos);
                          infoWindow.setContent(browserHasGeolocation ?
                                                'Error: The Geolocation service failed.' :
                                                'Error: Your browser doesn\'t support geolocation.');
                          infoWindow.open(map);
                        }
                        
                        
                      </script>
                    


                    <!-- end card body -->
                </div>
                <!-- end card -->
            </div>
            <!-- end col -->
        </div>
    </div>
</div>
<!-- end row -->

@endsection
