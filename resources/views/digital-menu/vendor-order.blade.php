@extends('layouts.master')

@section('title') @lang('translation.Cart') @endsection

@section('css')
    <!-- bootstrap-touchspin css -->
    <link href="{{ URL::asset('/assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css') }}" rel="stylesheet" />
@endsection

@section('content')
<div class="container">
    @component('components.breadcrumb')
        @slot('li_1') Orders @endslot
        @slot('title') Order #{{ $order_id }}-Table #{{ $orders[0]->table_number ?? '0' }} @endslot
    @endcomponent

    <div class="row">
        <div class="col-xl-8">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive" id="all-orders">
                        <table class="table align-middle mb-0 table-nowrap">
                            <thead class="table-light">
                                <tr>
                                    <th>Product</th>
                                    <th>Product Desc</th>
                                    <th>Price</th>
                                    <th>Total</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                               @php
                                   $total_order = 0;
                                   $total_option_price = 0; 
                                   $count_fs_nega_1 = 0;
                               @endphp
                               @foreach ($orders as $order)
                               @php
                                   $total_order = $total_order + ($order->price - $order->discount);
                                   $total_order *= $order->quantity;

                                   if($order->order_fulfillment_status == -1){
                                    $count_fs_nega_1 += 1;
                                   }
                                   
                               @endphp
                                    <tr>
                                        <td>
                                            <img src="{{ $order->image_link }}" alt="product-img" title="product-img"
                                                class="avatar-md" />
                                        </td>
                                        <td>
                                            <h5 class="font-size-14 text-truncate">
                                                <a class="text-dark">{{ $order->title }} ({{ $order->price }})</a></h5>
                                            @if($order->discount > 0)
                                                <p class="mb-0">Promo Discount : <span class="fw-medium"> -{{ number_format($order->discount,2) }}</span></p>
                                            @endif
                                            @php
                                                $option_prices = 0;;
                                            @endphp
                                            @foreach ($order_options as $option)
                                                @if($order->option_key == $option->option_key)
                                                    <p class="mb-1 text-muted" key="options" >{{ $option->option_name }} - <span id="addon-{{ $option->id }}">
                                                        @if($option->option_price == 0)
                                                            Free
                                                        @else
                                                            @php
                                                            $total_option_price += ($option->option_price * $order->quantity) ;
                                                            $option_prices  += $option->option_price;
                                                            @endphp
                                                            
                                                            {{ number_format($option->option_price * $order->quantity,2) }}

                                                        @endif
                                                        </span>
                                                    </p>
                                                      
                                                @endif    
                                            @endforeach  
                                           
                                            <small>{{ $order->store_name }}</small>  
                                        </td>
                                        <td>
                                            {{ $order->quantity }} x {{ number_format($order->price - $order->discount,2) }}
                                        </td>
                                       
                                        <td>
                                            <span id="sub_total-{{ $order->id }}">
                                            {{ number_format(((floatval($order->price) - floatval($order->discount)) + $option_prices)* floatval($order->quantity),2)  }}
                                            </span>    
                                        </td>
                                        <td>
                                            @if($order->order_fulfillment_status < 1)
                                            <button onclick="cancelOrder('{{ $order->order_id }}',{{ $order->product_id }},{{ $order->variation_id }},'{{ $order->option_key }}')" class="btn btn-danger">
                                                <i class="bx bx-block "></i>
                                            </button>  
                                            @endif
                                        </td>
                                        
                                    </tr>  
                                    
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row mt-4">
                        <div class="col-sm-6">
                            <a href="{{ route('orders.index') }}" class="btn btn-secondary">
                                <i class="mdi mdi-arrow-left me-1"></i> Back to Orders </a>
                        </div> <!-- end col -->
                        <div class="col-sm-6">
                            
                            @if($orders->count() == $count_fs_nega_1 )
                            <div class="text-sm-end mt-2 mt-sm-0">
                                Cancelled 
                            </div>
                            @else
                            <div class="text-sm-end mt-2 mt-sm-0">
                                @if($orders[0]->order_fulfillment_status == "")
                                    <button onclick="cancelAllOrders()" class="btn btn-danger">
                                        <i class="bx bx-block me-1"></i> Cancel 
                                    </button>
                                    <button onclick="acceptOrder()" class="btn btn-success">
                                        <i class="fas fa-fire me-1"></i> Accept 
                                    </button>
                                @elseif($orders[0]->order_fulfillment_status == "1")
                                <button onclick="serve()" class="btn btn-success">
                                    <i class="fas fa-fire me-1"></i> Serve
                                </button>
                                @else
                                    Served
                                @endif
                            </div>
                            @endif
                        </div> <!-- end col -->
                    </div> <!-- end row-->
                </div>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="card" id="order-summary">
                <div class="card-body">
                    <h4 class="card-title mb-3">Order Summary</h4>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td>Sub Total  :</td>
                                    <td><span id="total">{{ number_format($total_order + $total_option_price,2)  }}</span></td>
                                </tr>
                                <tr>
                                    <td>Discount : </td>
                                    <td>- <span id="discount">{{ $orders[0]->privilege_discount ?? '0' }}</span></td>
                                </tr>
                                <tr>
                                    <td>Shipping Charge :</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <th>Grand Total : </th>
                                    <th ><span id="grand_total">{{  number_format($total_order + $total_option_price - $orders[0]->privilege_discount,2)}}</span></th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- end table-responsive -->
                </div>
            </div>
            <div class="card" id="customer-details">
              <div class="card-body">
                  <h4 class="card-title mb-3">Customer Details</h4>
                  <div class="table-responsive">
                    <table class="table mb-0">
                        <tbody>
                            <tr>
                                <td>Name: {{ $orders[0]->customer_name }}</td>
                                <td><span id="total"></span></td>
                            </tr>
                            <tr>
                                <td>Phone : {{ $orders[0]->customer_contact }}</td>
                                <td></td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
              </div>
            </div>
        </div>
        <!-- end card -->
    </div>

<!-- end row -->  
</div>
<!-- init js -->

<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
    function cancelOrder(order_id,product_id,variation_id,option_key){
        console.log(order_id);
        console.log(product_id);
        console.log(variation_id);
        console.log(option_key);
        
        axios.post('/cancel-product-order', {
            order_id:order_id,    
            product_id: product_id,
            variation_id: variation_id,
            option_key:option_key,
            })
            .then(function (response) {
                console.log(response);
                alert('Order succesfully cancelled!');
                // location.reload();
           
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    function cancelAllOrders(){

        let orders = @json($orders);

        console.log(orders.length);
        let count = orders.length;
        orders.forEach(order => {
            axios.post('/cancel-all-orders', {
            order_id:order.order_id,    
            product_id: order.product_id,
            variation_id: order.variation_id,
            option_key:order.option_key,
            count:count
            })
            .then(function (response) {
                console.log(response);
                // alert('Order succesfully cancelled!');
                // location.reload();
           
            })
            .catch(function (error) {
                console.log(error);
            });
        });        
    }

function acceptOrder(){

    let orders = @json($orders);
    console.log(orders.length);
    let count = orders.length;
    orders.forEach(order => {
        axios.post('/accept-order', {
        order_id:order.order_id,    
        product_id: order.product_id,
        variation_id: order.variation_id,
        option_key:order.option_key,
        count:count
        })
        .then(function (response) {
            console.log(response);
            // alert('Order succesfully cancelled!');
            // location.reload();
    
        })
        .catch(function (error) {
            console.log(error);
        });
    });        
}

function serve(){
    let orders = @json($orders);
    console.log(orders.length);
    let count = orders.length;
    orders.forEach(order => {
        axios.post('/serve-order', {
        order_id:order.order_id,    
        product_id: order.product_id,
        variation_id: order.variation_id,
        option_key:order.option_key,
        count:count
        })
        .then(function (response) {
            console.log(response);
            // alert('Order succesfully cancelled!');
            // location.reload();

        })
        .catch(function (error) {
            console.log(error);
        });
    });        
}

    
</script>


@endsection
@section('script')


    
    
@endsection
