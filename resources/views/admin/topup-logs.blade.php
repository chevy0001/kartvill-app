@extends('layouts.master')

@section('title') @lang('translation.User_List') @endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Logs @endslot
        @slot('title') Logs @endslot
    @endcomponent
    
    <div class="row">
        <div class="col-lg-12 pagination pagination-rounded justify-content-center mt-4">
            {{ $logs->links()  }}
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table align-middle table-nowrap table-hover">
                            <thead class="table-light">
                                <tr>
                                    <th scope="col" style="width: 70px;">#</th>
                                    <th scope="col"> Rider Name</th>
                                    <th scope="col">Prev Balance</th> 
                                    <th scope="col">Topup Amount</th> 
                                    <th scope="col">New Balance</th> 
                                    <th scope="col">Status</th> 
                                    <th scope="col">Request Code</th>    
                                    <th scope="col">Created Date</th> 
                                    <th scope="col">Updated Date</th>      
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ( $logs as $log )
                                <tr>
                                    <td>
                                        <h5 class="font-size-14 mb-1">{{ $log->id }}</h5>
                                    </td>
                                    <td>
                                        <span>{{ $log->first_name }} {{ $log->last_name }}</span>
                                    </td>
                                    <td>
                                        <span>{{ number_format($log->prev_balance,2) ?? '' }}</span>
                                    </td>
                                    <td>
                                           <span> {{ number_format($log->amount,2) }} </span>    
                                    </td>
                                    <td>
                                        <span>{{ number_format($log->new_balance,2) ?? '' }}</span>
                                    </td>
                                    <td>
                                        <span> {{ $log->status }} </span>    
                                    </td>
                                    <td>
                                        <span> {{ $log->request_code }} </span>    
                                 </td>
                                 <td>
                                    <span> {{ $log->created_at }} </span>    
                                </td>
                                <td>
                                    <span> {{ $log->updated_at }} </span>    
                                </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 pagination pagination-rounded justify-content-center mt-4">
                            {{ $logs->links()  }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection


