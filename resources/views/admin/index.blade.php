<x-admin-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                <div>
                    Daily Commissions
                    <canvas id="myChart" style="width:100%;max-width:700px"></canvas>
                </div>
            </div>
        </div>
    </div>
</x-admin-layout>
<script>
    var commissions = @json($array_commissions);
    var date = @json($array_date);

    const max = Math.max.apply(null, Object.values(commissions));
    
    new Chart("myChart", {
      type: "line",
      data: {
        labels: date,
        datasets: [{
          fill: false,
          lineTension: 0,
          backgroundColor: "rgba(0,0,255,1.0)",
          borderColor: "rgba(0,0,255,0.1)",
          data: commissions
        }]
      },
      options: {
        legend: {display: false},
        scales: {
          yAxes: [{ticks: {min: 1, max:max+100}}],
        }
      }
    });
    </script>
