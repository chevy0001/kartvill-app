@extends('layouts.master')

@section('title') @lang('New Users') @endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Users @endslot
        @slot('title') New Users @endslot
    @endcomponent
   
    
    <form class="app-search d-none d-lg-block" method="POST" action="/user-search">
        @csrf
        @method('POST')
        <div class="position-relative">
            <input type="text" class="form-control" name="input" placeholder="@lang('translation.Search')" >
            <span class="bx bx-search-alt"></span>
        </div>
    </form>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                            <a href="/add-user"  class="btn header-item noti-icon waves-effect">
                            <i class="bx bx-user-plus"></i>
                            </a>
                        <table class="table align-middle table-nowrap table-hover">
                            <thead class="table-light">
                                <tr>
                                    <th scope="col" style="width: 70px;">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Contact</th>
                                    
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ( $users as $user )
                                <tr>
                                    <td>
                                        <div class="avatar-xs">
                                            <span class="avatar-title rounded-circle">
                                                D
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <h5 class="font-size-14 mb-1"><a href="/user-profile-{{ $user->id }}" class="text-dark">{{ $user->first_name }} {{ $user->last_name }}</a></h5>
                                        <p class="text-muted mb-0">@if($user->role == 1)Admin
                                            @elseif($user->role == 2)
                                                Rider
                                            @elseif($user->role == 4)
                                                Investor
                                            @elseif($user->role == 5)
                                                Vendor
                                            @elseif($user->role == 3)
                                                Staff
                                            @else
                                                New
                                            @endif</p>
                                    </td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        <div>
                                            <a href="#" class=" font-size-11 m-1">{{ $user->phone_number }}</a>
                                            
                                        </div>
                                    </td>
                                    
                                    <td>
                                        <ul class="list-inline font-size-20 contact-links mb-0">
                                            <li class="list-inline-item px-2">
                                                <a href="" title="Message"><i class="bx bx-message-square-dots"></i></a>
                                            </li>
                                            <li class="list-inline-item px-2">
                                                <a href="/user-profile-{{ $user->id }}" title="Profile"><i class="bx bx-user-circle"></i></a>
                                            </li>
                                            <li class="list-inline-item px-2">
                                                <a href="/edit-users-{{ $user->id }}" title="Edit"><i class="bx bx-edit"></i></a>
                                            </li>
                                            <li class="list-inline-item px-2">
                                                <a href="/delete-user-{{ $user->id }}" title="trash"><i class="bx bx-trash"></i></a>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 pagination pagination-rounded justify-content-center mt-4">
                            {{-- {{ $users->links()  }} --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection


