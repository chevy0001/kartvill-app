@extends('layouts.master')

@section('title') @lang('translation.User_List') @endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Logs @endslot
        @slot('title') Logs @endslot
    @endcomponent
    
    <div class="row">
        <div class="col-lg-12 pagination pagination-rounded justify-content-center mt-4">
            {{ $logs->links()  }}
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table align-middle table-nowrap table-hover">
                            <thead class="table-light">
                                <tr>
                                    <th scope="col" style="width: 70px;">#</th>
                                    <th scope="col"> Date</th>
                                    <th scope="col">Logs</th>      
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ( $logs as $log )
                                <tr>
                                    <td>
                                        <h5 class="font-size-14 mb-1">{{ $log->id }}</h5>
                                    </td>
                                    <td>
                                        <span>{{ $log->created_at }}</span>
                                    </td>
                                    <td>
                                           <span> {{ $log->logs }} </span>    
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 pagination pagination-rounded justify-content-center mt-4">
                            {{ $logs->links()  }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection


