@extends('layouts.master')

@section('title') @lang('Edit User') @endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') User @endslot
@slot('title') Edit User @endslot
@endcomponent

<div class="row">
    <div class="col-xl-8">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Edit Profile of {{ $user->first_name }} {{ $user->last_name }}</h4>

                <form method="post" action="/admin/update-user/{{ $user->id }}">
                        @csrf
                    @method('POST')
                    <div class="row mb-4">
                        <x-label for="first_name"  class="col-sm-3 col-form-label" :value="__('First Name')" />
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="text" name="first_name" id="first_name" :value="old('first_name')" value="{{ $user->first_name }}" placeholder="Enter First Name" required autofocus />   
                        </div>
                    </div>
                    <div class="row mb-4">
                        <x-label for="last_name"  class="col-sm-3 col-form-label" :value="__('Last Name')" />
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="text" id="last_name" name="last_name" :value="old('last_name')" value="{{ $user->last_name }}" placeholder="Enter Last Name" required /> 
                        </div>
                    </div>
                    
                    <div class="row mb-4">
                        <x-label for="email" class="col-sm-3 col-form-label" :value="__('Email')" />
                            
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="email" id="email" name="email" :value="old('email')" value="{{ $user->email }}"  placeholder="Enter Email Address" required />
                        </div>
                    </div>

                    <div class="row mb-4">
                        <x-label for="phone_number" class="col-sm-3 col-form-label" :value="__('Phone Number')" />     
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="text"  id="phone_number" name="phone_number" placeholder="Enter Phone Number" :value="old('phone_number')" value="{{ $user->phone_number }}" required /> 
                        </div>
                    </div>

                    <div class="row mb-4">
                        <x-label for="street_address" class="col-sm-3 col-form-label" :value="__('Street Address')" />
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="text"  id="street_address" name="street_address" placeholder="Enter Street Address" :value="old('street_address')" value="{{ $user->street_address ?? 'Robinsons GenSan' }}" required />
                        </div>
                    </div>
                    <div class="row mb-4">
                        <x-label for="city" class="col-sm-3 col-form-label" :value="__('City')" />
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="text" id="city"  name="city" :value="old('city')" placeholder="Enter City" :value="old('city')" value="{{ $user->city ?? 'General Santos City'}}" required />
                            
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label for="formrow-inputState" class="col-sm-3 col-form-label">State</label>
                        <div class="col-sm-9">
                            <select name="province" id="province" class="form-select">
                              <option value="{{ $user->province }}">{{ $user->province }}</option>
                                @foreach ( $provinces as $province )
                                <option value="{{ $province->province }}" >{{ $province->province }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <x-label for="country" class="col-sm-3 col-form-label" :value="__('Country')" />
                        <div class="col-sm-9">
                            <x-input  class="form-control"  type="text" id="country" name="country" :value="old('country')" placeholder="Enter Country" value="Philippines" required />
                        </div>
                    </div>
                    <div class="row mb-4">
                        <x-label for="zip" class="col-sm-3 col-form-label" :value="__('Zip')" />
                        <div class="col-sm-9">
                            <x-input class="form-control" type="text"  id="zip" name="zip" :value="old('zip')" value="{{ $user->zip ?? '9500'}}" placeholder="Enter Zip" required />
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label for="formrow-inputState" class="col-sm-3 col-form-label">Vehicle Type</label>
                        <div class="col-sm-9">
                            <select name="vehicle_type" id="vehicle_type" class="form-select">
                              <option value="{{ $user->vehicle_type }}" >{{ $user->vehicle_type }}</option>
                                <option value="n/a" >N/A</option>
                                <option value="bicycle" >Bicycle</option>
                                <option value="motorcycle">Motorcycle</option>
                                <option value="car">Car</option>
                                
                            </select>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <x-label for="plate_number" class="col-sm-3 col-form-label" :value="__('Plate Number')" />
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="text" id="plate_number" name="plate_number" placeholder="Enter Plate Number" :value="old('plate_number')" value="{{ $user->plate_number }}"  />
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label for="formrow-inputState" class="col-sm-3 col-form-label">Select Role</label>
                        <div class="col-sm-9">
                            <select class="form-select" name="role" id="role" onchange="ifVendor()">
                              @if($user->role==2)
                              <option value="2">Rider</option>
                              @elseif($user->role==1)
                              <option value="1">Admin</option>
                              @elseif($user->role==4)
                              <option value="4">Investor</option>
                              @elseif($user->role==5)
                              <option value="5">Vendor - {{ $user->store_id }} - {{ $store->store_name ?? ''}}</option>
                              @else
                              <option value="3">Staff</option>
                              @endif
                              @if(Auth::user()->role == 1)
                                  <option value="1" >Admin</option>
                              @endif
                              <option value="3">Staff</option>
                              <option value="2">Rider</option>
                              <option value="4">Investor</option>
                              <option value="5">Vendor - {{ $user->store_id }} - {{ $store->store_name ?? ''}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-4"   id="store_id" style="display:none;">
                        <x-label for="store_id" class="col-sm-3 col-form-label" :value="__('Store ID')" />
                        <div class="col-sm-9">
                            <x-input id="store_id"  class="form-control" type="text" name="store_id" placeholder="Enter Store ID" :value="old('store_id')" value="{{ $user->store_id }}"  />
                        </div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-sm-9">
                            <div>
                                <button type="submit" class="btn btn-primary w-md">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- end card body -->
        </div>
        <!-- end card -->
    </div>
    <!-- end col -->
</div>
<!-- end row -->

@endsection

<script>
    function ifVendor(){
        var selected = document.getElementById('role').value;
        let p = document.querySelector('#store_id');
       
        if(selected == 5){
            p.style.display = '';
            
        }
        else{
            p.style.display = 'none';
        }
    }
</script>