<x-admin-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                
                {{-- Commission Rate --}}
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="post" action="{{ route('admin.settings.update.commission') }}">
                        @csrf
                        <div class="form-group">
                            <label for="commission">Kartvill Commission %</label>
                            <input type="text" class="form-control" required name="commission" id="commission" aria-describedby="commission" value="{{ $commission->commission ?? '' }}" >
                            <input type="hidden" class="form-control" name="id" id="id" aria-describedby="id" value="{{ $commission->id ?? '' }}" >
                          </div>
                        <button type="submit" class="px-4 py-2 bg-indigo-500 hover:bg-indigo-700 rounded-lg text-white">Submit</button>
                      </form>
                </div>
                {{-- End of commission rate  --}}

                {{-- Delivery Rate --}}

                <div class="p-6 bg-white border-b border-gray-200">
                        <div class="form-group">
                            <form method="post" action="/admin/settings/sellers">
                              @csrf
                                <div class="mb-3">
                                  <label for="exampleInputEmail1" class="form-label">Set base rate for the first kilometer</label>
                                  <input type="number" class="form-control" step="0.1" min="0.1" value="{{ $rate->base_rate ?? '' }}" id="baseRate" name="baseRate" aria-describedby="baseRate">
                                  <input type="hidden" class="form-control" id="id" name="id" aria-describedby="id" value="{{ $rate->id ?? '' }}">
                                  
                                <div class="mb-3">
                                  <label for="exampleInputPassword1" class="form-label">Set rate for every additional kilometer</label>
                                  <input type="number" step="0.1" min="0.1" class="form-control" id="perKM" value="{{ $rate->perKm ?? ''}}" name="perKM">
                                </div>
         
                        <button type="submit" class="px-4 py-2 bg-indigo-500 hover:bg-indigo-700 rounded-lg text-white">Submit</button>
                      </form>
                        </div>
              </div>

              {{-- End of Delivery Rate  --}}

             {{-- Start SMS activation here --}}
             
                    
              <div class="form-group">
                  <form method="post" action="/admin/settings/sellers">
                    @csrf
                    <div class="form-check form-switch">
                      <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault" value="1" checked>
                      <label class="form-check-label" for="flexSwitchCheckDefault">Create Product SMS</label>
                    </div>
                    <div class="form-check form-switch">
                      <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" value="1" checked>
                      <label class="form-check-label" for="flexSwitchCheckChecked">Order Paid SMS</label>
                    </div>
                    <div class="form-check form-switch">
                      <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" value="1" checked>
                      <label class="form-check-label" for="flexSwitchCheckChecked">Order SMS</label>
                    </div>
                    <div class="form-check form-switch">
                      <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" value="1" checked>
                      <label class="form-check-label" for="flexSwitchCheckChecked">Order Paid SMS</label>
                    </div>
                    {{-- Fulfillment webhook --}}
                    <div class="form-check form-switch">
                      <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" value="1" checked>
                      <label class="form-check-label" for="flexSwitchCheckChecked">Pick-up Ready SMS</label>
                    </div>
                    {{-- ******************** --}}
                     
              <button type="submit" class="px-4 py-2 bg-indigo-500 hover:bg-indigo-700 rounded-lg text-white">Submit</button>
            </form>
             
    </div>
              {{-- End SMS Activation hete --}}
        
            </div>
          </div>
      </div>
</x-admin-layout>
