@extends('layouts.master')

@section('title') @lang('Store Settings') @endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Store @endslot
@slot('title') Settings @endslot
@endcomponent

<div class="row">
    <div class="col-xl-6">
        <form action="/save-fare-setting" method="post">
            @method("POST")
            @csrf
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">City</h4>

                        <div>
                        <select class="form-control mb-2" name="city_id">
                            <option value="14" selected>General Santos City</option>
                            @foreach ($cities as $city )
                                <option value="{{ $city->id }}">{{ $city->city }}</option>
                            @endforeach
                        </select>
                        </div>
                
                </div>
                <div class="card-body">
                    <h4 class="card-title mb-4">Transport Mode</h4>
                        <div>
                        <select class="form-control mb-2" name="trans_mode_id">
                                <option value="1" selected>Commute</option>
                                <option value="2" >Delivery</option>
                        </select>
                        </div>
                </div>
                <div class="card-body">
                    <h4 class="card-title mb-4">Vehicle Type</h4>
                        <div>
                        <select class="form-control mb-2" name="vehicle_type_id">
                                <option value="1" selected>Tricyle</option>
                                <option value="2" >Motorcycle</option>
                                <option value="3" >Jeep</option>
                                <option value="4" >Car</option>
                                <option value="5" >Truck</option>
                        </select>
                        </div>
                </div>
                <div class="card-body">
                    <h4 class="card-title mb-4">Minimum Fare</h4>
                        <div>
                            <input class="form-control mb-2" type="number" min="0" step="0.1" value="15" id="min_fare" name="min_fare" >
                        </div>
                </div>
                <div class="card-body">
                    <h4 class="card-title mb-4">Minimum Distance</h4>
                        <div>
                            <input class="form-control mb-2" type="number" min="0" step="0.1" value="4" id="min_distance" name="min_distance" >
                        </div>
                </div>
                <div class="card-body">
                    <h4 class="card-title mb-4">Fare per Kilometer</h4>
                        <div>
                            <input class="form-control mb-2" type="number" min="0" step="0.1" value="2" id="fare_per_km" name="fare_per_km" >
                        </div>
                </div>
                <div class="card-body">
                        <div>
                            <input class="btn btn-primary mb-2" type="submit" value="Save">
                        </div>
                </div>
                
                <!-- end card body -->
            </div>
        </form>
        <!-- end card -->
    </div>
   
</div>

<!-- end row -->
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>
        
    </script>
@endsection