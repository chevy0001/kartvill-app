@extends('layouts.master')

@section('title') @lang('Add User') @endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') User @endslot
@slot('title') Add User @endslot
@endcomponent

<div class="row">
    <div class="col-xl-8">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4"></h4>

                <form method="post" action="/store-user" >
                    @csrf
                    @method('POST')
                    <div class="row mb-4">
                        <x-label for="first_name"  class="col-sm-3 col-form-label" :value="__('First Name')" />
                        <div class="col-sm-9">
                            <x-input id="first_name" class="form-control" type="text" name="first_name" id="first_name" :value="old('first_name')" placeholder="Enter First Name" required autofocus />   
                        </div>
                    </div>
                    <div class="row mb-4">
                        <x-label for="last_name"  class="col-sm-3 col-form-label" :value="__('Last Name')" />
                        <div class="col-sm-9">
                            <x-input id="last_name" class="form-control" type="text" id="last_name" name="last_name" :value="old('last_name')" placeholder="Enter Last Name" required /> 
                        </div>
                    </div>
                    
                    <div class="row mb-4">
                        <x-label for="email" class="col-sm-3 col-form-label" :value="__('Email')" />
                            
                        <div class="col-sm-9">
                            <x-input id="email" class="form-control" type="email" id="email" name="email" :value="old('email')" placeholder="Enter Email Address" required />
                        </div>
                    </div>

                    <div class="row mb-4">
                        <x-label for="phone_number" class="col-sm-3 col-form-label" :value="__('Phone Number')" />     
                        <div class="col-sm-9">
                            <x-input id="phone_number" class="form-control" type="text" id="phone_number" name="phone_number" placeholder="Enter Phone Number" :value="old('phone_number')" required /> 
                        </div>
                    </div>

                    <div class="row mb-4">
                        <x-label for="street_address" class="col-sm-3 col-form-label" :value="__('Street Address')" />
                        <div class="col-sm-9">
                            <x-input id="street_address" class="form-control" type="text" id="street_address" name="street_address" placeholder="Enter Street Address" :value="old('street_address')" required />
                        </div>
                    </div>
                    <div class="row mb-4">
                        <x-label for="city" class="col-sm-3 col-form-label" :value="__('City')" />
                        <div class="col-sm-9">
                            <x-input id="city" class="form-control" type="text" id="city" name="city" :value="old('city')" placeholder="Enter City" :value="old('city')" required />
                            
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label for="formrow-inputState" class="col-sm-3 col-form-label">State</label>
                        <div class="col-sm-9">
                            <select name="province" id="province" class="form-select">
                                <option value="0" selected>Choose...</option>
                                @foreach ( $provinces as $province )
                                <option value="{{ $province->province }}" >{{ $province->province }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <x-label for="country" class="col-sm-3 col-form-label" :value="__('Country')" />
                        <div class="col-sm-9">
                            <x-input id="country" class="form-control"  type="text" id="country" name="country" :value="old('country')" placeholder="Enter Country" value="Philippines" required />
                        </div>
                    </div>
                    <div class="row mb-4">
                        <x-label for="zip" class="col-sm-3 col-form-label" :value="__('Zip')" />
                        <div class="col-sm-9">
                            <x-input id="zip" class="form-control" type="text" id="zip" name="zip" :value="old('zip')" placeholder="Enter Zip" required />
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label for="formrow-inputState" class="col-sm-3 col-form-label">Vehicle Type</label>
                        <div class="col-sm-9">
                            <select name="vehicle_type" id="vehicle_type" class="form-select">
                                <option value="0" selected>Choose...</option>
                                <option value="n/a" >N/A</option>
                                <option value="bicycle" >Bicycle</option>
                                <option value="motorcycle">Motorcycle</option>
                                <option value="car">Car</option>
                                
                            </select>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <x-label for="plate_number" class="col-sm-3 col-form-label" :value="__('Plate Number')" />
                        <div class="col-sm-9">
                            <x-input id="plate_number" class="form-control" type="text" id="plate_number" name="plate_number" placeholder="Enter Plate Number" :value="old('plate_number')"  />
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label for="formrow-inputState" class="col-sm-3 col-form-label">Select Role</label>
                        <div class="col-sm-9">
                            <select class="form-select" name="role" id="role">
                                @if(Auth::user()->role == 1)
                                    <option value="1" >Admin</option>
                                @endif
                                    <option value="3">Staff</option>
                                    <option value="2">Rider</option>
                                    <option value="4">Investor</option>
                            </select>
                        </div>
                    </div>
                    

                    <div class="row justify-content-end">
                        <div class="col-sm-9">
                            <div>
                                <button type="submit" class="btn btn-primary w-md">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- end card body -->
        </div>
        <!-- end card -->
    </div>
    <!-- end col -->
</div>
<!-- end row -->

@endsection