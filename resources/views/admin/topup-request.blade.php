@extends('layouts.master')

@section('title') @lang('Topup Requests') @endsection

@section('css')
    <!-- Responsive Table css -->
    <link href="{{ URL::asset('/assets/libs/rwd-table/rwd-table.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Topup @endslot
        @slot('title') Topu Request @endslot
    @endcomponent
    <form class="app-search d-none d-lg-block">
        <div class="position-relative">
            <input type="text" class="form-control" placeholder="@lang('translation.Search')">
            <span class="bx bx-search-alt"></span>
        </div>
    </form>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-rep-plugin">
                        <div class="table-responsive mb-0" data-pattern="priority-columns">
                            <table id="tech-companies-1" class="table table-striped">
                                <thead>
                                    <tr>
                                        {{-- <th>Order ID</th> --}}
                                        <th >#</th>
                                        <th data-priority="1">Code</th>
                                        <th data-priority="1">Full Name</th>
                                        <th data-priority="1">Requested Amount</th>
                                        <th data-priority="1">Date</th>
                                        <th data-priority="1">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($topups as $index => $topup )
                                    <tr >
                                        {{-- <th>{{ $order->order_id }}</th> --}}
                                        <td>{{ $index + 1 }}</td>
                                        <td>{{ $topup->request_code }}</td>
                                        <th>{{ $topup->first_name }} {{ $topup->last_name }}</th>
                                        <td>{{ $topup->requested_amount }}</td>
                                        <td>{{ $topup->created_at }}</td>
                                        <td>
                                            <li class="list-inline-item px-2">
                                               <form action="/approve-{{ $topup->hash }}" method="post">
                                                @csrf
                                                @method('POST')
                                                    <input type="hidden" name="hash" value="{{ $topup->hash }}">
                                                    <input type="hidden" name="rider_id" value="{{ $topup->user_id }}">
                                                    <button  id="but{{ $topup->id }}" class="btn btn-success waves-effect waves-light" 
                                                        title="Approve" data-hash="{{ $topup->hash }}" type="submit"><i class="bx bx-check-double font-size-16 align-middle me-2"></i>Approve</button>
                                                </form>
                                            </li>
                                            <li class="list-inline-item px-2">
                                                <form action="/reject-{{ $topup->hash }}" method="post">
                                                 @csrf
                                                 @method('POST')
                                                     <input type="hidden" name="hash" value="{{ $topup->hash }}">
                                                     <input type="hidden" name="rider_id" value="{{ $topup->user_id }}">
                                                     <button  id="butRej{{ $topup->id }}" class="btn btn-danger waves-effect waves-light" 
                                                         title="Reject" data-hash="{{ $topup->hash }}" type="submit"><i class="bx bx-block font-size-16 align-middle me-2""></i>Reject</button>
                                                 </form>
                                             </li>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            
                        </div>

                    </div>

                    
                </div>
            </div>
        </div> <!-- end col -->
        
    </div> <!-- end row -->
    <div class="row">
        <div class="col-lg-12 pagination pagination-rounded justify-content-center mt-4">
            {{-- {{ $orders->links()  }} --}}
        </div>
    </div>
    
@endsection
@section('script')
    <!-- Responsive Table js -->
    <script src="{{ URL::asset('/assets/libs/rwd-table/rwd-table.min.js') }}"></script>

    <!-- Init js -->
    <script src="{{ URL::asset('/assets/js/pages/table-responsive.init.js') }}"></script>

    <script>
     
      



       
    </script>
@endsection
