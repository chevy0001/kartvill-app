@extends('layouts.master-without-script')

@section('title') @lang('translation.Dashboards') @endsection

@section('css')
    <!-- Responsive Table css -->
    <link href="{{ URL::asset('/assets/libs/rwd-table/rwd-table.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')

@component('components.breadcrumb')
@slot('li_1') Dashboards @endslot
@slot('title') Dashboard @endslot
@endcomponent

<div class="row">
    <div class="col-xl-4">
        <div class="card overflow-hidden">
            <div class="bg-primary bg-soft">
                <div class="row">
                    <div class="col-7">
                        <div class="text-primary p-3">
                            <h5 class="text-primary">Welcome Back !</h5>
                            <p>{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</p>
                        </div>
                    </div>
                    <div class="col-5 align-self-end">
                        <img src="{{ URL::asset('/assets/images/profile-img.png') }}" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
            <div class="card-body pt-0">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="avatar-md profile-user-wid mb-4">
                            <img src="{{ isset(Auth::user()->avatar) ? asset(Auth::user()->avatar) : asset('/assets/images/users/avatar-1.jpg') }}" alt="" class="img-thumbnail rounded-circle">
                        </div>
                        <h5 class="font-size-15 text-truncate">{{ Str::ucfirst(Auth::user()->name) }}</h5>
                        <p class="text-muted mb-0 text-truncate">
                            @if(Auth::user()->role == 1 )
                                Admin
                            @elseif(Auth::user()->role == 2)
                                Rider
                            @elseif(Auth::user()->role == 3)
                                Staff
                            @elseif(Auth::user()->role == 4)
                                Investor
                            @elseif(Auth::user()->role == 5)
                                Vendor
                            @else
                            Hacker
                            @endif
                        </p>
                    </div>

                    <div class="col-sm-8">
                        <div class="pt-4">

                            <div class="row">
                                <div class="col-6">
                                    {{-- <h5 class="font-size-15">125</h5>
                                    <p class="text-muted mb-0">Projects</p> --}}
                                </div>
                                <div class="col-6">
                                    {{-- <h5 class="font-size-15">$12456</h5>
                                    <p class="text-muted mb-0">Revenue</p> --}}
                                </div>
                            </div>
                            <div class="mt-4">
                                {{-- <a href="/user-profile-{{ Auth::user()->id }}" class="btn btn-primary waves-effect waves-light btn-sm">View Profile <i class="mdi mdi-arrow-right ms-1"></i></a> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Overall Commission</h4>
                <div class="row">
                    <div class="col-sm-8">
                        <p class="text-muted"></p>
                        <h3>₱{{  number_format($totalCommission,2) }}</h3>
                        
                        <div class="mt-4">
                            {{-- <a href="" class="btn btn-primary waves-effect waves-light btn-sm">View More <i class="mdi mdi-arrow-right ms-1"></i></a> --}}
                        </div>
                    </div>
                    
                </div>
                <p class="text-muted mb-0">Overall commission since the start of operation.</p>
            </div>
        </div>
    </div>
    <div class="col-xl-8">
        <div class="row">
            <div class="col-md-4">
                <div class="card mini-stats-wid">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="flex-grow-1">
                                <p class="text-muted fw-medium">Daily Sales</p>
                                <h4 class="mb-0">₱{{  number_format($dailyTotalSales,2) }}</h4>
                            </div>

                            <div class="flex-shrink-0 align-self-center">
                                <div class="mini-stat-icon avatar-sm rounded-circle bg-primary">
                                    <span class="avatar-title">
                                        <i class="bx bx-copy-alt font-size-24"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="mt-4">
                            {{-- <a href="" class="btn btn-primary waves-effect waves-light btn-sm">View More <i class="mdi mdi-arrow-right ms-1"></i></a> --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mini-stats-wid">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="flex-grow-1">
                                <p class="text-muted fw-medium">Weekly Sales</p>
                                <h4 class="mb-0">₱{{  number_format($weeklyTotalSales,2) }}</h4>
                            </div>

                            <div class="flex-shrink-0 align-self-center ">
                                <div class="avatar-sm rounded-circle bg-primary mini-stat-icon">
                                    <span class="avatar-title rounded-circle bg-primary">
                                        <i class="bx bx-archive-in font-size-24"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="mt-4">
                            {{-- <a href="" class="btn btn-primary waves-effect waves-light btn-sm">View More <i class="mdi mdi-arrow-right ms-1"></i></a> --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mini-stats-wid">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="flex-grow-1">
                                <p class="text-muted fw-medium">Monthly Sales</p>
                                <h4 class="mb-0">₱{{  number_format($monthlyTotalSales,2) }}</h4>
                            </div>
                            
                            <div class="flex-shrink-0 align-self-center">
                                <div class="avatar-sm rounded-circle bg-primary mini-stat-icon">
                                    <span class="avatar-title rounded-circle bg-primary">
                                        <i class="bx bx-purchase-tag-alt font-size-24"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="mt-4">
                            {{-- <a href="" class="btn btn-primary waves-effect waves-light btn-sm">View More <i class="mdi mdi-arrow-right ms-1"></i></a> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
       
        <div class="card-body">

            <h4 class="card-title">Orders</h4>
            <p class="card-title-desc">Click on tab to show orders </p>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-bs-toggle="tab" href="#daily1" role="tab">
                        <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                        <span class="d-none d-sm-block">Daily</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#weekly1" role="tab">
                        <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                        <span class="d-none d-sm-block">Weekly</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#monthly1" role="tab">
                        <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                        <span class="d-none d-sm-block">Monthly</span>
                    </a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content p-3 text-muted">
                <div class="tab-pane active" id="daily1" role="tabpanel">
                    <div class="col-md-4">
                        <input class="form-control" type="date" value="{{ $date }}" id="date-input" onchange="getDailyOrders()">
                    </div>
                    <div class="table-responsive mb-0" data-pattern="priority-columns">
                        <h5 class="mt-2 pl-3">Total Commission <span id="totalCommission"> ₱{{  number_format($dailyTotalSales,2) }}</span></h5>
                        <table id="dailyOrders" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th data-priority="1">Vendor</th>
                                    <th data-priority="1">Total Price</th>
                                    <th data-priority="1">Commission</th>
                                    
                                </tr>
                            </thead>
                            <tbody id="defaultDataDaily">
                                @foreach ($defaultDailySales as $dailySales)
                                    <tr>
                                        <td>{{ $dailySales->created_at }}</td>
                                        <td>{{ $dailySales->vendor }}</td>
                                        <td>₱{{  number_format($dailySales->total_price,2) }}</td>
                                        <td>₱{{  number_format($dailySales->commission,2) }}</td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="weekly1" role="tabpanel">
                    <div class="col-md-4">
                        <input class="form-control" type="date" value="{{ $date }}" id="date-input-weekly" onchange="getWeeklySales()">
                    </div>
                    <div class="table-responsive mb-0" data-pattern="priority-columns">
                        <h5 class="mt-2 pl-3">Total Commission <span id="totalCommissionWeekly"> ₱{{  number_format($weeklyTotalSales,2) }}</span></h5>
                        <table id="weeklyOrders" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th data-priority="1">Vendor</th>
                                    <th data-priority="1">Total Price</th>
                                    <th data-priority="1">Commission</th>
                                    
                                </tr>
                            </thead>
                            <tbody id="defaultDataDaily">
                                @foreach ($defaultWeeklyTotalSales as $weeklySales)
                                    <tr>
                                        <td>{{ $weeklySales->created_at }}</td>
                                        <td>{{ $weeklySales->vendor }}</td>
                                        <td>₱{{  number_format($weeklySales->total_price,2) }}</td>
                                        <td>₱{{  number_format($weeklySales->commission,2) }}</td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="monthly1" role="tabpanel">
                    <div class="col-md-4">
                        <input class="form-control" type="date" value="{{ $date }}" id="date-input-monthly" onchange="getMonthlySales()">
                    </div>
                    <div class="table-responsive mb-0" data-pattern="priority-columns">
                        <h5 class="mt-2 pl-3" id="totalcomm">Total Commission for the month of <span id="monthLabel">{{ $monthName }}</span> is <span id="totalCommissionMonthly"> ₱{{  number_format($weeklyTotalSales,2) }}</span></h5>
                        <table id="monthlyOrders" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th data-priority="1">Vendor</th>
                                    <th data-priority="1">Total Price</th>
                                    <th data-priority="1">Commission</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($defaultMonthlyTotalSales as $monthlySales)
                                    <tr>
                                        <td>{{ $monthlySales->created_at }}</td>
                                        <td>{{ $monthlySales->vendor }}</td>
                                        <td>₱{{  number_format($monthlySales->total_price,2) }}</td>
                                        <td>₱{{  number_format($monthlySales->commission,2) }}</td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>  
        
    </div>
</div>



<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
    
    function getDailyOrders(){
        let date = document.getElementById('date-input').value;
        var table = document.getElementById("dailyOrders");
        let x = 0;
        let row;
        let cell0,cell1,cell2,cell3;
            
        let elmtTable = document.getElementById('dailyOrders');
        let rowCount = elmtTable.rows.length; 
        
        while(--rowCount){
            elmtTable.deleteRow(rowCount);
        }

                axios.post('/dailyorders', {
                date:date,    
                })
                .then(function (response) {

                    if(response['data']['status'] == "1"){
                      
                        
                        response['data']['data'].forEach(orders => {
                           
                            
                            x+=1;
                            row = table.insertRow(x);
                            cell0 = row.insertCell(0)
                            cell1 = row.insertCell(1)
                            cell2 = row.insertCell(2)
                            cell3 = row.insertCell(3)
                            
                            let d = new Date(orders['created_at']);
                            let date = d.getDate()+"/"+(d.getMonth()+1)  + "/" +d.getFullYear();

                            cell0.innerHTML = date;
                            cell1.innerHTML = orders['vendor'];
                            cell2.innerHTML = parseFloat(orders['total_sales']).toFixed(2);
                            cell3.innerHTML = parseFloat(orders['commission']).toFixed(2);
                            
                            
                        });
                        
                        document.getElementById('date-input').value = date;
                        document.getElementById('totalCommission').innerHTML = "₱" + parseFloat(response['data']['commission']).toFixed(2);
                    }else{
                        document.getElementById('totalCommission').innerHTML = "₱0.00";
                        document.getElementById('totalCommission').innerHTML = "₱0.00";
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
    }
    function getWeeklySales(){
        let date = document.getElementById('date-input-weekly').value;
        var table = document.getElementById("weeklyOrders");
        let x = 0;
        let row;
        let cell0,cell1,cell2,cell3;
            
        let elmtTable = document.getElementById('weeklyOrders');
        let rowCount = elmtTable.rows.length; 

        while(--rowCount){
            elmtTable.deleteRow(rowCount);
        }
        axios.post('/weeklyorders', {
                date:date,    
                })
                .then(function (response) {
                    if(response['data']['status'] == "1"){
                        response['data']['data'].forEach(orders => {
                            x+=1;
                            row = table.insertRow(x);
                            cell0 = row.insertCell(0)
                            cell1 = row.insertCell(1)
                            cell2 = row.insertCell(2)
                            cell3 = row.insertCell(3)
                            
                            let d = new Date(orders['created_at']);
                            let date = d.getDate()+"/"+(d.getMonth()+1)  + "/" +d.getFullYear();

                            cell0.innerHTML = date;
                            cell1.innerHTML = orders['vendor'];
                            cell2.innerHTML = parseFloat(orders['total_sales']).toFixed(2);
                            cell3.innerHTML = parseFloat(orders['commission']).toFixed(2);
                            
                            
                        });
                        
                        document.getElementById('date-input').value = date;
                        document.getElementById('totalCommissionWeekly').innerHTML = "₱" + parseFloat(response['data']['commission']).toFixed(2);
                    }else{
                        document.getElementById('totalCommissionWeekly').innerHTML = "₱0.00";
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
    }

    function getMonthlySales(){
        let date = document.getElementById('date-input-monthly').value;
        var table = document.getElementById("monthlyOrders");
        let x = 0;
        let row;
        let cell0,cell1,cell2,cell3;
            
        let elmtTable = document.getElementById('monthlyOrders');
        let rowCount = elmtTable.rows.length; 
        while(--rowCount){
            elmtTable.deleteRow(rowCount);
        }
        axios.post('/monthlyorders', {
                date:date,    
                })
                .then(function (response) {
                    
                    if(response['data']['status'] == "1"){
                        response['data']['data'].forEach(orders => {
                            x+=1;
                            row = table.insertRow(x);
                            cell0 = row.insertCell(0)
                            cell1 = row.insertCell(1)
                            cell2 = row.insertCell(2)
                            cell3 = row.insertCell(3)
                            
                            let d = new Date(orders['created_at']);
                            let date = d.getDate()+"/"+(d.getMonth()+1)  + "/" +d.getFullYear();

                            cell0.innerHTML = date;
                            cell1.innerHTML = orders['vendor'];
                            cell2.innerHTML = parseFloat(orders['total_sales']).toFixed(2);
                            cell3.innerHTML = parseFloat(orders['commission']).toFixed(2);
                            
                            
                        });
                        
                        document.getElementById('date-input-monthly').value = date;
                        document.getElementById('totalCommissionMonthly').innerHTML = "₱" + parseFloat(response['data']['commission']).toFixed(2);
                        document.getElementById('monthLabel').innerHTML = response['data']['monthName'];
                        
                    }else{
                        document.getElementById('totalcomm').innerHTML = "";
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
    }


</script>




@endsection
@section('script')
<!-- apexcharts -->
<script src="{{ URL::asset('/assets/libs/apexcharts/apexcharts.min.js') }}"></script>

<!-- dashboard init -->
<script src="{{ URL::asset('/assets/js/pages/dashboard.init.js') }}"></script>
<!-- Responsive Table js -->
<script src="{{ URL::asset('/assets/libs/rwd-table/rwd-table.min.js') }}"></script>

<!-- Init js -->
<script src="{{ URL::asset('/assets/js/pages/table-responsive.init.js') }}"></script>
@endsection