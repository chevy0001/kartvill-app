@extends('layouts.master-without-nav-no-script2')

@section('title') @lang('translation.Cart') @endsection

@section('css')
    <!-- bootstrap-touchspin css -->
    <link href="{{ URL::asset('/assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css') }}" rel="stylesheet" />
@endsection

@section('content')
<div class="container">
    @component('components.breadcrumb')
        @slot('li_1') Ecommerce @endslot
        @slot('title') Cart @endslot
    @endcomponent

    <div class="row">
        <div class="col-xl-8">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive" id="all-orders">
                        <table class="table align-middle mb-0 table-nowrap">
                            <thead class="table-light">
                                <tr>
                                    <th>Product</th>
                                    <th>Product Desc</th>
                                    <th>Price</th>
                                   
                                    <th colspan="2">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $grand_total = 0;
                                @endphp
                               @foreach ($orders as $order)
                                    <tr>
                                        <td>
                                            <img src="{{ $order->image_link }}" alt="product-img" title="product-img"
                                                class="avatar-md" />
                                        </td>
                                        <td>
                                            <h5 class="font-size-14 text-truncate"><a href="ecommerce-product-detail"
                                                    class="text-dark">{{ $order->title }} ({{ $order->price }})</a></h5>
                                            @if($order->discount > 0)
                                                <p class="mb-0">Discount : <span class="fw-medium"> -{{ number_format($order->discount,2) }}</span></p>
                                            @endif
                                            @php
                                             $total_option_price = 0; 
                                            @endphp
                                            @foreach ($order_options as $option)
                                                @if($order->option_key == $option->option_key)
                                                    <p class="mb-1 text-muted" key="options" >{{ $option->option_name }} - 
                                                        @if($option->option_price == 0)
                                                            Free
                                                        @else
                                                            @php
                                                            $total_option_price += $option->option_price;
                                                            @endphp
                                                        
                                                            <span id="addon-{{ strval(str_replace('::','',$order->option_key)) }}-{{ $option->id }}">{{ number_format($option->option_price * $order->quantity,2) }}</span>
                                                            
                                                        @endif
                                                        
                                                    </p>
                                                    @if($option->option_price == 0)
                                                    @else
                                                    @endif     
                                                @endif  
                                            @endforeach  
                                            <small>{{ $order->store_name }}</small>    
                                            <div style="width: 60px;">
                                                {{-- <input type="number" value="02" > --}}
                                                <input class="form-control" onchange="updateQuantity({{ $order->id }},{{  strval(str_replace('::','',$order->option_key)) }})" type="number" value="{{ $order->quantity }}"  id="quantity-{{ $order->id }}" name="demo_vertical">
                                            </div>
                                        </td>
                                        <td>
                                            {{ number_format($order->price - $order->discount,2) }}
                                        </td>
                                       
                                        <td>
                                            <span id="sub_total-{{ $order->id }}">
                                            {{ number_format(((floatval($order->price) - floatval($order->discount)) + $total_option_price)* floatval($order->quantity),2)  }}
                                            </span>    
                                        </td>
                                        <td>
                                            <a href="javascript:void(0);" onclick="deleteOrder({{ $order->id }})" class="action-icon text-danger"> <i
                                                    class="mdi mdi-trash-can font-size-18"></i></a>
                                        </td>
                                    </tr>  
                                    @php
                                        $grand_total = number_format($grand_total + ((floatval($order->price) - floatval($order->discount)) + $total_option_price)*floatval($order->quantity),2) 
                                    @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row mt-4">
                        <div class="col-sm-6">
                            <a href="/all-products" class="btn btn-secondary">
                                <i class="mdi mdi-arrow-left me-1"></i> Continue Shopping </a>
                        </div> <!-- end col -->
                        <div class="col-sm-6">
                            <div class="text-sm-end mt-2 mt-sm-0">
                                <button onclick="checkout()" class="btn btn-success">
                                    <i class="mdi mdi-cart-arrow-right me-1"></i> Checkout </button>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row-->
                </div>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="card" id="order-summary">
                <div class="card-body">
                    <h4 class="card-title mb-3">Order Summary</h4>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td>Sub Total :</td>
                                    <td><span id="total">{{ $grand_total }}</span></td>
                                </tr>
                                <tr>
                                    <td>Discount : </td>
                                    <td>- <span id="discount">0</span></td>
                                </tr>
                                <tr>
                                    <td>Shipping Charge :</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <th>Grand Total :</th>
                                    <th ><span id="grand_total">{{ $grand_total }}</span></th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- end table-responsive -->
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title mb-4">Customer Details <sup id="error" style="color:red;"></sup></h5>
                            
                            <div class="card  text-white  mb-0">
                                <div class="card-body">
                                    <input type="text" class="form-control mb-2" name="customer_name" id="customer_name" placeholder="Name" value="{{ $customer_name ?? '' }}">
                                    <input type="text" class="form-control mb-2" name="customer_contact" id="customer_contact" placeholder="Phone Number (Optional)" value="{{ $customer_contact ?? '' }}">
                                    <input type="number" class="form-control mb-2" name="table_number" id="table_number" placeholder="Table Number" value="{{ $table_number ?? '' }}">
                                    <textarea class="form-control" name="request" id="request" cols="30" rows="3" placeholder="Request/Note"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- end card -->
    </div>

<!-- end row -->

 <!-- bootstrap-touchspin -->
 <script src="https://code.jquery.com/jquery-3.6.4.min.js" integrity="sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8=" crossorigin="anonymous"></script>
 <script src="{{ URL::asset('/assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js') }}"></script>

 <!-- init js -->
 <script src="{{ URL::asset('/assets/js/pages/ecommerce-cart.init.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
    
    total = @json($grand_total);
    gTotal = total;
    function setTotal(gtotal){
        this.gTotal = gtotal;
    }
    function getTotal(){
        return this.gTotal;
    }
    function updateQuantity(id,option_key){
        console.log(id);
        order_options = @json($order_options);
        orders = @json($orders);
        
        sub_total = document.getElementById('sub_total-'+id).innerHTML;
        let quantity = document.getElementById('quantity-'+id).value; 
        let option_total = 0;
        let product_total = 0;
        let grand_total = 0;
        
        axios.post('/update-order-quantity', {
        id:id,    
        quantity: quantity,
        })
        .then(function (response) {
            order_options.forEach(options => {
                // console.log(options.option_key.replaceAll('::',''));
                if(options.option_key.replaceAll('::','') == option_key){
                    
                   
                        if(options.option_price > 0){
                            value = document.getElementById('addon-'+option_key+"-"+options.id).innerHTML.trim();
                            value = quantity * options.option_price;
                            option_total += parseFloat(value);
                            document.getElementById('addon-'+option_key+"-"+options.id).innerHTML = value.toFixed(2);
                            
                        }
                        
                        // $( "#order-summary" ).load(window.location.href + " #order-summary" );
                    
                    
                }
            });
            
            orders.forEach(order => {
                    if(order.id == id){
                        product_total = order.price - order.discount ;
                        // console.log(order.price);
                        
                    }           
                });  
            product_total = (product_total * quantity) + option_total;  
                // grand_total = grand_total + product_total;
                // console.log(product_total);
                document.getElementById('sub_total-'+id).innerHTML = product_total.toFixed(2);
                
        
                orders.forEach(order => {
                    grand_total += parseFloat(document.getElementById('sub_total-'+order.id).innerHTML);
                    
                });
                setTotal(grand_total);
                document.getElementById('grand_total').innerHTML = grand_total.toFixed(2);
                document.getElementById('total').innerHTML = grand_total.toFixed(2);
        
            })
        .catch(function (error) {
            console.log(error);
        });
    }

    function checkout(){
        let customer_name = document.getElementById('customer_name').value;
        let customer_contact = document.getElementById('customer_contact').value;
        let table_number = document.getElementById('table_number').value;
        
        if(customer_name.trim().length < 1 ){
            document.getElementById('error').innerHTML = "*Name must not be empty.";
        }else{
            axios.post('/checkout', {
            customer_name:customer_name,    
            customer_contact: customer_contact,
            table_number: table_number,
            total:getTotal(),
            })
            .then(function (response) {
                alert('Order succesfully placed!');
                location.reload();
            setTimeout(() => {
                window.location.href = '/order-status';
            }, 3000);
            })
            .catch(function (error) {
                console.log(error);
            });
            console.log(customer_name.trim().length);
            console.log("success");
        }
        
    
    }

    function deleteOrder(id){
        if(confirm('Are you sure you want to delete the order?')){
            axios.post('/remove-order', {
                id:id,    
                
            })
            .then(function (response) {
                location.reload();
                
            })
            .catch(function (error) {
                console.log(error);
            });
        }
    }
</script>    
        
</div>
@endsection
@section('script')
   
    
@endsection
