@extends('layouts.master-without-nav-no-script')

@section('title') @lang('translation.Product_Detail') @endsection
@section('css')
    <link href="{{ URL::asset('/assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('/assets/libs/spectrum-colorpicker/spectrum-colorpicker.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('/assets/libs/bootstrap-timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('/assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ URL::asset('/assets/libs/datepicker/datepicker.min.css') }}">
@endsection

@section('content')

<section class="my-5 pt-sm-5">
 <div class="container">
    @component('components.breadcrumb')
    @slot('li_1') All Products @endslot
    @slot('title') {{ ucfirst($vendor) }}  @endslot
    @endcomponent

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-6">
                            <div class="product-detai-imgs">
                                <div class="row">
                                    <div class="col-md-2 col-sm-3 col-4">
                                        <div class="nav flex-column nav-pills " id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                        @foreach ($product_images as $images)
                                        @if($loop->iteration == 1)
                                        <a class="nav-link active" id="product-{{ $loop->iteration }}-tab" data-bs-toggle="pill" href="#product-{{ $loop->iteration }}" role="tab" aria-controls="product-{{ $loop->iteration }}" aria-selected="true">
                                        @else
                                        <a class="nav-link" id="product-{{ $loop->iteration }}-tab" data-bs-toggle="pill" href="#product-{{ $loop->iteration }}" role="tab" aria-controls="product-{{ $loop->iteration }}" aria-selected="true">
                                        @endif
                                        
                                            <img src="{{ $images->image_link }}" alt="" class="img-fluid mx-auto d-block rounded">
                                        </a>
                                        @endforeach
                                            
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-7 offset-md-1 col-sm-9 col-8">
                                        <div class="tab-content" id="v-pills-tabContent">
                                            @foreach ( $product_images as $image)
                                            @if($loop->iteration == 1)
                                                <div class="tab-pane fade show active" id="product-{{ $loop->iteration }}" role="tabpanel" aria-labelledby="product-{{ $loop->iteration }}-tab">
                                            
                                            @else
                                                <div class="tab-pane fade" id="product-{{ $loop->iteration }}" role="tabpanel" aria-labelledby="product-{{ $loop->iteration }}-tab">
                                            
                                            @endif

                                                <div>
                                                    <img src="{{ $image->image_link ?? '' }}" alt="" class="img-fluid mx-auto d-block">
                                                </div>
                                            </div>
                                            @endforeach
                                            
                                            
                                        </div>
                                        <div class="pt-4">
                                            <p class="text-muted mb-4">{{ $product->description ?? ''}}</p>
                                        </div>
                                        
                                        


                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-6">
                            <div class="mt-4 mt-xl-3">
                                {{-- Category Section --}}
                                @foreach ($categories as $category)
                                <span id="categories">
                                    <a href="/categories-{{ $category->category }}" class="text-primary">{{ ucfirst($category->category) }} </a>
                                </span>
                                    @endforeach 
                                {{-- End category Section --}}
                                <h4 class="mt-1 mb-3">{{ $product->title ?? ''}}</h4>

                                {{-- <p class="text-muted float-start me-3">
                                    <span class="bx bxs-star text-warning"></span>
                                    <span class="bx bxs-star text-warning"></span>
                                    <span class="bx bxs-star text-warning"></span>
                                    <span class="bx bxs-star text-warning"></span>
                                    <span class="bx bxs-star"></span>
                                </p>
                                <p class="text-muted mb-4">( 152 Customers Review )</p> --}}
                                @if($product->discount > 0)
                                    <h6 class="text-success text-uppercase">{{ $product->discount ?? '0.00'}} Pesos Off</h6>
                                @endif
                                
                                
                                <h5 class="mb-4">Price : @if($product->discount > 0)<span class="text-muted me-2"><del></del></span>@endif 
                                    @if($product_variations->count() > 1) 
                                        <b id="variantprice"></b>
                                    @else 
                                    @foreach ( $product_variations as $variation)
                                        <b id="variantprice">{{ $variation->price ?? '0.00'}}</b>
                                    @endforeach 
                                        
                                    @endif
                                </h5>
                                
                                <div class="mb-2">
                                    <h5 class="font-size-15">Quantity</h5>
                                    <input data-toggle="touchspin" type="number" id="item-quantity" value="1" min="1" max="{{ $product->quantity }}">
                                    
                                </div>
                                {{-- <div class="row mb-3">
                                    <div class="col-md-12">
                                        <div>
                                            <p class="text-muted"><i class="bx bx-unlink font-size-16 align-middle text-primary me-1"></i> Wireless</p>
                                            <p class="text-muted"><i class="bx bx-shape-triangle font-size-16 align-middle text-primary me-1"></i> Wireless Range : 10m</p>
                                            <p class="text-muted"><i class="bx bx-battery font-size-16 align-middle text-primary me-1"></i> Battery life : 6hrs</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div>
                                            <p class="text-muted"><i class="bx bx-user-voice font-size-16 align-middle text-primary me-1"></i> Bass</p>
                                            <p class="text-muted"><i class="bx bx-cog font-size-16 align-middle text-primary me-1"></i> Warranty : 1 Year</p>
                                        </div>
                                    </div>
                                </div> --}}
                            @if($product_variations->count() > 1) 
                                <div class="product-color" >
                                    <h5 class="font-size-15">Variants</h5>
                                    <select class="form-control" id="variations" onchange="changePrice()">
                                        @foreach ($product_variations as $variant )
                                            <option value="{{ $variant->price }} :: {{ $variant->id }}" >{{ $variant->variation_title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @else
                                <input type="hidden" id="variations" value="{{ $product_variations[0]['price'] }}::{{ $product_variations[0]['id'] }}">
                            @endif

                                {{-- <div class="product-color mt-2">
                                        <h5 class="font-size-15">Note</h5>
                                        <textarea class="form-control"  id="note" placeholder="Add note here"></textarea>
                                </div> --}}
                                
                                <div class="product-color mt-2">
                                    @if($options->count() > 0)
                                        <h5 class="font-size-15">Add-ons</h5>
                                        <div class="form-check form-switch form-switch-lg mb-3" dir="ltr">
                                            @foreach ($options as $option)
                                                <input class="form-check-input" type="checkbox" id="option{{ $loop->iteration }}" name="option{{ $loop->iteration }}" value="{{ $option->option_price }}" >
                                                <label class="form-check-label" for="SwitchCheckSizelg" id="optionLabel{{ $loop->iteration }}" name="optionLabel{{ $loop->iteration }}">{{ $option->option_name }} 
                                                    @if($option->option_price == 0)
                                                        Free
                                                    @else
                                                        Just add {{ $option->option_price }} Pesos
                                                    @endif
                                                    
                                                    </label><br/>
                                            @endforeach 
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="button" onclick="addToCart({{ $product->id }})" class="btn btn-primary waves-effect waves-light mt-2 me-1">
                                    <i class="bx bx-cart me-2"></i> Add to cart
                                </button>
                                <button type="button" class="btn btn-success waves-effect  mt-2 waves-light">
                                    <i class="bx bx-shopping-bag me-2"></i>Buy now
                                </button>   
                            </div>
                        </div>
                        
                    </div>
                    <!-- end row -->
                {{-- Specs --}}
                    {{-- <div class="mt-5">
                        <h5 class="mb-3">Specifications :</h5>

                        <div class="table-responsive">
                            <table class="table mb-0 table-bordered">
                                <tbody>
                                    <tr>
                                        <th scope="row" style="width: 400px;">Category</th>
                                        <td>Headphone</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Brand</th>
                                        <td>JBL</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Color</th>
                                        <td>Black</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Connectivity</th>
                                        <td>Bluetooth</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Warranty Summary</th>
                                        <td>1 Year</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div> --}}
                    <!-- end Specifications -->

                    {{-- <div class="mt-5">
                        <h5>Reviews :</h5>

                        <div class="d-flex py-3 border-bottom">
                            <div class="flex-shrink-0 me-3">
                                <img src="assets/images/users/avatar-2.jpg" class="avatar-xs rounded-circle" alt="img" />
                            </div>

                            <div class="flex-grow-1">
                                <h5 class="mb-1 font-size-15">Brian</h5>
                                <p class="text-muted">If several languages coalesce, the grammar of the resulting language.</p>
                                <ul class="list-inline float-sm-end mb-sm-0">
                                    <li class="list-inline-item">
                                        <a href="javascript: void(0);"><i class="far fa-thumbs-up me-1"></i> Like</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="javascript: void(0);"><i class="far fa-comment-dots me-1"></i> Comment</a>
                                    </li>
                                </ul>
                                <div class="text-muted font-size-12"><i class="far fa-calendar-alt text-primary me-1"></i> 5 hrs ago</div>
                            </div>
                        </div>
                        <div class="d-flex py-3 border-bottom">
                            <div class="flex-shrink-0 me-3">
                                <img src="assets/images/users/avatar-4.jpg" class="avatar-xs rounded-circle" alt="img" />
                            </div>

                            <div class="flex-grow-1">
                                <h5 class="font-size-15 mb-1">Denver</h5>
                                <p class="text-muted">To an English person, it will seem like simplified English, as a skeptical Cambridge</p>
                                <ul class="list-inline float-sm-end mb-sm-0">
                                    <li class="list-inline-item">
                                        <a href="javascript: void(0);"><i class="far fa-thumbs-up me-1"></i> Like</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="javascript: void(0);"><i class="far fa-comment-dots me-1"></i> Comment</a>
                                    </li>
                                </ul>
                                <div class="text-muted font-size-12"><i class="far fa-calendar-alt text-primary me-1"></i> 07 Oct, 2019</div>
                                <div class="d-flex mt-4">
                                    <div class="flex-shrink-0 me-2">
                                        <img src="assets/images/users/avatar-5.jpg" class="avatar-xs me-3 rounded-circle" alt="img" />
                                    </div>

                                    <div class="flex-grow-1">
                                        <h5 class="mb-1 font-size-15">Henry</h5>
                                        <p class="text-muted">Their separate existence is a myth. For science, music, sport, etc.</p>
                                        <ul class="list-inline float-sm-end mb-sm-0">
                                            <li class="list-inline-item">
                                                <a href="javascript: void(0);"><i class="far fa-thumbs-up me-1"></i> Like</a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a href="javascript: void(0);"><i class="far fa-comment-dots me-1"></i> Comment</a>
                                            </li>
                                        </ul>
                                        <div class="text-muted font-size-12"><i class="far fa-calendar-alt text-primary me-1"></i> 08 Oct, 2019</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="d-flex py-3 border-bottom">
                            <div class="flex-shrink-0 me-3">
                                <div class="avatar-xs">
                                    <span class="avatar-title bg-primary bg-soft text-primary rounded-circle font-size-16">
                                        N
                                    </span>
                                </div>
                            </div>

                            <div class="flex-grow-1">
                                <h5 class="mb-1 font-size-15">Neal</h5>
                                <p class="text-muted">Everyone realizes why a new common language would be desirable.</p>
                                <ul class="list-inline float-sm-end mb-sm-0">
                                    <li class="list-inline-item">
                                        <a href="javascript: void(0);"><i class="far fa-thumbs-up me-1"></i> Like</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="javascript: void(0);"><i class="far fa-comment-dots me-1"></i> Comment</a>
                                    </li>
                                </ul>
                                <div class="text-muted font-size-12"><i class="far fa-calendar-alt text-primary me-1"></i> 05 Oct, 2019</div>
                            </div>
                        </div>
                    </div> --}}
                {{-- End Specs --}}    
                
            </div>
            </div>
            <!-- end card -->
        </div>
    </div>
    <!-- end row -->

    <div class="row mt-3">
        <div class="col-lg-12">
            <div>
                <h5 class="mb-3">Products you may like:</h5>

                <div class="row">
                    <div class="col-xl-4 col-sm-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col-md-4">
                                        <img src="assets/images/product/img-7.png" alt="" class="img-fluid mx-auto d-block">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="text-center text-md-start pt-3 pt-md-0">
                                            <h5 class="text-truncate"><a href="javascript: void(0);" class="text-dark">Wireless Headphone</a></h5>
                                            {{-- <p class="text-muted mb-4">
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star"></i>
                                            </p> --}}
                                            <h5 class="my-0"><span class="text-muted me-2"><del>$240</del></span> <b>$225</b></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-sm-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col-md-4">
                                        <img src="assets/images/product/img-4.png" alt="" class="img-fluid mx-auto d-block">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="text-center text-md-start pt-3 pt-md-0">
                                            <h5 class="text-truncate"><a href="javascript: void(0);" class="text-dark">Phone patterned cases</a></h5>
                                            <p class="text-muted mb-4">
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star"></i>
                                            </p>
                                            <h5 class="my-0"><span class="text-muted me-2"><del>$150</del></span> <b>$145</b></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-sm-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col-md-4">
                                        <img src="assets/images/product/img-6.png" alt="" class="img-fluid mx-auto d-block">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="text-center text-md-start pt-3 pt-md-0">

                                            <h5 class="text-truncate"><a href="javascript: void(0);" class="text-dark">Phone Dark Patterned cases</a></h5>
                                            <p class="text-muted mb-4">
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star text-warning"></i>
                                                <i class="bx bxs-star"></i>
                                            </p>
                                            <h5 class="my-0"><span class="text-muted me-2"><del>$138</del></span> <b>$135</b></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>
        </div>
    </div>
<!-- end row -->
    </div>
    <section>

<script>

    // function getOptions(){
    //     let option1 = document.getElementById('option1');
    //     let option2 = document.getElementById('option2');
    //     if(option1.checked == true){
    //         console.log(document.getElementById('option1').value);
    //     }
    //     if(option2.checked == true){
    //         console.log(document.getElementById('option2').value);
    //     }
        
        
    // }
    
    
    
    let variant_price_id = document.getElementById('variations').value;
    let product_price = variant_price_id[0];
    product_price = variant_price_id.split("::");

    document.getElementById('variantprice').innerHTML = parseFloat(product_price[0]).toFixed(2);
    
    function changePrice(){
        
        let variant_price_id = document.getElementById('variations').value;
        let product_price = variant_price_id[0];
        product_price = variant_price_id.split("::");
        document.getElementById('variantprice').innerHTML = parseFloat(product_price[0]).toFixed(2);

    }

    function addToCart(product_id){
            let quantity = document.getElementById('item-quantity').value;
            let variation_id = document.getElementById('variations').value;
            variation_id = variation_id.split("::");

            
            let option_count = @json($options->count());
            var x = 1;
            var option_ids = "";
            var option_counter = 0;
            @json($options).forEach(element => {
                let option = document.getElementById('option'+x++);
                if(option.checked == true){
            
                    option_counter += 1;
                    option_ids = option_ids + "::" + element.id;
                }
            });
            
           
        axios.post('/add-to-cart', {
            variation_id:variation_id[1],    
            product_id: product_id,
            quantity: quantity,
            option_ids:option_ids,
            option_counter:option_counter

            })
            .then(function (response) {
                console.log(response);
                document.getElementById('cartCount').innerHTML = response['data']['order_count']['original']['order_count'];
                $( "#cart-notification" ).load(window.location.href + " #cart-notification" );
            })
            .catch(function (error) {
                console.log(error);
            }); 
        }

        
</script>
@endsection
@section('script')
    <script src="{{ URL::asset('/assets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/spectrum-colorpicker/spectrum-colorpicker.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/datepicker/datepicker.min.js') }}"></script>

    <!-- form advanced init -->
    <script src="{{ URL::asset('/assets/js/pages/form-advanced.init.js') }}"></script>
@endsection
