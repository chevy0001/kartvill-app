@extends('layouts.master')

@section('title') @lang('translation.Add_Product') @endsection

@section('css')
<!-- select2 css -->
<link href="{{ URL::asset('/assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />

<!-- dropzone css -->
<link href="{{ URL::asset('/assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Ecommerce @endslot
@slot('title') Add Product @endslot
@endcomponent

@if (session('status'))
<div class="alert alert-success">
    {{ session('status') }}
</div>
@endif
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="card-title">Basic Information</h4>
                <p class="card-title-desc">Fill all information below</p>

                <form method="post" action="/save-product">
                    @csrf
                    @method('POST')
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="title">Product Name</label>
                                <input id="title" name="title" type="text" class="form-control" placeholder="Product Name">
                            </div>
                            <div class="mb-3">
                                <label class="control-label">Vendor Name</label>
                                <select class="form-control select2" name="vendor_id">
                                    @foreach ($vendors as $vendor)
                                    <option value="{{ $vendor->id }}">{{ $vendor->seller_name }}</option>
                                    @endforeach
                                    
                                
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="price">Price</label>
                                <input id="price" name="price" type="number" step="0.1" min="0" class="form-control" placeholder="Price">
                            </div>
                            <div class="mb-3">
                                <label for="discount">Product Discount</label>
                                <input id="discount" name="discount" type="number" min="0" step="0.1" value="0" class="form-control" placeholder="Discount">
                            </div>
                            <div class="mb-3">
                                <label for="weight">Weight</label>
                                <input id="weight" name="weight" type="number" step="0.1" min="0" class="form-control" placeholder="Weight">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="quantity">Quantity</label>
                                <input id="quantity" name="quantity" type="number" step="0.1" min="0" class="form-control" placeholder="Quantity">
                            </div>
                            <div class="mb-3">
                                <label class="control-label">Category</label>
                                <select class="form-control select2" name="category">
                                    <option>Select</option>
                                    <option value="food">Foods</option>
                                    <option value="goods">Goods</option>
                                 @foreach ($categories as $category)
                                    <option value="{{ $category->category }}">{{ ucfirst($category->category) }}</option>   
                                         
                                 @endforeach
                                </select>
                            </div>
                            <div class="mb-3">
                                <label class="control-label">Image Link</label>
                                <input id="imagelink" name="imagelink" type="text"  class="form-control" placeholder="Image Link">
                                

                            </div>
                            <div class="mb-3">
                                <label for="productdesc">Product Description</label>
                                <textarea class="form-control" id="description" name="description" rows="5" placeholder="Product Description"></textarea>
                            </div>

                        </div>
                    </div>



                    <div class="d-flex flex-wrap gap-2">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save Changes</button>
                    </div>
                </form>

            </div>
        </div>

        <!--------Images 
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-3">Product Images</h4>

                <form action="/" method="post" class="dropzone">
                    <div class="fallback">
                        <input name="file" type="file" multiple />
                    </div>

                    <div class="dz-message needsclick">
                        <div class="mb-3">
                            <i class="display-4 text-muted bx bxs-cloud-upload"></i>
                        </div>

                        <h4>Drop files here or click to upload.</h4>
                    </div>
                </form>
            </div>
            

        </div> 
          ----> 
        <!-- end card-->

        <div class="card">
            <div class="card-body">

                <h4 class="card-title">Meta Data</h4>
                <p class="card-title-desc">Fill all information below</p>

                <form>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="metatitle">Meta title</label>
                                <input id="metatitle" name="productname" type="text" class="form-control" placeholder="Metatitle">
                            </div>
                            <div class="mb-3">
                                <label for="metakeywords">Meta Keywords</label>
                                <input id="metakeywords" name="manufacturername" type="text" class="form-control" placeholder="Meta Keywords">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="metadescription">Meta Description</label>
                                <textarea class="form-control" id="metadescription" rows="5" placeholder="Meta Description"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="d-flex flex-wrap gap-2">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save Changes</button>
                        <button type="submit" class="btn btn-secondary waves-effect waves-light">Cancel</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<!-- end row -->

@endsection
@section('script')
<!-- select 2 plugin -->
<script src="{{ URL::asset('/assets/libs/select2/select2.min.js') }}"></script>

<!-- dropzone plugin -->
<script src="{{ URL::asset('/assets/libs/dropzone/dropzone.min.js') }}"></script>

<!-- init js -->
<script src="{{ URL::asset('/assets/js/pages/ecommerce-select2.init.js') }}"></script>

<!-- form repeater js -->

@endsection