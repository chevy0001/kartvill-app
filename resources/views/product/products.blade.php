@extends('layouts.master')

@section('title') @lang('translation.Add_Product') @endsection

@section('css')
<!-- select2 css -->
<link href="{{ URL::asset('/assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />

<!-- dropzone css -->
<link href="{{ URL::asset('/assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Ecommerce @endslot
@slot('title')All Products @endslot
@endcomponent

@if (session('status'))
<div class="alert alert-success">
    {{ session('status') }}
</div>
@endif
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title"><a href="{{ route('product.addproduct') }}" class="btn btn-outline-primary"><i class="bx bx-plus"></i></a></h4>
                <form>
                    <div class="mt-4 mt-sm-0 float-sm-end d-sm-flex align-items-center"></div>
                    <div class="input-group  search-box "  id="searchForm">
                        <input type="text"  name="search" id="search" class="form-control border-1" placeholder="Enter Product Name, Store/Owner Name, Category " value="" aria-label="Search input" >
                        <button class="btn btn-primary" onclick="redirectToSearch()" id="searchBut">Search</button>
                    </div>
                </form>

                <div class="table-responsive">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Product</th>
                                <th>Store</th>
                                <th>Owner</th>
                                <th>Action</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $product)
                            <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td>{{ $product->title }}</td>
                                <td>{{ $product->store_name }}</td>
                                <td>{{ $product->seller_name }}</td>
                                <td>
                                    @if(isset($search))
                                        <a href="{{ route('product.displayProduct',['producttitle'=>strtolower($product->title),'productid'=>$product->product_id]) }}" target="_blank" class="btn btn-outline-info "><i class="bx bx-window-open"></i></button></a>
                                        <a href="{{ route('product.product',['id'=>$product->product_id]) }}" class="btn btn-outline-success "><i class="bx bx-edit"></i></button></a>
                                        <a onclick="deleteProduct({{ $product->product_id }})" class="btn btn-outline-danger "><i class="bx bx-trash"></i></button></a>
                                    @else
                                        <a href="{{ route('product.displayProduct',['producttitle'=>strtolower($product->title),'productid'=>$product->id]) }}" target="_blank" class="btn btn-outline-info "><i class="bx bx-window-open"></i></button></a>
                                        <a href="{{ route('product.product',['id'=>$product->id]) }}" class="btn btn-outline-success "><i class="bx bx-edit"></i></button></a>
                                        <a onclick="deleteProduct({{ $product->id }})" class="btn btn-outline-danger "><i class="bx bx-trash"></i></button></a>
                                    @endif
                                </td>
                            </tr> 
                            @endforeach
                     

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- end row -->

@endsection
@section('script')
<script>
    function deleteProduct(id){
        if(confirm('Are you sure you want to delete the product?')){
        axios.post('/delete-product', {
                id:id,  
            })
            .then(function (response) {
                location.reload();
            })
            .catch(function (error) {
            
            });
        }
    }
    
    function redirectToSearch(){
            event.preventDefault();
            let keyword = document.getElementById('search').value;
            location.href = '/products-results-'+keyword.toLowerCase();
        }
</script>

@endsection