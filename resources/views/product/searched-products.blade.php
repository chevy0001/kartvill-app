@extends('layouts.master-without-nav-no-script')

@section('title') @lang('translation.Products') @endsection

@section('css')
    <!-- ION Slider -->
    <link href="{{ URL::asset('/assets/libs/ion-rangeslider/ion-rangeslider.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Ecommerce @endslot
        @slot('title') Products @endslot
    @endcomponent
<div class="container">
    <div class="row mt-5">
        {{-- <div class="col-lg-2">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Filter</h4>

                    <div>
                        <h5 class="font-size-14 mb-3">Categories</h5>
                        <ul class="list-unstyled product-list">
                            @foreach ($categories as $category)
                            <li><a href="#">{{ ucfirst($category->category) }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    
                </div>
            </div>

        </div> --}}
        <div class="col-lg-12">

            <div class="row mb-3">
                <div class="col-xl-4 col-sm-6">
                    <div class="mt-2">
                        <h5 ><a class="text-dark" href="{{ route('product.displayAllProducts') }}">Menu</a></h5>
                    </div>
                </div>
                <div class="col-lg-8 col-sm-6">
                    {{-- <form  class="mt-4 mt-sm-0 float-sm-end d-sm-flex align-items-center"> --}}
                        {{-- <div class="search-box me-2">
                            <div class="position-relative">
                                <input type="text" id="search" class="form-control border-0"  placeholder="Search...">
                                <i class="bx bx-search-alt search-icon"></i>
                            </div>
                        </div> --}}
                    <form>
                        <div class="mt-4 mt-sm-0 float-sm-end d-sm-flex align-items-center"></div>
                        <div class="input-group  search-box "  id="searchForm">
                            <input type="text"  name="search" id="search" class="form-control border-0" placeholder="{{ $keyword }}" value="{{ $keyword }}" aria-label="Search input" >
                            <button class="btn btn-primary" onclick="redirectToSearch()" id="searchBut">Search</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                @foreach ($products as $product)
                <div class="col-xl-4 col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="product-img position-relative">
                                <div class="avatar-sm product-ribbon">
                                    @if($product->discount > 0)
                                        <span class="avatar-title rounded-circle  bg-danger">
                                            {{ $product->discount }}
                                        </span>
                                    @endif
                                    
                                </div>
                                {{-- <img src="{{ URL::asset('/assets/images/product/img-1.png') }}" alt="" class="img-fluid mx-auto d-block"> --}}
                                <a href="{{ route('product.displayProduct',['producttitle'=>$product->title,'productid'=>$product->product_id]) }}">
                                    <img src="{{ $product->image_link }}" alt="" class="img-fluid mx-auto d-block">
                                </a>
                            </div>
                            <div class="mt-4 text-center">
                                <h5 class="text-truncate mb-3"><a href="{{ route('product.displayProduct',['producttitle'=>$product->title,'productid'=>$product->product_id]) }}" class="text-dark">{{ $product->title }} </a><br/>
                                    <small><a href="/" class="text-dark pb-3">{{ $product->store_name }}</a></small>
                                </h5>
                                
                                
                                @if($product->discount > 0)
                                <h5 class="my-0"><span class="text-muted "><del>{{ number_format($product->price,2) }}</del></span> <b>{{ number_format($product->price - $product->discount,2) }}</b></h5>
                                @else
                                <h5 class="my-0"><span class="text-muted "></span> <b>{{ number_format($product->price,2) }}</b></h5>
                                @endif

                                
                            </div>
                            <div class="text-center">
                                @if($product->variant_count > 1)
                                <a href="{{ route('product.displayProduct',['producttitle'=>$product->title,'productid'=>$product->product_id]) }}" type="button" id="but-{{ $loop->iteration }}" value="{{ $product->product_id }}" class="btn btn-primary waves-effect waves-light mt-2 me-1">
                                    <i class="bx bx-cart me-2"></i> Choose Options
                                </a> 
                                @else
                                <a type="button" id="but-{{ $loop->iteration }}" value="{{ $product->product_id }}" class="btn btn-primary waves-effect waves-light mt-2 me-1">
                                    <i class="bx bx-cart me-2"></i> Add to cart
                                </a> 
                                @endif
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            <!-- end row -->
          
            <div class="row">
                <div class="col-lg-12">
                    <ul class="pagination pagination-rounded justify-content-center mt-3 mb-4 pb-1">
                        {{ $products->links()  }}
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- end row -->
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>   
    <script>
        function redirectToSearch(){
            event.preventDefault();
            let keyword = document.getElementById('search').value;
            location.href = '/search-'+keyword.toLowerCase();;
        }

        // function changeURL(){
        //     let searchVal = document.getElementById('search').value;
        //     window.history.replaceState("string", "Title", "/search-"+searchVal);
        //     if(searchVal == ""){
        //         window.history.replaceState("string", "Title", "/all-products");
        //     }




        // }
        
    </script>
@endsection
@section('script')
    <!-- Ion Range Slider-->
    <script src="{{ URL::asset('/assets/libs/ion-rangeslider/ion-rangeslider.min.js') }}"></script>

    <!-- init js -->
    <script src="{{ URL::asset('/assets/js/pages/product-filter-range.init.js') }}"></script>
    

   
@endsection
