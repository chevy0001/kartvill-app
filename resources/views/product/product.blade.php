@extends('layouts.master-without-script')

@section('title') @lang('Product Details') @endsection

@section('css')
<style>
    a{
        cursor: pointer;
        color:blue;
    }
</style>
@endsection
@section('content')

@component('components.breadcrumb')
@slot('li_1') <a href="{{ route('product.products') }}">All Products</a> @endslot
@slot('title') {{ $vendor }}<a href="{{ route('product.displayProduct',['producttitle'=>$product->title,'productid'=>$product->id]) }}" target="_blank">[Preview]</a>@endslot
@endcomponent

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-xl-6">
                        <div class="product-detai-imgs">
                            <div class="row">
                                <div class="col-md-2 col-sm-3 col-4">
                                    <div class="nav flex-column nav-pills " id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                       @foreach ($product_images as $images)
                                       @if($loop->iteration == 1)
                                       <a class="nav-link active" id="product-{{ $loop->iteration }}-tab" data-bs-toggle="pill" href="#product-{{ $loop->iteration }}" role="tab" aria-controls="product-{{ $loop->iteration }}" aria-selected="true">
                                       @else
                                       <a class="nav-link" id="product-{{ $loop->iteration }}-tab" data-bs-toggle="pill" href="#product-{{ $loop->iteration }}" role="tab" aria-controls="product-{{ $loop->iteration }}" aria-selected="true">
                                       @endif
                                      
                                        <img src="{{ $images->image_link }}" alt="" class="img-fluid mx-auto d-block rounded">
                                       </a>
                                       @endforeach
                                        
                                        
                                    </div>
                                </div>
                                <div class="col-md-7 offset-md-1 col-sm-9 col-8">
                                    <div class="tab-content" id="v-pills-tabContent">
                                        @foreach ( $product_images as $image)
                                        @if($loop->iteration == 1)
                                            <div class="tab-pane fade show active" id="product-{{ $loop->iteration }}" role="tabpanel" aria-labelledby="product-{{ $loop->iteration }}-tab">
                                                
                                        @else
                                            <div class="tab-pane fade" id="product-{{ $loop->iteration }}" role="tabpanel" aria-labelledby="product-{{ $loop->iteration }}-tab">
                                        
                                        @endif

                                            <div>
                                                <div class="col-xl-3 col-lg-4 col-sm-6">
                                                    <button class="btn btn-outline-danger" id="deleteImageBut" onclick="deleteImage({{ $image->id }})" ><i class="bx bx-trash-alt"></i></button>
                                                </div>
                                                <img src="{{ $image->image_link }}" alt="" class="img-fluid mx-auto d-block">
                                            </div>
                                        </div>
                                        @endforeach
                                        
                                    </div>
                                    
                                        <span>Write product description below</span>
                                        <textarea type="text" id="description" oninput="saveDescription({{ $product->id }})" class="form-control text-muted mb-4 mt-2 " placeholder="Add description" cols="50%" >{{ $product->description ?? 'Add description' }}</textarea>
                                    
                                    <div class="text-center">
                                        <input type="text" id="image_link" name="image_link" class="form-control" placeholder="Add image link here">
                                        <button class="btn btn-primary mt-2 waves-light" onclick="saveImage({{ $product->id }})">Add Image</button>
                                        
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-6">
                        <div class="mt-4 mt-xl-3">
                            {{-- Category Section --}}
                            <small><a class="text-primary" data-toggle="modal" data-target="#editCategoryModal">[Customize Category]</a></small>
                            <div id="categories">
                                @foreach ($categories as $category)
                                    <span >
                                        <a href="/categories-{{ $category->category }}" class="text-primary">{{ ucfirst($category->category) }} </a>
                                    </span>
                                @endforeach
                            </div>   
                                
                                
                                
                                
                            {{-- End category Section --}}
                            <h4 class="mt-1 mb-3"><span id="product-title" >{{ $product->title }}<sup class="text-primary" id="edit-product-title" onclick="hideProductTitle()" ><a>Edit</a></sup></span>
                                <span id="input-product" style="display: none;">    
                                    <input class="form-control" type="text"  id="input-product-title" value="{{ $product->title }}" >
                                    <button id="save-title" onclick="editProductTitle({{ $product->id }})" class="btn btn-primary">Save</button>
                                </span>
                            </h4>

                            {{-- <p class="text-muted float-start me-3">
                                <span class="bx bxs-star text-warning"></span>
                                <span class="bx bxs-star text-warning"></span>
                                <span class="bx bxs-star text-warning"></span>
                                <span class="bx bxs-star text-warning"></span>
                                <span class="bx bxs-star"></span>
                            </p>
                            <p class="text-muted mb-4">( 152 Customers Review )</p> --}}
                            @if($product->discount > 0)
                                <div id="discount-div"><h6 class="text-success text-uppercase">{{ $product->discount }} Off</h6></div>
                            @endif
                            
                            
                            <h5 class="mb-4">Price : @if($product->discount > 0)<span class="text-muted me-2"><del></del></span>@endif 
                                
                                
                                @if($product_variations->count() > 1) 
                                    <b id="variantprice"></b>
                                @else @foreach ( $product_variations as $variation)
                                <b id="variantprice">{{ $variation->price }}</b>
                                @endforeach 
                                    
                                @endif
                                <sub>Click Customize Variant to edit price</sub>
                            </h5>
                            <div class=""  id="discountForm">
                                <span>Add discount below.</label><br/>
                                <input type="number"  name="discount" id="discount" onchange="saveDiscount({{ $product->id }})" class="form-control" min="0" max="" value="{{ $product->discount ?? "0" }}" aria-label="Discount input" >
                            </div>
                            {{-- <div class="row mb-3">
                                <div class="col-md-12">
                                    <div>
                                        <p class="text-muted"><i class="bx bx-unlink font-size-16 align-middle text-primary me-1"></i> Wireless</p>
                                        <p class="text-muted"><i class="bx bx-shape-triangle font-size-16 align-middle text-primary me-1"></i> Wireless Range : 10m</p>
                                        <p class="text-muted"><i class="bx bx-battery font-size-16 align-middle text-primary me-1"></i> Battery life : 6hrs</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div>
                                        <p class="text-muted"><i class="bx bx-user-voice font-size-16 align-middle text-primary me-1"></i> Bass</p>
                                        <p class="text-muted"><i class="bx bx-cog font-size-16 align-middle text-primary me-1"></i> Warranty : 1 Year</p>
                                    </div>
                                </div>
                            </div> --}}
                            <div id="variants-dropdown">
                                <h5 class="font-size-15">Variants
                                    <small><a class="text-primary" data-toggle="modal" data-target="#editVariantModal">
                                    [Customize Variants]
                                    </a></small></h5>
                                @if($product_variations->count() > 1) 
                                    <div class="product-color" >
                                    
                                    <select class="form-control" id="variations" onchange="changePrice()">
                                        @foreach ($product_variations as $variant )
                                            <option value="{{ $variant->price }}" >{{ $variant->variation_title }} - {{ $variant->price }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @endif
                            </div>
                            <div class="product-color mt-2">
                                <h5 class="font-size-15">Options 
                                    <small>
                                        <a  class="text-primary" data-toggle="modal" data-target="#editOptionModal">
                                        [Customize Options]
                                        </a>
                                    </small></h5>
                                
                                <div class="form-check form-switch form-switch-lg mb-3" id="select-options" dir="ltr">
                                    @if($options->count() > 0)
                                       @foreach ($options as $option)
                                        <input class="form-check-input" type="checkbox" id="option{{ $option->id }}" name="option{{ $option->id }}" value="{{ $option->option_price }}" >
                                        <label class="form-check-label" for="SwitchCheckSizelg" id="optionLabel{{ $loop->iteration }}" name="optionLabel{{ $loop->iteration }}">{{ $option->option_name }} 
                                            @if($option->option_price == 0)
                                                Free
                                            @else
                                                just add {{ $option->option_price }} Pesos
                                            @endif
                                            
                                            </label><br/>
                                       @endforeach 
                                    @endif
                                    
                                    
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <!-- end row -->
            {{-- Specs --}}
                {{-- <div class="mt-5">
                    <h5 class="mb-3">Specifications :</h5>

                    <div class="table-responsive">
                        <table class="table mb-0 table-bordered">
                            <tbody>
                                <tr>
                                    <th scope="row" style="width: 400px;">Category</th>
                                    <td>Headphone</td>
                                </tr>
                                <tr>
                                    <th scope="row">Brand</th>
                                    <td>JBL</td>
                                </tr>
                                <tr>
                                    <th scope="row">Color</th>
                                    <td>Black</td>
                                </tr>
                                <tr>
                                    <th scope="row">Connectivity</th>
                                    <td>Bluetooth</td>
                                </tr>
                                <tr>
                                    <th scope="row">Warranty Summary</th>
                                    <td>1 Year</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div> --}}
                <!-- end Specifications -->

                {{-- <div class="mt-5">
                    <h5>Reviews :</h5>

                    <div class="d-flex py-3 border-bottom">
                        <div class="flex-shrink-0 me-3">
                            <img src="assets/images/users/avatar-2.jpg" class="avatar-xs rounded-circle" alt="img" />
                        </div>

                        <div class="flex-grow-1">
                            <h5 class="mb-1 font-size-15">Brian</h5>
                            <p class="text-muted">If several languages coalesce, the grammar of the resulting language.</p>
                            <ul class="list-inline float-sm-end mb-sm-0">
                                <li class="list-inline-item">
                                    <a href="javascript: void(0);"><i class="far fa-thumbs-up me-1"></i> Like</a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="javascript: void(0);"><i class="far fa-comment-dots me-1"></i> Comment</a>
                                </li>
                            </ul>
                            <div class="text-muted font-size-12"><i class="far fa-calendar-alt text-primary me-1"></i> 5 hrs ago</div>
                        </div>
                    </div>
                    <div class="d-flex py-3 border-bottom">
                        <div class="flex-shrink-0 me-3">
                            <img src="assets/images/users/avatar-4.jpg" class="avatar-xs rounded-circle" alt="img" />
                        </div>

                        <div class="flex-grow-1">
                            <h5 class="font-size-15 mb-1">Denver</h5>
                            <p class="text-muted">To an English person, it will seem like simplified English, as a skeptical Cambridge</p>
                            <ul class="list-inline float-sm-end mb-sm-0">
                                <li class="list-inline-item">
                                    <a href="javascript: void(0);"><i class="far fa-thumbs-up me-1"></i> Like</a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="javascript: void(0);"><i class="far fa-comment-dots me-1"></i> Comment</a>
                                </li>
                            </ul>
                            <div class="text-muted font-size-12"><i class="far fa-calendar-alt text-primary me-1"></i> 07 Oct, 2019</div>
                            <div class="d-flex mt-4">
                                <div class="flex-shrink-0 me-2">
                                    <img src="assets/images/users/avatar-5.jpg" class="avatar-xs me-3 rounded-circle" alt="img" />
                                </div>

                                <div class="flex-grow-1">
                                    <h5 class="mb-1 font-size-15">Henry</h5>
                                    <p class="text-muted">Their separate existence is a myth. For science, music, sport, etc.</p>
                                    <ul class="list-inline float-sm-end mb-sm-0">
                                        <li class="list-inline-item">
                                            <a href="javascript: void(0);"><i class="far fa-thumbs-up me-1"></i> Like</a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a href="javascript: void(0);"><i class="far fa-comment-dots me-1"></i> Comment</a>
                                        </li>
                                    </ul>
                                    <div class="text-muted font-size-12"><i class="far fa-calendar-alt text-primary me-1"></i> 08 Oct, 2019</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="d-flex py-3 border-bottom">
                        <div class="flex-shrink-0 me-3">
                            <div class="avatar-xs">
                                <span class="avatar-title bg-primary bg-soft text-primary rounded-circle font-size-16">
                                    N
                                </span>
                            </div>
                        </div>

                        <div class="flex-grow-1">
                            <h5 class="mb-1 font-size-15">Neal</h5>
                            <p class="text-muted">Everyone realizes why a new common language would be desirable.</p>
                            <ul class="list-inline float-sm-end mb-sm-0">
                                <li class="list-inline-item">
                                    <a href="javascript: void(0);"><i class="far fa-thumbs-up me-1"></i> Like</a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="javascript: void(0);"><i class="far fa-comment-dots me-1"></i> Comment</a>
                                </li>
                            </ul>
                            <div class="text-muted font-size-12"><i class="far fa-calendar-alt text-primary me-1"></i> 05 Oct, 2019</div>
                        </div>
                    </div>
                </div> --}}
            {{-- End Specs --}}    
            
        </div>
        </div>
        <!-- end card -->
    </div>
</div>
<!-- end row -->


{{-- Edit Variant Modal --}}
    <div class="modal fade" id="editVariantModal" tabindex="-1" role="dialog" aria-labelledby="editVariantModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Edit Variants</h5>
            </div>
            <div class="modal-body">
            
            
            <div class="table-responsive" id="list-variants">
                <table class="table table-borderless mb-0">

                    <thead>
                        <tr>
                            
                            <th >Variant Title</th>
                            <th >Price</th>
                            <th >Stocks</th>
                            <th >Weight</th>
                            <th >Action</th>
                        </tr>
                    </thead>
                    <tr>
                        <td ><input type="text" id="variant-title" class="form-control" placeholder="Title"></td>
                        <td ><input type="number" id="variant-price" min="1" class="form-control" placeholder="Price"></td>
                        <td ><input type="number" id="variant-quantity" min="1" class="form-control" placeholder="Stocks"></td>
                        <td ><input type="number" id="variant-weight" min="1" class="form-control" placeholder="Weight"></td>
                        <td >
                            <button class="btn btn-outline-primary btn-sm" onclick="addVariant({{ $product->id }})">Add Variant</button>
                            
                        </td>
                        </tr>
                    <tbody>
                        @foreach ($product_variations as $variant)
                        <tr>
                            <td ><input type="text" id="variant-title-{{ $variant->id }}" class="form-control" value="{{ $variant->variation_title }}"></td>
                            <td ><input type="number" min="1" id="variant-price-{{ $variant->id }}" class="form-control" value="{{ $variant->price }}"></td>
                            <td ><input type="number" min="1" id="variant-quantity-{{ $variant->id }}" class="form-control" value="{{ $variant->stocks }}"></td>
                            <td ><input type="number" min="1" id="variant-weight-{{ $variant->id }}" class="form-control" value="{{ $variant->weight }}"></td>
                            <td >
                                <span class="btn btn-outline-primary btn-sm" onclick="saveVariant({{ $variant->id }})"><i class="bx bx-save"></i></span>
                                @if($variant->is_active == 1)
                                <span class="btn btn-outline-success btn-sm" id="variant-active-{{ $variant->id }}"  onclick="disableVariant({{ $variant->id }},{{ $product->id }})"><i class="bx bxs-check-square"></i></span>
                                @else
                                <span class="btn btn-outline-secondary btn-sm" id="variant-disable-{{ $variant->id }}" onclick="enableVariant({{ $variant->id }},{{ $product->id }})"><i class="bx bx-block"></i></span>
                                @endif
                                <span class="btn btn-outline-danger btn-sm" onclick="deleteVariant({{ $variant->id}},{{ $product->id }})"><i class="bx bxs-trash"></i></span>
                            </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="window.location.reload(true)">Exit</button>
            </div>
        </div>
        </div>
    </div>
{{-- End Edit Variant Modal --}}

{{-- Edit Category Modal --}}
<div class="modal fade" id="editCategoryModal" tabindex="-1" role="dialog" aria-labelledby="editCategoryModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Edit Category</h5>
        </div>
        <div class="modal-body">
        
        
            <div class="table-responsive" id="all-category">
                <table class="table table-borderless mb-0">
                    <div class="input-group"  id="categoryForm">
                        <input type="text"  name="category" id="category" class="form-control border" placeholder="Add new category" aria-label="Search input" >
                        <button class="btn btn-primary" type="submit" onclick="addCategory()" id="addcategory">+</button>
                    </div>
                    <thead>
                        <tr>
                            <th colspan="2">Category Title</th>
                            <th >Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($categories as $category)
                        <tr>
                            <td colspan="2"><input type="text" id="category-{{ $category->id }}" class="form-control" value="{{ $category->category }}"></td>
                            
                            <td >
                                <span class="btn btn-outline-primary btn-sm" onclick="saveCategory({{ $category->id }})"><i class="bx bx-save"></i></span>
                                <span class="btn btn-outline-danger btn-sm" onclick="deleteCategory({{ $category->id }})"><i class="bx bxs-trash"></i></span>
                            </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div>
        
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="window.location.reload(true)">Exit</button>
        </div>
    </div>
    </div>
</div>
{{-- End Edit Category Modal --}}

{{-- Start Edit Option Modal --}}
    <div class="modal fade" id="editOptionModal" tabindex="-1" role="dialog" aria-labelledby="editVariantModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Edit Options</h5>
            </div>
            <div class="modal-body">
            
            
            <div class="table-responsive" id="list-options">
                <table class="table table-borderless mb-0">

                    <thead>
                        <tr>
                            <th>Name</th>
                            <th >Price</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                           <td><input type="text" class="form-control" id="option_name" name="option_name" placeholder="Option Name"></td>
                           <td><input type="number" class="form-control" min="0" step="0.1" id="option_price" name="option_price" placeholder="Price"></td>
                           <td><button class="btn btn-primary" onclick="saveOptions({{ $product->id }})">Add Option</button></td>
                        
                        @foreach ($options as $option)
                        <tr>
                            <td ><input type="text" id="option-title-{{ $option->id }}" class="form-control" value="{{ $option->option_name }}"></td>
                            <td ><input type="text" id="option-price-{{ $option->id }}" class="form-control" value="{{ $option->option_price }}"></td>
                            <td >
                                <span class="btn btn-outline-primary btn-sm" onclick="saveEditOption({{ $option->id }})"><i class="bx bx-save"></i></span>
                                <span class="btn btn-outline-danger btn-sm" onclick="deleteOption({{ $option->id }})"><i class="bx bxs-trash"></i></span>
                            </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="window.location.reload(true)">Exit</button>
            </div>
        </div>
        </div>
    </div>

{{-- End Edit Option Modal --}}

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script>

    // function getOptions(){
    //     let option1 = document.getElementById('option1');
    //     let option2 = document.getElementById('option2');
    //     if(option1.checked == true){
    //         console.log(document.getElementById('option1').value);
    //     }
    //     if(option2.checked == true){
    //         console.log(document.getElementById('option2').value);
    //     }
        
        
    // }
    
    document.getElementById('options').style.display = "none";
    document.getElementById('hideoptionlink').style.display = "none";
    let product_price1 = document.getElementById('variations').value;
    document.getElementById("discount").max = product_price1;

    let product_price = document.getElementById('variations').value;
    document.getElementById('variantprice').innerHTML = product_price;
    
    function changePrice(){
        
        let product_price = document.getElementById('variations').value;
        document.getElementById('variantprice').innerHTML = product_price;
        document.getElementById("discount").max = product_price;

    }

    function saveDescription(product_id){
        let description = document.getElementById('description').value;
        
        axios.post('/save-description', {
            id:product_id,
            description:description,
            })
            .then(function (response) { 
               
            })
            .catch(function (error) {
                console.log(error);
            });
        
        }
    
    
    
        function saveOptions(product_id){
        let option_name = document.getElementById('option_name').value;
        let option_price = document.getElementById('option_price').value;
        option_name = option_name.trim();
        if(option_name.length > 0){
            axios.post('/save-option', {
            product_id:product_id,
            option_name:option_name,    
            option_price: option_price
            
            })
            .then(function (response) { 
               alert("Saved Option");
               $( "#list-options" ).load(window.location.href + " #list-options" );
               $( "#select-options" ).load(window.location.href + " #select-options" );

            })
            .catch(function (error) {
                console.log(error);
            });
        }else{
            document.getElementById('category').value = " ";
            alert("Please enter a valid category");
        }
        // location.reload();
    }

    function saveImage(product_id){
        let image_link = document.getElementById('image_link').value;
        image_link = image_link.trim();
        if(image_link.length > 0){
            axios.post('/add-image', {
            product_id:product_id,    
            image_link: image_link,
            
            })
            .then(function (response) {

                // var a = document.createElement('a');
                
                // a.href = '/categories-'+category;
                // a.innerText = category;

                // var cont = document.getElementById('categories');
                // cont.appendChild(a);
                
                console.log(response);
               
                // document.getElementById('category').value = " ";

            })
            .catch(function (error) {
                console.log(error);
            });
        }else{
            document.getElementById('category').value = " ";
            alert("Please enter a valid category");
        }
        location.reload();
    }

    function deleteImage(image_id){
        // let id = document.getElementById('deleteImageBut').value;
        
        if(confirm('Are you sure you want to delete the image?')){
            axios.post('/delete-image', {
            id:image_id,
            })
            .then(function (response) { 
               alert("Image Deleted");
            //    location.reload();
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });
        }
    }
    function addCategory(){
            
        let category = document.getElementById('category').value;
        category = category.trim();
        let product_id = @json($product->id);

        if(category.length > 0){
            axios.post('/add-category', {
            product_id:product_id,    
            category: category,
            
            })
            .then(function (response) {

                // var a = document.createElement('a');
                
                // a.href = '/categories-'+category;
                // a.innerText = category;

                // var cont = document.getElementById('categories');
                // cont.appendChild(a);
                $( "#categories" ).load(window.location.href + " #categories" );
                $( "#all-category" ).load(window.location.href + " #all-category" );
                
                // document.getElementById('category').value = " ";

            })
            .catch(function (error) {
                console.log(error);
            });
        }else{
            document.getElementById('category').value = " ";
            alert("Please enter a valid category");
        }
            
        }
    function saveCategory(id){
        let category = document.getElementById('category-'+id).value;
        axios.post('/update-category', {
            id:id,
            category:category,    
        })
        .then(function (response) {
            $( "#categories" ).load(window.location.href + " #categories" );
            $( "#all-category" ).load(window.location.href + " #all-category" );
            alert("Category updated succesfully!");
            
        })
        .catch(function (error) {
            console.log("Something went wrong! Operation cannot proceed.");
        });
    }    
    function deleteCategory(id){
        if(confirm('Are you sure you want to delete the category?')){
            axios.post('/delete-category', {
                    id:id,  
                })
                .then(function (response) {
                    $( "#categories" ).load(window.location.href + " #categories" );
                    $( "#all-category" ).load(window.location.href + " #all-category" ); 
                })
                .catch(function (error) {
                console.log(error);
                });
            }
    }    

function editProductTitle(id){
    let title = document.getElementById('input-product-title').value;
    axios.post('/edit-title', {
            id:id, 
            title:title,   
        })
        .then(function (response) {
           
            location.reload();
            
        })
        .catch(function (error) {
           console.log("Something went wrong! Operation cannot proceed.");
        });
   
}

function hideProductTitle(){
 
    document.getElementById('product-title').style.display = "none";
    document.getElementById('input-product').style.display = "";

}

function addVariant(product_id){
    let variation_title = document.getElementById('variant-title').value;
    let price = document.getElementById('variant-price').value;
    let stocks = document.getElementById('variant-quantity').value;
    let weight = document.getElementById('variant-quantity').value;
    axios.post('/save-variant', {
        product_id:product_id,
            variation_title:variation_title, 
            price:price,
            weight:weight,
            stocks:stocks,     
        })
        .then(function (response) {
           
            alert("Variant added succesfully!");
            $( "#list-variants" ).load(window.location.href + " #list-variants" ); 
            $( "#variants-dropdown" ).load(window.location.href + " #variants-dropdown" );
            
        })
        .catch(function (error) {
            console.log(error);
        });
}

function saveVariant(variantId){
    let title = document.getElementById('variant-title-'+variantId).value;
    let price = document.getElementById('variant-price-'+variantId).value;
    let quantity = document.getElementById('variant-quantity-'+variantId).value;
    axios.post('/save-edited-variant', {
            id:variantId,
            variation_title:title, 
            price:price,
            quantity:quantity,     
        })
        .then(function (response) {
           
            alert("Variant updated succesfully!");
            $( "#variants-dropdown" ).load(window.location.href + " #variants-dropdown" );
            $( "#list-variants" ).load(window.location.href + " #list-variants" ); 
            
        })
        .catch(function (error) {
            console.log("Something went wrong! Operation cannot proceed.");
        });
}
function disableVariant(id,productId){
    if(confirm('Are you sure you want to disable the variant?')){
    axios.post('/disable-variant', {
            id:id,  
            productId:productId
        })
        .then(function (response) {
            $( "#variants-dropdown" ).load(window.location.href + " #variants-dropdown" );
            $( "#list-variants" ).load(window.location.href + " #list-variants" ); 
        })
        .catch(function (error) {
           console.log(error);
        });
    }
}
function enableVariant(id,productId){
    
    axios.post('/enable-variant', {
            id:id,
            productId:productId
        })
        .then(function (response) {
            $( "#variants-dropdown" ).load(window.location.href + " #variants-dropdown" );
            $( "#list-variants" ).load(window.location.href + " #list-variants" ); 
        })
        .catch(function (error) {
           
        });
    }



function deleteVariant(variantId,product_id){
    if(confirm('Are you sure you want to delete the variant?')){
    axios.post('/delete-variant', {
            id:variantId,
            productId:product_id,
        })
        .then(function (response) {
            alert("Successfully deleted the variant!");
            $( "#variants-dropdown" ).load(window.location.href + " #variants-dropdown" );
            $( "#list-variants" ).load(window.location.href + " #list-variants" ); 
        })
        .catch(function (error) {
           console.log(error)
        });
    }
}

function saveEditOption(optionId){
    let option_name = document.getElementById('option-title-'+optionId).value;
    let option_price = document.getElementById('option-price-'+optionId).value;
    
    axios.post('/save-edited-option', {
            id:optionId,
            option_name:option_name, 
            option_price:option_price, 
        })
        .then(function (response) {
            
            alert("Option saved succesfully!");
            
        })
        .catch(function (error) {
            console.log("Something went wrong! Operation cannot proceed.");
        });
   
}
function deleteOption(optionId){
    if(confirm('Are you sure you want to delete the option?')){
    axios.post('/delete-option', {
            id:optionId,  
        })
        .then(function (response) {
            alert("Successfully deleted the option!");
            $( "#list-options" ).load(window.location.href + " #list-options" );
            $( "#select-options" ).load(window.location.href + " #select-options" );
            // location.reload();
            
           
            
        })
        .catch(function (error) {
           
        });
    }
}

function saveDiscount(productId){
    let discount = parseInt(document.getElementById('discount').value);
    let product_price = parseInt(document.getElementById('variantprice').value);
    
    let variations = @json($product_variations);
    let pass = 0;
    variations.forEach(variant => {
        if(discount >= variant.price ){
            pass++;
        }
    });
    if(pass > 0){
        alert("The discount cannot be more than the price of any variant.")
    }else{
        axios.post('/save-discount', {
            id:productId,
            discount:discount, 
        })
        .then(function (response) {
            $( "#discount-div" ).load(window.location.href + " #discount-div" );
            
        })
        .catch(function (error) {
            console.log(error);
        });
      console.log("Discount saved succesfully!");  
    }
     
}
</script>

@endsection
