@extends('layouts.master')

@section('title') @lang('translation.Add_Product') @endsection

@section('css')
<!-- select2 css -->
<link href="{{ URL::asset('/assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />

<!-- dropzone css -->
<link href="{{ URL::asset('/assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Product @endslot
@slot('title') Add Variant @endslot
@endcomponent

@if (session('status'))
<div class="alert alert-success">
    {{ session('status') }}
</div>
@endif
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="card-title">Add Variant</h4>
                <p class="card-title-desc">Fill all information below</p>

                <form method="post" action="/save-variant">
                    @csrf
                    @method('POST')
                    <input id="price" name="product_id" type="hidden" class="form-control" placeholder="Price" value="{{ $product_id }}">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="title">Variant Title</label>
                                <input id="title" name="title" type="text" class="form-control" placeholder="Product Name" required>
                            </div>
                            
                                
                           
                            <div class="mb-3">
                                <label for="price">Price</label>
                                <input id="price" name="price" type="number" step="0.1" min="0" class="form-control" placeholder="Price" required>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="weight">Weight</label>
                                <input id="weight" name="weight" type="number" step="0.1" min="0" class="form-control" placeholder="Weight" >
                            </div>
                            <div class="mb-3">
                                <label for="quantity">Quantity</label>
                                <input id="quantity" name="quantity" type="number" step="0.1" min="0" class="form-control" placeholder="Quantity" required>
                            </div>
                            
                        </div>
                    </div>
                    <div class="d-flex flex-wrap gap-2">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save Changes</button>
                    </div>
                </form>

            </div>
        </div>

        <!--------Images 
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-3">Product Images</h4>

                <form action="/" method="post" class="dropzone">
                    <div class="fallback">
                        <input name="file" type="file" multiple />
                    </div>

                    <div class="dz-message needsclick">
                        <div class="mb-3">
                            <i class="display-4 text-muted bx bxs-cloud-upload"></i>
                        </div>

                        <h4>Drop files here or click to upload.</h4>
                    </div>
                </form>
            </div>
            

        </div> 
          ----> 
        <!-- end card-->

        
    </div>
</div>
<!-- end row -->

@endsection
@section('script')
<!-- select 2 plugin -->
<script src="{{ URL::asset('/assets/libs/select2/select2.min.js') }}"></script>

<!-- dropzone plugin -->
<script src="{{ URL::asset('/assets/libs/dropzone/dropzone.min.js') }}"></script>

<!-- init js -->
<script src="{{ URL::asset('/assets/js/pages/ecommerce-select2.init.js') }}"></script>

<!-- form repeater js -->

@endsection