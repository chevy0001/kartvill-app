@extends('layouts.master-without-nav-no-script')

@section('title') @lang('All Products') @endsection

@section('css')
    <!-- ION Slider -->
    <link href="{{ URL::asset('/assets/libs/ion-rangeslider/ion-rangeslider.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Ecommerce @endslot
        @slot('title') All Products @endslot
    @endcomponent
<div class="container">
    <div class="row mt-5">
        {{-- <div class="col-lg-2">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Filter</h4>

                    <div>
                        <h5 class="font-size-14 mb-3">Categories</h5>
                        <ul class="list-unstyled product-list">
                            @foreach ($categories as $category)
                            <li><a href="#">{{ ucfirst($category->category) }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    
                </div>
            </div>

        </div> --}}
        <div class="col-lg-12">

            <div class="row mb-3">
                <div class="col-xl-4 col-sm-6">
                    <div class="mt-2">
                        <h5 ><a class="text-dark" href="{{ route('product.displayAllProducts') }}">Menu</a></h5>
                    </div>
                </div>
                <div class="col-lg-8 col-sm-6">
                    {{-- <form  class="mt-4 mt-sm-0 float-sm-end d-sm-flex align-items-center"> --}}
                        {{-- <div class="search-box me-2">
                            <div class="position-relative">
                                <input type="text" id="search" class="form-control border-0"  placeholder="Search...">
                                <i class="bx bx-search-alt search-icon"></i>
                            </div>
                        </div> --}}
                        <form>
                        <div class="mt-4 mt-sm-0 float-sm-end d-sm-flex align-items-center"></div>
                        <div class="input-group  search-box "  id="searchForm">
                            <input type="text"  name="search" id="search" class="form-control border-0" placeholder="Enter Product Name, Category, Vendor Name" aria-label="Search input" >
                            <button class="btn btn-primary" onclick="redirectToSearch()" id="searchBut">Search</button>
                        </div>
                       


                    </form>
                </div>
            </div>
            <div class="row">
                @foreach ($products as $product)
                <div class="col-xl-4 col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="product-img position-relative">
                                <div class="avatar-sm product-ribbon">
                                    @if($product->discount > 0)
                                        <span class="avatar-title rounded-circle  bg-danger">
                                            {{ $product->discount }}
                                        </span>
                                    @endif
                                    
                                </div>
                                {{-- <img src="{{ URL::asset('/assets/images/product/img-1.png') }}" alt="" class="img-fluid mx-auto d-block"> --}}
                                <a href="{{ route('product.displayProduct',['producttitle'=>$product->title,'productid'=>$product->product_id]) }}">
                                    <img src="{{ $product->image_link }}" alt="" class="img-fluid mx-auto d-block">
                                </a>
                            </div>
                            <div class="mt-4 text-center">
                                <h5 class="text-truncate mb-3"><a href="{{ route('product.displayProduct',['producttitle'=>$product->title,'productid'=>$product->product_id]) }}" class="text-dark">{{ $product->title }} </a><br/>
                                    <small><a href="/search-{{ strtolower($product->store_name) }}" class="text-dark pb-3">{{ $product->store_name }}</a></small>
                                </h5>
                                
                                
                                @if($product->discount > 0)
                                <h5 class="my-0"><span class="text-muted "><del>{{ number_format($product->price,2) }}</del></span> <b>{{ number_format($product->price - $product->discount,2) }}</b></h5>
                                @else
                                <h5 class="my-0"><span class="text-muted "></span> <b>{{ number_format($product->price,2) }}</b></h5>
                                <input data-toggle="touchspin" type="number" id="item-quantity-{{ $product->product_id }}" value="1" min="1" max="{{ $product->stocks }}">
                                @endif

                                
                                
                            </div>
                            <div class="text-center">
                                @if($product->variant_count > 1)
                                <a href="{{ route('product.displayProduct',['producttitle'=>$product->title,'productid'=>$product->product_id]) }}" type="button" id="but-{{ $loop->iteration }}" value="{{ $product->product_id }}" class="btn btn-primary waves-effect waves-light mt-2 me-1">
                                    <i class="bx bx-cart me-2"></i> Choose Options
                                </a> 
                                @else
                                <button onclick="addToCart({{ $product->id }},{{ $product->product_id }})" type="button" id="but-{{ $loop->iteration }}" value="{{ $product->product_id }}" class="btn btn-primary waves-effect waves-light mt-2 me-1">
                                    <i class="bx bx-cart me-2"></i> Add to cart
                                </button> 
                                @endif
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            <!-- end row -->
          
            <div class="row">
                <div class="col-lg-12">
                    <ul class="pagination pagination-rounded justify-content-center mt-3 mb-4 pb-1">
                        {{ $products->links()  }}
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- end row -->
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>   
    
    <script>

        const timeOut = setTimeout(() => {
            console.log("time");cartCount
            $( "#cart-notification" ).load(window.location.href + " #cart-notification" );
            
        }, 5000);
        
        function redirectToSearch(){
            event.preventDefault();
            let keyword = document.getElementById('search').value;
            location.href = '/search-'+keyword;
        }

        function addToCart(variation_id,product_id){
            let quantity = document.getElementById('item-quantity-'+product_id).value;

        axios.post('/add-to-cart', {
            variation_id:variation_id,    
            product_id: product_id,
            quantity: quantity
            })
            .then(function (response) {
                $( "#cart-notification" ).load(window.location.href + " #cart-notification" );
                document.getElementById('cartCount').innerHTML = response['data']['order_count']['original']['order_count'];
            })
            .catch(function (error) {
                
                console.log(error);
            }); 
        }
        
        // function changeURL(){
        //     let searchVal = document.getElementById('search').value;
        //     window.history.replaceState("string", "Title", "/search-"+searchVal);
        //     if(searchVal == ""){
        //         window.history.replaceState("string", "Title", "/all-products");
        //     }
        // }
        
    </script>
@endsection
@section('script')
    <!-- Ion Range Slider-->
    <script src="{{ URL::asset('/assets/libs/ion-rangeslider/ion-rangeslider.min.js') }}"></script>

    <!-- init js -->
    <script src="{{ URL::asset('/assets/js/pages/product-filter-range.init.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/spectrum-colorpicker/spectrum-colorpicker.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
    

    <script src="{{ URL::asset('/assets/js/pages/form-advanced.init.js') }}"></script>

   
@endsection
