@extends('layouts.master')

@section('title') @lang('Store Settings') @endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Store @endslot
@slot('title') Settings @endslot
@endcomponent

<div class="row">
    <div class="col-xl-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Store Availability</h4>

              
                    <div>
                        <button onclick="updateAvailability()" id="availability" class="btn w-md">Open Store</button>
                    </div>
               
            </div>
            <!-- end card body -->
        </div>
        <!-- end card -->
    </div>
   
</div>
@if(Auth::user()->role == 1)

<div class="row">
    <div class="col-xl-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Default Kartvill Commission</h4>

              
                    <div>
                        <input class="form-control mb-2" type="number" id="default_kartvill_commission" name="default_kartvill_commission" value="{{ $kartvill_setting->default_kartvill_commission }}">
                        
                    </div>
               
            </div>
            <!-- end card body -->
        </div>
        <!-- end card -->
    </div>
   
</div>

<div class="row">
    <div class="col-xl-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Default Rider Commission</h4>

              
                    <div>
                        <input class="form-control mb-2" type="number" id="default_rider_commission" name="default_rider_commission" value="{{ $kartvill_setting->default_rider_commission }}">
                        
                    </div>
               
            </div>
            <!-- end card body -->
        </div>
        <!-- end card -->
    </div>
</div>
<div class="row">
    <div class="col-xl-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Delivery Rate Per Kilometer </h4>
                    <div>
                        <input class="form-control mb-2" type="number" id="delivery_rate" name="delivery_rate" value="{{ $kartvill_setting->delivery_rate }}">
                        
                    </div>
            </div>
            <!-- end card body -->
        </div>
        <!-- end card -->
    </div>
</div>
<div class="row">
    <div class="col-xl-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Additional Delivery Rate </h4>
                    <div>
                        <input class="form-control mb-2" type="number" id="additional_delivery_rate" name="additional_delivery_rate" value="{{ $kartvill_setting->additional_delivery_rate }}">
                        
                    </div>
            </div>
            <!-- end card body -->
        </div>
        <!-- end card -->
    </div>
</div>
<div class="row">
    <div class="col-xl-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Bonus Delivery Fee </h4>
                    <div>
                        <input class="form-control mb-2" type="number" id="bonus_delivery_fee" name="bonus_delivery_fee" value="{{ $kartvill_setting->bonus_delivery_fee ?? '0' }}">
                        
                    </div>
            </div>
            <!-- end card body -->
        </div>
        <!-- end card -->
    </div>
</div>
<div class="row">
    <div class="col-xl-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Minimum Kilometer</h4>
                    <div>
                        <input class="form-control mb-2" type="number" id="minimum_kilometer" name="minimum_kilometer" value="{{ $kartvill_setting->minimum_kilometer }}">
                        
                    </div>
            </div>
            <!-- end card body -->
        </div>
        <!-- end card -->
    </div>
</div>
<div class="row">
    <div class="col-xl-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Minimum Kilometer Rate</h4>
                    <div>
                        <input class="form-control mb-2" type="number" id="minimum_kilometer_rate" name="minimum_kilometer_rate" value="{{ $kartvill_setting->minimum_kilometer_rate }}">
                        <button class="btn btn-success w-md" onclick="saveSettings()" >Save</button>
                    </div>
            </div>
            <!-- end card body -->
        </div>
        <!-- end card -->
    </div>
</div>

@endif
<!-- end row -->
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>
        var avail = @json($availability->availability);
       
        if(avail == 1){
            document.getElementById("availability").classList.add('btn-success');
            document.getElementById("availability").classList.remove('btn-dark');
            document.getElementById("availability").innerHTML = 'Close Store';
            }
            else{
            document.getElementById("availability").classList.add('btn-dark');
            document.getElementById("availability").classList.remove('btn-success');
            document.getElementById("availability").innerHTML = 'Open Store';
            }


        function updateAvailability(){
            avail = !avail;
            console.log(avail);
            if(avail == true){
            document.getElementById("availability").classList.add('btn-success');
            document.getElementById("availability").classList.remove('btn-dark');
            document.getElementById("availability").innerHTML = 'Close Store';
            }
            else{
            document.getElementById("availability").classList.add('btn-dark');
            document.getElementById("availability").classList.remove('btn-success');
            document.getElementById("availability").innerHTML = 'Open Store';
            }

            axios.post('/update-store-availability', {  
            availability: avail,
            
            })
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });
        }

        function saveSettings(){

            var default_kartvill_commission = document.getElementById('default_kartvill_commission').value;
            var default_rider_commission = document.getElementById('default_rider_commission').value;
            var delivery_rate = document.getElementById('delivery_rate').value;
            var additional_delivery_rate = document.getElementById('additional_delivery_rate').value;
            var minimum_kilometer = document.getElementById('minimum_kilometer').value;
            var minimum_kilometer_rate = document.getElementById('minimum_kilometer_rate').value;
            var bonus_delivery_fee = document.getElementById('bonus_delivery_fee').value;
            bonus_delivery_fee
            axios.post('/save-settings', {  
                default_kartvill_commission: default_kartvill_commission,
                default_rider_commission:default_rider_commission,
                delivery_rate:delivery_rate,
                additional_delivery_rate:additional_delivery_rate,
                minimum_kilometer:minimum_kilometer,
                minimum_kilometer_rate:minimum_kilometer_rate,
                bonus_delivery_fee:bonus_delivery_fee,
            })
            .then(function (response) {
                alert("Saved Settings");
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });
        }
    </script>
@endsection