@extends('layouts.master')

@section('title') @lang('translation.Task_List') @endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Tasks @endslot
        @slot('title') Task List @endslot
    @endcomponent

    <div class="row">
        {{-- <p id="first_name"></p>
        <button onclick="updateCustomer()">Submit</button>
        <button onclick="getData()">Get Data</button> --}}
    </div>
    <!-- end row -->

@endsection
@section('script')
    <!-- apexcharts -->
    <script src="{{ URL::asset('/assets/libs/apexcharts/apexcharts.min.js') }}"></script>

    <!-- dashboard init -->
    <script src="{{ URL::asset('/assets/js/pages/tasklist.init.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script>
        
        // function updateCustomer(){
            
        //     axios.post('/updateCustomer1', {
        //     id:'2',    
        //     first_name: 'Fred',
        //     last_name: 'Flintstone'
        //     })
        //     .then(function (response) {
        //         console.log(response);
        //     })
        //     .catch(function (error) {
        //         console.log(error);
        //     });
        // }

        // function getData(){
            // axios.get('/customerpoints-6061797933213')
            // .then(function (response) {
            //     // handle success4
            //     //value = 
            //     document.getElementById('first_name').innerHTML = response['data']['remaining_points'];
            //     console.log(response);
            // })
            // .catch(function (error) {
            //     // handle error
            //     console.log(error);
            // })
            // .then(function () {
            //     // always executed
            // });
        // }
       
    </script>
@endsection
