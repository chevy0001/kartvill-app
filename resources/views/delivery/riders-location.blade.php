@extends('layouts.master')

@section('title') @lang('Order') @endsection

@section('css')
    <!-- bootstrap-touchspin css -->
    <link href="{{ URL::asset('/assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css') }}" rel="stylesheet" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Riders @endslot
        @slot('title')  Map @endslot
    @endcomponent

    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
    <div class="row">
       

        <div class="col-xl-12">
            
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-3">Rider map</h4>
                    
                    <div class="table-responsive" id="map" style="width:100;height:600px;"></div>
                    
                    {{-- Customer MAP --}}
                    
                    <script>

                    var locations = @json($location);
                   
                    

                       function initMap() { 
                        
                        map = new google.maps.Map(document.getElementById('map'), {
                        center: {lat: 6.124139672031334, lng: 125.16919205293425}, 
                        zoom: 12
                        });
                       
                        locations.forEach(function(location) {
                    
                            infowindow = new google.maps.InfoWindow({
                                content: location.first_name,
                            });

                            marker = new google.maps.Marker({
                                    position: new google.maps.LatLng(location.latitude,location.longitude),
                                    name:name,
                                    map: map,
                                    title: "Hello World!",
                                }); 

                            // marker.addListener("click", () => {
                                infowindow.open({
                                anchor: marker,
                                map,
                                shouldFocus: false,
                                });
                            // });    
                    
                    
                    });
      
                }
                
            
                
  
                
               
                    </script>

                   
                </div>
            </div>
            <!-- end card -->
        </div>
    </div>
    <!-- end row -->

@endsection
@section('script')
    <!-- bootstrap-touchspin -->
    <script src="{{ URL::asset('/assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js') }}"></script>

    <!-- init js -->
    <script src="{{ URL::asset('/assets/js/pages/ecommerce-cart.init.js') }}"></script>
    
@endsection
