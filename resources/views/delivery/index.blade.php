<x-admin-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>  
        <div class="container">
            <div class="row">
                <div class="card w-full" style="">
                    {{-- <img class="card-img-top"  src="" alt="Card image cap"> --}}
                    <div class="card-body">
                        <form action="{{ route('delivery.availability') }}" method="post">
                            @csrf
                           
                        <div class="d-flex justify-content-between">
                            <label for="availability" class="pt-2">Availability</label> 
                            <select class="form-control ml-2" id="availability" name="availability">
                                <option selected value="{{ $availability->availability }}">
                                    @if($availability->availability == 0)
                                        Off
                                    @else
                                        Available
                                    @endif
                                </option>
                                <option value="1">Available</option>
                                <option value="0">Off</option>
                              </select>
                              <button class="btn btn-success ml-1">Submit</button>
                        </div>
                        
                    </div>
                </form>
                </div>
            </div>
        </div>
    
</x-admin-layout>
  
