@extends('layouts.master')

@section('title') @lang('translation.Wallet') @endsection

@section('css')
<!-- DataTables -->
<link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') GCash @endslot
@slot('title') Wallet @endslot
@endcomponent

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

<div class="row">
    <div class="col-xl-4">
        <div class="card">
            <div class="card-body">

                <div class="d-flex align-items-start">
                    <div class="flex-shrink-0 me-4">
                        <i class="mdi mdi-account-circle text-primary h1"></i>
                    </div>

                    <div class="flex-grow-1">
                        <div class="text-muted">
                            <h5>{{ ucfirst(Auth::user()->first_name) }} {{ ucfirst(Auth::user()->last_name) }}</h5>
                            <p class="mb-1">{{ $store_name->store_name ?? '' }}</p>
                            <p class="mb-1">{{ Auth::user()->email }}</p>
                            <p class="mb-0">{{ Auth::user()->phone_number }}</p>
                        </div>

                    </div>

                    <div class="dropdown ms-2">
                        <a class="text-muted" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                        </a>

                        {{-- <div class="dropdown-menu dropdown-menu-end">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div> --}}
                    </div>
                </div>
            </div>
            <div class="card-body border-top">

                <div class="row">
                    <div class="col-sm-6">
                        <div>
                            <p class="text-muted mb-2">Available Balance</p>
                            <h5>₱{{ number_format($wallet->balance,2) ?? '0'}}</h5>
                        </div>
                    </div>
                    {{-- <div class="col-sm-6">
                        <div class="text-sm-end mt-4 mt-sm-0">
                            <p class="text-muted mb-2">Since last month</p>
                            <h5>+ $ 248.35 <span class="badge bg-success ms-1 align-bottom">+ 1.3 %</span></h5>

                        </div>
                    </div> --}}
                </div>
            </div>

            <div class="card-body border-top">
                <p class="text-muted mb-4"></p>
                <div class="text-center">
                    <div class="row">
                        {{-- <div class="col-sm-4">
                            <div>
                                <div class="font-size-24 text-primary mb-2">
                                    <i class="bx bx-send"></i>
                                </div>

                                <p class="text-muted mb-2">Send</p>
                                <h5> <input class="form-control" min="1" type="number" max="{{ $wallet->balance ?? '0' }}" id="send" name="send" value="1000"></h5>

                                <div class="mt-3">
                                    <a href="javascript: void(0);" class="btn btn-primary btn-sm w-md">Send</a>
                                </div>
                            </div>
                        </div> --}}
                        <div class="col-sm-4">
                            <div class="mt-4 mt-sm-0">
                                <div class="font-size-24 text-primary mb-2">
                                    <i class="bx bx-import"></i>
                                </div>

                                <p class="text-muted mb-2">Top-up </p>
                                <form method="post" action="/topup">
                                    @csrf
                                    @method('POST')
                                <h5> 
                                    <input class="form-control" min="10" type="number" id="topup" name="topup" value="1000">
                                </h5>
                                <small>Please note that Gcash Getpaid will charge 2% for every transaction. 
                                    However, if you pay over the counter we can avoid the fee. 
                                    Please go to our cashier/staff for instructions. Thank you.</small>
                                <div class="mt-3">
                                   
                                    <button type="submit" class="btn btn-primary btn-sm w-md">Top-up via Gcash</button>
                                    
                                </div>
                            </form>
                            <div class="mt-3">
                                <button  class="btn btn-primary btn-sm w-md" onclick="topUpCash()">Top-up Cash</button>
                            </div>
                            </div>
                        </div>

                        {{-- <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
                        <script>
                            
                             function topUp(){
            
                            var amount = document.getElementById('topup').value;
                            
                            axios.post('/topup', { 
                            amount: amount,
                           
                            })
                            .then(function (response) {
                                console.log(response);
                            })
                            .catch(function (error) {
                                console.log(error);
                            });
                        }
                        </script>     --}}


                        {{-- <div class="col-sm-4">
                            <div class="mt-4 mt-sm-0">
                                <div class="font-size-24 text-primary mb-2">
                                    <i class="bx bx-wallet"></i>
                                </div>

                                <p class="text-muted mb-2">Withdraw</p>
                                <h5> <input class="form-control" type="number" id="topup" name="topup" value="1000"></h5>

                                <div class="mt-3">
                                    <a href="javascript: void(0);" class="btn btn-primary btn-sm w-md">Withdraw</a>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-xl-8">
        <div class="row">
            {{-- <div class="col-sm-4">
                <div class="card mini-stats-wid">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="flex-shrink-0 me-3 align-self-center">
                                <i class=" h2 text-warning mb-0"></i>
                            </div>
                            <div class="flex-grow-1">
                                <p class="text-muted mb-2">Wallet Balance</p>
                                <h5 class="mb-0"> ₱{{ number_format($wallet->balance,2) ?? '0'}}</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
            {{-- <div class="col-sm-4">
                <div class="card mini-stats-wid">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="flex-shrink-0 me-3 align-self-center">
                                <i class="mdi mdi-ethereum h2 text-primary mb-0"></i>
                            </div>
                            <div class="flex-grow-1">
                                <p class="text-muted mb-2">Ethereum Wallet</p>
                                <h5 class="mb-0">0.04121 ETH <span class="font-size-14 text-muted">= $ 8235.00</span></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card mini-stats-wid">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="flex-shrink-0 me-3 align-self-center">
                                <i class="mdi mdi-litecoin h2 text-info mb-0"></i>
                            </div>
                            <div class="flex-grow-1">
                                <p class="text-muted mb-2">litecoin Wallet</p>
                                <h5 class="mb-0">0.00356 BTC <span class="font-size-14 text-muted">= $ 4721.00</span></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
        </div>
        <!-- end row -->

        {{-- <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-3">Overview</h4>

                <div>
                    <div id="overview-chart" class="apex-charts" dir="ltr"></div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
<!-- end row -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Activities</h4>


                <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-bs-toggle="tab" href="#home1" role="tab">
                            <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                            <span class="d-none d-sm-block">Top-ups</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="tab" href="#acceptedOrders" role="tab">
                            <span class="d-block d-sm-none"><i class="fas fa-cart-plus"></i></span>
                            <span class="d-none d-sm-block">
                                @if(Auth::user()->role !=5)    
                                    Accepted Orders
                                @else
                                    Transaction History
                                @endif
                            
                            </span>
                        </a>
                    </li>
                @if(Auth::user()->role != 5)
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="tab" href="#messages1" role="tab">
                            <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                            <span class="d-none d-sm-block">Sent Funds </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="tab" href="#settings1" role="tab">
                            <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                            <span class="d-none d-sm-block">Withdrawals</span>
                        </a>
                    </li>
                @endif
                </ul>

                <div class="tab-content p-3 text-muted">
                    <div class="tab-pane active" id="home1" role="tabpanel">
                        <div class="table-responsive">
                            <table id="datatable" class="table table-hover dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr>
                                        <th>Request Code</th>
                                        <th>Amount</th>
                                        <th>Date</th>
                                        
                                    </tr>
                                </thead>
    
                                <tbody>
                                    @foreach ( $topups as $topup )
                                        <tr>
                                            @if ($topup->status == 'pending')
                                            <td><a href="https://getpaid.gcash.com/checkout/{{ $topup->hash }}" class="text-body fw-bold">Continue Topup</a></td>
                                            @else
                                            <td><span class="text-body fw-bold">{{ $topup->request_code }} - Paid</span></td>
                                            @endif
                                            
                                            
                                            <td>{{ $topup->amount }}</td>
                                            <td>{{ $topup->created_at }}</td>
                                        </tr>
                                    @endforeach
                                   
    
    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="acceptedOrders" role="tabpanel">
                        <div class="table-responsive">
                            <table id="datatable" class="table table-hover dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr>
                                        <th>Order Number</th>
                                        <th>Vendor Price</th>
                                        <th>Commission</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th>Remarks</th>
                                        <th>MOP</th>
                                        <th>Date</th>
                                        
                                    </tr>
                                </thead>
    
                                <tbody>
                                    @foreach ($orders as $order)
                                        
                                    
                                    <tr>
                                        <td>{{ $order->order_number }}</td>
                                        <td>{{ $order->vendor_net }}</td>
                                        <td>{{ $order->commission }}</td>
                                        <td>{{ $order->total_price }}</td>
                                        <td>{{ $order->financial_status }}</td>
                                        <td>@if ($order->fulfillment_status == 'Cancelled')
                                            Commission Refunded
                                            @else
                                            {{ $order->fulfillment_status }}
                                        @endif</td>
                                        <td>@if($order->gateway == "Cash on Delivery (COD)")
                                            Cash
                                           @else
                                            Online Payment
                                           @endif
                                       </td>
                                        <td>{{ $order->created_at }}</td>
                                    </tr>
    
                                    @endforeach
    
    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="messages1" role="tabpanel">
                        <p class="mb-0">
                            Etsy mixtape wayfarers, ethical wes anderson tofu before they
                            sold out mcsweeney's organic lomo retro fanny pack lo-fi
                            farm-to-table readymade. Messenger bag gentrify pitchfork
                            tattooed craft beer, iphone skateboard locavore carles etsy
                            salvia banksy hoodie helvetica. DIY synth PBR banksy irony.
                            Leggings gentrify squid 8-bit cred pitchfork. Williamsburg banh
                            mi whatever gluten-free carles.
                        </p>
                    </div>
                    <div class="tab-pane" id="settings1" role="tabpanel">
                        <p class="mb-0">
                            Trust fund seitan letterpress, keytar raw denim keffiyeh etsy
                            art party before they sold out master cleanse gluten-free squid
                            scenester freegan cosby sweater. Fanny pack portland seitan DIY,
                            art party locavore wolf cliche high life echo park Austin. Cred
                            vinyl keffiyeh DIY salvia PBR, banh mi before they sold out
                            farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral,
                            mustache readymade keffiyeh craft.
                        </p>
                    </div>
                </div>

                <div class="mt-4">
                    

                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row -->

@endsection
@section('script')
<!-- apexcharts -->
<script src="{{ URL::asset('/assets/libs/apexcharts/apexcharts.min.js') }}"></script>

<!-- Required datatable js -->
<script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>

<!-- crypto-wallet init -->
<script src="{{ URL::asset('/assets/js/pages/crypto-wallet.init.js') }}"></script>

<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
    function topUpCash(){
        var amount = document.getElementById('topup').value;
        axios.post('/topUpCash', {
                // store_address:store_address,    
                // customer_address: customer_address,
                //Send customer and store coordinates

                amount: amount,
                

                })
                .then(function (response) {
                    
                   
                    if(response['data']['success'] == 'success'){
                        alert("Top-up request successful. Please wait for the admin approval. Thank you!");
                    }
                     console.log(response);
                })
                .catch(function (error) {
                    // console.log(error);
                });
    }
</script> 
@endsection