<x-admin-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="relative overflow-x-auto shadow-md sm:rounded-lg" id="reload">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th scope="col" class="px-6 py-3">
                        Order Number
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Total
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Delivery Fee
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Status
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Date
                    </th>
                   
                    
                </tr>
            </thead>
        <tbody>
               @foreach ($orders as $order )

                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                        <th scope="row" class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">
                            <a href="/delivery/orders/{{ $order->order_id }}"> {{ $order->order_id }}</a>
                        </th>
                        <td class="px-6 py-4">
                            {{ $order->order_number }}
                        </td>
                        <td class="px-6 py-4">
                            {{ $order->delivery_fee }}
                        </td>
                        <td class="px-6 py-4">
                            {{  strtoupper($order->order_status) }}
                        </td>
                        <td class="px-6 py-4">
                            {{ $order->created_at }}
                        </td>
                    </tr>
                @endforeach
        
        </tbody>
        </table>
</div>
</x-admin-layout>
  
