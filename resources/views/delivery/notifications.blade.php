@extends('layouts.master')

@section('title') @lang('Orders') @endsection

@section('css')
    <!-- Responsive Table css -->
    <link href="{{ URL::asset('/assets/libs/rwd-table/rwd-table.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Orders @endslot
        @slot('title') Order Details @endslot
    @endcomponent
    <form class="app-search d-none d-lg-block">
        <div class="position-relative">
            <input type="text" class="form-control" placeholder="@lang('translation.Search')">
            <span class="bx bx-search-alt"></span>
        </div>
    </form>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-rep-plugin">
                        <div class="table-responsive mb-0" data-pattern="priority-columns">
                            
                            @if($orders != null)
                            <table id="tech-companies-1" class="table table-striped">
                                <thead>
                                    <tr>
                                        {{-- <th>Order ID</th> --}}
                                        <th >Order Number</th>     
                                        <th data-priority="1">Vendor</th>
                                        <th data-priority="1">Total Price</th>
                                        <th data-priority="1">Vendor Net</th>
                                        {{-- <th data-priority="1">Commission</th> --}}
                                        <th data-priority="1">Delivery Fee</th>
                                        <th data-priority="6">Fulfillment/Order/Financial Status</th>
                                        <th data-priority="6">Rider</th>
                                        <th data-priority="6">Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($orders as $order )
                                    <tr>
                                        {{-- <th>{{ $order->order_id }}</th> --}}
                                        <th><a href="/delivery-orders-{{ $order->order_id }}">{{ $order->order_number }}</a></th>
                                        <td>{{ $order->vendor }}</td>
                                        <td>{{ $order->total_price }}</td>
                                        <td>{{ $order->vendor_net }}</td>
                                        {{-- <td>{{ $order->commission }}</td> --}}
                                        <td>{{ $order->delivery_fee }}</td>
                                        <td>@if ($order->rider_id == 0 && $order->fulfillment_status == null)
                                            Open
                                        @elseif ($order->rider_id == 0 && $order->fulfillment_status == "Cancelled")
                                            Cancelled
                                        @elseif ($order->rider_id != 0 && $order->fulfillment_status == "Cancelled")
                                            Cancelled
                                        @else
                                        {{ ucfirst($order->fulfillment_status) ?? '' }}/{{ ucfirst($order->order_status) ?? ''}}/{{ ucfirst($order->financial_status) ?? '' }}
                                        @endif</td>
                                        <td><a href="user-profile-{{ $order->rider_id }}"> {{ $order->first_name ?? '' }} {{ $order->last_name ?? '' }}</a></td>
                                        
                                        <td>{{ $order->created_at }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @else
                                <a href="/">Turn on your availability to access orders.</a>
                            @endif
                        </div>

                    </div>

                    
                </div>
            </div>
        </div> <!-- end col -->
        
    </div> <!-- end row -->
    <div class="row">
        <div class="col-lg-12 pagination pagination-rounded justify-content-center mt-4">
            @if($orders != null)
            {{ $orders->links()  }}
            @endif
        </div>
    </div>
    
@endsection
@section('script')
    <!-- Responsive Table js -->
    <script src="{{ URL::asset('/assets/libs/rwd-table/rwd-table.min.js') }}"></script>

    <!-- Init js -->
    <script src="{{ URL::asset('/assets/js/pages/table-responsive.init.js') }}"></script>
@endsection
