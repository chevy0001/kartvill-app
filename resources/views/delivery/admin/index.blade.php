@extends('layouts.master')

@section('title') @lang('translation.Responsive_Table') @endsection

@section('css')
    <!-- Responsive Table css -->
    <link href="{{ URL::asset('/assets/libs/rwd-table/rwd-table.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Ecommerce @endslot
        @slot('title') Deliveries @endslot
    @endcomponent
    <form class="app-search d-none d-lg-block">
        <div class="position-relative">
            <input type="text" class="form-control" placeholder="@lang('translation.Search')">
            <span class="bx bx-search-alt"></span>
        </div>
    </form>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-rep-plugin">
                        <div class="table-responsive mb-0" data-pattern="priority-columns">
                            <table id="tech-companies-1" class="table table-striped">
                                <thead>
                                    <tr>
                                        {{-- <th>Order ID</th> --}}
                                        <th >Order Number</th>     
                                        <th data-priority="1">Customer ID</th>
                                        <th data-priority="1">Rider</th>
                                        <th data-priority="1">Delivery Fee</th>
                                        <th data-priority="1">Fulfillment Status</th>
                                        <th data-priority="1">Order Date</th>
                                        <th data-priority="1">Order Completed</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($deliveries as $delivery )
                                    <tr>
                                        {{-- <th>{{ $order->order_id }}</th> --}}
                                        <th><a href="/delivery-orders-{{ $delivery->order_id }}">{{ $delivery->order_number }}</a></th>
                                        <td>{{ $delivery->customer_id }}</td>
                                        <td>{{ ucfirst($delivery->first_name) }} {{ ucfirst($delivery->last_name) }}</td>
                                        <td>{{ $delivery->delivery_fee }}</td>
                                        @if($delivery->fulfillment_status == "Cancelled")
                                            <td>{{ $delivery->fulfillment_status }}</td>
                                        @else
                                            <td>{{ $delivery->order_status }}</td>
                                        @endif                                        
                                        <td>{{  $delivery->created_at  }}</td>
                                        <td>{{  $delivery->updated_at  }}</td>
                                        
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            
                        </div>

                    </div>

                    
                </div>
            </div>
        </div> <!-- end col -->
        
    </div> <!-- end row -->
    <div class="row">
        <div class="col-lg-12 pagination pagination-rounded justify-content-center mt-4">
            {{ $deliveries->links()  }}
        </div>
    </div>
    
@endsection
@section('script')
    <!-- Responsive Table js -->
    <script src="{{ URL::asset('/assets/libs/rwd-table/rwd-table.min.js') }}"></script>

    <!-- Init js -->
    <script src="{{ URL::asset('/assets/js/pages/table-responsive.init.js') }}"></script>
@endsection
