@extends('layouts.master')

@section('title') @lang('Order') @endsection

@section('css')
    <!-- bootstrap-touchspin css -->
    <link href="{{ URL::asset('/assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css') }}" rel="stylesheet" />
    <style type="text/css" media="print">
        .noPrint{
          display: none;
        }
      </style>
@endsection

@section('content')
<div id="nonPrintable" class="noPrint"> 
    @component('components.breadcrumb')
        @slot('li_1') Orders @endslot
        @slot('title') Order Number: {{ $order->order_number ?? ''}} / Status: {{ $orders['order']['financial_status'] ?? ''}} @endslot
    @endcomponent

    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    @if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
    @endif
    @if(Auth::user()->role == 1 || Auth::user()->role == 3)
    {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Print Receipts
    </button> --}}
    <a href="print-receipt-{{ $order->order_id }}" class="btn btn-primary">Print Receipt</a>
    @endif 
    <div class="row">
        <div class="col-xl-8">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table align-middle mb-0 table-nowrap">
                            <thead class="table-light">
                                <tr>
                                    <th>Product</th>
                                    <th>Product Description</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th colspan="2">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($orders['order']['line_items'] as $product)
                
                                    @php
                                        $total_price_per_product = $product['price'] * $product['quantity'];
                                        
                                    @endphp
                                <tr>
                                    <td>
                                        @php 
                                        
                                        $images=App\Http\Controllers\FunctionsController::shopify_call2($access_token, 'market-spays.myshopify.com','/admin/api/2022-01/products/'.$product['product_id'].'.json', $array, 'GET');  
                                        $images = json_decode($images['response'], JSON_PRETTY_PRINT);
                                        if(isset($images['product']['image']['src'])){
                                            echo'<img src='.$images['product']['image']['src'].' class="avatar-md">'; 
                                        }
                                        else{
                                            echo'No Image';
                                        }
                                        @endphp

                                        @foreach ($product['properties'] as $props )
                                            <p><small>
                                                @if ($props['name'] == '_wk_variant_id' || $props['name'] == '_wk_vendor' )
                                                    {{ $props['value'] = '' }}
                                                    {{ $props['name'] = '' }}
                                                    
                                                @else
                                                {{ $props['name'] }} : {{ $props['value'] }}
                                                @endif
                                                
                                            </small></p>

                                        @endforeach

                                        {{-- <img src="{{ URL::asset('/assets/images/product/img-1.png') }}" alt="product-img" title="product-img"
                                            class="avatar-md" /> --}}
                                    </td>
                                    <td>
                                        <h5 class="font-size-14 text-truncate">
                                            <p class="text-dark">{{ $product['name'] ?? '' }}</p></h5>
                                        {{-- <p class="mb-0">Color : <span class="fw-medium">Maroon</span></p> --}}
                                    </td>
                                    <td>
                                        ₱{{ number_format($product['price'],2) ?? ''}}
                                    </td>
                                    <td>
                                        <div style="width: 120px;">
                                            {{ $product['quantity'] }}
                                        </div>
                                    </td>
                                    <td>
                                        ₱{{ number_format($total_price_per_product,2) ?? ''}}
                                    </td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row mt-4">
                        <div class="col-sm-12">
                            <div class="text-sm mt-2 mt-sm-0">
                                <p><b>Note/Instructions:</b> {{ $orders['order']['note'] ?? $order->note }}</p>
                                <p><b>Estimated Delivery Time:</b> {{ $duration + 25 }} - {{ $duration + 25 + 10 }} Minutes</p>
                                <p><b>Distance:</b> {{ $km }} Km</p>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row-->
                    <div class="row mt-4">
                        <div class="col-sm-6">
                            <a onClick="history.go(-1);" class="btn btn-secondary">
                                <i class="mdi mdi-arrow-left me-1"></i> Back </a>
                                

                                @if($product['name'] == "Pabili" && $order->rider_id == 0)
                                    {{-- <a href="/editfee-{{ $order->order_id }}" class="btn btn-primary">
                                        Edit Fees 
                                   </a> --}}
                                   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                                    Edit Fees
                                  </button>
                                @endif
                               
                                <!-- Button trigger modal -->

  
  <!-- Modal -->
  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Edit Delivery Fee</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <form action="/edit/delivery-fee" method="post">
            @csrf
            @method("PATCH")
        {{-- <div class="input-group col-md-12 ">
            <button class="btn btn-primary" type="button" onclick="getRate()"id="search_button">Calculate</button>
            <input type="text" class="form-control" name="store_address" id="store_address" aria-describedby="store_address"  placeholder="Enter Store Address Here " aria-label="Calculate">
        </div> --}}
         
          <div class="col-md-12 mt-2">
            Estimated delivery fee is <b><span id="estimated_fee"></span></b>
          </div>
          <div class="col-md-12 mt-2">
            Distance <b><span id="estimated_distance"></span></b>
          </div>
          {{-- Seller Coords --}}
          <input type="hidden" id="pabili_lat" name="seller_pabili_lat">
          <input type="hidden" id="pabili_long" name="seller_pabili_long">
          {{-- End of seller coords --}}
          {{-- Customer Coords --}}
          <input type="hidden" id="customer_pabili_lat">
          <input type="hidden" id="customer_pabili_long">
          {{-- End Customer Coords --}}
          <div class="table-responsive" id="map3" style="width:100;height:300px;"></div>
          
          <div class="col-md-12 mt-2">
            <label for="delivery_fee">Delivery Fee </label>  
            <input type="number" onkeypress="whenInput()" min="10" step="0.1" max="" class="form-control" id="delivery_fee" name="delivery_fee" onkeypress="" placeholder="Delivery Fee" required>
            <input type="hidden" id="pabili_order_id" name="pabili_order_id" value="{{ $order->order_id }}">
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" id="update-delivery-but">Update Delivery Fee</button>
        </div>
    </form>
    </div>
    </div>
  </div>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

                


</div> <!-- end col -->
                        <div class="col-sm-6">
                            <div class="text-sm-end mt-2 mt-sm-0">
                              
                                <form action="/delivery/accept-reject" method="post">
                                    @csrf
                                    @method('POST')
                                    <input type="hidden" name="duration" id="duration" value="{{ $duration }}">
                                    @if($order->fulfillment_status != "Cancelled")
                                    <input type="hidden" name="order_id" id="order_id" value="{{ $orders['order']['id'] ?? ''}}">
                                    
                                    
                                    @if($order->rider_id == 0)

                                       @if($product['name'] == "Pabili" && $order->delivery_fee == 0)
                                       <button class="btn btn-success" disabled>Accept </button>
                                       @else
                                            @if($orders['order']['financial_status'] === 'voided' || $orders['order']['financial_status'] === 'refunded')
                                            <p class="btn">The order is cancelled.</p>
                                            @elseif(Auth::user()->availability == 1)
                                                <button class="btn btn-success">Accept </button>
                                            @else
                                            <p class="btn">Please turn on availability</p>
                                            @endif
                                       @endif     
                                        {{-- <button class="btn btn-danger">Reject</button> --}}
                                    @else
                                        @if($order->rider_id == Auth::user()->id && $order->order_status == "accepted")
                                            <input type="hidden" name="status" id="status" value="picked-up">
                                            <p><button class="btn btn-success">Pick up</button></p>
                                        @elseif($order->rider_id == Auth::user()->id && $order->order_status == null)
                                            <button class="btn btn-success">Accept </button>   
                                        @elseif($order->rider_id == Auth::user()->id && $order->order_status == "picked-up")
                                        <input type="hidden" name="status" id="status" value="delivered">
                                        <input type="hidden" name="customerLatitude" id="customerLatitude" >
                                        <input type="hidden" name="customerLongitude" id="customerLongitude" >
                                        
                                        <p><button class="btn btn-success">Delivered</button></p>
                                        @elseif($order->rider_id == Auth::user()->id && $order->order_status == "delivered")
                                        <p>Delivered</p>
                                        @else
                                            <p>Already Assigned to Others</p>
                                        @endif
                                    @endif
                                    @else
                                    <p>Cancelled</p>
                                    @endif
                                 </form>
                               
                                
                                {{-- @if (Auth::user()->role == 1)
                                    <form method="post" action="/cancel-order">
                                        @csrf
                                        @method('POST')
                                        <input type="hidden" name="order_number" value="{{ $order->order_number }}">
                                        <button class="btn btn-success">Cancel</button>
                                    </form>
                                @endif  --}}
                                           
                            
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row-->
                   

                </div>
            </div>
        </div>
        <div class="col-xl-4">
            
            <div class="card">
                <div class="card-body">    
                    <h4 class="card-title mb-3">Order Summary - {{ date('M d, Y',strtotime($order->created_at)) }}</h4>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td>Customer Balance  :</td>
                                    <td>₱{{ $orders['order']['current_total_price'] }}</td>
                                </tr>
                                <tr>
                                    <td>Discount : </td>
                                    <td>-  {{ $orders['order']['total_discounts'] }}</td>
                                </tr>
                                <tr>
                                    <td>Delivery Fee :</td>
                                    <td>
                                       @if($product['name'] == "Pabili")
                                       {{  $order->delivery_fee ?? 'Edit pickup location'}}
                                       @else 
                                            @if($orders['order']['total_shipping_price_set']['shop_money']['amount'] == 0)
                                                Pickup (Kartvillage)
                                            @else
                                               
                                                {{ $orders['order']['total_shipping_price_set']['shop_money']['amount'] }}
                                            @endif
                                        @endif
                                    </td>
                                    
                                </tr>
                                
                                <tr>
                                    <th>Total :</th>
                                    <th>₱{{ $orders['order']['current_total_price'] }}</th>
                                </tr>

                                <tr>
                                    <th>Payable to Merchant :</th>
                                    <th>₱{{ $merchant_payable }}</th>
                                </tr>
                                <tr>
                                    <td>Kartvill commission from product:</td>
                                    <td>@if($product['name'] == "Pabili")
                                            0
                                        @else
                                            ₱{{ $kartvill_commission}}
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Kartvill commission from delivery fee:</td>
                                    <td>@if($product['name'] == "Pabili")
                                        {{ $order->delivery_commission }}
                                        @else
                                            ₱{{ $order->delivery_commission }}
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Total Deductions:</td>
                                    <td>₱{{number_format($kartvill_commission + $order->delivery_commission,2) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- end table-responsive -->
                </div>
            </div>
            <!-- end card -->
        </div>

        <div class="col-xl-12">
            
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-3">Customer Details</h4>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td>Customer: </td>
                                    <td>{{ $orders['order']['customer']['first_name'] ?? '' }} {{ $orders['order']['customer']['last_name'] ?? '' }} </td>
                                </tr>
                                <tr>
                                    <td>Phone Number: </td>
                                    @if(isset($orders['order']['shipping_address']['phone']))
                                    <td><a href="tel:{{  $orders['order']['shipping_address']['phone'] ?? $orders['order']['billing_address']['phone']}}">{{ $orders['order']['shipping_address']['phone'] ?? $orders['order']['billing_address']['phone']  }}</a> </td>
                                    @else
                                    <td><a href="tel:{{  $orders['order']['billing_address']['phone'] ?? $orders['order']['shipping_address']['phone']}}">{{ $orders['order']['billing_address']['phone'] ?? $orders['order']['shipping_address']['phone'] }}</a></td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>Address:  </td>
                                    <td id="customer_addresses">
                                   
                                    @if(isset($orders['order']['shipping_address']))
                                        {{ $orders['order']['shipping_address']['address1'].','.$orders['order']['shipping_address']['city'].','.$orders['order']['shipping_address']['country']}}
                                    
                                    @elseif(isset($orders['order']['customer']['default_address']['address1']))
                                        {{ $orders['order']['customer']['default_address']['address1'].','.$orders['order']['customer']['default_address']['city'].','.$orders['order']['customer']['default_address']['country']}}
                                    @else
                                        
                                    @endif
                                   
                                </td>
                                </tr>
                                <tr>
                                    <td>Coordinates:  </td>
                                    <td>{{ $customer->latitude ?? 'Not Available' }},{{ $customer->longitude ?? 'Not Available'}}</td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                    @if(isset($orders['order']['customer']['id']))
                    <a href="/send-map-link-{{ $orders['order']['customer']['id'] }}" class="btn btn-secondary">Send Update Map Link</a>
                    @endif
                    <div class="table-responsive" id="map" style="width:100;height:300px;"></div>
                    <div id="floating-panel">
                        <b>Mode of Travel: </b>
                        <select id="mode">
                          <option value="DRIVING">Driving</option>
                          <option value="WALKING">Walking</option>
                        </select>
                      </div>
                    {{-- Customer MAP --}}
                    
                    <script>
                        var position = [0, 0];
                        var rider_long = 0, rider_lat = 0;
                        var  infoWindow;
                        var cust_long = parseFloat(@json($orders['order']['shipping_address']['longitude'] ?? '0'));
                        var cust_lat = parseFloat(@json($orders['order']['shipping_address']['latitude'] ?? '0'));

                        var customer_latitude = parseFloat(@json($customer->latitude ?? '0'));
                        var customer_longitude = parseFloat(@json($customer->longitude ?? '0'));

                        var seller_latitude = parseFloat(@json($seller->latitude ?? '0'));
                        var seller_longitude = parseFloat(@json($seller->longitude ?? '0'));
                        
                        var customer_lati = customer_latitude;
                        var customer_long = customer_longitude;
                        if(customer_longitude == 0 || customer_latitude == 0){
                            customer_lati = cust_lat;
                            customer_long = cust_long;
                        }
                      
                                           

                    function initMap() {
                        infoWindow = new google.maps.InfoWindow;
                        if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(function(position) {
                                
                                rider_lat = position.coords.latitude;
                                rider_long = position.coords.longitude;
                                position[0] = rider_lat;
                                position[1] = rider_long;

                            //this must be used for the rider to update the customer's location    
                            //    if(customer_latitude == 0 && customer_longitude == 0){
                            //     document.getElementById("customerLatitude").value = rider_lat;
                            //     document.getElementById("customerLongitude").value = rider_long;
                            //    }
                                const directionsRenderer = new google.maps.DirectionsRenderer();
                                const directionsRenderer2 = new google.maps.DirectionsRenderer();

                                const directionsService = new google.maps.DirectionsService();
                                    const map = new google.maps.Map(document.getElementById("map"), {
                                    zoom: 14,
                                    center: { lat: rider_lat, lng: rider_long },
                                });

                                const directionsService2 = new google.maps.DirectionsService();
                                    const map2 = new google.maps.Map(document.getElementById("map2"), {
                                    zoom: 14,
                                    center: { lat: rider_lat, lng: rider_long },
                                });
                            /* Pabili Map*/
                               const map3 = new google.maps.Map(document.getElementById('map3'), {
                                    zoom: 15,
                                    center: {lat: rider_lat, lng: rider_long},
                                    
                                });
                                marker = new google.maps.Marker({
                                    position: new google.maps.LatLng(rider_lat,rider_long),
                                    name:name,
                                    map: map3,
                                    draggable:true,
                                    title:'Rider',
                                    
                                }); 
                                    document.getElementById('customer_pabili_lat').value = customer_latitude;
                                    document.getElementById('customer_pabili_long').value = customer_longitude;
                                    document.getElementById('pabili_lat').value = rider_lat;
                                    document.getElementById('pabili_long').value = rider_long;
                                    
                                    google.maps.event.addListener(marker, 'dragend', function(marker) {
                                    var latLng = marker.latLng;
                                    document.getElementById('pabili_lat').value = latLng.lat();
                                    document.getElementById('pabili_long').value = latLng.lng();
                                        //Calculate delivery fee here
                                        getRate();
                                });
                             /*End of Pabili map*/   
                                directionsRenderer.setMap(map);

                                directionsRenderer2.setMap(map2);

                                calculateAndDisplayRoute(directionsService, directionsRenderer);
                                document.getElementById("mode").addEventListener("change", () => {
                                    calculateAndDisplayRoute(directionsService, directionsRenderer);
                                });  
                                
                                calculateAndDisplayRoute2(directionsService2, directionsRenderer2);
                                document.getElementById("mode2").addEventListener("change", () => {
                                    calculateAndDisplayRoute2(directionsService2, directionsRenderer2);
                                }); 

                            }, function() {
                              handleLocationError(true, infoWindow, map.getCenter());
                            });
                            
                            var latlng = new google.maps.LatLng(position[0], position[1]);
                            google.maps.event.addListener(map, 'click', function(event) {
                                var result = [event.latLng.lat(), event.latLng.lng()];
                                transition(result);
                            });
                        
                        }
                            else {
                            // Browser doesn't support Geolocation
                            handleLocationError(false, infoWindow, map.getCenter());
                            alert(document.getElementById("customerLatitude").value);
                            //document.getElementById("customerLongitude").value = "";
                          }

                            
                    }

                        google.maps.event.addDomListener(window, 'load', initialize);
                        var numDeltas = 100;
                        var delay = 10; //milliseconds
                        var i = 0;
                        var deltaLat;
                        var deltaLng;

                        function transition(result){
                            i = 0;
                            deltaLat = (result[0] - position[0])/numDeltas;
                            deltaLng = (result[1] - position[1])/numDeltas;
                            moveMarker();
                        }
                        function moveMarker(){
                            position[0] += deltaLat;
                            position[1] += deltaLng;
                            var latlng = new google.maps.LatLng(position[0], position[1]);
                            marker.setTitle("Latitude:"+position[0]+" | Longitude:"+position[1]);
                            marker.setPosition(latlng);
                            if(i!=numDeltas){
                                i++;
                                setTimeout(moveMarker, delay);
                            }
                            
                        }

                        function calculateAndDisplayRoute(directionsService, directionsRenderer) {
                        const selectedMode = document.getElementById("mode").value;
                        directionsService
                            .route({
                            origin: { lat: rider_lat, lng: rider_long },
                            destination: { lat: customer_lati, lng: customer_long },
                            // Note that Javascript allows us to access the constant
                            // using square brackets and a string value as its
                            // "property."
                            travelMode: google.maps.TravelMode[selectedMode],
                            })
                            .then((response) => {
                            directionsRenderer.setDirections(response);
                            })
                            .catch((e) => console.log("Directions request failed. Please check the customer's coordinates. Call the customer to update their address." + e));
                        }

                        function calculateAndDisplayRoute2(directionsService, directionsRenderer) {
                        const selectedMode = document.getElementById("mode2").value;
                        directionsService
                            .route({
                            origin: { lat: rider_lat, lng: rider_long },
                            destination: { lat: seller_latitude, lng: seller_longitude },
                            // Note that Javascript allows us to access the constant
                            // using square brackets and a string value as its
                            // "property."
                            travelMode: google.maps.TravelMode[selectedMode],
                            })
                            .then((response2) => {
                            directionsRenderer.setDirections(response2);
                            })
                            .catch((e) => console.log("Directions request failed. Please check the merchant coordinates. Call the admin to fix the problem."));
                        }

                        window.initMap = initMap;
                    </script>

                    {{-- End Customer Map  --}}


                    <h4 class="card-title mb-3 mt-3">Merchant Details</h4>

                    @if(!isset($seller))
                    <table class="table mb-0">
                        <tbody>
                            <tr>
                                <td>Address:  </td>
                                <td>{{ $warehouse ?? ''}}</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="table-responsive" id="map2" style="width:100;height:300px;display:none;"></div>
                        <div id="floating-panel" style="display: none;">
                            <b>Mode of Travel: </b>
                            <select id="mode2">
                            <option value="DRIVING">Driving</option>
                            </select>
                        </div>
                    @else
                        <div class="table-responsive">
                            <table class="table mb-0">
                                <tbody>
                                    <tr>
                                        <td>Merchant: </td>
                                        <td>{{ $seller->store_name ?? ''}} </td>
                                    </tr>
                                    <tr>
                                        <td>Phone Number: </td>
                                        <td><a href="tel:{{ $seller->seller_contact ?? '' }}">{{ $seller->seller_contact ?? '' }}</a> </td>
                                    </tr>
                                    <tr>
                                        <td>Address:  </td>
                                        <td>{{ $seller->store_address ?? ''}}</td>
                                    </tr>
                                    <tr>
                                        <td>Coordinates:  </td>
                                        <td>{{ $seller->latitude ?? 'Not Available'}},{{ $seller->longitude ?? 'Not Available'}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="table-responsive" id="map2" style="width:100;height:300px;"></div>
                        <div id="floating-panel">
                            <b>Mode of Travel: </b>
                            <select id="mode2">
                            <option value="DRIVING">Driving</option>
                            </select>
                        </div>
                        @endif
                        
                    
                    
                    <!-- end table-responsive -->
                </div>
            </div>
            <!-- end card -->
        </div>
    </div>
</div>
    <!-- end row -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title " id="exampleModalLabel">FoodRx Reciept </h5>
              <div class="noPrint"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                </div>
              
            </div>
            <div class="modal-body" table-responsive>
                <p><b>Order Number: {{ $order->order_number ?? ''}}</b> |
                    Date: {{ $order->created_at ?? ''}} | Vendor:
                {{ $seller->store_name ?? ''}} |    
              @if(isset($orders['order']['shipping_address']))
                {{ $orders['order']['shipping_address']['address1'].','.$orders['order']['shipping_address']['city'].','.$orders['order']['shipping_address']['country']}}
                @elseif(isset($orders['order']['customer']['default_address']['address1']))
                    {{ $orders['order']['customer']['default_address']['address1'].','.$orders['order']['customer']['default_address']['city'].','.$orders['order']['customer']['default_address']['country']}}
                @endif
              
              <p>Waiter: {{ Auth::user()->first_name }}</p>
              <table class="table">
                <tr>
                    <td>Quantity</td>
                    <td>Item</td>
                    <td>Price</td>
                    <td>Total</td>
                </tr>
              @foreach ($orders['order']['line_items'] as $product)
              <tr>
                <td>{{ $product['quantity'] }}</td>
                <td>{{ $product['name'] ?? '' }}</td>
                <td>{{ number_format($product['price'],2) ?? ''}}</td>
                <td> ₱{{ number_format($total_price_per_product,2) ?? ''}}</td>
              </tr>
               @endforeach
              </table>
              <hr/>
              <div id="noprints">
                Total ₱{{ $orders['order']['current_total_price'] }}<br/>
                Cash: <span id="cash_print">0.00</span><br/>
                Change: <span id="change_print">0.00</span><br/>
              </div>
              <div class="noPrint">
                Enter Cash: <input type="number" class="form-control" value="{{ $orders['order']['current_total_price'] }}" id="cash" oninput="computeChange()">
              </div>
            </div>
            <div class="modal-footer">
            <div class="noPrint">   
              <button type="button"  onClick="functionPrint()" class="btn btn-primary ">Print</button>
            </div>
            </div>
          </div>
        </div>
      </div>
@endsection
@section('script')
    <!-- bootstrap-touchspin -->
    <script src="{{ URL::asset('/assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js') }}"></script>

    <!-- init js -->
    <script src="{{ URL::asset('/assets/js/pages/ecommerce-cart.init.js') }}"></script>


    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
   <script>
    function computeChange(){
        let cash = document.getElementById('cash').value;
        let total = @json($orders['order']['current_total_price']);
        let change = 0;
        
        if(cash <= 0 || cash == null){
            document.getElementById("noprints").className += "noPrint";
            document.getElementById('cash_print').innerHTML = "0.00";
            document.getElementById('change_print').innerHTML = change + "0.00 ";
        }else{
            change = parseFloat(cash) - parseFloat(total);
            document.getElementById('cash_print').innerHTML = cash + " ";
            document.getElementById('change_print').innerHTML = change + " ";
            console.log(change);
        }
    }
       
    
     function functionPrint() {
                        // document.getElementById("nonPrintable").className += "noPrint";
                            window.print();
                        }   
    // var store_address = document.getElementById("store_address").value;
    // var customer_address = document.getElementById("customer_addresses").innerHTML;
    document.getElementById("update-delivery-but").disabled = true;
    function whenInput(){
        document.getElementById("update-delivery-but").disabled = false;
    }
    function getRate(){
        // store_address = document.getElementById("store_address").value;
        // customer_address = document.getElementById("customer_addresses").innerHTML;
        let store_coords = document.getElementById('pabili_lat').value +','+ document.getElementById('pabili_long').value;
        let customer_coords = document.getElementById('customer_pabili_lat').value + ',' + document.getElementById('customer_pabili_long').value;

        console.log(store_coords + ',' + customer_coords);
                axios.post('/get-rate', {
                // store_address:store_address,    
                // customer_address: customer_address,
                //Send customer and store coordinates

                store_coords: store_coords,
                customer_coords: customer_coords,

                })
                .then(function (response) {
                    let fee = 10;
                    fee = response['data']['km'] * 10;
                    if(response['data']['km'] < 3){
                        fee = 29;
                    }else{
                        fee = 10 * response['data']['km'];
                    }

                    max_fee = (fee * 0.1) + fee;
                    
                    document.getElementById("estimated_fee").innerHTML = fee.toFixed(2) + " up to " +max_fee +" Pesos";
                    document.getElementById("estimated_distance").innerHTML = response['data']['km'] + " KM";
                    document.getElementById("delivery_fee").value = fee;
                    document.getElementById("delivery_fee").max = max_fee;
                    document.getElementById("update-delivery-but").disabled = false;
                    // console.log(response);
                })
                .catch(function (error) {
                    // console.log(error);
                });
            }
    
    </script>     
@endsection
