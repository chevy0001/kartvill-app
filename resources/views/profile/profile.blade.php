@extends('layouts.master')

@section('title') @lang('translation.Profile') @endsection

@section('css')
<link href="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Contacts @endslot
@slot('title') Profile @endslot
@endcomponent
@if (session('error'))
<div class="alert alert-danger">
    {{ session('error') }}
</div>
@endif
<div class="row">
    <div class="col-xl-4">
        <div class="card overflow-hidden">
            <div class="bg-primary bg-soft">
                <div class="row">
                    <div class="col-7">
                        <div class="text-primary p-3">
                            <h5 class="text-primary">Welcome Back !</h5>
                            <p>Happy day.</p>
                        </div>
                    </div>
                    <div class="col-5 align-self-end">
                        <img src="{{ URL::asset('/assets/images/profile-img.png') }}" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
            <div class="card-body pt-0">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="avatar-md profile-user-wid mb-4">
                            <img src="{{ isset(Auth::user()->avatar) ? asset(Auth::user()->avatar) : asset('/assets/images/users/avatar-1.jpg') }}" alt="" class="img-thumbnail rounded-circle">
                        </div>
                        <h5 class="font-size-15 text-truncate">{{ ucfirst($user->first_name) }} {{ ucfirst($user->last_name) }}</h5>
                        <p class="text-muted mb-0 ">
                            @if($user->role == 1)Admin
                            @elseif($user->role == 2)Rider
                            @elseif($user->role == 3)Staff
                            @elseif($user->role==4)Investor
                            @elseif($user->role==5)Vendor - {{ $store_name->store_name }}
                            @else
                                Please ask the admin to verify your position.
                            @endif
                        </p>
                    </div>

                    <div class="col-sm-8">
                        <div class="pt-4">
                         @if(Auth::user()->role != 5 && Auth::user()->role != 4)   
                            @if($user->id == Auth::user()->id)  
                                <div class="row">
                                    <div class="col-8">
                                        <h5 class="font-size-15">Active</h5>
                                        <div id="availRider">
                                            <button type="button" id="btnAvail" onclick="avail()" 
                                            class="btn btn-success waves-effect waves-light">
                                             Online</button>
                                       </div>    
                                    </div>
                                </div>
                            @endif 
                         @endif   
                            <div class="mt-4">
                                {{-- <a href="" class="btn btn-primary waves-effect waves-light btn-sm">View Profile <i class="mdi mdi-arrow-right ms-1"></i></a> --}}

                                @if(Auth::user()->role == 1 || Auth::user()->role == 3 )
                                    <a href="/edit-users-{{ $user->id }}" class="btn btn-primary waves-effect waves-light btn-sm" >Edit Profile</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end card -->

        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Personal Information</h4>

                <p class="text-muted mb-4">Hi I'm {{ ucfirst($user->first_name) }}</p>
                <div class="table-responsive">
                    <table class="table table-nowrap mb-0">
                        <tbody>
                            <tr>
                                <th scope="row">Full Name :</th>
                                <td>{{ ucfirst($user->first_name) }} {{ ucfirst($user->last_name) }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Phone Number :</th>
                                <td><a href="tel:{{ $user->phone_number }}"> {{ $user->phone_number }}</a></td>
                            </tr>
                            <tr>
                                <th scope="row">E-mail :</th>
                                <td>{{ $user->email }}</td>
                            </tr>
                            
                            @if (Auth::user()->role !=5)
                                <tr>
                                    <th scope="row">Vehicle Type :</th>
                                    <td>{{ $user->vehicle_type }}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Plate Number :</th>
                                    <td>{{ $user->plate_number }}</td>
                                </tr>
                            @else
                            <tr>
                                <th scope="row">Store Name :</th>
                                <td>{{ $user->plate_number }}</td>
                            </tr>
                            @endif
                            
                            <tr>
                                <th scope="row">Location :</th>
                                <td>{{ $user->street_address }}, {{ $user->city }}, {{ $user->country }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- end card -->

        {{-- <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-5">Experience</h4>
                <div class="">
                    <ul class="verti-timeline list-unstyled">
                        <li class="event-list active">
                            <div class="event-timeline-dot">
                                <i class="bx bx-right-arrow-circle bx-fade-right"></i>
                            </div>
                            <div class="d-flex">
                                <div class="flex-shrink-0 me-3">
                                    <i class="bx bx-server h4 text-primary"></i>
                                </div>
                                <div class="flex-grow-1">
                                    <div>
                                        <h5 class="font-size-15"><a href="javascript: void(0);" class="text-dark">Back end Developer</a></h5>
                                        <span class="text-primary">2016 - 19</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="event-list">
                            <div class="event-timeline-dot">
                                <i class="bx bx-right-arrow-circle"></i>
                            </div>
                            <div class="d-flex">
                                <div class="flex-shrink-0 me-3">
                                    <i class="bx bx-code h4 text-primary"></i>
                                </div>
                                <div class="flex-grow-1">
                                    <div>
                                        <h5 class="font-size-15"><a href="javascript: void(0);" class="text-dark">Front end Developer</a></h5>
                                        <span class="text-primary">2013 - 16</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="event-list">
                            <div class="event-timeline-dot">
                                <i class="bx bx-right-arrow-circle"></i>
                            </div>
                            <div class="d-flex">
                                <div class="flex-shrink-0 me-3">
                                    <i class="bx bx-edit h4 text-primary"></i>
                                </div>
                                <div class="flex-grow-1">
                                    <div>
                                        <h5 class="font-size-15"><a href="javascript: void(0);" class="text-dark">UI /UX Designer</a></h5>
                                        <span class="text-primary">2011 - 13</span>

                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>

            </div>
        </div> --}}
        <!-- end card -->
    </div>

    <div class="col-xl-8">
        
        <div class="row">
            <div class="col-md-4">
                <div class="card mini-stats-wid">
                    <div class="card-body">
                        <div class="d-flex">
                            @if($user->role <> 5)
                            <div class="flex-grow-1">
                                <p class="text-muted fw-medium mb-2">Completed Deliveries</p>
                                <h4 class="mb-0">{{ $completed_deliveries }}</h4>
                            </div>
                            @else
                            <div class="flex-grow-1">
                                <p class="text-muted fw-medium mb-2">Wallet Balance</p>
                                <h4 class="mb-0">{{ number_format($wallet_balance,2) }}</h4>
                            </div>  
                            @endif
                            <div class="flex-shrink-0 align-self-center">
                                <div class="mini-stat-icon avatar-sm rounded-circle bg-primary">
                                    <span class="avatar-title">
                                        <i class="bx bx-check-circle font-size-24"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if($user->role <> 5)
            <div class="col-md-4">
                <div class="card mini-stats-wid">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="flex-grow-1">
                                <p class="text-muted fw-medium mb-2">Pending Deliveries</p>
                                <h4 class="mb-0">{{ $pending_deliveries }}</h4>
                            </div>

                            <div class="flex-shrink-0 align-self-center">
                                <div class="avatar-sm mini-stat-icon rounded-circle bg-primary">
                                    <span class="avatar-title">
                                        <i class="bx bx-hourglass font-size-24"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mini-stats-wid">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="flex-grow-1">
                                <p class="text-muted fw-medium mb-2">Total Revenue</p>
                                <h4 class="mb-0">₱{{ number_format($revenue,2) }}</h4>
                            </div>

                            <div class="flex-shrink-0 align-self-center">
                                <div class="avatar-sm mini-stat-icon rounded-circle bg-primary">
                                    <span class="avatar-title">
                                        <i class="bx bx-package font-size-24"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
        
        <div class="card">
            <div class="card-body">
                @if($user->role != 5) 
                    <h4 class="card-title mb-4">Deliveries</h4>
                @else
                    <h4 class="card-title mb-4">Orders</h4>
                @endif

                <div class="table-responsive">
                   @if($user->role != 5) 
                        <table class="table table-nowrap table-hover mb-0">
                            <thead>
                                <tr>
                                    <th scope="col">Order#</th>
                                    <th scope="col">Customer ID</th>
                                    <th scope="col">Delivery Fee</th>
                                    <th scope="col">Delivery Status</th>
                                    <th scope="col">Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ( $deliveries as $delivery )
                                    <tr>
                                        <th scope="row"><a href="delivery-orders-{{ $delivery->order_id }}">{{ $delivery->order_number }}</th>
                                        <td>{{ $delivery->customer_id }}</td>
                                        <td>₱{{ $delivery->delivery_fee }}</td>
                                        <td>{{ $delivery->order_status }}</td>
                                        <td>{{ $delivery->created_at }}</td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    @else
                    <table class="table table-nowrap table-hover mb-0">
                        <thead>
                            <tr>
                                <th scope="col">Date</th>
                                <th scope="col">Order#</th>
                                <th scope="col">Vendor Price</th>
                                <th scope="col">Commission</th>
                                <th scope="col">Total</th>
                                <th scope="col">Status</th>
                                <th scope="col">Remarks</th>
                                <th scope="col">MOP</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ( $deliveries as $order )<tr>
                            <td>{{ $order->created_at }}</td>
                            <td><a href="/delivery-orders-{{ $order->order_id }}" class="text-body fw-bold">{{ $order->order_number }}</a></td>
                            <td>{{ number_format($order->vendor_net,2) }}</td>
                            <td>{{ number_format($order->commission,2) }}</td>
                            <td>{{ number_format($order->total_price,2) }}</td>
                            <td>{{ $order->financial_status }}</td>
                            <td>@if ($order->fulfillment_status == 'Cancelled')
                                Commission Refunded
                                @else
                                {{ $order->fulfillment_status }}
                            @endif</td>
                            <td>@if($order->gateway == "Cash on Delivery (COD)")
                                 Cash
                                @else
                                 Online Payment
                                @endif
                            </td>
                            
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                    @endif
                </div>
                
                    <div class="col-lg-12 pagination pagination-rounded justify-content-center mt-4">
                        {{ $deliveries->links()  }}
                    </div>
                
            </div>
        </div>
        {{-- <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Revenue</h4>
                <div id="revenue-chart" class="apex-charts" dir="ltr"></div>
            </div>
        </div> --}}

       
    </div>
</div>
<!-- end row -->

<!--  Update Profile example -->
<div class="modal fade update-profile" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myLargeModalLabel">Edit Profile</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="POST" enctype="multipart/form-data" id="update-profile">
                    @csrf
                    <input type="hidden" value="{{ Auth::user()->id }}" id="data_id">
                    <div class="mb-3">
                        <label for="useremail" class="form-label">Email</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror" id="useremail" value="{{ Auth::user()->email }}" name="email" placeholder="Enter email" autofocus>
                        <div class="text-danger" id="emailError" data-ajax-feedback="email"></div>
                    </div>

                    <div class="mb-3">
                        <label for="username" class="form-label">Username</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" value="{{ Auth::user()->name }}" id="username" name="name" autofocus placeholder="Enter username">
                        <div class="text-danger" id="nameError" data-ajax-feedback="name"></div>
                    </div>

                    <div class="mb-3">
                        <label for="userdob">Date of Birth</label>
                        <div class="input-group" id="datepicker1">
                            <input type="text" class="form-control @error('dob') is-invalid @enderror" placeholder="dd-mm-yyyy" data-date-format="dd-mm-yyyy" data-date-container='#datepicker1' data-date-end-date="0d" value="{{ date('d-m-Y', strtotime(Auth::user()->dob)) }}" data-provide="datepicker" name="dob" autofocus id="dob">
                            <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                        </div>
                        <div class="text-danger" id="dobError" data-ajax-feedback="dob"></div>
                    </div>

                    <div class="mb-3">
                        <label for="avatar">Profile Picture</label>
                        <div class="input-group">
                            <input type="file" class="form-control @error('avatar') is-invalid @enderror" id="avatar" name="avatar" autofocus>
                            <label class="input-group-text" for="avatar">Upload</label>
                        </div>
                        <div class="text-start mt-2">
                            <img src="{{ asset(Auth::user()->avatar) }}" alt="" class="rounded-circle avatar-lg">
                        </div>
                        <div class="text-danger" role="alert" id="avatarError" data-ajax-feedback="avatar"></div>
                    </div>

                    <div class="mt-3 d-grid">
                        <button class="btn btn-primary waves-effect waves-light UpdateProfile" data-id="{{ Auth::user()->id }}" type="submit">Update</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection
@section('script')
<!-- apexcharts -->
<script src="{{ URL::asset('/assets/libs/apexcharts/apexcharts.min.js') }}"></script>

<script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

<!-- profile init -->
<script src="{{ URL::asset('/assets/js/pages/profile.init.js') }}"></script>

<script>
    $('#update-profile').on('submit', function(event) {
        event.preventDefault();
        var Id = $('#data_id').val();
        let formData = new FormData(this);
        $('#emailError').text('');
        $('#nameError').text('');
        $('#dobError').text('');
        $('#avatarError').text('');
        $.ajax({
            url: "{{ url('update-profile') }}" + "/" + Id,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function(response) {
                $('#emailError').text('');
                $('#nameError').text('');
                $('#dobError').text('');
                $('#avatarError').text('');
                if (response.isSuccess == false) {
                    alert(response.Message);
                } else if (response.isSuccess == true) {
                    setTimeout(function() {
                        window.location.reload();
                    }, 1000);
                }
            },
            error: function(response) {
                $('#emailError').text(response.responseJSON.errors.email);
                $('#nameError').text(response.responseJSON.errors.name);
                $('#dobError').text(response.responseJSON.errors.dob);
                $('#avatarError').text(response.responseJSON.errors.avatar);
            }
        });
    });
 
</script>

<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>  

<script>
    var availability = @json($user->availability);
    if(availability == 1){
            document.getElementById("btnAvail").classList.add('btn-success');
            document.getElementById("btnAvail").classList.remove('btn-light');
            document.getElementById("btnAvail").innerHTML = 'Online';
            }
            else{
            document.getElementById("btnAvail").classList.add('btn-light');
            document.getElementById("btnAvail").classList.remove('btn-success');
            document.getElementById("btnAvail").innerHTML = 'Offline';
            }
    function avail(){
        availability = !availability;
       
        if(availability == 1){
            document.getElementById("btnAvail").classList.add('btn-success');
            document.getElementById("btnAvail").classList.remove('btn-light');
            document.getElementById("btnAvail").innerHTML = 'Online';
            }
            else{
                document.getElementById("btnAvail").classList.add('btn-light');
            document.getElementById("btnAvail").classList.remove('btn-success');
            document.getElementById("btnAvail").innerHTML = 'Offline';
            }
           

        axios.post('/delivery/availability', {   
            availability: availability
        })
        .then(function (response) {
            
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        });
    }
</script>  
@endsection



