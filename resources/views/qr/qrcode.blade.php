@extends('layouts.master-without-nav-no-script')


@section('title') @lang('QR Code Generator') @endsection

@section('content')
<div class="home-btn d-none d-sm-block">
    <a href="/qrcode" class="text-white"><i class="fas fa-home h2"></i></a>
</div>

<div class="my-5 pt-sm-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center">
                    
                        Kartvill
                        {{-- <img src="{{ URL::asset('/assets/images/logo-dark.png') }}" alt="" height="20" class="auth-logo-dark mx-auto">
                        <img src="{{ URL::asset('/assets/images/logo-light.png') }}" alt="" height="20" class="auth-logo-light mx-auto"> --}}
                        
                                    <h5 class="card-title mb-4">Enter URL HERE</h5>
                    
                                    
                                    <form action="/edit-qrcode" method="POST" class="hstack gap-3">
                                        @csrf
                                        @method("POST")
                                        <input class="form-control me-auto" required name="url" id="url" type="text" placeholder="Add your item here..." aria-label="Add your item here...">
                                        <button type="submit"  class="btn btn-secondary">Generate QR Code</button>
                                        <div class="vr"></div>
                                        <button type="button"  onclick="clearForm()" class="btn btn-outline-danger">Clear</button>
                                    </form>
                                    
                    
                            

                    <div class="row justify-content-center mt-5">
                        <div class="col-sm-4">
                            <div class="maintenance-img">
                                
                                {{ $qr }}
                            </div>
                        </div>
                    </div>
                    <h4 class="mt-5">{{ $url }}</h4>
                    <p class="text-muted">Scan the QR Code above.</p>

                    <div class="row justify-content-center mt-5">
                        <div class="col-md-8">
                            <div data-countdown="2021/12/31" class="counter-number"></div>
                        </div> <!-- end col-->
                    </div> <!-- end row-->
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    
    function clearForm(){
        
        document.getElementById('url').value = "";
    }
    
</script>

@endsection


    
