@extends('layouts.master')

@section('title') @lang('Orders') @endsection

@section('css')
    <!-- Responsive Table css -->
    <link href="{{ URL::asset('/assets/libs/rwd-table/rwd-table.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Orders @endslot
        @slot('title') Order Details @endslot
    @endcomponent
    <form class="app-search  d-lg-block" method="post" action="/vendor-search-order">
        @csrf
        @method("POST")
        <div class="input-group">
            <input type="text"  name="order_number" class="form-control" placeholder="Search" aria-label="Search input" required>
            <button class="btn btn-primary" type="submit"><i class="mdi mdi-magnify"></i></button>
        </div>
    </form>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-rep-plugin">
                        <div class="table-responsive mb-0" data-pattern="priority-columns">
                            
                           
                            <table id="tech-companies-1" class="table table-striped">
                                <thead>
                                    <tr>
                                        {{-- <th>Order ID</th> --}}
                                        <th data-priority="1">Date</th>
                                        <th >Order Number</th>     
                                        <th data-priority="1">Vendor</th>
                                        <th data-priority="1">Total Price</th>
                                        <th data-priority="1">Vendor Net</th>
                                        {{-- <th data-priority="1">Commission</th> --}}
                                        <th data-priority="1">KV Commission</th>
                                        <th data-priority="1">Fulfillment/Order/Payment Status</th>
                                        
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                        
                                    
                                    @foreach ($orders as $order )
                                    <tr>@isset($order)
                                        <td>{{ $order->created_at }}</td>
                                        {{-- <th>{{ $order->order_id }}</th> --}}
                                        @if($order->fulfillment_status !="served" && $order->order_status !="delivered" && $order->order_status !="canceled" && $order->financial_status !="paid" )
                                        <th><a href="/vendor-order-{{ $order->order_id }}" style="font-weight: 600">{{ $order->order_number }}</a></th>
                                        @else
                                        <th><a href="/vendor-order-{{ $order->order_id }}" >{{ $order->order_number }}</a></th>
                                        @endif
                                        <td>{{ $order->vendor }}</td>
                                        <td>{{ $order->total_price }}</td>
                                        <td>{{ $order->vendor_net }}</td>
                                        {{-- <td>{{ $order->commission }}</td> --}}
                                        <td>{{ $order->commission }}</td>
                                        <td>@if ($order->rider_id == 0 && $order->fulfillment_status == null)
                                            Open
                                        @elseif ($order->rider_id == 0 && $order->fulfillment_status == "Cancelled")
                                            Cancelled
                                        @elseif ($order->rider_id != 0 && $order->fulfillment_status == "Cancelled")
                                            Cancelled
                                        @else
                                        {{ ucfirst($order->fulfillment_status) ?? '' }}/{{ ucfirst($order->order_status) ?? ''}}/{{ ucfirst($order->financial_status) ?? '' }}
                                        @endif</td>
                                        </td>
                                        
                                        
                                    </tr>@endisset
                                    @endforeach
                                    
                                </tbody>
                            </table>
                            
                               
                            
                        </div>

                    </div>

                    
                </div>
            </div>
        </div> <!-- end col -->
        
    </div> <!-- end row -->
    <div class="row">
        <div class="col-lg-12 pagination pagination-rounded justify-content-center mt-4">
            @if($orders != null)
            {{ $orders->links()  }}
            @endif
        </div>
    </div>
    
@endsection
@section('script')
    <!-- Responsive Table js -->
    <script src="{{ URL::asset('/assets/libs/rwd-table/rwd-table.min.js') }}"></script>

    <!-- Init js -->
    <script src="{{ URL::asset('/assets/js/pages/table-responsive.init.js') }}"></script>
@endsection
