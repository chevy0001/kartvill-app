@extends('layouts.master')

@section('title') @lang('translation.Responsive_Table') @endsection

@section('css')
    <!-- Responsive Table css -->
    <link href="{{ URL::asset('/assets/libs/rwd-table/rwd-table.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Ecommerce @endslot
        @slot('title') Sellers @endslot
    @endcomponent
    
    <form class="app-search d-none d-lg-block" method="POST" action="/seller-search">
        @csrf
        @method('POST')
        <div class="position-relative">
            <input type="text" class="form-control" name="input" placeholder="@lang('translation.Search')">
            <span class="bx bx-search-alt"></span>
        </div>
    </form>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-rep-plugin">
                        
                        <div class="table-responsive mb-0" data-pattern="priority-columns">
                            <a href="/add-seller"  class="btn header-item noti-icon waves-effect">
                            <i class="bx bx-user-plus"></i>
                            </a>
                            <table id="tech-companies-1" class="table table-striped">
                                <thead>
                                    <tr>
                                        {{-- <th>Order ID</th> --}}
                                        <th data-priority="1">Coords</th>
                                        <th >Store Name</th>     
                                        <th data-priority="1">Seller Name</th>
                                        <th data-priority="1">Email</th>
                                        <th data-priority="1">Phone #</th>
                                        <th data-priority="1">Address</th>
                                        <th data-priority="1">Warehouse</th>
                                        <th data-priority="6">City</th>
                                        <th data-priority="1">Action</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($sellers as $seller )
                                    <tr>
                                        {{-- <th>{{ $order->order_id }}</th> --}}
                                        <td>{{ $seller->latitude }},{{  $seller->longitude }}</td>
                                        <th><a href="/seller-edit-{{ $seller->id }}">{{ $seller->store_name  }}</a></th>
                                        <td>{{ $seller->seller_name }}</td>
                                        <td>{{ $seller->seller_email }}</td>
                                        <td>{{ $seller->seller_contact }}</td>
                                        <td>{{ $seller->store_address }}</td>
                                        <td>{{ $seller->warehouse_address }}</td>
                                        <td>{{ $seller->city }}</td>
                                        <td><ul class="list-inline font-size-20 contact-links mb-0">
                                            <li class="list-inline-item px-2">
                                                <a href="/seller-edit-{{ $seller->id }}" title="Edit"><i class="bx bx-edit"></i></a>
                                            </li>
                                            <li class="list-inline-item px-2">
                                                <a href="delete-seller-{{ $seller->id }}" title="trash"><i class="bx bx-trash"></i></a>
                                            </li>
                                        </ul></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            
                        </div>

                    </div>
                </div>
            </div>
        </div> <!-- end col -->
        
    </div> <!-- end row -->
    <div class="row">
        <div class="col-lg-12 pagination pagination-rounded justify-content-center mt-4">
            {{ $sellers->links()  }}
        </div>
    </div>
    
@endsection
@section('script')
    <!-- Responsive Table js -->
    <script src="{{ URL::asset('/assets/libs/rwd-table/rwd-table.min.js') }}"></script>

    <!-- Init js -->
    <script src="{{ URL::asset('/assets/js/pages/table-responsive.init.js') }}"></script>
@endsection
