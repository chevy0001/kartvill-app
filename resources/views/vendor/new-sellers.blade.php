@extends('layouts.master')

@section('title') @lang('translation.Responsive_Table') @endsection

@section('css')
    <!-- Responsive Table css -->
    <link href="{{ URL::asset('/assets/libs/rwd-table/rwd-table.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Orders @endslot
        @slot('title') Order Details @endslot
    @endcomponent
    <form class="app-search d-none d-lg-block" method="POST" action="/seller-search">
        @csrf
        @method('POST')
        <div class="position-relative">
            <input type="text" class="form-control" name="input" placeholder="@lang('translation.Search')">
            <span class="bx bx-search-alt"></span>
        </div>
    </form>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-rep-plugin">
                        
                        <div class="table-responsive mb-0" data-pattern="priority-columns">
                            <table id="tech-companies-1" class="table table-striped">
                                <thead>
                                    <tr>
                                        {{-- <th>Order ID</th> --}}
                                        <th >Store Name</th>     
                                        <th >Action</th> 
                                        
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($sellers as $seller )
                                    <tr>
                                        {{-- <th>{{ $order->order_id }}</th> --}}
                                        <th><a href="/seller-edit-{{ $seller->id }}">{{ $seller->store_name  }}</a></th>
                                        <td>
                                            <ul class="list-inline font-size-20 contact-links mb-0">
                                                <li class="list-inline-item px-2">
                                                    <a href="/seller-edit-{{ $seller->id }}" title="Edit"><i class="bx bx-edit"></i></a>
                                                </li>
                                                <li class="list-inline-item px-2">
                                                    <a href="delete-seller-{{ $seller->id }}" title="trash"><i class="bx bx-trash"></i></a>
                                                </li>
                                        </td>
                                        
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            
                        </div>

                    </div>
                </div>
            </div>
        </div> <!-- end col -->
        
    </div> <!-- end row -->
    <div class="row">
        <div class="col-lg-12 pagination pagination-rounded justify-content-center mt-4">
            {{ $sellers->links()  }}
        </div>
    </div>
    
@endsection
@section('script')
    <!-- Responsive Table js -->
    <script src="{{ URL::asset('/assets/libs/rwd-table/rwd-table.min.js') }}"></script>

    <!-- Init js -->
    <script src="{{ URL::asset('/assets/js/pages/table-responsive.init.js') }}"></script>
@endsection
