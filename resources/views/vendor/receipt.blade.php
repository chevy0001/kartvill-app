@extends('layouts.master-without-nav-no-script')
@section('title') {{ $order->order_number }} @endsection

@section('css')
    <!-- bootstrap-touchspin css -->
    <link href="{{ URL::asset('/assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css') }}" rel="stylesheet" />
    <style type="text/css" media="print">
        .noPrint{
          display: none;
        }
        .modal-dialog {
        width: 100% !important;
        height: 100% !important;
        padding: 0;
        }

        .modal-content {
        height: 100% !important;
        border-radius: 0;
        }
        
      </style>
@endsection

@section('content')

<div class="text-center">
  <h1><b>FoodRx Receipt</b></h1>
    <h1><b>New Daliaon Street, Whitehouse, Toril Davao City</b></h1>
    <h1><b>{{ $date }}<h1><b>
    <hr/>
    
    <h1><b>Order Number:{{ $order->order_number }} |
        Vendor: {{ $seller->store_name ?? ''}} |
        Waiter: {{ Auth::user()->first_name }} |
        Table#: 
            @if(isset($orders['order']['shipping_address']))
                {{ $orders['order']['shipping_address']['address1']}}
            @elseif(isset($orders['order']['customer']['default_address']['address1']))
                {{ $orders['order']['customer']['default_address']['address1']}}
            @endif
        </b></h1>

</div>
<hr/>

<table class="table">
    <thead>
      <tr class="">

        <th scope="col"><h1 class="text-base"><h1><b>Order</b></h1></th>
        <th scope="col"><h1><b>Price</b></h1></th>
        
      </tr>
    </thead>
    <tbody>
        @foreach ($orders['order']['line_items'] as $product)
        @php

            $total_price_per_product = $product['price'] * $product['quantity'];
        
        @endphp
      <tr>
        <td scope="row"><h1><b>{{ $product['name'] ?? '' }} <br/> {{ $product['quantity'] }} x {{ number_format($product['price'],2) ?? ''}}</b></h1></td>
        <td><h1><b>{{ $total_price_per_product }}</b></h1></td>
        
      </tr>
     @endforeach

     @if($order->delivery_fee > 0)
     <tr>
        <td><h1><b>Delivery Fee</b></h1></td>
        <td><h1><b>{{ $order->delivery_fee }}</b></h1></td>
     </tr>
     @endif
         
  
     <tr>
        <td><h1><b>Discount</b></h1></td>
        <td><h1><b>{{ number_format($orders['order']['total_discounts']) }}</b></h1></td>
     </tr>
     <tr>
         <th> <h1><b>Total</b></h1></th>
        <th><h1><b>₱{{ $orders['order']['current_total_price'] }}</b></h1></th>
     </tr>
    </tbody>
  </table>

  <div id="noprints">
    <h1><b>Cash: <span id="cash_print">0.00</span></b></h1>
    <h1><b>Change: <span id="change_print">0.00</b></h1>
  </div>
  <div class="noPrint">
    <small>Enter Cash:</small> <input type="number" class="form-control" value="{{ $orders['order']['current_total_price'] }}" id="cash" oninput="computeChange()">
  </div>

  <div class="noPrint ">  
    <button type="button"  onClick="functionPrint()" class="btn btn-primary ">Print Receipt</button>
  </div>

  <script>
    function computeChange(){
        let cash = document.getElementById('cash').value;
        let total = @json($orders['order']['current_total_price']);
        let change = 0;
        
        if(cash <= 0 || cash == null){
            document.getElementById("noprints").className += "noPrint";
            document.getElementById('cash_print').innerHTML = "0.00";
            document.getElementById('change_print').innerHTML = change + "0.00 ";
        }else{
            change = parseFloat(cash) - parseFloat(total);
            document.getElementById('cash_print').innerHTML = cash + " ";
            document.getElementById('change_print').innerHTML = change + " ";
            console.log(change);
        }
    }

    function functionPrint() {
        // document.getElementById("nonPrintable").className += "noPrint";
        window.print();
    }
</script>
@endsection
