<x-admin-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            
            {{-- <form action="{{ route('post.sales.daily') }}" method="POST">
                @csrf
                Select Date  <input type="datetime-local" id="date" name="date" 
                value=""
                class="transition duration-150 ease-in-out appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                
                    <button type="submit" value="Submit" class="px-4 m-1 py-2 bg-indigo-500 hover:bg-indigo-700 rounded-lg text-white">  Submit</button>   
                
            </form> --}}
            <form action="{{ route('post.sales.daily') }}" method="POST">
                @csrf
                Select Date  <input type="datetime-local" id="date" name="date" 
                value=""
                class="transition duration-150 ease-in-out appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                
                <input type="text" placeholder="Enter Store" value="{{ $vendor }}" id="vendor" name="vendor" class=" transition duration-150 ease-in-out appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                <button type="submit" value="Submit" class="px-4 m-1 py-2 bg-indigo-500 hover:bg-indigo-700 rounded-lg text-white">  Submit</button>   
                
            </form>
            <div><h1>Daily Sales Report - {{ $date }} ---- {{ $vendor }} </h1></div>        
<div class="relative overflow-x-auto shadow-md sm:rounded-lg" id="reload">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th scope="col" class="px-6 py-3">
                        Order Number
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Vendor Name 
                    </th>
                    
                    <th scope="col" class="px-6 py-3">
                        Product
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Price
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Discount
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Commission
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Vendor Net
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Status
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Date   
                    </th>
                    
                </tr>
            </thead>
        <tbody>
                @foreach ($dailySales as $sales )
                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">
                        {{ $sales->order_id }}

                    {{-- <a href="/delivery/orders/{{ $order->order_id }}" 
                        @if($order->is_accepted == -1) 
                            style="color:red"
                        @elseif($order->is_accepted == 1)
                        style="color:green;" class="font-weight-bold"
                        @endif
                        >    {{ $order->order_id }}</a> --}}
                    
                </th>
                    <td class="px-6 py-4">
                        {{ $sales->vendor }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $sales->product_name }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $sales->product_price }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $sales->total_discount }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $sales->commission }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $sales->vendor_net }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $sales->financial_status }}
                    </td>

                    <td class="px-6 py-4">
                        {{ date('d-m-Y', strtotime($sales->created_at)) }}
                    </td>

                    
                </tr>
            @endforeach
        </tbody>
        </table>
</div>
    
        </div>
    </div>
</x-admin-layout>
  
{{-- <div id="newpost"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
 $(function() {
  function newPost() {
    // alert('asd');
      $("#data").empty().load("/admin/sales/orders-table");
   }
    var res = setInterval(newPost, 5000);
 });
 
 function stopAutoRefresh() {
  clearInterval(res);
}

</script> --}}