@extends('layouts.master')

@section('title') @lang('Edit Seller') @endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Seller @endslot
@slot('title') Edit Seller @endslot
@endcomponent

@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
@if (session('error'))
<div class="alert alert-danger">
    {{ session('error') }}
</div>
@endif
<div class="row">
    <div class="col-xl-8">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Edit Seller Form</h4>

                <form method="post" action="/seller/update/{{ $seller->id }}">
                    @csrf
                    @method('POST')
                    <div class="row mb-4">
                        <x-label for="seller_id"  class="col-sm-3 col-form-label" :value="__('Seller ID')" />
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="text" name="seller_id" id="seller_id" :value="old('seller_id')" placeholder="Enter Seller ID" value="{{ $seller->seller_id ?? ''}}" required autofocus />   
                        </div>
                    </div>
                    <div class="row mb-4">
                        <x-label for="seller_name"  class="col-sm-3 col-form-label" :value="__('Seller Name')" />
                        <div class="col-sm-9">
                            <p id="sellerName" style="display: none">{{ $seller->seller_name }}</p>
                            <x-input  class="form-control" type="text" id="seller_name" name="seller_name" :value="old('seller_name')" placeholder="Enter Seller Name" value="{{ $seller->seller_name ?? ''}}" required /> 
                        </div>
                    </div>
                    

                    <div class="row mb-4">
                        <x-label for="store_name" class="col-sm-3 col-form-label" :value="__('Store Name')" />
                            
                        <div class="col-sm-9">
                            <p id="storeName" style="display: none">{{ $seller->store_name }}</p>
                            <x-input  class="form-control" type="text"  name="store_name" :value="old('store_name')" placeholder="Enter Store Name"  value="{{ $seller->store_name ?? ''}}" required />
                        </div>
                    </div>
                    

                    <div class="row mb-4">
                        <x-label for="email" class="col-sm-3 col-form-label" :value="__('Email')" />     
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="email" id="email" name="seller_email" placeholder="Enter Email Address" :value="old('email')" value="{{ $seller->seller_email ?? ''}} " required /> 
                        </div>
                    </div>
                    <div class="row mb-4">
                        <x-label for="seller_phone" class="col-sm-3 col-form-label" :value="__('Phone Number')" />     
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="text" id="seller_phone" name="seller_contact" placeholder="Enter Phone Number" :value="old('seller_phone')"  value="{{ $seller->seller_contact ?? ''}}" required /> 
                        </div>
                    </div>

                    {{-- <div class="row mb-4">
                        <p id="streetAddress" style="display: none">{{ $seller->store_address}}</p>
                        <x-label for="street_address" class="col-sm-3 col-form-label" :value="__('Street Address')" />
                        <div class="col-sm-9">
                            <x-input id="street_address" class="form-control" type="text" id="street_address" name="street_address" placeholder="Enter Street Address" :value="old('street_address')" value="{{ $seller->store_address}}" required />
                        </div>
                    </div> --}}
                    <div class="row mb-4">
                        <p id="storeAddress" style="display: none">{{ $seller->store_address ?? ''}}</p>
                        <x-label for="store_address" class="col-sm-3 col-form-label" :value="__('Store Address')" />
                        <div class="col-sm-9">
                            <x-input id="store_address" class="form-control" type="text"  name="store_address"  placeholder="Enter Store Address" :value="old('store_address')" value="{{ $seller->store_address ?? ''}}" required />
                            
                        </div>
                    </div>
                    <div class="row mb-4">
                        <x-label for="city" class="col-sm-3 col-form-label" :value="__('City')" />
                        <div class="col-sm-9">
                            <x-input id="city" class="form-control" type="text" name="city" :value="old('city')" placeholder="Enter City" :value="old('city')" value="{{ $seller->city }}" required />
                            
                        </div>
                    </div>
                    
                    <div class="row mb-4">
                        <p id="warehouseAddress" style="display: ">{{ htmlspecialchars_decode($seller->warehouse_address) ?? ''}}</p>
                        <x-label for="warehouse_address" class="col-sm-3 col-form-label" :value="__('Warehouse Address')" />
                        <div class="col-sm-9">
                            <input  class="form-control" type="text" id="warehouse_address" name="warehouse_address"  placeholder="Enter Warehouse Address" :value="old('warehouse_address')" value="{{ htmlspecialchars_decode($seller->warehouse_address) ?? ''}}" required />
                            
                        </div>
                    </div>
                    
                    <div class="row mb-4">
                        <label for="formrow-inputState" class="col-sm-3 col-form-label">Province</label>
                        <div class="col-sm-9">
                            <select name="province_code" id="province_code" class="form-select">
                                <option value="{{ $seller->province_code }}">{{ $seller->province_code }}</option>
                                @foreach ( $province_codes as $province )
                                <option value="{{ $province->province_code }}" >{{ $province->province }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <x-label for="latitude" class="col-sm-3 col-form-label" :value="__('Latitude')" />
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="text" id="latitude" name="latitude" :value="old('latitude')" placeholder="Enter Latitude" :value="old('latitude')" value="{{ $seller->latitude }}" required />
                            
                        </div>
                    </div>
                    <div class="row mb-4">
                        <x-label for="longitude" class="col-sm-3 col-form-label" :value="__('Longitude')" />
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="text" id="longitude" name="longitude" :value="old('longitude')" placeholder="Enter Longitude" :value="old('longitude')" value="{{ $seller->longitude }}" required />
                            
                        </div>
                    </div>
                    <div class="row mb-4">
                        <x-label for="country" class="col-sm-3 col-form-label" :value="__('Country')" />
                        <div class="col-sm-9">
                            <x-input  class="form-control"  type="text" id="country" name="country" placeholder="Enter Country" :value="old('country')"  value="{{ $seller->country ?? ''}}  " required />
                        </div>
                    </div>
                    <div class="row mb-4">
                        <x-label for="zip" class="col-sm-3 col-form-label" :value="__('Zip')" />
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="text" id="zip" name="zip" placeholder="Enter Zip Code" :value="old('zip')"  value="{{ $seller->zip ?? '' }}" required />
                        </div>
                    </div>
                    
                    <div class="row mb-4">
                        <x-label for="commission" class="col-sm-3 col-form-label" :value="__('Commission')" />
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="number" min="0" max="100" step="0.1"  id="commission" name="commission" placeholder="Enter Commission - Leave blank for 20%"  :value="old('commission')" value="{{ $seller->commission}}"  />
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label for="store_type" class="col-sm-3 col-form-label">Select Type</label>
                        <div class="col-sm-9">
                            <select class="form-select" name="store_type" id="store_type">
                                <option value="{{ $seller->store_type }}">{{ $seller->store_type ?? 'Choose'}}</option>
                                <option value="Food">Food</option>
                                <option value="Goods">Goods</option>
                                <option value="Gadgets">Gadgets</option>
                                <option value="Appliances">Appliances</option>
                                <option value="Non-Perishable">Non Perishable</option>
                                <option value="Others">Others</option> 
                            </select>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label for="isKartvillage" class="col-sm-3 col-form-label">Join Kartvillage</label>
                        <div class="col-sm-9">
                            <select class="form-select" name="isKartvillage" id="isKartvillage">
                                <option value="{{ $seller->isKartvillage }}">
                                    @if(!$seller->isKartvillage)
                                    No
                                    @else
                                    Yes
                                    @endif
                                
                                </option>
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                                
                            </select>
                        </div>
                    </div>
                    

                    <div class="row justify-content-end">
                        <div class="col-sm-9">
                            <div>
                                <button type="submit" class="btn btn-primary w-md">Submit</button>
                                <a href="delete-seller-{{ $seller->id }}"  class="btn btn-danger w-md">Delete</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- end card body -->
        </div>
        <!-- end card -->
    </div>
    <!-- end col -->
</div>
<!-- end row -->
<script>
    var sellerName = document.getElementById('sellerName').innerHTML;
    var storeName = document.getElementById('storeName').innerHTML;
    var storeAddress = document.getElementById('storeAddress').innerHTML;
    var warehouseAddress = document.getElementById('warehouseAddress').innerHTML;
    var streetAddress = document.getElementById('streetAddress').innerHTML;
    document.getElementById('seller_name').value = sellerName;
    document.getElementById('store_name').value = storeName;
    document.getElementById('store_address').value = storeAddress;
    document.getElementById('street_address').value = streetAddress;
    document.getElementById('warehouse_address').value = warehouseAddress;
    
    //alert(sellerName);
</script>
@endsection