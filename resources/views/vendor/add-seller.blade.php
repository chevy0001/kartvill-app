@extends('layouts.master')

@section('title') @lang('translation.Form_Layouts') @endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') <a href="/sellers">Sellers</a> @endslot
@slot('title') Add Seller @endslot
@endcomponent

@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
<div class="row">
    <div class="col-xl-8">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4"></h4>

                <form method="post" action="{{ route('seller.store') }}">
                    @csrf
                    @method('POST')
                    <div class="row mb-4">
                        <x-label for="seller_id"  class="col-sm-3 col-form-label" :value="__('Seller ID')" />
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="text" name="seller_id" id="seller_id" :value="old('seller_id')" placeholder="Enter Seller ID" required autofocus />   
                        </div>
                    </div>
                    <div class="row mb-4">
                        <x-label for="seller_name"  class="col-sm-3 col-form-label" :value="__('Seller Name')" />
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="text" id="seller_name" name="seller_name" :value="old('seller_name')" placeholder="Enter Seller Name" required /> 
                        </div>
                    </div>
                    
                    <div class="row mb-4">
                        <x-label for="store_name" class="col-sm-3 col-form-label" :value="__('Store Name')" />
                            
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="store_name" id="store_name" name="store_name" :value="old('store_name')" placeholder="Enter Store Name" required />
                        </div>
                    </div>
                    <div class="row mb-4">
                        <x-label for="email" class="col-sm-3 col-form-label" :value="__('Email')" />     
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="email" id="email" name="seller_email" placeholder="Enter Email Address" :value="old('email')" required /> 
                        </div>
                    </div>
                    <div class="row mb-4">
                        <x-label for="seller_phone" class="col-sm-3 col-form-label" :value="__('Phone Number')" />     
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="text" id="seller_phone" name="seller_contact" placeholder="Enter Phone Number" :value="old('seller_phone')" required /> 
                        </div>
                    </div>

                    {{-- <div class="row mb-4">
                        <x-label for="street_address" class="col-sm-3 col-form-label" :value="__('Street Address')" />
                        <div class="col-sm-9">
                            <x-input id="street_address" class="form-control" type="text" id="street_address" name="street_address" placeholder="Enter Street Address" :value="old('street_address')" required />
                        </div>
                    </div> --}}
                    
                    <div class="row mb-4">
                        <x-label for="store_address" class="col-sm-3 col-form-label" :value="__('Store Address')" />
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="text" id="store_address" name="store_address" :value="old('store_address')" placeholder="Enter Store Address" :value="old('store_address')" required />
                            
                        </div>
                    </div>
                    <div class="row mb-4">
                        <x-label for="warehouse_address" class="col-sm-3 col-form-label" :value="__('Warehouse Address')" />
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="text" id="warehouse_address" name="warehouse_address" :value="old('warehouse_address')" placeholder="Enter Warehouse Address" :value="old('warehouse_address')" required />
                            
                        </div>
                    </div>
                    
                    <div class="row mb-4">
                        <x-label for="city" class="col-sm-3 col-form-label" :value="__('City')" />
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="text" id="city" name="city" :value="old('city')" placeholder="Enter City" :value="old('city')" value="General Santos City" required />
                            
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label for="formrow-inputState" class="col-sm-3 col-form-label">Province</label>
                        <div class="col-sm-9">
                            <select name="province_code" id="province_code" class="form-select">
                                <option value="PH-SCO" selected>South Cotabato</option>
                                @foreach ( $city_code as $province )
                                <option value="{{ $province->province_code }}" >{{ $province->province }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <x-label for="latitude" class="col-sm-3 col-form-label" :value="__('Latitude')" />
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="text" id="latitude" name="latitude" :value="old('latitude')" placeholder="Enter Latitude" :value="old('latitude')" required />
                            
                        </div>
                    </div>
                    <div class="row mb-4">
                        <x-label for="longitude" class="col-sm-3 col-form-label" :value="__('Longitude')" />
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="text" id="longitude" name="longitude" :value="old('longitude')" placeholder="Enter Longitude" :value="old('longitude')" required />
                            
                        </div>
                    </div>
                    <div class="row mb-4">
                        <x-label for="country" class="col-sm-3 col-form-label" :value="__('Country')" />
                        <div class="col-sm-9">
                            <x-input  class="form-control"  type="text" id="country" name="country" :value="old('country')" placeholder="Enter Country" value="Philippines" required />
                        </div>
                    </div>
                    <div class="row mb-4">
                        <x-label for="zip" class="col-sm-3 col-form-label" :value="__('Zip')" />
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="text" id="zip" name="zip" :value="old('zip')" placeholder="Enter Zip" required />
                        </div>
                    </div>
                    
                    <div class="row mb-4">
                        <x-label for="commission" class="col-sm-3 col-form-label" :value="__('Commission')" />
                        <div class="col-sm-9">
                            <x-input  class="form-control" type="number" min="0" max="100" step="0.1" id="commission" name="commission" placeholder="Enter Commission" :value="old('commission')"  />
                        </div>
                    </div>
                    <div class="row mb-4">
                        <label for="store_type" class="col-sm-3 col-form-label">Select Type</label>
                        <div class="col-sm-9">
                            <select class="form-select" name="store_type" id="store_type">
                                <option value="Food">Food</option>
                                <option value="Goods">Goods</option>
                                <option value="Gadgets">Gadgets</option>
                                <option value="Appliances">Appliances</option>
                                <option value="Non-Perishable">Non Perishable</option>
                                <option value="Others">Others</option> 
                            </select>
                        </div>
                    </div>
                    

                    <div class="row justify-content-end">
                        <div class="col-sm-9">
                            <div>
                                <button type="submit" class="btn btn-primary w-md">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- end card body -->
        </div>
        <!-- end card -->
    </div>
    <!-- end col -->
</div>
<!-- end row -->

@endsection