@extends('layouts.master')

@section('title') @lang('Order') @endsection

@section('css')
    <!-- bootstrap-touchspin css -->
    <link href="{{ URL::asset('/assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css') }}" rel="stylesheet" />
    <style type="text/css" media="print">
        .noPrint{
          display: none;
        }
        .modal-dialog {
        width: 100% !important;
        height: 100% !important;
        padding: 0;
        }

        .modal-content {
        height: 100% !important;
        border-radius: 0;
        }
      </style>
@endsection

@section('content')
<div id="nonPrintable" class="noPrint"> 
    @component('components.breadcrumb')
        @slot('li_1') Orders @endslot
        @slot('title') Order Number: {{ $order->order_number ?? ''}} / Status: {{ $orders['order']['financial_status'] ?? ''}} @endslot
    @endcomponent

    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    @if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
    @endif
    
   
        
        <a href="print-receipt-{{ $order->order_id }}" class="btn btn-primary">Print Receipt</a>
   
      {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Print Receipt
      </button> --}}
    <div class="row">
        <div class="col-xl-8">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table align-middle mb-0 table-nowrap">
                            <thead class="table-light">
                                <tr>
                                    <th>Product</th>
                                    <th>Product Description</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th colspan="2">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($orders['order']['line_items'] as $product)
                
                                    @php
                                        $total_price_per_product = $product['price'] * $product['quantity'];
                                        
                                    @endphp
                                <tr>
                                    <td>
                                        @php 
                                        
                                        $images=App\Http\Controllers\FunctionsController::shopify_call2($access_token, 'market-spays.myshopify.com','/admin/api/2022-01/products/'.$product['product_id'].'.json', $array, 'GET');  
                                        $images = json_decode($images['response'], JSON_PRETTY_PRINT);
                                        if(isset($images['product']['image']['src'])){
                                            echo'<img src='.$images['product']['image']['src'].' class="avatar-md">'; 
                                        }
                                        else{
                                            echo'No Image';
                                        }
                                        @endphp

                                        @foreach ($product['properties'] as $props )
                                            <p><small>
                                                @if ($props['name'] == '_wk_variant_id' || $props['name'] == '_wk_vendor' )
                                                    {{ $props['value'] = '' }}
                                                    {{ $props['name'] = '' }}
                                                    
                                                @else
                                                {{ $props['name'] }} : {{ $props['value'] }}
                                                @endif
                                                
                                            </small></p>

                                        @endforeach

                                        {{-- <img src="{{ URL::asset('/assets/images/product/img-1.png') }}" alt="product-img" title="product-img"
                                            class="avatar-md" /> --}}
                                    </td>
                                    <td>
                                        <h5 class="font-size-14 text-truncate">
                                            <p class="text-dark">{{ $product['name'] ?? '' }}</p></h5>
                                        {{-- <p class="mb-0">Color : <span class="fw-medium">Maroon</span></p> --}}
                                    </td>
                                    <td>
                                        ₱{{ number_format($product['price'],2) ?? ''}}
                                    </td>
                                    <td>
                                        <div style="width: 120px;">
                                            {{ $product['quantity'] }}
                                        </div>
                                    </td>
                                    <td>
                                        ₱{{ number_format($total_price_per_product,2) ?? ''}}
                                    </td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row mt-4">
                        <div class="col-sm-12">
                            <div class="text-sm mt-2 mt-sm-0">
                                <p><b>Note/Instructions:</b> {{ $orders['order']['note'] ?? $order->note }}</p>
                                {{-- <p><b>Estimated Preparation Time:</b> {{ $duration + 25 }} - {{ $duration + 25 + 10 }} Minutes</p> --}}
                                {{-- <p><b>Distance:</b> {{ $km }} Km</p> --}}
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row-->
                    <div class="row mt-4">
                        <div class="col-sm-6">
                            <a onClick="history.go(-1);" class="btn btn-secondary">
                                <i class="mdi mdi-arrow-left me-1"></i> Back </a>
                                

                               
                               
                                <!-- Button trigger modal -->

  
 

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

                


</div> <!-- end col -->
                        <div class="col-sm-6">
                            <div class="text-sm-end mt-2 mt-sm-0">
                              
                                <form action="/vendor/acceptorder" method="post">
                                    @csrf
                                    @method('POST')
                                    <input type="hidden" name="duration" id="duration" value="{{ $duration }}">
                                    @if($order->fulfillment_status != "Cancelled")
                                    <input type="hidden" name="order_id" id="order_id" value="{{ $orders['order']['id'] ?? ''}}">
                                    
                                    
                                    @if($order->fulfillment_status == null)

                                       @if($product['name'] == "Pabili" && $order->delivery_fee == 0)
                                       <button class="btn btn-success" disabled>Accept </button>
                                       @else
                                            @if($orders['order']['financial_status'] === 'voided' || $orders['order']['financial_status'] === 'refunded')
                                            <p class="btn">The order is cancelled.</p>
                                            @elseif(Auth::user()->role == 5)
                                               <div id="accept">
                                                    <input type="hidden" name="status" id="status" value="accepted">
                                                    <button class="btn btn-success" id="accept_but">Accept </button>
                                                    <a class="btn btn-danger text-white" onclick="reject()">Reject </a>
                                                </div> 
                                                @else
                                            <p class="btn">Please turn on availability</p>  
                                            @endif
                                       @endif     
                                        {{-- <button class="btn btn-danger">Reject</button> --}}
                                    @else
                                        @if($vendor->seller_id == Auth::user()->store_id && $order->fulfillment_status == "accepted")
                                            <input type="hidden" name="status" id="status" value="ready">
                                            <p><button class="btn btn-success" id="accept_but">Ready</button></p>
                                        @elseif($vendor->seller_id == Auth::user()->store_id && $order->fulfillment_status == null)
                                            <button class="btn btn-success" id="accept_but">Accept </button>   
                                        @elseif($vendor->seller_id == Auth::user()->store_id && $order->fulfillment_status == "ready")
                                        <input type="hidden" name="status" id="status" value="served">
                                        
                                        
                                        <p><button class="btn btn-success">Serve</button></p>
                                        @elseif($vendor->seller_id == Auth::user()->store_id && $order->fulfillment_status == "served")
                                        <p>Served</p>
                                        @else
                                            <p>Already Served</p>
                                        @endif
                                    @endif
                                    @else
                                    <p>Cancelled</p>
                                    @endif
                                 </form>
                               
                                
                                {{-- @if (Auth::user()->role == 1)
                                    <form method="post" action="/cancel-order">
                                        @csrf
                                        @method('POST')
                                        <input type="hidden" name="order_number" value="{{ $order->order_number }}">
                                        <button class="btn btn-success">Cancel</button>
                                    </form>
                                @endif  --}}
                                           
                            
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row-->
                   

                </div>
            </div>
        </div>
        <div class="col-xl-4">
            
            <div class="card">
                <div class="card-body">    
                    <h4 class="card-title mb-3">Order Summary - {{ date('M d, Y',strtotime($order->created_at)) }}</h4>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td>Customer Balance  :</td>
                                    <td>₱{{ $orders['order']['current_total_price'] }}</td>
                                </tr>
                                <tr>
                                    <td>Discount : </td>
                                    <td>-  {{ $orders['order']['total_discounts'] }}</td>
                                </tr>
                                <tr>
                                    <td>Delivery Fee :</td>
                                    <td>
                                       @if($product['name'] == "Pabili")
                                       {{  $order->delivery_fee ?? 'Edit pickup location'}}
                                       @else 
                                            @if($orders['order']['total_shipping_price_set']['shop_money']['amount'] == 0)
                                                Pickup (Kartvillage)
                                            @else
                                               
                                                {{ $orders['order']['total_shipping_price_set']['shop_money']['amount'] }}
                                            @endif
                                        @endif
                                    </td>
                                    
                                </tr>
                                
                                <tr>
                                    <th>Total :</th>
                                    <th>₱{{ $orders['order']['current_total_price'] }}</th>
                                </tr>

                                <tr>
                                    <th>Merchant Earnings :</th>
                                    <th>₱{{ $merchant_payable }}</th>
                                </tr>
                                <tr>
                                    <td>Kartvill commission from product:</td>
                                    <td>@if($product['name'] == "Pabili")
                                            0
                                        @else
                                            ₱{{ $kartvill_commission}}
                                        @endif
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>Total Deductions:</td>
                                    <td>₱{{number_format($kartvill_commission + $order->delivery_commission,2) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- end table-responsive -->
                </div>
            </div>
            <!-- end card -->
        </div>

        <div class="col-xl-12">
            
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-3">Customer Details</h4>

                    <div class="table-responsive">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td>Customer: </td>
                                    <td>{{ $orders['order']['customer']['first_name'] ?? '' }} {{ $orders['order']['customer']['last_name'] ?? '' }} </td>
                                </tr>
                                <tr>
                                    <td>Phone Number: </td>
                                    @if(isset($orders['order']['shipping_address']['phone']))
                                    <td><a href="tel:{{  $orders['order']['shipping_address']['phone'] ?? $orders['order']['billing_address']['phone']}}">{{ $orders['order']['shipping_address']['phone'] ?? $orders['order']['billing_address']['phone']  }}</a> </td>
                                    @else
                                    <td><a href="tel:{{  $orders['order']['billing_address']['phone'] ?? $orders['order']['shipping_address']['phone']}}">{{ $orders['order']['billing_address']['phone'] ?? $orders['order']['shipping_address']['phone'] }}</a></td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>Address:  </td>
                                    <td id="customer_addresses">
                                   
                                        @if(isset($orders['order']['shipping_address']))
                                        {{ $orders['order']['shipping_address']['address1'].','.$orders['order']['shipping_address']['city'].','.$orders['order']['shipping_address']['country']}}
                                    
                                    @elseif(isset($orders['order']['customer']['default_address']['address1']))
                                        {{ $orders['order']['customer']['default_address']['address1'].','.$orders['order']['customer']['default_address']['city'].','.$orders['order']['customer']['default_address']['country']}}
                                    @else
                                        
                                    @endif
                                   
                                </td>
                                </tr>
                               
                                
                            </tbody>
                        </table>
                    </div>
                    <!-- end table-responsive -->
                </div>
            </div>
            <!-- end card -->
        </div>
    </div>
    <!-- end row -->
</div>   
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title " id="exampleModalLabel">FoodRx Reciept </h5>
              <div class="noPrint"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                </div>
              
            </div>
            <div class="modal-body" table-responsive>
                <p><b>Order Number: {{ $order->order_number ?? ''}}</b> |
                    Date: {{ $order->created_at ?? ''}} | Vendor:
                {{ $seller->store_name ?? ''}} |    
              @if(isset($orders['order']['shipping_address']))
                {{ $orders['order']['shipping_address']['address1'].','.$orders['order']['shipping_address']['city'].','.$orders['order']['shipping_address']['country']}}
                @elseif(isset($orders['order']['customer']['default_address']['address1']))
                    {{ $orders['order']['customer']['default_address']['address1'].','.$orders['order']['customer']['default_address']['city'].','.$orders['order']['customer']['default_address']['country']}}
                @endif
              
              <p>Waiter: {{ Auth::user()->first_name }}</p>
              <table class="table">
                <tr>
                    <td>Quantity</td>
                    <td>Item</td>
                    <td>Price</td>
                    <td>Total</td>
                </tr>
              @foreach ($orders['order']['line_items'] as $product)
              <tr>
                <td>{{ $product['quantity'] }}</td>
                <td>{{ $product['name'] ?? '' }}</td>
                <td>{{ number_format($product['price'],2) ?? ''}}</td>
                <td> ₱{{ number_format($total_price_per_product,2) ?? ''}}</td>
              </tr>
               @endforeach
              </table>
              <hr/>
              <div id="noprints">
                Total ₱{{ $orders['order']['current_total_price'] }}<br/>
                Cash: <span id="cash_print">0.00</span><br/>
                Change: <span id="change_print">0.00</span><br/>
              </div>
              <div class="noPrint">
                <small>Enter Cash:</small> <input type="number" class="form-control" value="{{ $orders['order']['current_total_price'] }}" id="cash" oninput="computeChange()">
              </div>
            </div>
            <div class="modal-footer">
            <div class="noPrint">   
              <button type="button"  onClick="functionPrint()" class="btn btn-primary ">Print</button>
            </div>
            </div>
            <p>******************************END***************************************</p>
          </div>
        </div>
      </div>


@endsection
@section('script')
    <!-- bootstrap-touchspin -->
    <script src="{{ URL::asset('/assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js') }}"></script>

    <!-- init js -->
    <script src="{{ URL::asset('/assets/js/pages/ecommerce-cart.init.js') }}"></script>


    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
   <script>
    // var store_address = document.getElementById("store_address").value;
    // var customer_address = document.getElementById("customer_addresses").innerHTML;
    
    function computeChange(){
        let cash = document.getElementById('cash').value;
        let total = @json($orders['order']['current_total_price']);
        let change = 0;
        
        if(cash <= 0 || cash == null){
            document.getElementById("noprints").className += "noPrint";
            document.getElementById('cash_print').innerHTML = "0.00";
            document.getElementById('change_print').innerHTML = change + "0.00 ";
        }else{
            change = parseFloat(cash) - parseFloat(total);
            document.getElementById('cash_print').innerHTML = cash + " ";
            document.getElementById('change_print').innerHTML = change + " ";
            console.log(change);
        }
    }

    function functionPrint() {
        // document.getElementById("nonPrintable").className += "noPrint";
        window.print();
    }

    function reject(){
        let order_id = document.getElementById('order_id').value;
        
        if(confirm('Are you sure you want to delete the order?')){
                axios.post('/rejectorder', {
                order_id:order_id,
                })
                .then(function (response) {
                    console.log(response);
                    document.getElementById("accept_but").disabled = true;
                    window.location.reload();    
                    
                })
                .catch(function (error) {
                     console.log(error);
                });
            }
        }
    
    </script>     
@endsection
