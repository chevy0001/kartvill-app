<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LogController;
use App\Http\Controllers\RiderLocation;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\APITestController;
use App\Http\Controllers\GCashController;
use App\Http\Controllers\RatesController;
use App\Http\Controllers\StaffController;
use App\Http\Controllers\StoreController;
use App\Http\Controllers\SellerController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\CustomerWalletController;
use App\Http\Controllers\DeliveryController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\SalesReportsController;
use App\Http\Controllers\ShopifyOrdersController;
use App\Http\Controllers\ShopifyWebhooksController;
use App\Http\Controllers\KartvillSettingsController;
use App\Http\Controllers\ShopifyInstallerController;
use App\Http\Controllers\DeliveryNotificationController;
use App\Http\Controllers\Delivery\DeliveryCrewController;
use App\Http\Controllers\FunctionsController;
use App\Http\Controllers\Products\ProductController;
use App\Http\Controllers\QRCodeController;
use App\Http\Controllers\RetrieveAllOrdersController;
use App\Http\Controllers\WalletController;
use App\Http\Controllers\Investor\InvestorController;
use App\Http\Controllers\Orders\CartController;
use App\Http\Controllers\Orders\RestoOrderController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');



Route::get('/optimize', function() {

    Artisan::call('optimize');

    return ('test');
});
Route::get('/clear', function() {

    Artisan::call('config:clear');

    return ('test');
});
Route::get('/refresh', function() {

    Artisan::call('cache:clear');

    return ('test');
});



Route::get('/forgotpass',[ProfileController::class,'forgotPassword'])->name('password.forgot');
Route::get('/resetpassword-{resettoken}',[ProfileController::class,'confirmPassword'])->name('passwordconfirm');
Route::post('/change-pass',[ProfileController::class,'changePassword'])->name('password.change');
Route::post('/reset-pass',[ProfileController::class,'resetPasswordMail'])->name('password.reset.mail');
Route::get('/shopify', [ShopifyInstallerController::class,'index'])->name('shopify.index');
Route::get('/qrcode',[QRCodeController::class,'index'])->name('qr.index');

Route::post('/edit-qrcode',[QRCodeController::class,'qrcode'])->name('qr.qrcode');

Route::get('/walletbalance-{vendorname}', [WalletController::class,'vendorWalletBalance1'])->name('balance.vendorWallet');

require __DIR__.'/auth.php';

Route::get('/', [HomeController::class, 'root'])->name('root');
Route::get('/404', [HomeController::class, 'error'])->name('error');
//Route::get('/dummy-{id}-{id2}', [HomeController::class, 'dummy'])->name('dummy');
//  Route::get('{any}', [HomeController::class, 'index'])->name('index');

Route::get('/dashboard', [DashboardController::class,'index'])->middleware(['auth'])->name('dashboard.index');


Route::post('/rates', [RatesController::class,'index'])->name('shopify.rates');

Route::get('/shopify/token', [ShopifyInstallerController::class,'token'])->name('shopify.token');
Route::get('/shopify/install', [ShopifyInstallerController::class,'install'])->name('shopify.intsall');





Route::post('/webhook/create-product-webhook', [ShopifyWebhooksController::class,'createProductWebHook'])->name('webhook.create-product');
Route::post('/webhook/order-paid-webhook', [ShopifyWebhooksController::class,'orderPaidWebHook'])->name('webhook.order-paid');
Route::post('/webhook/order-product-webhook', [ShopifyWebhooksController::class,'orderProductWebHook'])->name('webhook.order-product');
Route::post('/webhook/fulfill-item', [ShopifyWebhooksController::class,'fulfillItems'])->name('webhook.fulfill-items');
Route::post('/webhook/partial-fulfill-item', [ShopifyWebhooksController::class,'partialFulfillItems'])->name('webhook.partial-fulfill-items');
Route::post('/webhook/cancel-order', [ShopifyWebhooksController::class,'cancelOrder'])->name('webhook.cancelOrder');

//Route::post('/gcash/get-paid/{ordernumber}', [GCashController::class,'gcashpaid'])->name('gcash.paid');
Route::get('/gcash/get-paid/{ordernumber}', [GCashController::class,'gcashpaid2'])->name('gcash.paid2');
Route::get('/user-pin-{customer_id}',[CustomerController::class,'customer_map'])->name('customer.map');
Route::post('/customer-map-update',[CustomerController::class,'customer_map_update'])->name('customer.map.update');

Route::post('/customer-coordinate-update',[CustomerController::class,'updateCreateCustomerCoordinates']);
Route::get('/currentcoords-{customer_id?}',[CustomerController::class,'getCurrentCoords']);
Route::get('/store-availability',[StoreController::class,'getAvailability'])->name('store.get.availability');

Route::post('/successpaid',[GCashController::class,'successPaid']);
Route::get('/successgcash-{requestcode}',[GCashController::class,'success']);

Route::get('/customerpoints-{customer_id}',[CustomerWalletController::class,'returnWalletPoints']);
Route::get('/redeem-{customer_id?}',[CustomerWalletController::class,'showRedeem'])->name('customer.redeem');

Route::post('/sendemailcode',[CustomerWalletController::class,'sendEmailCode']);
Route::post('/getemailcode',[CustomerWalletController::class,'getEmailCode']);
Route::post('/process-redeem',[CustomerWalletController::class,'processRedeem']);

Route::get('/rider-{order_id}-{customer_id}',[DeliveryController::class,'assignedRider'])->name('rider.details');
        

//Route::post('/webhook/app-uninstall-webhook/{$shop_url}', [ShopifyWebhooksController::class,'appUninstallWebhook'])->name('webhook.app-uninstall');



//Shopify Links
//Route::post('/admin/settings/sellers',[RatesController::class,'store'])->name('set-delivery.rate');


    
// Webhook API Testing URL
//  Route::get('/readOrderApi', [APITestController::class,'readOrderApi'])->name('readOrderApi');
//  Route::get('/testRateCoords', [APITestController::class,'testRateCoords'])->name('testRateCoords');
//  Route::get('/orderPaidApi', [APITestController::class,'orderPaidApi'])->name('orderPaidApi');
//  Route::get('/cancelOrderAPI', [APITestController::class,'cancelOrderAPI'])->name('cancelOrderAPI');

Route::middleware(['auth','admin'])->group(function(){
    //Test for axios
    Route::get('/test', [DashboardController::class,'test'])->name('test');
    Route::get('/testPaidWallet', [DashboardController::class,'testPaidWallet'])->name('testPaidWallet');

    Route::post('/updateCustomer1', [DashboardController::class,'updateCustomer'])->name('updateCustomer');
    Route::get('/get-test-{id}', [DashboardController::class,'getTest'])->name('gettest');
    //End test for axios
    Route::get('/admin',[AdminController::class,'index'])->name('admin.index');
    Route::post('/save-settings',[KartvillSettingsController::class,'saveSettings'])->name('admin.settings.save');
    Route::get('/admin/settings',[KartvillSettingsController::class,'index'])->name('admin.settings.index');
    Route::post('/admin/settings/update',[KartvillSettingsController::class,'updateCommission'])->name('admin.settings.update.commission');

    Route::get('/admin/sales/reports',[SalesReportsController::class,'index'])->name('sales.all');
    Route::get('/admin/sales/orders',[SalesReportsController::class,'orders'])->name('sales.orders');

    Route::post('/admin/sales/reports/daily',[SalesReportsController::class,'daily'])->name('post.sales.daily');
    Route::get('/admin/sales/reports/daily/{date?}/{vendor?}',[SalesReportsController::class,'dailySales'])->name('sales.daily');
    
    Route::post('/admin/sales/reports/weekly',[SalesReportsController::class,'weekly'])->name('post.sales.weekly');
    Route::get('/admin/sales/reports/weekly/{date?}/{vendor?}',[SalesReportsController::class,'weeklySales'])->name('sales.weekly');
    
    Route::post('/admin/sales/reports/monthly',[SalesReportsController::class,'monthly'])->name('post.sales.monthly');
    Route::get('/admin/sales/reports/monthly/{date?}/{year?}/{vendor?}',[SalesReportsController::class,'monthlySales'])->name('sales.monthly');
    
    Route::get('/count-orders',[ShopifyOrdersController::class,'countNewOrder'])->name('delivery.count-orders');
    Route::post('/admin/assign-rider',[DeliveryNotificationController::class,'assignRider'])->name('admin.assign.rider');
    
    //Order Cancelled by admin 
    Route::post('/cancel-order',[ShopifyOrdersController::class,'cancelOrder'])->name('order.cancel');

    Route::get('/vendor/daily-sales/{vendorName?}/{date?}',[SalesReportsController::class,'vendorDailySales'])->name('admin.vendor.daily.sales');
    Route::get('/vendor/weekly-sales/{vendorName?}/{date?}',[SalesReportsController::class,'vendorWeeklySales'])->name('admin.vendor.weekly.sales');
    Route::get('/vendor/monthly-sales/{vendorName?}/{date?}',[SalesReportsController::class,'vendorMonthlySales'])->name('admin.vendor.monthly.sales');
    Route::get('/autocomplete-search', [SalesReportsController::class, 'autoCompleteSearch'])->name('admin.autoCompleteSearch');
    Route::get('/logs', [LogController::class,'index'])->name('admin.logs');
    Route::get('/topup-logs', [LogController::class,'topupLogs'])->name('admin.topup.logs');

    
    Route::get('/editwallet-{id}', [WalletController::class,'editWallet'])->name('edit.wallet.balance');
    Route::post('/updatewallet-{id}', [WalletController::class,'updateWallet'])->name('update.wallet.balance');
    

    Route::get('/topup-count', [GCashController::class,'countRequest'])->name('admin.topup.count');
    
    Route::get('/fare-setting', [KartvillSettingsController::class,'fareSettings'])->name('fare.settings');
    Route::post('/save-fare-setting', [KartvillSettingsController::class,'saveFareSettings'])->name('save.fare.settings');
    //Route::get('/', 'App\Http\Controllers\Admin\AdminController@index');
});


Route::middleware(['auth','staff'])->group(function(){
    Route::get('/verification',[AdminController::class,'verification'])->name('verification');
    Route::get('/users-{value?}',[AdminController::class,'users'])->name('admin.user');
    Route::get('/newusers',[AdminController::class,'newUsers'])->name('admin.newuser');
    Route::get('/vendors',[AdminController::class,'vendors'])->name('admin.vendors');
    Route::get('/edit-users-{userid}',[AdminController::class,'editUser'])->name('admin.edit.user');
    Route::post('/admin/update-user/{id}',[AdminController::class,'updateUser'])->name('admin.update.user');

    Route::get('/staff',[StaffController::class,'index'])->name('staff.index');
    Route::get('/sellers-{value?}',[KartvillSettingsController::class,'sellers'])->name('sellers.all');
    
    Route::get('/admin/settings/bazaar',[KartvillSettingsController::class,'bazaar'])->name('admin.settings.bazaar');
    Route::get('/admin/settings/cashier',[KartvillSettingsController::class,'cashier'])->name('admin.settings.cashier');
    Route::get('/seller-edit-{sellerid}',[SellerController::class,'edit'])->name('seller.edit');
    Route::get('/add-seller',[SellerController::class,'create'])->name('seller.create');
    Route::post('/add-seller/store',[SellerController::class,'store'])->name('seller.store');
    Route::get('/delete-seller-{id}',[SellerController::class,'destroy'])->name('seller.delete');
    Route::post('/seller/update/{sellerid?}',[SellerController::class,'update'])->name('seller.update');
    Route::post('/seller-search',[KartvillSettingsController::class,'sellerSearch'])->name('sellers.search');
    Route::post('/user-search',[AdminController::class,'userSearch'])->name('user.search');
    Route::get('/new-sellers',[SellerController::class,'newSellers'])->name('sales.newsellers');
    Route::get('/map',[RiderLocation::class,'index'])->name('map.index');
    Route::get('/add-user',[AdminController::class,'addUser'])->name('admin.add.user');
    Route::post('/store-user',[AdminController::class,'store'])->name('admin.store.user');
    Route::get('/delete-user-{userid}',[ProfileController::class,'destroy'])->name('delete.user');
    Route::post('/rider-search',[AdminController::class,'riderSearch'])->name('rider.search');
    Route::get('/riders-{value?}',[AdminController::class,'riders'])->name('admin.riders');

    Route::get('/customers-{customerid?}',[CustomerController::class,'customers'])->name('customers');
    Route::get('/customer-update-{customerid}',[CustomerController::class,'customerUpdate'])->name('customer.update');
    Route::post('/customer-update-{customerid}',[CustomerController::class,'update'])->name('update');
    Route::post('/customer-search',[CustomerController::class,'customerSearch'])->name('customer.search');
    
    Route::post('/update-store-availability',[StoreController::class,'availability'])->name('store.update.availability');
    Route::get('/store-settings',[StoreController::class,'index'])->name('store.settings');
    
    Route::post('/approve-{hash}',[GCashController::class,'approveTopUp'])->name('gcash.approve');
    Route::post('/reject-{hash}',[GCashController::class,'rejectTopUp'])->name('gcash.reject');
    Route::get('/rider-location',[RiderLocation::class,'ridersLocation'])->name('ridersLocation');

    Route::post('/item-redeem-post',[CustomerWalletController::class,'store'])->name('item.redeem.store');
    Route::get('/item-redeem-post',[CustomerWalletController::class,'create'])->name('item.redeem.form');

    Route::get('/redeemrequests',[CustomerWalletController::class,'redeemRequest'])->name('redeem.requests');
    Route::get('/count-redeem',[CustomerWalletController::class,'countRedeem'])->name('item.redeem.count');

    Route::get('/vendor-balance', [WalletController::class,'vendorWalletBalance'])->name('wallet.balance.vendor');
    Route::get('/vendor-balance-davao', [WalletController::class,'vendorWalletBalanceDavao'])->name('wallet.balance.vendordavao');

    Route::get('/rider-balance', [WalletController::class,'walletBalance'])->name('wallet.balance');

    /**Product Management Route */
    Route::get('/products', [ProductController::class,'products'])->name('product.products');
    Route::get('/product-{id}', [ProductController::class,'product'])->name('product.product');
    Route::get('/add-product', [ProductController::class,'addProduct'])->name('product.addproduct');
    Route::get('/edit-product-{product_id}', [ProductController::class,'editProduct'])->name('product.editproduct');
    Route::post('/save-product', [ProductController::class,'saveProduct'])->name('product.saveproduct');
    Route::post('/delete-product', [ProductController::class,'deleteProduct'])->name('product.deleteProduct');
    
    Route::get('/add-variant-{product_id}', [ProductController::class,'addVariant'])->name('product.addvariant');
    Route::post('/save-edited-variant', [ProductController::class,'editVariant'])->name('product.editVariant');
    Route::post('/delete-variant', [ProductController::class,'deleteVariant'])->name('product.deleteVariant');
    Route::post('/save-variant', [ProductController::class,'saveVariant'])->name('product.saveVariant');
    Route::post('/disable-variant', [ProductController::class,'disableVariant'])->name('product.disableVariant');
    Route::post('/enable-variant', [ProductController::class,'enableVariant'])->name('product.enableVariant');
    
    Route::post('/add-category', [ProductController::class,'addCategory'])->name('product.addCategory');
    Route::post('/update-category', [ProductController::class,'updateCategory'])->name('product.updateCategory');
    Route::post('/delete-category', [ProductController::class,'deleteCategory'])->name('product.deleteCategory');
    Route::post('/add-image', [ProductController::class,'addImage'])->name('product.addImage');
    Route::post('/save-option', [ProductController::class,'saveOption'])->name('product.saveOption');
    Route::post('/save-description', [ProductController::class,'saveDescription'])->name('product.saveDescription');
    Route::post('/delete-image', [ProductController::class,'deleteImage'])->name('product.deleteImage');
    Route::post('/edit-title', [ProductController::class,'editTitle'])->name('product.editTitle');
    Route::post('/save-edited-option', [ProductController::class,'saveEditedOption'])->name('product.saveEditedOption');
    Route::post('/delete-option', [ProductController::class,'deleteOption'])->name('product.deleteOption');
    Route::post('/save-discount', [ProductController::class,'saveDiscount'])->name('product.saveDiscount');
    
    Route::get('/products-results-{keyword}', [ProductController::class,'searchedAdminproducts'])->name('product.searchedAdminproducts');
    /**End Product Management Route */
});


Route::middleware(['auth','deliverycrew'])->group(function(){
    Route::post('/delivery/availability',[DeliveryCrewController::class,'availability'])->name('delivery.availability');
    Route::post('/delivery/accept-reject',[DeliveryCrewController::class,'acceptReject'])->name('delivery.accept.reject');
    Route::post('/delivery/delivered/{orderId}',[DeliveryCrewController::class,'delivered'])->name('delivery.delivered');
    Route::patch('/edit/delivery-fee',[DeliveryCrewController::class,'editDeliveryFee'])->name('delivery.editDeliveryFee');
    
    Route::get('/delivery',[DeliveryCrewController::class,'index'])->name('delivery.index');
    Route::get('/order-notifications',[DeliveryCrewController::class,'notifications'])->name('orders.notifications');
    Route::get('/order-kartvillage-{ordernumber?}',[DeliveryCrewController::class,'kartvillage'])->name('orders.kartvillage');
    Route::post('/search-order-kartvillage',[DeliveryCrewController::class,'searchKartvillageOrder'])->name('search.orders.kartvillage');
    Route::get('/delivery-orders-{ordernumber}',[DeliveryCrewController::class,'orders'])->name('delivery.orders');
    Route::get('/delivery/accepted',[DeliveryCrewController::class,'acceptedOrders'])->name('delivery.accepted');
    

    Route::get('/user-profile-{id}',[ProfileController::class,'index'])->name('user.profile');
    Route::get('/deliveries',[DeliveryController::class,'index'])->name('deliveries');
    Route::get('/send-map-link-{customerid}',[CustomerController::class,'updateMap'])->name('update.map.link');

    Route::get('/wallet',[GCashController::class,'wallet'])->name('wallet');
    Route::post('/topup', [GCashController::class,'topup'])->name('gcash.topup');
    Route::post('/topUpCash', [GCashController::class,'topUpCash'])->name('cash.topUpCash');

    Route::get('/topuprequest',[GCashController::class,'topUpRequest'])->name('wallet.topup.request');
    
    Route::get('/countOrders',[DeliveryCrewController::class,'countOrders'])->name('orders.count');
    
    Route::post('/save-current-coords',[RiderLocation::class,'create'])->name('save-current-coords');

    Route::post('/get-rate',[FunctionsController::class,'getDeliveryRate'])->name('getRate');

    Route::post('/vendor/acceptorder',[SellerController::class,'acceptOrder'])->name('vendor.accept.order');
    Route::post('/rejectorder',[SellerController::class,'vendorRejectOrder'])->name('vendor.reject.order');
    Route::post('/vendor-search-order',[SellerController::class,'searchOrders'])->name('vendor.search.orders');
    Route::get('/kartvillorders-{ordernumber?}',[SellerController::class,'vendorOrders'])->name('vendor.orders');

    Route::get('/vendor-order-{ordernumber}',[SellerController::class,'vendorOrder'])->name('vendor.order');
    Route::get('/todayorders',[SellerController::class,'todayOrders'])->name('vendor.today.order');

    Route::get('/print-receipt-{ordernumber}',[SellerController::class,'printReceipt'])->name('vendor.receipt');
    
    Route::get('/d-orders',[RestoOrderController::class,'index'])->name('orders.index');
    Route::get('/d-order-{orderid}',[RestoOrderController::class,'order'])->name('orders.order');
    Route::get('/vendor-orders',[RestoOrderController::class,'vendorOrders'])->name('orders.vendorOrders');
    Route::get('/d-vendor-order-{orderid}',[RestoOrderController::class,'vendorOrder'])->name('orders.vendorOrder');
    
    Route::post('/cancel-product-order',[CartController::class,'cancelProductOrder'])->name('vendor.cancelProductOrder');
    Route::post('/cancel-all-orders',[CartController::class,'cancelAllOrders'])->name('vendor.cancelAllOrders');
    Route::post('/accept-order',[CartController::class,'acceptAllOrders'])->name('vendor.acceptAllOrders');
    Route::post('/serve-order',[CartController::class,'serveAllOrders'])->name('vendor.serveAllOrders');

});
    
Route::get('/kmenu-{producttitle}-{productid}', [ProductController::class,'displayProduct'])->name('product.displayProduct');
Route::get('/search-{keyword}', [ProductController::class,'searchedProducts'])->name('product.searchedProducts');
Route::get('/all-products', [ProductController::class,'displayAllProducts'])->name('product.displayAllProducts');
Route::get('/getvariationcount-{id}', [ProductController::class,'variantCount'])->name('product.variantCount');
Route::get('/cart', [CartController::class,'cart'])->name('product.cart');
Route::post('/add-to-cart',[CartController::class,'addToCart'])->name('cart.addtocart'); 
Route::get('/order-count',[CartController::class,'cartOrderCount'])->name('cart.cartOrderCount'); 
Route::get('/display-cart-items',[CartController::class,'displayCartItems'])->name('cart.displayCartItems'); 
Route::post('/remove-order',[CartController::class,'removeOrder'])->name('cart.removeOrder'); 
Route::post('/update-order-quantity',[CartController::class,'updateOrderQuantity'])->name('cart.updateOrderQuantity'); 
Route::post('/checkout',[CartController::class,'checkout'])->name('cart.checkout'); 
Route::get('/order-status',[CartController::class,'orderStatus'])->name('cart.orderStatus'); 



Route::middleware(['auth','investorpage'])->group(function(){
    Route::get('/salesdashboard',[InvestorController::class,'index'])->name('investor.salesdashboard'); 
    Route::post('/dailyorders',[InvestorController::class,'dailyOrders'])->name('investor.daily.orders'); 
    Route::post('/weeklyorders',[InvestorController::class,'weeklyOrders'])->name('investor.weekly.orders');
    Route::post('/monthlyorders',[InvestorController::class,'monthlyOrders'])->name('investor.monthly.orders');  
});

Route::get('/bluetoothprint',[SellerController::class,'bluetooth'])->name('bluetooth');
Route::get('/get-cities',[KartvillSettingsController::class,'getCities'])->name('getCities');
Route::post('/get-fare',[FunctionsController::class,'getFare'])->name('getFare');
Route::patch('/fcm-token', [AdminController::class, 'updateToken'])->name('fcmToken');   

